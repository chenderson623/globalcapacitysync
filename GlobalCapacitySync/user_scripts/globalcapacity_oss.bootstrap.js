"use strict";

if (document === undefined) {
    throw "This can only run in a browser";
}

document.addEventListener('OSSLoaded', function(event) {
    setTimeout(boot_oss, 100);
});

(function() {
    var host = document.location.host;
    var root_url;

    // try: https://bitbucket.org/chenderson623/globalcapacitysync/raw/master/

    if(host.indexOf('globalcapacitysync.dev') > -1) {
        root_url = 'http://globalcapacitysync.dev';
    } else {
        //root_url = 'http://tci.fieldportal.net';
        root_url = 'https://globalcapacitysync.aerobatic.io';
    }

    /**
     *
     * ===============Setup local variables===============================================
     *
     */
    var debug = true;
    var request_time = new Date().getTime();
    //Paths:
    var lib_url = root_url + '/GlobalCapacitySync/libs/';
    var public_url = root_url + '/GlobalCapacitySync/';
    var scriptloader_url = lib_url + "basket.js-0.4.0/dist/basket.full.min.js";
    var labjs_url = lib_url + "LABjs-2.0.3/LAB.min.js";

    /**
     *
     *
     * ===============Script Injectors====================================================
     */
    var inject_css_file = function inject_css_file(href) {
        var cssElement = document.createElement('link');
        cssElement.rel = 'stylesheet';
        cssElement.type = 'text/css';
        cssElement.href = href;
        document.getElementsByTagName('head')[0].appendChild(cssElement);
    };
    var inject_script = function inject_script(src, callback, async) {
        if (typeof async === 'undefined') {
            async = true;
        }
        var script_elem = document.createElement('script');
        script_elem.src = src;
        if (async === true) {
            script_elem.async = true;
        }
        script_elem.onreadystatechange = script_elem.onload = function() {
            var state = script_elem.readyState;
            if (callback && !callback.done && (!state || /loaded|complete/.test(state))) {
                callback.done = true;
                callback();
            }
        };
        document.getElementsByTagName('head')[0].appendChild(script_elem);
    };
    var inject_scripts = function inject_scripts(src_array, callback, async) {
        var loader_callback = false;
        for (var i = 0, len = src_array.length; i < len; i++) {
            if (i === len - 1) {
                loader_callback = callback;
            }
            inject_script(src_array[i] + '?' + request_time, loader_callback, async);
        }
    };
    var scriptloader_scripts = function scriptloader_scripts(scripts_array, callback) {
        basket.clear();
        basket.require.apply(
            this, scripts_array.map(function create_array_of_backet_objects(script) {
                return {
                    url: script
                };
            })
        ).then(function() {
            callback();
        }, function() {
            console.log("BASKET ERROR",arguments);
        });
    };
    /**
     *
     * ===============GlobalCapacity OSS========================================================
     *
     */
    var load_oss = function(callback) {
        //Load OSS, with some preload items.
        //preload items could all be loaded when needed, but might be a benefit to preload

        var scripts = [
            public_url + 'csrms/Csrms.js',
            public_url + 'csrms/Csrms.Session.js',
            lib_url + 'jquery-1.11.0/jquery.min.js',
            public_url + 'csrms/js/DOMElements.js',
            public_url + 'csrms/js/DOMTableWrapper.js',
            public_url + 'app/OSS/GlobalCapacity.OSS.js'
        ];

        if(debug) {
            inject_scripts(scripts, callback, false);
        } else {
            scriptloader_scripts(scripts, callback);
        }
    };
    /**
     *
     * ===================Bootstrap======================================================
     *
     * load script loader and run initializers
     */
    var after_load_oss = function after_load_oss() {
        CSrms.root_url = root_url;
        document.dispatchEvent(new CustomEvent('OSSLoaded'));
    };
    //Run:
    if(!debug) {
        inject_script(scriptloader_url, function after_scriptloader_load(){
            load_oss(after_load_oss);
        });
    } else {
        load_oss(after_load_oss);
    }

}());

function start_ui(oss, callback) {
    console.log("START UI");
    oss.getOSSUI(function(ui){
        console.log("HERE");
        ui.init(oss.getSessionFactory()); //load with default settings
        ui.attachToDOM();
        callback(ui);
    });
}

function start_controls(oss, document_factory, callback) {
    console.log("START CONTROLS");
    oss.createOSSControlsFactory(document_factory, function(controls_factory){
        console.log("DONE createOSSControlsFactory");
        controls_factory.createDocumentControlsFactory(callback);
    });
}

function boot_oss() {
    console.log("OSS BOOT");
    var debug = true;
    var oss = GlobalCapacity.OSS;

    //We can do this here to set options. Behind the scene, not really needed. will be created with run()
    var document_factory = oss.createDocumentFactory(document.body, document.location); //only one per page load. OSS creates instance of DocumentFactory but doesn't run it yet
    var session_factory = oss.createSessionFactory(); //only one per page load. OSS creates instance of SessionFactory but doesn't run it yet
    session_factory.session_provider_type = 'user_script_messaging';

    start_ui(oss, function(ui){
        start_controls(oss, document_factory, function(controls){
            console.log('DONE BOOTSTRAP');
        });
    });
}