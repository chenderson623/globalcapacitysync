// ==UserScript==
// @name           GlobalCapacityOSS
// @version        2016-06-08
// @namespace      CJH
// @include        http://testdocs.globalcapacitysync.dev/*
// TimeMatrix:
// @include        http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp*
// @include        http://status-int.oss.covad.com:15558/csp/details*
// @include        http://dart.oss.globalcapacity.com:11100/dart/*
// @include        https://dart.globalcapacity.com/dart/*
// @include        http://reports02.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp*
// @grant GM_log
// @grant unsafeWindow
// @grant GM_getValue
// @grant GM_setValue
// @grant GM_deleteValue
// @grant GM_xmlhttpRequest
// ==/UserScript==

/**
 *
 * ===============User Script Init=====================================================
 *
 */
var debug = false;
if (unsafeWindow.console) {
    var GM_log = unsafeWindow.console.log;
}

if (debug) {
    console.log('GlobalCapacityOSS: GM START');
}
//Request Time:
var last_request_time = GM_getValue('last_request_time');
var request_time = new Date().getTime();
var request_interval = 0;
if (last_request_time) {
    request_interval = (request_time - parseInt(last_request_time)) / 1000;
}
GM_setValue('last_request_time', request_time);
GM_setValue('request_interval', request_interval);

//unset SessionData
if (request_interval === 0 || request_interval > 25000) { //5 minutes
    GM_deleteValue('SessionData');
}

/**
 *
 * ===============DOM Modifications==================================================
 *
 */
// Insert room for control bar. Doing this early to avoid flicker
var body_elems = document.getElementsByTagName('BODY');
//body_elems[0].style.paddingTop = "45px";

//
//
document.addEventListener('globalcapacity_oss.bootstrap', function(evnt) {
    if (debug) {
        console.log("LISTEN FROM GM: ", evnt.detail);
    }
}, false);
//
//


/**
 *
 * ===============Setup local variables===============================================
 *
 */
//var root_url      = 'http://globalcapacitysync.dev/';
var root_url      = 'https://bitbucket.org/chenderson623/globalcapacitysync/raw/master/';
var app_url       = root_url + 'GlobalCapacitySync/app/';
var libs_url      = root_url + 'GlobalCapacitySync/libs/';
var gebo_url      = root_url + 'gebo_admin/';
var session_url   = 'http://tci.fieldportal.net/fieldportal/ajax_session.php';
var bootstrap_url = root_url + "GlobalCapacitySync/user_scripts/globalcapacity_oss.bootstrap.js";
/**
 *
 * ===============Script Injectors====================================================
 *
 */
function inject_script(src, callback) {
    var script_elem = document.createElement('script');
    script_elem.src = src;
    script_elem.async = true;
    if (debug) {
        console.log("GlobalCapacityOSS: inject_script: " + src + '?' + request_time);
    }
    script_elem.onreadystatechange = script_elem.onload = function() {
        var state = script_elem.readyState;
        if (callback && !callback.done && (!state || /loaded|complete/.test(state))) {
            callback.done = true;
            callback();
        }
    };
    document.getElementsByTagName('head')[0].appendChild(script_elem);
}

function inject_css(href) {
    var cssElement = document.createElement('link');
    cssElement.rel = 'stylesheet';
    cssElement.type = 'text/css';
    if(debug) {
        href = href + '?' + request_time
    }
    cssElement.href = href;
    document.getElementsByTagName('head')[0].appendChild(cssElement);
}

/**
 *
 * ===============Inject CSS==========================================================
 * more efficient to load this here
 */
inject_css(app_url + "OSS/css/oss-resets.css");
inject_css(gebo_url + "bootstrap/css/bootstrap.min.css");
inject_css(gebo_url + "lib/qtip2/jquery.qtip.min.css");
inject_css(gebo_url + "img/flags/flags.css");
inject_css(gebo_url + "img/splashy/splashy.css");

inject_css(app_url + "OSS/css/gebo-style-lite.css");
inject_css(gebo_url + "lib/datepicker/datepicker.css");
inject_css(libs_url + "select2-3.5.4/select2.css");
inject_css(libs_url + "select2-3.5.4/select2-bootstrap.css");
inject_css(app_url + "OSS/css/oss-page-controls.css");
inject_css(app_url + "OSS/css/login-dropdown.css");

inject_css(app_url + "TimeMatrix/css/oss-timematrix.css");

/**
 *
 * ===============USer Script / Page Communications==================================
 *
 */
function setSessionData(session_data) {
    GM_setValue('SessionData', session_data);
}

function getSessionData(callback) {
    //try GM_getValue first:
    var session_data = GM_getValue('SessionData');
    if (session_data !== undefined) {
        callback(session_data);
        return;
    }
    GM_xmlhttpRequest({
        method: "GET",
        url: session_url,
        onload: function(response) {
            session_data = JSON.parse(response.responseText);
            setSessionData(session_data);
            callback(session_data);
        }
    });
}

function logIn(username, password, callback) {
    GM_xmlhttpRequest({
        method: "POST",
        url: session_url,
        data: 'submit=Login&username=' + username + '&password=' + password,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        onload: function(response) {
            session_data = JSON.parse(response.responseText);
            setSessionData(session_data);
            callback(session_data);
        }
    });
}

function logOut() {
    GM_deleteValue('SessionData');
    GM_xmlhttpRequest({
        method: "POST",
        url: session_url,
        data: 'submit=Logout',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        onload: function(response) {
            callback();
        }
    });
}

/*
 *
 * This is one-way communication. PageSide will always be requesting from UserScriptSide, never the other way around
 */
var UserScriptMessaging_PageSideResponder = function() {
    var self = this;
    self.debug = false;
    this.user_script_messaging_channel = window;
    this.self_name = "UserScriptSide";
    //************************Process Action*******************************
    var processActionRequest = function(request_message) {
        if (self.debug) console.log("UserScriptSide:PROCESS ACTION", ping_message);
        var action = request_message.action;
        switch (action) {
            case 'getSessionData':
                //external function
                getSessionData(function(session_data) {
                    sendActionResponse(action, session_data, request_message);
                });
                break;
            case 'logIn':
                var username = request_message.username;
                var password = request_message.password;
                logIn(username, password, function(session_data) {
                    sendActionResponse(action, session_data, request_message);
                });
                break;
            case 'logOut':
                logOut(function() {
                    sendActionResponse(action, {}, request_message);
                });
                break;
        }
    };
    var sendActionResponse = function(action, data, request_message) {
        var response_message = {
            response: request_message.request_id,
            type: "action:" + action,
            from: self.self_name,
            to: request_message.from,
            success: true,
            data: data
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(response_message), '*');
    };
    //************************Process Post*********************************
    var processPostRequest = function(post_message) {
        if (self.debug) console.log("UserScriptSide:PROCESS POST", ping_message);
        var post_key = post_message.post;
        var value = post_message.data;
        var response = GM_setValue(post_key, value);
        sendPostResponse(post_key, true, '', post_message);
    };
    var sendPostResponse = function(post_key, success, message, post_message) {
        var response_message = {
            response: post_message.request_id,
            type: "post:" + post_key,
            from: self.self_name,
            to: post_message.from,
            success: success,
            message: message
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(response_message), '*');
    };
    //************************Process Get**********************************

    var processGetRequest = function(request_message) {
        if (self.debug) console.log("UserScriptSide:PROCESS GET", ping_message);
        var get_key = request_message.get;
        var value = GM_getValue(get_key);
        sendGetResponse(get_key, value, request_message);
    };
    var sendGetResponse = function(get_key, data, request_message) {
        var response_message = {
            response: request_message.request_id,
            type: "get:" + get_key,
            from: self.self_name,
            to: request_message.from,
            success: true,
            data: data
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(response_message), '*');
    };
    //************************Requests*************************************
    var processPing = function(ping_message) {
        if (self.debug) console.log("UserScriptSide:PROCESS PING", ping_message);
        var request_id = ping_message.ping;
        var response_message = {
            pong: request_id,
            from: self.self_name,
            to: ping_message.from,
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(response_message), '*');
    };
    var aknowledge = function(request_message) {
        if (self.debug) console.log("UserScriptSide:AKNOWLEDGE: ", request_message);
        if (!request_message.request_id) return;
        var message_obj = {
            aknowledge: request_message.request_id,
            from: self.self_name,
            to: request_message.from
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(message_obj), '*');
    };
    var recievePostMessage = function(event) {
        if (self.debug) console.log("UserScriptSide:receivePostMessage:", received_message);
        var received_message;
        try {
            received_message = JSON.parse(event.data);
        } catch (e) {
            return;
        }

        //only need to listen to messages to this
        if (!received_message.to || received_message.to !== self.self_name) return;

        if (received_message.ping) {
            processPing(received_message);
        }

        if (received_message.action) {
            aknowledge(received_message);
            return processActionRequest(received_message);
        }

        if (received_message.get) {
            aknowledge(received_message);
            return processGetRequest(received_message);
        }

        if (received_message.post) {
            aknowledge(received_message);
            return processPostRequest(received_message);
        }

    };
    //
    // Setup listener
    self.user_script_messaging_channel.addEventListener('message', recievePostMessage, false);

};

/**
 *
 * ===============URL Functions===========================================================
 *
 */
function parseUri(str) {
    var o = parseUri.options,
        m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i = 14;

    while (i--)
        uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function($0, $1, $2) {
        if ($1)
            uri[o.q.name][$1] = $2;
    });

    return uri;
}

parseUri.options = {
    strictMode: false,
    key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
    q: {
        name: "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};


/**
 *
 * ===============Bootstrap==========================================================
 *
 */
var user_script_channel = new UserScriptMessaging_PageSideResponder();
window.postMessage('{"notify":"User Script Ready"}', '*');
inject_script(bootstrap_url);