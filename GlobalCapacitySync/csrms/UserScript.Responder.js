/*
 *
 * This is one-way communication. PageSide will always be requesting from UserScriptSide, never the other way around
 */
var UserScriptMessaging_PageSideResponder = function() {
    var self = this;
    self.debug = false;
    this.user_script_messaging_channel = window;
    this.self_name = "UserScriptSide";
    //************************Process Post*********************************
    var processPostRequest = function(post_message) {
        if (self.debug) console.log("UserScriptSide:PROCESS POST", ping_message);
        var post_key = post_message.post;
        var value = post_message.data;
        var response = GM_setValue(post_key, value);
        sendPostResponse(post_key, true, '', post_message);
    };
    var sendPostResponse = function(post_key, success, message, post_message) {
        var response_message = {
            response: post_message.request_id,
            type: "post:" + post_key,
            from: self.self_name,
            to: post_message.from,
            success: success,
            message: message
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(response_message), '*');
    };
    //************************Process Get**********************************

    var processGetRequest = function(request_message) {
        if (self.debug) console.log("UserScriptSide:PROCESS GET", ping_message);
        var get_key = request_message.get;
        var value = GM_getValue(get_key);
        sendGetResponse(get_key, value, request_message);
    };
    var sendGetResponse = function(get_key, data, request_message) {
        var response_message = {
            response: request_message.request_id,
            type: "get:" + get_key,
            from: self.self_name,
            to: request_message.from,
            success: true,
            data: data
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(response_message), '*');
    };
    //************************Requests*************************************
    var processPing = function(ping_message) {
        if (self.debug) console.log("UserScriptSide:PROCESS PING", ping_message);
        var request_id = ping_message.ping;
        var response_message = {
            pong: request_id,
            from: self.self_name,
            to: ping_message.from,
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(response_message), '*');
    };
    var aknowledge = function(request_message) {
        if (self.debug) console.log("UserScriptSide:AKNOWLEDGE: ", request_message);
        if (!request_message.request_id) return;
        var message_obj = {
            aknowledge: request_message.request_id,
            from: self.self_name,
            to: request_message.from
        };
        self.user_script_messaging_channel.postMessage(JSON.stringify(message_obj), '*');
    };
    var recievePostMessage = function(event) {
        if (self.debug) console.log("UserScriptSide:receivePostMessage:", received_message);
        var received_message;
        try {
            received_message = JSON.parse(event.data);
        } catch (e) {
            return;
        }

        //only need to listen to messages to this
        if (!received_message.to || received_message.to !== self.self_name) return;

        if (received_message.ping) {
            processPing(received_message);
        }

        if (received_message.get) {
            aknowledge(received_message);
            return processGetRequest(received_message);
        }

        if (received_message.post) {
            aknowledge(received_message);
            return processPostRequest(received_message);
        }

    };
    //
    // Setup listener
    self.user_script_messaging_channel.addEventListener('message', recievePostMessage, false);

};


/*
 *useage:
var user_script_channel = new UserScriptMessaging_PageSideResponder();
 *
 */