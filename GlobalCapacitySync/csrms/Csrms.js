var CSrms = {
	root_url           : null,
	server_url         : "http://tci.fieldportal.net",
	webCsrmsUrl        : "/GlobalCapacitySync/csrms/",
	webAppUrl          : "/GlobalCapacitySync/app/",
	webLibsUrl         : "/GlobalCapacitySync/libs/",
	webAPIUrl          : "/GlobalCapacitySync/api/",
	webGeboUrl         : "/gebo_admin/",
	//webResourcesUrl    : "",
	//webModulesUrl      : "public/",

	getCsrmsUrl        : function(page_path) {
		return this.root_url + this.webCsrmsUrl + page_path;
	},
	getLibsUrl        : function(page_path) {
		return this.root_url + this.webLibsUrl + page_path;
	},
	getAPIUrl         : function(page_path) {
		return this.server_url + this.webAPIUrl + page_path;
	},
	getGeboUrl        : function(page_path) {
		return this.root_url + this.webGeboUrl + page_path;
	},
	//getResourcesUrl        : function(page_path) {
	//	return this.root_url + this.webResourcesUrl + page_path;
	//},
	getAppUrl        : function(page_path) {
		return this.root_url + this.webAppUrl + page_path;
	}
};