
var TableDataFilters = {};

function TableDataFilter_ColumnTHWithSections() {
    var current_table_dom;
    var data;
    var section;

    //there could be cases where cells need to be revisited. keep a row map 
    this.section_row_map = null;
    this.data_row_map = null;
    var self = this;

    var onRowEnter = function(row_elem) {
        //test key value pair?
    };

    var onCell = function(cell_elem) {
        //scope of 'this' is TableProcess. We can access _cell_index, _row_index
        if(cell_elem.nodeName !== 'TH') {
            return;
        }
        var data_cell = current_table_dom.getCell(this._row_index,this._cell_index + 1);
        var cell_value = DOMElements.Filters.getElementCleanText(cell_elem);
        if(data_cell === null) {
            //is section header
            section = {};
            self.section_row_map[cell_value] = this._row_index;
            data[cell_value] = section;
        } else {
            //is data label
            if(section) {
                //we are in a section
                section[cell_value] = DOMElements.Filters.getElementCleanText(data_cell);
            } else {
                //no section yet
                data[cell_value] = DOMElements.Filters.getElementCleanText(data_cell);
            }
            self.data_row_map[cell_value] = this._row_index;
            this._cell_index++;
        }
                    
    };

    var table_process = new DOMTableTools.TableProcess({
        onCell: onCell
    });

    this.getData = function(table_dom) {
        current_table_dom = table_dom;
        data = {};
        this.section_row_map = {};
        this.data_row_map = {};
        table_process.processTable(current_table_dom);
        return data;
    };
}

function TableDataFilter_RowData() {
    //temp vars
    var current_table_dom;
    var data;
    var row;

    this.column_array = null;
    var self = this;

    var isTHRow = function(row_elem) {
        var cell_elem = TableTools.TableDOM.prototype.getRowCell(row_elem,0);
        return cell_elem.nodeName === 'TH';
    };

    var onRowEnter = function(row_elem) {
        if(self.column_array === null) {
            //see if in first th row
            if(!isTHRow(row_elem)) {
                return false; //skip this row
            }
        }
        row = [];
        return true;
    };

    var afterProcessRow = function(row_elem) {
        if(self.column_array === null) {
            //see if in first th row
            if(isTHRow(row_elem)) {
                self.column_array = row;
            }
        } else {
            data[data.length] = row;
        }
    };

    var onCell = function(cell_elem) {
        var cell_value = DOMElements.Filters.getElementCleanText(cell_elem);
        row[row.length] = cell_value;                    
    };

    var table_process = new TableTools.TableProcess({
        onCell: onCell,
        onRow: onRowEnter,
        afterProcessRow: afterProcessRow
    });

    this.getData = function(table_dom) {
        current_table_dom = table_dom;
        data = [];
        this.column_array = null;
        table_process.processTable(current_table_dom);
        return data;
    };
}
