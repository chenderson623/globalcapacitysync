function ChildWindowOpener(options, name) {
    this.name = name || 'child_window' + Date.now;
    this.options = {
        //std window options:
        width: 800, // sets the width in pixels of the window.
        height: 600, // sets the height in pixels of the window.
        toolbar: 0, // determines whether a toolbar (includes the forward and back buttons) is displayed {1 (YES) or 0 (NO)}.
        scrollbars: 1, // determines whether scrollbars appear on the window {1 (YES) or 0 (NO)}.
        location: 1, // determines whether the address bar is displayed {1 (YES) or 0 (NO)}.
        status: 1, // whether a status line appears at the bottom of the window {1 (YES) or 0 (NO)}.
        menubar: 0, // determines whether the menu bar is displayed {1 (YES) or 0 (NO)}.
        resizable: 1, // whether the window can be resized {1 (YES) or 0 (NO)}. Can also be overloaded using resizable.

        //centering options:
        centerBrowser: 0, // center window over browser window? {1 (YES) or 0 (NO)}. overrides top and left
        centerScreen: 0, // center window over entire screen? {1 (YES) or 0 (NO)}. overrides top and left
        left: 0, // left position when the window appears.
        top: 0 // top position when the window appears.
    };
    this.window = null;
    this.setOptions(options);
    if (this.options.centerBrowser === 1) {
        this.centerBrowser();
    }
    if (this.options.centerScreen === 1) {
        this.centerScreen();
    }
}

ChildWindowOpener.prototype = {
    getWindow: function() {
        if (!this.window) {
            throw "Need to open window first";
        }
        return this.window;
    },

    isWindowOpened: function () {
        console.log("INSIDE isWindowOpened ", this.window, typeof this.window);
    },

    open: function(url, onload) {
        if(!url) {
            url = 'about:blank';
        }
        console.log("OPENING");
        console.log("ISWINDOWOPENED", this.isWindowOpened());
        this.window = window.open(url, this.name, this.getOptionsString());
        try {
            this.window.focus();
        } catch(e) {}
        if(onload) {
            console.log("SET ONOLAD");
            try {
                onload();
                //this.window.onload = onload;
            } catch (err) {
                console.log('Cannot set child window.onload. Probably a cross-domain issue, or possibly a blocked pop-up window.');
            }
        }
        return this.window;
    },

    focus: function() {
        console.log("FOCUS");
        this.getWindow().focus();
    },

    close: function() {
        this.getWindow().close();
        console.log("ISWINDOWOPENED", this.isWindowOpened());
    },

    centerBrowser: function() {
        if (this.window) {
            throw "This needs to be called before open()";
        }

        if (window.screenY && window.outerHeight) {
            this.options.top = window.screenY + (((window.outerHeight / 2) - (this.options.height / 2)));
            this.options.left = window.screenX + (((window.outerWidth / 2) - (this.options.width / 2)));
        } else if (window.screenTop && document.documentElement && document.documentElement.clientHeight) { //IE
            this.options.top = window.screenY + (((window.outerHeight / 2) - (this.options.height / 2)));
            this.options.left = window.screenX + (((window.outerWidth / 2) - (this.options.width / 2)));
        } else {
            throw "Cannot center browser";
        }
    },

    centerScreen: function() {
        this.options.top = (screen.height - this.options.height) / 2;
        this.options.left = (screen.width - this.options.width) / 2;
    },

    getOptionsString: function() {
        var return_string = "";

        for (var i in this.options) {
            if (i === 'centerBrowser' || i === 'centerScreen') {
                continue;
            }
            if (return_string !== "") {
                return_string += ",";
            }
            return_string += i + "=" + this.options[i];
        }
        return return_string;
    },

    setOptions: function(options) {
        for (var i in options) {
            this.options[i] = options[i];
        }
    }

};