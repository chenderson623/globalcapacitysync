//TODO take dependency on body element out of this
ControlBar = (function() {
    function ControlBar() {
        var options = {
            container_height: 40,
            control_bar_id: 'fieldportal_controls_container'
        };
        //this.$body_element = null;
        this.$control_bar_element = null; //available right away
        this.$control_container_element = null; //available right away
        this.control_container = null;
        this.getControlContainerElem = function() {
            return this.$control_container_element;
        };
        this.getControlContainer = function() {
            if (this.control_container === null) {
                this.control_container = new ControlsContainer(this.getControlContainerElem());
                this.control_container.init();
            }
            return this.control_container;
        };
        this.appendToControlBar = function(elem) {
            this.getControlContainerElem().append(elem);
        };
        this.setHeight = function(height) {
            //this.$body_element.height(height + 'px');
            this.$control_bar_element.height(height + 'px');
            this.$control_bar_element.find('.container').height(height + 'px');
        };
        this.show = function() {
            var self = this;
            //this.$body_element.show();
            self.$control_bar_element.slideDown();
            /*
            this.$body_element.slideDown(function() {
                self.$control_bar_element.slideDown();
            });
            */
        };
        this.hide = function() {
            var self = this;
            this.$control_bar_element.slideUp(function() {
                //self.$body_element.slideUp();
            });
        };
        this.init = function() {
            //this.$body_element = $("<div class=\"control-bar-body-insert\">");
            //this.$body_element.hide();
            this.$control_bar_element.hide();
            //$('body').prepend(this.$body_element);
            //this.$body_element.append(this.$control_bar_element);
            $('body').append(this.$control_bar_element);
            this.setHeight(options.container_height);
        };
        /*=========Init===================*/
        //Note that this element is available right away
        this.$control_bar_element = $("<div class=\"controlbar controlbar-fixed-top\" id=\"" + options.control_bar_id + "\"><\/div>");
        var $control_bar_inner = $('<div class="controlbar-inner"><\/div>');
        this.$control_container_element = $('<div class="container"><\/div>');
        this.$control_bar_element.append($control_bar_inner);
        $control_bar_inner.append(this.$control_container_element);
    }
    return ControlBar;
})();
/**
 * Takes care of the split (left, middle, right) container
 * This is what we want to add content to
 */
ControlsContainer = (function() {
    function ControlsContainer($container_elem) {
        var options = {};
        this.$container_elem = $container_elem;
        this.$controls_left = null;
        this.$controls_middle = null;
        this.$controls_right = null;
        this.init = function() {
            this.$controls_left = $("<div class=\"control-container left\"><\/div>");
            this.$controls_middle = $("<div class=\"control-container middle\"><\/div>");
            this.$controls_right = $("<div class=\"control-container right\"><\/div>");
            $container_elem.append(this.$controls_left);
            $container_elem.append(this.$controls_middle);
            $container_elem.append(this.$controls_right);
        };
    }
    return ControlsContainer;
})();
LoginControl = (function() {
    function LoginControl() {
        this.$element = $(login_template);
        this.setLoggedIn = function(full_name) {
            this.$element.find('.login-trigger').html(full_name + ' <span>&#x25BC;<\/span>');
            this.$element.find('.login-content').html(logout_form_template);
        };
        /*====Init======*/
        this.$element.find('.login-trigger').click(function() {
            $(this).next('.login-content').slideToggle();
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $(this).find('span').html('&#x25B2;');
            } else {
                $(this).find('span').html('&#x25BC;');
            }
        });
    }
    var login_template = ['<ul id="user-drop-controls">', '    <li class="user-drop-login">', '        <a class="login-trigger" href="#">', '            Log in <span>&#x25BC;<\/span>', '        <\/a>', '        <div style="display: none;" class="login-content">', '            <form>', '                <fieldset class="inputs">', '                    <input id="username" name="Email" placeholder="Your email address" required="" type="email">', '                    <input id="password" name="Password" placeholder="Password" required="" type="password">', '                <\/fieldset>', '               <fieldset class="actions">', '                    <input class="submit-button" value="Log in" type="submit">', '               <\/fieldset>', '            <\/form>', '        <\/div>', '    <\/li>', '<\/ul>'].join("\n");
    var logout_form_template = ['            <form>', '               <fieldset class="actions">', '                    <input class="submit-button" value="Log Out" type="submit">', '               <\/fieldset>', '            <\/form>', ].join("\n");
    return LoginControl;
})();
MessageControl = (function() {
    function MessageControl() {
        this.$element = $('<div class="message-control"><\/div>');
        this.message_log = [];
        this.setMessage = function(message) {
            this.message_log.push(message);
            this.$element.html(message);
        };
    }
    return MessageControl;
})();
StatusBarControl = (function() {
    function StatusBarControl() {
        this.$element = $('<div class="status-bar"><\/div>');
        this.status_items = [];
        this.setStatusItem = function(status_item) {
            this.status_items.push(status_item);
            this.$element.append(status_item.$element);
        };
    }
    return StatusBarControl;
})();
StatusItem = (function() {
    function StatusItem(label) {
        this.$element = $('<div class="status-item"><span class="status-label">' + label + '<\/span><span class="status-value"><\/span><\/div>');
    }
    StatusItem.prototype.setStatus = function(status) {
        this.$element.find('.status-value').html(status);
    };
    return StatusItem;
})();