function ParentWindow() {
    this.window = window.opener || window.parent; 
}

ParentWindow.prototype = {
    getWindow: function() {
        if (!this.window) {
            throw "Need to open window first";
        }
        return this.window;
    },

    hasParent: function() {
        return this.window !== null;
    }
};