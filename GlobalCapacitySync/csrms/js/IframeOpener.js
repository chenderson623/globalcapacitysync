function IframeOpener(options, name, target_elem) {
    this.target_elem = target_elem;
    this.name = name || 'iframe_window';
    this.options = {
        //std iframe options:
        width: 800, // sets the width in pixels of the window.
        height: 600, // sets the height in pixels of the window.
        scrolling: 'yes',
        frameBorder: 0
    };
    this.iframe = null;
    this.setOptions(options);
}

IframeOpener.prototype = {
    getWindow: function() {
        if (!this.iframe) {
            throw "Need to open iframe first";
        }
        return this.iframe.contentWindow;
    },

    open: function(url, onload) {
        if(!url) {
            url = 'about:blank';
        }

        var iframe = document.createElement('iframe');

        if(this.options.width) {
            iframe.width = this.options.width;
        }
        if(this.options.height) {
            iframe.height = this.options.height;
        }
        if(this.options.scrolling) {
            iframe.scrolling = this.options.scrolling;
        }
        if(this.options.frameBorder) {
            iframe.frameBorder = this.options.frameBorder;
        }

        this.target_elem.appendChild(iframe);
        iframe.contentWindow.name = this.name;
        iframe.src = url;

        this.iframe = iframe;

        if(onload) {
            this.iframe.onload = onload;
        }
        return this.iframe;
    },

    focus: function() {
        this.getWindow().focus();
    },

    setOptions: function(options) {
        for (var i in options) {
            this.options[i] = options[i];
        }
    }

};