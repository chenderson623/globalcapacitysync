function ChildWindow(window_elem) {
    this.window = window_elem;
}

ChildWindow.prototype = {
    getWindow: function() {
        if (!this.window) {
            throw "Need to open window first";
        }
        return this.window;
    },

    focus: function() {
        this.getWindow().focus();
    },

    goto: function(url) {
        this.getWindow().location = url;
    },

    close: function() {
        this.getWindow().close();
    }

};