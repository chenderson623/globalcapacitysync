
    var CSrms = CSrms || {};


    CSrms.Session = (function() {

        var Session = function() {
            var _this = this;

            this.logged_in = null; //null = never attempted, unknown
            this.message = "";
            this.username = "";
            this.full_name = "";
            this.session_id = "";

            this.session_url = 'ajax_session.php';
            this.last_checked = null;
            this.time_threshold = 15; //minutes

            this.on_login_fail = null;
            this.on_login_success = null;
            this.when_logged_in = null;

            this.isLoggedIn = function(check_server) {
                if (typeof check_server == 'undefined')
                    check_server = false;

                if (_this.last_checked === null)
                    _this.last_checked = new Date();

                var current_time = new Date();

                var time_lapsed = (current_time - _this.last_checked) / 1000 / 60;

                if (time_lapsed < _this.time_threshold && _this.logged_in !== null) {
                    //nothing. assume it is still good

                    if (_this.logged_in === true) {
                        if (_this.when_logged_in) {
                            _this.when_logged_in(_this);
                            _this.when_logged_in = null;
                        }
                    }

                    return _this.logged_in;
                }

                if (check_server === true) {
                    _this.getAjaxSession();
                    _this.last_checked = current_time;
                }

                return _this.logged_in;
            };

            this.getAjaxSession = function() {
                var request = new CSrms.AjaxRequest({url: _this.session_url}, null, _this.process);
                request.post();
            };

            this.process = function(data, request_obj) {
                _this.logged_in = data.logged_in;
                _this.message = data.message;
                _this.username = data.username;
                _this.full_name = data.full_name;
                _this.session_id = data.session_id;

                if (data.logged_in === true) {
                    _this.on_login_success(_this);
                    if (_this.when_logged_in) {
                        _this.when_logged_in(_this);
                        _this.when_logged_in = null;
                    }
                }

                if (data.logged_in === false) {
                    _this.on_login_fail(_this);
                }

            };

            this.getUserName = function() {
                return this.username;
            };

            this.getFullName = function() {
                return this.full_name;
            };

            this.whenLoggedIn = function(callback) {
                this.when_logged_in = callback;
                this.isLoggedIn(true);
            };

            this.logIn = function(username, password) {
                var data = {submit: "Login", username: username, password: password};
                var request = new CSrms.AjaxRequest({url: _this.session_url}, data, _this.process);
                request.post();

            };

            this.logOut = function() {
                var data = {action: "Logout"};
                var request = new CSrms.AjaxRequest({url: _this.session_url}, data, _this.process);
                request.post();

            };

            this.onAjaxFail = function(callback) {

            };

            this.onLoginFail = function(callback) {
                this.on_login_fail = callback;
                //check in the case that isLoggedIn is not null
                if (this.isLoggedIn() === false) {
                    callback(this);
                }
            };

            this.onLoginSuccess = function(callback) {
                this.on_login_success = callback;
                //check in the case that isLoggedIn is not null
                if (this.isLoggedIn() === true) {
                    callback(this);
                }
            };

            this.getLoginFormHtml = function() {
                var form_html = '\
		  <div class="login-form-message">message</div>\
		  <form class="login-form">\
		    <div class="fieldset">\
				<fieldset>\
		            <legend>\
		                <span>Login</span>\
		            </legend>\
		            <label for="username">\
		                Username:\
		                <input class="username" name="username" value="" type="text">\
		            </label>\
		            <label for="password">\
		                Password:\
		                <input class="password" name="password" value="" type="password">\
		            </label>\
		            <input class="submit" name="submit" value="Login" type="submit">\
		        </fieldset>\
			</div>\
	    </form>\
				';
                return form_html;
            };

            this.getLoginFormJquery = function() {
                var $form = $(this.getLoginFormHtml());

                $form.find('.username').val(_this.username);
                $($form[0]).html(_this.message); //.login-form-message

                $($form[2]).submit(function() {

                    var data = {};
                    data.username = $(this).find('.username').val();
                    data.password = $(this).find('.password').val();

                    _this.logIn(data.username, data.password);

                    return false;
                });
                return $form;
            };

        };

        return Session;

    })();


    CSrms.AjaxRequest = (function() {

        function AjaxRequest(configs, post_data, callback) {
            var self = this;
            this.configs = jQuery.extend({
                url: '',
                type: 'POST'
            }, configs);
            this.post_data = post_data;  //this can be overridded in the post() method
            this.callback = callback;  //this can be overridded in the post() method

            this.status = 'incomplete'; //changes to success or error

            this.post = function(post_data, callback) {
                //post_data and callback are optional
                if (post_data)
                    self.post_data = post_data;
                if (callback)
                    self.callback = callback;

                if (self.configs.url === '')
                    self.configs.url = window.location.href;
                $.ajax({
                    url: self.configs.url,
                    type: self.configs.type,
                    data: self.post_data,
                    dataType: "json",
                    success: function(data, textStatus, xhr) {
                        self.posted(data);
                    },
                    complete: function(xhr, textStatus) {
                        self.complete(xhr, textStatus);
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        self.error(xhr, textStatus, errorThrown);
                    }
                });
            };
            this.posted = function(data) {
                //can override for other functionality
                self.status = 'success';
                self.callback(data, self);
            };
            this.complete = function(xhr, textStatus) {
            };
            this.error = function(xhr, textStatus, errorThrown) {
                self.status = 'error';
                if (xhr.status != 200) {
                    alert("Error, Data request could not be completed.");
                    return;
                }
                if (textStatus == "parsererror") {
                    //expected a json response, but some kind of parsing error.
                    //We can get the response text with xhr.responseText

                    //alert("Error, Data response has errors.");

                    //Send the response text as successful data
                    //Important! the receiver of this data needs to check that response is an object

                    self.posted(xhr.responseText);
                    return;
                }
                alert("Error, Unknown error: " + textStatus);
            };
        }

        return AjaxRequest;

    })();
