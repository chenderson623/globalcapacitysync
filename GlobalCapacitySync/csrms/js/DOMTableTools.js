

var DOMTableTools = (function () {

    function TableUtil(table_elem) {
        this.table_elem = table_elem;
    }

    TableUtil.prototype = {

        countRows: function() {
            return this.table_elem.rows.length;
        },
        getRow: function (index) {
            if (this.table_elem.rows[index] !== undefined) {
                return this.table_elem.rows[index];
            }
            return null;
        },
        getCell: function(row_index, cell_index) {
            return this.getRowCell(this.getRow(row_index), cell_index);
        },

        filterCell: function(row_index, cell_index, filter) {
            var cell_elem = this.getCell(row_index, cell_index);
            return filter.call(this, cell_elem);
        },
        getCellText: function(row_index, cell_index) {
            return this.filterCell(row_index, cell_index, DOMElements.Filters.getElementCleanText);
        },

        //=========Static functions==============

        countRowCells: function(row_elem) {
            if (row_elem && row_elem.cells !== undefined) {
                return row_elem.cells.length;
            }
            return 0;
        },
        getRowCell: function(row_elem, cell_index) {
            if (row_elem && row_elem.cells[cell_index] !== undefined) {
                return row_elem.cells[cell_index];
            }
            return null;
        }
    };


    function RowUtil(row_elem) {
        this.row_elem = row_elem;
    }


    function TableSet(container_elem) {
        this.table_elems = container_elem.getElementsByTagName('TABLE');
        this.table_doms = [];
    }
    TableSet.prototype = {
        countTables: function() {
            return this.table_elems.length;
        },
        getTableElem: function(index) {
            if(this.table_elems[index] !== undefined) {
                return this.table_elems[index];
            }
            return null;
        },
        getTableUtil: function(index) {
            if(this.table_doms[index] === undefined) {
                var table_elem = this.getTableElem(index);
                if(table_elem === null) {
                    return null;
                }
                this.table_doms[index] = new TableUtil(table_elem);
            }
            return this.table_doms[index];
        },
        findTableByCell: function(row_index, cell_index, elem_compare) {
            var table_dom, cell_elem;
            for(var i=0, len=this.countTables();i<len;i++) {
                table_dom = this.getTableUtil(i);
                cell_elem = table_dom.getCell(row_index, cell_index);
                if (elem_compare(cell_elem)) {
                    return table_dom;
                }
            }
            return null;
        },
        //ALT:
        findTableByCellText: function(row_index, cell_index, text) { //introduces a dependency and an extra line
            var table_dom, cell_elem, cell_text;
            for(var i=0, len=this.countTables();i<len;i++) {
                table_dom = this.getTableUtil(i);
                cell_elem = table_dom.getCell(row_index, cell_index);
                cell_text = DOMElements.Filters.getElementCleanText(cell_elem);
                if (cell_text === text) {
                    return table_dom;
                }
            }
            return null;
        },
        findTableByCellInRow: function(col_index, elem_compare) {

        },
        findTableByCellInColumn: function(col_index, elem_compare) {

        },
    };

    //========================Row Array Column Map======================================
    function Iterator(items) {
        this.index = 0;
        this.items = items;
    }

    Iterator.prototype = {
        first: function() {
            this.reset();
            return this.next();
        },
        next: function() {
            return this.items[this.index++];
        },
        hasNext: function() {
            return this.index <= this.items.length;
        },
        reset: function() {
            this.index = 0;
        },
        each: function(callback) {
            var result;
            for (var item = this.first(); this.hasNext(); item = this.next()) {
                result = callback(item);
                if(result === false) {
                    break;
                }
            }
        }
    };

    function RowArrayColumnMap(row_array, column_label_array) {
        this.row_array = row_array;
        this.column_map = null;
        this.createColumnMap(column_label_array);
    }
    RowArrayColumnMap.prototype = {
        size: function() {
            return this.row_array.length;
        },
        createColumnMap: function(column_array) {
            this.column_map = {};
            var col_label;
            for(var index in column_array) {
                col_label = column_array[index];
                if(this.column_map[col_label] === undefined) {
                    this.column_map[col_label] = index;
                } else {
                    //warn already set? otherwise, keep the first instance
                }
            }
        },
        getColIndex: function(column_name) {
            if(this.column_map[column_name] === undefined) {
                return null;
            }
            return this.column_map[column_name];
        },
        getRowVal: function(row_array, column_name) {
            var col_index = this.getColIndex(column_name);
            if(!row_array[col_index]) {
                return null;
            }
            return row_array[col_index];
        },
        getIterator: function() {
            return new Iterator(this.row_array);
        }
    };


    //========================TableProcess======================================
    //useful when we have a common process to be run on several tables
    function TableProcess(options) {
        this.options = {
            onRow: options.onRow || function(row_elem){},
            afterProcessRow: options.afterProcessRow || function(row_elem){},
            onCell: options.onCell || function(cell_elem){},
            row_start: 0,
            col_start: 0
        };
        this._row_index = null;
        this._cell_index = null;
        //this.table_dom = null; //needed? what if calling processRow by itself?
    }

    TableProcess.prototype = {
        /**
         * @returns null
         */
        processRow: function(row_elem) {
            var num_cells = TableUtil.prototype.countRowCells(row_elem);
            var cell_elem;
            for (this._cell_index = this.options.col_start; this._cell_index < num_cells; this._cell_index++) {
                cell_elem = TableUtil.prototype.getRowCell(row_elem,this._cell_index);
                if(typeof this.options.onCell === 'function') {
                    if(this.options.onCell.call(this,cell_elem) === false) {
                        break;
                    }
                }
            }
        },
        processTable: function(table_dom) {
            var num_rows = table_dom.countRows();
            var row_elem;
            var on_row_result;
            for (this._row_index = this.options.row_start; this._row_index < num_rows; this._row_index++) {
                row_elem = table_dom.getRow(this._row_index);
                if(typeof this.options.onRow === 'function') {
                    if(this.options.onRow.call(this,row_elem) === false) {
                        continue; //should be a diff between break and continue
                    }
                }
                this.processRow(row_elem);
                if(typeof this.options.afterProcessRow === 'function') {
                    if(this.options.afterProcessRow.call(this,row_elem) === false) {
                        break;
                    }
                }
            }
        },
        processColumn: function(table_com, col_index) {}
    };

    //======================TableTraverse=====================================
    //useful when we have serval operations to run on one table
    function TableTraverse(table_dom) {
        this.table_dom = table_dom;

        this.row_start = 0;
        this.col_start = 0;

        this._row_index= null;
        this._cell_index= null;
    }

    TableTraverse.prototype = {
        traverseRows: function(row_callback) {}, //returns null
        traverseColumn: function(row_callback, cell_callback, col_index) {}, //returns null
        traverseCells: function(row_callback, cell_callback, row_complete_callback) {}, //returns null
    };

    //====================TableRowIterator=======================================

    function TableRowIterator(table_dom) {
        this.table_dom = table_dom;
        this.row_start = 0;
    }
    TableRowIterator.prototype = {
        current: function(){}, //returns row_dom
        index: function(){}, //current index
        next: function(){}, //next row or null
        rewind: function(){}, //null
        hasNext: function(){}, //t/f
        valid: function(){} //t/f - if current key is valid
    };


    //====================RowCellIterator=======================================

    function RowCellIterator(table_dom) {
        this.table_dom = table_dom;
        this.row_start = 0;
    }
    RowCellIterator.prototype = {
        current: function(){}, //returns row_dom
        index: function(){}, //current index
        next: function(){}, //next row or null
        rewind: function(){}, //null
        hasNext: function(){}, //t/f
        valid: function(){} //t/f - if current key is valid
    };

    //====================TableDataMappers=======================================

    var TableDataMappers = {};

    TableDataMappers.EachRowHasHeaderAndData_WithSections = function() {
        var current_table_dom;
        var data;
        var section;

        //there could be cases where cells need to be revisited. keep a row map
        this.section_row_map = null;
        this.data_row_map = null;
        var self = this;

        var onRowEnter = function(row_elem) {
            //test key value pair?
        };

        var onCell = function(cell_elem) {
            //scope of 'this' is TableProcess. We can access _cell_index, _row_index
            if(cell_elem.nodeName !== 'TH') {
                return;
            }
            var data_cell = current_table_dom.getCell(this._row_index,this._cell_index + 1);
            var cell_value = DOMElements.Filters.getElementCleanText(cell_elem);
            if(data_cell === null) {
                //is section header
                section = {};
                self.section_row_map[cell_value] = this._row_index;
                data[cell_value] = section;
            } else {
                //is data label
                if(section) {
                    //we are in a section
                    section[cell_value] = DOMElements.Filters.getElementCleanText(data_cell);
                } else {
                    //no section yet
                    data[cell_value] = DOMElements.Filters.getElementCleanText(data_cell);
                }
                self.data_row_map[cell_value] = this._row_index;
                this._cell_index++;
            }

        };

        var table_process = new DOMTableTools.TableProcess({
            onCell: onCell
        });

        this.getData = function(table_dom) {
            current_table_dom = table_dom;
            data = {};
            this.section_row_map = {};
            this.data_row_map = {};
            table_process.processTable(current_table_dom);
            return data;
        };
    };

    TableDataMappers.FirstRowHasHeaders = function() {
        //temp vars
        var current_table_dom;
        var data;
        var row;

        this.column_array = null;
        var self = this;

        var isTHRow = function(row_elem) {
            var cell_elem = DOMTableTools.TableUtil.prototype.getRowCell(row_elem,0);
            return cell_elem.nodeName === 'TH';
        };

        var onRowEnter = function(row_elem) {
            if(self.column_array === null) {
                //see if in first th row
                if(!isTHRow(row_elem)) {
                    return false; //skip this row
                }
            }
            row = [];
            return true;
        };

        var afterProcessRow = function(row_elem) {
            if(self.column_array === null) {
                //see if in first th row
                if(isTHRow(row_elem)) {
                    self.column_array = row;
                }
            } else {
                data[data.length] = row;
            }
        };

        var onCell = function(cell_elem) {
            var cell_value = DOMElements.Filters.getElementCleanText(cell_elem);
            row[row.length] = cell_value;
        };

        var table_process = new DOMTableTools.TableProcess({
            onCell: onCell,
            onRow: onRowEnter,
            afterProcessRow: afterProcessRow
        });

        this.getData = function(table_dom) {
            current_table_dom = table_dom;
            data = [];
            this.column_array = null;
            table_process.processTable(current_table_dom);
            return data;
        };
    };

    TableDataMappers.FourColumnData = function() {
        //has data arranged as col 0 label, col1 data, col2 label, col3 data
        var current_table_dom;
        var data;

        var self = this;

        var getDataPair = function(row_elem, label_index, value_index) {
            var label_cell = TableUtil.prototype.getRowCell(row_elem, label_index);
            var label_value = DOMElements.Filters.getElementCleanText(label_cell);

            var value_cell = TableUtil.prototype.getRowCell(row_elem, value_index);
            var value = DOMElements.Filters.getElementCleanText(value_cell);

            data[label_value] = value;
        };

        var getRowData = function(row_elem) {
            getDataPair(row_elem,0,1);
            getDataPair(row_elem,2,3);
        };

        var onRow = function(row_elem) {
            var num_cells = TableUtil.prototype.countRowCells(row_elem);
            if(num_cells === 4) {
                getRowData(row_elem);
            }
        };

        var table_process = new DOMTableTools.TableProcess({
            onRow: onRow
        });

        this.getData = function(table_dom) {
            current_table_dom = table_dom;
            data = {};
            table_process.processTable(current_table_dom);
            return data;
        };
    };


    return {
        TableUtil: TableUtil,
        RowUtil: RowUtil,
        TableSet: TableSet,
        TableProcess: TableProcess,
        TableTraverse: TableTraverse,
        RowArrayColumnMap: RowArrayColumnMap,
        TableDataMappers: TableDataMappers
    };
})();