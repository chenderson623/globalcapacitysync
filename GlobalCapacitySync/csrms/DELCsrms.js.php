<?php 
if(!class_exists('CsrmsApplication')) {
	$require_login = false;
	require 'init.php';
	
	Header("content-type: application/javascript");
}
?>

var CSrms = {
	root_url           : "<?php echo WebPaths::getSiteRoot().'/'; ?>",
	webCsrmsUrl        : "<?php echo WebPaths::URL('WEB_CSRMS_PATH'); ?>",
	webAppUrl          : "<?php echo WebPaths::URL('WEB_APP_PATH'); ?>",
	webLibsUrl         : "<?php echo WebPaths::URL('WEB_LIBS'); ?>",
	webResourcesUrl    : "<?php echo WebPaths::URL('WEB_CSRMS_RESOURCES'); ?>",
	webModulesUrl      : "<?php echo WebPaths::URL('WEB_CSRMS_MODULES'); ?>",
	
	getCsrmsUrl        : function(page_path) {
		return this.webCsrmsUrl + page_path;
	},
	getLibsUrl        : function(page_path) {
		return this.webLibsUrl + page_path;
	},
	getResourcesUrl        : function(page_path) {
		return this.webResourcesUrl + page_path;
	},
	getModulesUrl        : function(page_path) {
		return this.webModulesUrl + page_path;
	}
};