
/*
Notes:

  - Assume that queue job will be the smallest set. Index and loop queue jobs (at least once a day the list will be empty)
    Will always have to loop task_control_collection to mark tested (can be done in checking potentially cleared on wrong date)

  - 2014-07-16: maybe match multiple fieldportal jobs first.
    - get all multiple
    - make sure only one in timematrix: multiple-fieldportal to on timematrix
    - find best match
    - put the rest in delete candidate set (don't decide yet)
    - remove all from fieldportal queue
  - 2014-07-22: better - loop job index, not job queue. Will get all sets
    - always able to do many-to-many match

  - matching hierarchy
    1. loop queue jobs, try to match task id and seq
    2. loop queue jobs, try to match task id
    3. loop unmatched timematrix jobs, see if cleared in portal on previous day
    //maybe get an ajax list of known task_id/task_seq/task_type cleared on wrong date

  - Unmatched timematrix jobs that are CLEARED are potentially closed on the wrong date
*/

/*
Many to many matches:

Time Matrix:                            Field Portal:

workorder_id: 45612,                    workorder_id: 45612,  --this one is valid
JobType: Trouble Ticket,                JobType: Trouble Ticket,
ContractRefNum: 4171009,                ContractRefNum: 4171009,
SchedSeq: 1,                            SchedSeq: 1,
AssignedTechName: Chuck Oti,            AssignedTechName: Chuck Oti,
AssignedDate: 2011-10-05                AssignedDate: 2011-10-05
    matches: [*11110, 11000, 11000]

workorder_id: 45612,                    workorder_id: 45612, --This one is valid
JobType: Trouble Ticket,                JobType: Trouble Ticket,
ContractRefNum: 4171009,                ContractRefNum: 4171009,
SchedSeq: 2,                            SchedSeq: 2,
AssignedTechName: David Martinez,       AssignedTechName: Chris Barnes,
AssignedDate: 2011-10-05                AssignedDate: 2011-10-05
    matches: [11000, 11100, *11110]

                                        workorder_id: 45612,
                                        JobType: Trouble Ticket,
                                        ContractRefNum: 4171009,
                                        SchedSeq: 2,
                                        AssignedTechName: David Martinez,
                                        AssignedDate: 2011-10-05

Fieldportal case 2: was moved in the portal (or manually created) and another job was created on other tech
  - need a scoring system for portal transactions

 */


/*
From fieldportal first:
Field Portal:                          Time Matrix:

workorder_id: 45612,                    workorder_id: 45612,
JobType: Trouble Ticket,                JobType: Trouble Ticket,
ContractRefNum: 4171009,                ContractRefNum: 4171009,
SchedSeq: 1,                            SchedSeq: 1,
AssignedTechName: Chuck Oti,            AssignedTechName: Chuck Oti,
AssignedDate: 2011-10-05                AssignedDate: 2011-10-05
    matches: [11110]

workorder_id: 45612,
JobType: Trouble Ticket,
ContractRefNum: 4171009,
SchedSeq: 1,
AssignedTechName: David Martinez,
AssignedDate: 2011-10-05
    matches: [11000]




Scenario:
tech has job, creates for himself
job goes in the matrix on a different tech


Rules:
Never move a job that tech creates (can't match it either. score -100000)
Never delete a job that tech creates (this is an unmatched job)
Never move a job that was manually moved to tech (can't match it either. score -100000)


 */

var task_control_model = {
    timematrix_task: null,
    fieldportal_job: null
};

var queue_job_index = [31244,31265,31276]; //this doesn't index anything
var queue_job_index2 = { //index = task_id (for fast lookup) - no, we are not looking up task ids
    126789: [
        34,
        67
    ]
};

//This will not have a 0 index
var fieldportal_job_queue = [ //array of workorder_id from queue_job_collection
    31244,
    31265,
    31276
];

//create with:
var fieldportal_job_queue = [];
fieldportal_job_collection.each(function(fieldportal_job){
    fieldportal_job_queue.push(fieldportal_job.getId());
});

//provides fast lookup of task_control_model
//(task_control_collection_task_id_index)
//this could be mutable (when matched), or use another structure (task_control_queue)
var timematrix_task_index = { //index = task_id (could potentially contain both a trouble ticket and work order, for example)
    1268966: [ //array of task_control_collection indexes
        34,
        67
    ]
};
//create with:
var timematrix_task_index = {};
var task_control_collection_index = 0;
task_control_collection.each(function(task_control){
    var task_id = task_control.timematrix_task.task_id;
    if(!timematrix_task_index[task_id]) {
        timematrix_task_index[task_id] = [];
    }
    timematrix_task_index[task_id].push(task_control_collection_index);
    task_control_collection_index++;
});

//find a task_control_model
function find_task_control(task_id, task_seq, task_type) {
    var return_array = [];
    if(!timematrix_task_index[task_id]) {
        return return_array;
    }
    if(!timematrix_task_index[task_id].length) {
        //either not an array or is empty
        return return_array;
    }
    //if still here, we have an array with length >= 1
    var task_id_array = timematrix_task_index[task_id];
    if(!task_seq || !task_type) {
        //if we don't pass task_seq || task_type, return the array
        return task_id_array;
    }

    var task_model, task_control_index;
    for(var i=0, len=task_id_array.length; i < len; i++) {
        task_control_index = task_id_array[i];
        task_model = task_control_collection.at(task_control_index).timematrix_task;
        if(task_model.task_seq === task_seq && task_model.task_type === task_type) {
            return_array.push(task_control_index); //theoretically there could be more than one
        }
    }
    return return_array;
}

//This will have a zero index
var task_control_queue = [ //array of task_control_collection indexes
    0,
    1,
    2,
    3,
    4
];
//create with:
var task_control_queue = [];
for(var i=0, len=task_control_collection.size(); i<len; i++) {
    task_control_queue.push(i);
}
//remove items by setting to null
//reindex with .filter(function(a){return a !== null;})


//removing items from list:
//  - splice (might be inefficient for large array, sonce javascript needs to reindex)
//  - set to al falsy value (0, false, -1, null)

/*reindexing array:
var my_array = [1,2,3,4,5,6];
my_array[4] = null;
console.log(my_array.filter(function(a){return a !== null;}));
*/

//Loop 1 in hierarchy
for (var i=0, len=fieldportal_job_queue; i<len;i++) {
    var fieldportal_job = fieldportal_job_collection.get(fieldportal_job_queue[i]);
    //get task task id and seq, look up in timematrix task index
    if(timematrix_task_index[task_id]) {
        //loop array and see if its there
    }
    //if found, match task type
    //if found:
    //do some matching
    fieldportal_job_queue[i] = null;
}
//reindex:
fieldportal_job_queue = fieldportal_job_queue.filter(function(a){return a !== null;});

//Loop 2: if in fieldportal, with no seq, and unmatched task in time matrix, assume they should match

//array.splice(index, 1);

var QueueCompare = function(task_control_collection, fieldportal_job_collection) {
    this.task_control_collection = task_control_collection;
    this.queue_job_collection = queue_job_collection;


    var add_index = function() {

    };
    this.createQueueJobCollectionIndex = function() {
        var index = {};

        this.queue_job_collection.each(function(queue_job_model){
            var workorder_id = queue_job_model.getId();
            if(!index[workorder_id]) {

            } else {

            }
        });
    };

};