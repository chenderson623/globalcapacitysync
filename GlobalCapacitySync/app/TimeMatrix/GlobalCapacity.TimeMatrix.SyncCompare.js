

if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

GlobalCapacity.TimeMatrix.SyncCompare = (function() {
    "use strict";

    var debug = false;

    var SyncCompare = {
    };

    /*
     *
     * ===========TaskControlTaskIdIndex=================
     *
     */
    SyncCompare.TaskControlTaskIdIndex = function() {
        this.task_control_task_id_index = {};
    };
    SyncCompare.TaskControlTaskIdIndex.prototype = {
        add: function(task_control_model, index) {
            var task_id = task_control_model.timematrix_task.get('task_id');
            if (!this.task_control_task_id_index[task_id]) {
                this.task_control_task_id_index[task_id] = [];
            }
            this.task_control_task_id_index[task_id].push(index);
        },
        findTaskId: function(task_id) {
            task_id = parseInt(task_id);
            if (!this.task_control_task_id_index[task_id]) {
                return [];
            }

            if (!this.task_control_task_id_index[task_id].length) {
                //either not an array or is empty
                return [];
            }
            //if still here, we have an array with length >= 1
            return this.task_control_task_id_index[task_id];
        },
        findTaskControlIds: function(task_id, task_seq, task_type, task_control_collection) {
            var return_array = [];
            var task_control_id_array = this.findTaskId(task_id);
            var task_model, task_control_id;
            for (var i = 0, len = task_control_id_array.length; i < len; i++) {
                task_control_id = task_control_id_array[i];
                task_model = task_control_collection.at(task_control_id).timematrix_task;
                if (parseInt(task_model.get('task_seq')) === parseInt(task_seq) && task_model.get('task_type') === task_type) {
                    return_array.push(task_control_id); //theoretically there could be more than one
                }
            }
            return return_array;
        },
        getTaskIdArray: function() {
            var return_array = [];
            for (var task_id in this.task_control_task_id_index) {
                return_array.push(task_id);
            }
            return return_array;
        }
    };
    /*
     *
     * ===========IDQueue=================
     *
     */
    SyncCompare.IDQueue = function() {
        this.id_queue = []; //always use getQueueArray to access this as some values may have been nullified
    };
    SyncCompare.IDQueue.prototype = {
        add: function(id) {
            //only add if does not exist
            if (this.findID(id) === -1) {
                this.id_queue.push(id);
            }
        },
        findID: function(id) {
            return this.id_queue.indexOf(id);
        },
        hasID: function(id) {
            return this.findID(id) > -1;
        },
        removeIdFromQueue: function(id) {
            var index = this.findID(id);
            if (index > -1) {
                this.id_queue[index] = null;
            }
        },
        getQueueArray: function() {
            return this.id_queue.filter(function(id) {
                return id !== null;
            });
        }
    };
    /*
     *
     * ===========FieldportalJobIdIndex=================
     *
     */
    SyncCompare.FieldportalJobIdIndex = function() {
        this.fieldportal_job_id_index = {};
    };
    SyncCompare.FieldportalJobIdIndex.prototype = {
        add: function(fieldportal_job, index) {
            var task_id = fieldportal_job.get('ContractRefNum');
            if (!this.fieldportal_job_id_index[task_id]) {
                this.fieldportal_job_id_index[task_id] = [];
            }
            this.fieldportal_job_id_index[task_id].push(index);
        },
        findTaskId: function(task_id) {
            task_id = parseInt(task_id);
            if (!this.fieldportal_job_id_index[task_id]) {
                return [];
            }

            if (!this.fieldportal_job_id_index[task_id].length) {
                //either not an array or is empty
                return [];
            }
            //if still here, we have an array with length >= 1
            return this.fieldportal_job_id_index[task_id];
        },
        findFieldportalJobIds: function(task_id, task_seq, task_type, fieldportal_job_collection) {
            var return_array = [];
            var fieldportal_job_id_array = this.findTaskId(task_id);
            var search_job_type = FieldPortal.JobTypesForAbbrev.getJobType(task_type);
            var fieldportal_job, fieldportal_job_id;
            for (var i = 0, len = fieldportal_job_id_array.length; i < len; i++) {
                fieldportal_job_id = fieldportal_job_id_array[i];
                fieldportal_job = fieldportal_job_collection.at(fieldportal_job_id);
                if (parseInt(fieldportal_job.get('SchedSeq')) === parseInt(task_seq) && fieldportal_job.get('JobType') === search_job_type) {
                    return_array.push(fieldportal_job_id); //theoretically there could be more than one
                }
            }
            return return_array;
        },
        getTaskIdsWithMultipleJobs: function() {
            var return_array = [];
            for (var task_id in this.fieldportal_job_id_index) {
                if (this.fieldportal_job_id_index[task_id].length > 1) {
                    return_array.push(task_id);
                }
            }
            return return_array;
        },
        getTaskIdArray: function() {
            var return_array = [];
            for (var task_id in this.fieldportal_job_id_index) {
                return_array.push(task_id);
            }
            return return_array;
        }
    };
    /*
     *
     * ===========FieldportalJobTaskControlMatch=================
     *
     */
    SyncCompare.FieldportalJobTaskControlMatch = function() {
        this.task_id = false;
        this.task_seq = false;
        this.task_type = false;
        this.tech = false;
        this.date = false;
        //TODO: use these?
        //fieldportal job was moved to this tech
        this.fieldportal_moved = false; //cannot match if job was moved to fieldportal_job and task_control tech does not match
        //fieldportal job was created by this tech
        this.fieldportal_created = false; //cannot match if job was created by tech in fieldportal_job and task_control tech does not match
    };
    SyncCompare.FieldportalJobTaskControlMatch.prototype = {
        matchesTask: function() {
            return this.task_id && this.task_type;
        },
        //TODO: how to use these with fieldportal_moved? how to use with MatchRank?
        matchesTaskAndSeq: function() {
            return this.matchesTask() && this.task_seq;
        },
        matchesThruTech: function() {
            return this.matchesTaskAndSeq() && this.tech;
        }
    };
    /*
     *
     * MatchRank
     */
    SyncCompare.FieldportalJobTaskControlMatchRank = {
        ranks: {
            task_id: 100000,
            task_type: 10000,
            date: 1000,
            task_seq: 100,
            tech: 10,
            fieldportal_moved: -10000, //cannot match if job was moved to fieldportal_job and task_control tech does not match
            fieldportal_created: -100000 //cannot match if job was created by tech in fieldportal_job and task_control tech does not match
        },
        rank: function(match) {
            var rank = 0;
            for (var match_type in SyncCompare.FieldportalJobTaskControlMatchRank.ranks) {
                if (match[match_type] === true) {
                    rank += SyncCompare.FieldportalJobTaskControlMatchRank.ranks[match_type];
                }
            }
            return rank;
        }
    };
    /*
     *
     * MatchFactory
     */
    SyncCompare.FieldportalJobTaskControlMatchFactory = {
        fieldportal_log: null,
        matchTaskId: function(timematrix_task, fieldportal_job) {
            return parseInt(timematrix_task.get('task_id')) === parseInt(fieldportal_job.get('ContractRefNum'));
        },
        matchTaskType: function(timematrix_task, fieldportal_job) {
            var match_job_type = FieldPortal.JobTypesForAbbrev.getJobType(timematrix_task.get('task_type'));
            return match_job_type === fieldportal_job.get('JobType');
        },
        hasSchedSeq: function(fieldportal_job) {
            var fieldportal_seq = parseInt(fieldportal_job.get('SchedSeq'));
            if (fieldportal_seq === 0) {
                return false;
            }
            return true;
        },
        matchTaskSeq: function(timematrix_task, fieldportal_job) {
            if (!SyncCompare.FieldportalJobTaskControlMatchFactory.hasSchedSeq(fieldportal_job)) {
                return null;
            }
            return parseInt(timematrix_task.get('task_seq')) === parseInt(fieldportal_job.get('SchedSeq'));
        },
        matchTech: function(task_control_model, fieldportal_job) {
            var tech_control = task_control_model.getTechControlModel();
            var task_fieldportal_tech = tech_control.getFieldportalTech();
            if (!task_fieldportal_tech) {
                return null;
            }
            return task_fieldportal_tech.get('tech_name') === fieldportal_job.get('AssignedTechName');
        },
        matchDate: function(timematrix_task, fieldportal_job) {
            var timeslot = timematrix_task.getTimeslot();
            var tech_schedule = timeslot.getTechSchedule();
            var schedule_model = tech_schedule.getSchedule();
            var iso_date = schedule_model.getISODate();
            return iso_date === fieldportal_job.get('AssignedDate');
        },
        //TODO implement:
        markFieldportalJobWasMoved: function(timematrix_task, fieldportal_job) {
            //determines if job was moved to fieldportal job was moved to tech
            //only meaninful if tech does not match
            if (SyncCompare.FieldportalJobTaskControlMatchFactory.fieldportal_log) {

            }
        },
        markFieldportalJobWasCreated: function(timematrix_task, fieldportal_job) {
            //determines if job was created in fieldportal by tech
            //only meaninful if tech does not match. don't want to match it as it might not be right

        },
        getMatch: function(task_control_model, fieldportal_job) {
            var match = new SyncCompare.FieldportalJobTaskControlMatch();
            var timematrix_task = task_control_model.timematrix_task;
            match.task_id = SyncCompare.FieldportalJobTaskControlMatchFactory.matchTaskId(timematrix_task, fieldportal_job);
            match.task_type = SyncCompare.FieldportalJobTaskControlMatchFactory.matchTaskType(timematrix_task, fieldportal_job);
            match.task_seq = SyncCompare.FieldportalJobTaskControlMatchFactory.matchTaskSeq(timematrix_task, fieldportal_job);
            match.tech = SyncCompare.FieldportalJobTaskControlMatchFactory.matchTech(task_control_model, fieldportal_job);
            if (!match.tech) {
                match.fieldportal_moved = SyncCompare.FieldportalJobTaskControlMatchFactory.markFieldportalJobWasMoved(task_control_model, fieldportal_job);
                match.fieldportal_created = SyncCompare.FieldportalJobTaskControlMatchFactory.markFieldportalJobWasCreated(task_control_model, fieldportal_job);
            }
            match.date = SyncCompare.FieldportalJobTaskControlMatchFactory.matchDate(timematrix_task, fieldportal_job);
            return match;
        }
    };
    /*
     *
     * MatchItem
     */
    SyncCompare.FieldportalJobTaskControlMatchItem = function(task_control_model, fieldportal_job, fieldportal_job_collection_id, task_control_collection_id) {
        //Tasks to match
        this.task_control_model = task_control_model;
        this.fieldportal_job = fieldportal_job;
        //Need indexes to re-find in collections
        this.fieldportal_job_collection_id = fieldportal_job_collection_id;
        this.task_control_collection_id = task_control_collection_id;
        //These are overrideable
        this.match_rank_method = SyncCompare.FieldportalJobTaskControlMatchRank.rank;
        this.match_method = SyncCompare.FieldportalJobTaskControlMatchFactory.getMatch;
        //Internal:
        this.match = null;
        this.match_rank = null;
    };
    SyncCompare.FieldportalJobTaskControlMatchItem.prototype = {
        getMatch: function() {
            if (this.match === null) {
                this.match = this.match_method(this.task_control_model, this.fieldportal_job);
            }
            return this.match;
        },
        getMatchRank: function() {
            if (this.match_rank === null) {
                this.match_rank = this.match_rank_method(this.getMatch());
            }
            return this.match_rank;
        },
        toString: function() {
            var fieldportal_tech = this.task_control_model.getTechControlModel().getFieldportalTech();
            var fieldportal_tech_name = '';
            if (fieldportal_tech) {
                fieldportal_tech_name = fieldportal_tech.get('tech_name');
            }
            var return_str = "Task ID: tc:" + this.task_control_model.timematrix_task.get('task_id') + " fp:" + this.fieldportal_job.get('ContractRefNum');
            return_str += "\nTaskSeq: tc:" + this.task_control_model.timematrix_task.get('task_seq') + " fp:" + this.fieldportal_job.get('SchedSeq');
            return_str += "\nTech: tc:" + fieldportal_tech_name + " fp:" + this.fieldportal_job.get('AssignedTechName');
            return return_str;
        }
    };

    /*
     *
     * ===========FieldportalJobIdArrayTaskMatch=================
     *
     */

    SyncCompare.FieldportalJobIdArrayTaskMatch = function(fieldportal_job_id_array, task_control_id_array, compare) {
        this.fieldportal_job_id_array = fieldportal_job_id_array;
        this.task_control_id_array = task_control_id_array;
        this.compare = compare;
        //Internal:
        this.ranked_matches_array = null; //[fieldportal_job_id_array_index][task_control_id_array_index] = match item
        this.ranked_match_scores_array = null; //[fieldportal_job_id_array_index][task_control_id_array_index] = match score
        this.many_to_many_filter = null;
    };
    SyncCompare.FieldportalJobIdArrayTaskMatch.prototype = {
        _generate_ranked_match_for_fieldportal: function(fieldportal_job_id_array_index) {
            if (!this.ranked_matches_array[fieldportal_job_id_array_index]) {
                this.ranked_matches_array[fieldportal_job_id_array_index] = [];
            }
            var fieldportal_job = this.compare.getFieldportalJob(this.fieldportal_job_id_array[fieldportal_job_id_array_index]);
            var task_control, match_item;
            for (var i = 0, len = this.task_control_id_array.length; i < len; i++) {
                task_control = this.compare.getTaskControl(this.task_control_id_array[i]);
                match_item = new SyncCompare.FieldportalJobTaskControlMatchItem(task_control, fieldportal_job, this.fieldportal_job_id_array[fieldportal_job_id_array_index], this.task_control_id_array[i]);
                this.ranked_matches_array[fieldportal_job_id_array_index][i] = match_item;
            }
        },
        _generate_ranked_matches: function() {
            this.ranked_matches_array = [];
            for (var i = 0, len = this.fieldportal_job_id_array.length; i < len; i++) {
                this._generate_ranked_match_for_fieldportal(i);
            }
        },
        _generate_ranked_match_scores: function() {
            this.ranked_match_scores_array = [];
            var ranked_matches = this.getRankedMatchesArray();
            var tc_array;
            for (var fp = 0, fplen = ranked_matches.length; fp < fplen; fp++) {
                this.ranked_match_scores_array[fp] = [];
                tc_array = ranked_matches[fp];
                for (var tc = 0, tclen = tc_array.length; tc < tclen; tc++) {
                    this.ranked_match_scores_array[fp][tc] = tc_array[tc].getMatchRank();
                }
            }
        },
        getManyToManyFilter: function() {
            if (this.many_to_many_filter === null) {
                this.many_to_many_filter = new SyncCompare.MatchedScoresArrayManyToManyFilter(this.getRankedMatchScoresArray());
            }
            return this.many_to_many_filter;
        },
        getRankedMatch: function(fieldportal_job_id_array_index, task_control_id_array_index) {
            var matches = this.getRankedMatchesArray();
            if (!matches[fieldportal_job_id_array_index]) {
                throw "Bad fieldportal_job_id_array_index";
            }
            if (!matches[fieldportal_job_id_array_index][task_control_id_array_index]) {
                throw "Bad task_control_id_array_index";
            }
            return matches[fieldportal_job_id_array_index][task_control_id_array_index];
        },
        getRankedMatchesArray: function() {
            if (this.ranked_matches_array === null) {
                this._generate_ranked_matches();
            }
            return this.ranked_matches_array;
        },
        getRankedMatchScoresArray: function() {
            if (this.ranked_match_scores_array === null) {
                this._generate_ranked_match_scores();
            }
            return this.ranked_match_scores_array;
        },
        getBestMatch: function(fieldportal_job_id_array_index) {
            if (!this.getRankedMatchScoresArray()[fieldportal_job_id_array_index]) {
                throw "Bad fieldportal_job_id_array_index";
            }
            if (this.fieldportal_job_id_array.length > 1) {
                //do a many-to-many match
                var best_match_tc_index = this.getManyToManyFilter().getBestMatchIndex(fieldportal_job_id_array_index);
                if (best_match_tc_index === null) {
                    return null;
                }
                return this.getRankedMatch(fieldportal_job_id_array_index, best_match_tc_index);
            }
            //TODO: this should probably be in its own function
            var scores = this.getRankedMatchScoresArray()[fieldportal_job_id_array_index];
            var max = 0;
            var max_score_index = 0;
            var score;
            for (var i = 0, len = scores.length; i < len; i++) {
                score = scores[i];
                if (score > max) {
                    max = score;
                    max_score_index = i;
                }
            }
            return this.getRankedMatchesArray()[fieldportal_job_id_array_index][max_score_index];
        }
    };

    /*
     *
     * ===========MatchedScoresArrayManyToManyFilter=================
     *
     */
    SyncCompare.MatchedScoresArrayManyToManyFilter = function(matched_scores_array) {
        this.matched_scores_array = matched_scores_array;
        this.reduced_matched_scores_array = null;
    };
    SyncCompare.MatchedScoresArrayManyToManyFilter.prototype = {
        cloneArray: function(array_to_clone) {
            return JSON.parse(JSON.stringify(array_to_clone));
        },
        _get_best_tc_match_row_index: function(tc_index) {
            var scores = this.reduced_matched_scores_array;
            var max = 0;
            var max_score_index = 0;
            var score;
            for (var fp = 0, len = scores.length; fp < len; fp++) {
                score = scores[fp][tc_index];
                if (score > max) {
                    max = score;
                    max_score_index = fp;
                }
            }
            return max_score_index;
        },
        _null_all_tc_rows_except: function(tc_index, fp_index) {
            var scores = this.reduced_matched_scores_array;
            for (var fp = 0, len = this.reduced_matched_scores_array.length; fp < len; fp++) {
                if (fp !== fp_index) {
                    this.reduced_matched_scores_array[fp][tc_index] = null;
                }
            }
        },
        _filter_best_tc_score: function(tc_index) {
            var best_fp = this._get_best_tc_match_row_index(tc_index);
            this._null_all_tc_rows_except(tc_index, best_fp);
        },
        _get_non_null_column: function(row_index) {
            var row = this.reduced_matched_scores_array[row_index];
            for (var col = 0, len = row.length; col < len; col++) {
                if (row[col] !== null) {
                    return col;
                }
            }
            return null;
        },
        reduce: function() {
            this.reduced_matched_scores_array = this.cloneArray(this.matched_scores_array);
            var tc_len = this.reduced_matched_scores_array[0].length;
            for (var tc = 0; tc < tc_len; tc++) {
                this._filter_best_tc_score(tc);
            }
        },
        getBestMatchIndex: function(fieldportal_job_id_array_index) {
            if (this.reduced_matched_scores_array === null) {
                this.reduce();
            }
            return this._get_non_null_column(fieldportal_job_id_array_index);
        }
    };


    /*
     *
     * ===========FieldportalJobTaskControlMatch=================
     *
     */
    SyncCompare.TaskControlCollectionCompare = function(task_control_collection, fieldportal_job_collection) {
        this.task_control_collection = task_control_collection;
        this.fieldportal_job_collection = fieldportal_job_collection;
        //Internal:
        this.fieldportal_job_index = null;
        this.fieldportal_job_queue = null;
        this.task_control_index = null;
        this.task_control_queue = null;
    };
    SyncCompare.TaskControlCollectionCompare.prototype = {
        getFieldportalJobIndex: function() {
            if (this.fieldportal_job_index === null) {
                this.fieldportal_job_index = new SyncCompare.FieldportalJobIdIndex();
                this.fieldportal_job_queue = new SyncCompare.IDQueue();
                var fieldportal_job;
                for (var i = 0, len = this.fieldportal_job_collection.size(); i < len; i++) {
                    fieldportal_job = this.fieldportal_job_collection.at(i);
                    this.fieldportal_job_index.add(fieldportal_job, i);
                    this.fieldportal_job_queue.add(i);
                }
            }
            return this.fieldportal_job_index;
        },
        getFieldportalJobQueue: function() {
            if (this.fieldportal_job_queue === null) {
                this.getFieldportalJobIndex(); //built together
            }
            return this.fieldportal_job_queue;
        },
        getFieldportalJobQueueArray: function() {
            return this.getFieldportalJobQueue().getQueueArray();
        },
        getTaskControlIndex: function() {
            if (this.task_control_index === null) {
                this.task_control_index = new SyncCompare.TaskControlTaskIdIndex();
                this.task_control_queue = new SyncCompare.IDQueue();
                var task_control;
                for (var i = 0, len = this.task_control_collection.size(); i < len; i++) {
                    task_control = this.task_control_collection.at(i);
                    this.task_control_index.add(task_control, i);
                    this.task_control_queue.add(i);
                }
            }
            return this.task_control_index;
        },
        getTaskControlQueue: function() {
            if (this.task_control_queue === null) {
                this.getTaskControlIndex();
            }
            return this.task_control_queue;
        },
        /*
         *
         * =======TimeMatrix Methods=============
         */
        getTaskControl: function(task_control_id) {
            return this.task_control_collection.at(task_control_id);
        },
        timematrixHasTaskId: function(task_id) {
            return this.getTaskControlIndex().findTaskId(task_id).length > 0;
        },
        timematrixHasUnmatchedTaskId: function(task_id) {
            return this.getUnmatchedTaskControlIds(task_id).length > 0;
        },
        getUnmatchedTaskControlIds: function(task_id) {
            var task_control_id_array = this.getTaskControlIndex().findTaskId(task_id);
            var return_array = [];
            for (var i = 0, len = task_control_id_array.length; i < len; i++) {
                if (!this.getTaskControlQueue().hasID(task_control_id_array[i])) {
                    continue;
                }
                return_array.push(task_control_id_array[i]);
            }
            return return_array;
        },
        /*
         *
         * =======Fieldportal Methods=============
         */
        getFieldportalJob: function(fieldportal_job_id) {
            return this.fieldportal_job_collection.at(fieldportal_job_id);
        },
        fieldportalHasTaskId: function(task_id) {
            return this.getFieldportalJobIndex().findTaskId(task_id).length > 0;
        },
        fieldportalHasUnmatchedTaskId: function(task_id) {
            return this.getUnmatchedFieldportalJobIds(task_id).length > 0;
        },
        getUnmatchedFieldportalJobIds: function(task_id) {
            var fieldportal_job_id_array = this.getFieldportalJobIndex().findTaskId(task_id);
            var return_array = [];
            for (var i = 0, len = fieldportal_job_id_array.length; i < len; i++) {
                if (!this.getFieldportalJobQueue().hasID(fieldportal_job_id_array[i])) {
                    continue;
                }
                return_array.push(fieldportal_job_id_array[i]);
            }
            return return_array;
        },
        getTaskIdsWithManyFieldportalJobIds: function() {
            return this.getFieldportalJobIndex().getTaskIdsWithMultipleJobs();
        },
        /*
         *
         * =======Main Matching Methods=============
         */
        registerMatch: function(match_item) {
            if(debug) console.log("registerMatch", match_item.getMatchRank());
            if(debug) console.log("MATCH ITEM", match_item);
            this.fieldportal_job_queue.removeIdFromQueue(match_item.fieldportal_job_collection_id);
            this.task_control_queue.removeIdFromQueue(match_item.task_control_collection_id);
            //TODO: added this to match. not sure this is the right place yet
            var task_control_model = this.getTaskControl(match_item.task_control_collection_id);
            var fieldportal_job = this.getFieldportalJob(match_item.fieldportal_job_collection_id);
            task_control_model.setFieldPortalJob(fieldportal_job);
            task_control_model.setSyncCompareMatch(match_item);
        }
    };

    return SyncCompare;

})();