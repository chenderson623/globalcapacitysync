if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}
if (typeof GlobalCapacity.TimeMatrix.SyncCompare == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix.SyncCompare is required.';
}

GlobalCapacity.TimeMatrix.SyncCompare.Document = (function() {
    "use strict";

    var TimeMatrixDocumentSyncCompare = function(document_elem) {
        this.document_elem = document_elem;
    };
    TimeMatrixDocumentSyncCompare.prototype = {
        loadFieldPortal: function() {
            var self = this;
            var promise = new Promise(function(resolve, error_hanlder) {
                oss.getFieldPortal(function(field_portal) {
                    resolve(field_portal);
                });
            });
            return promise;
        },
        loadEmployeeSet: function() {
            this._employee_set = FieldPortal.getFieldPortalEmployeeSet();
            this._employee_set.employees = employee_data; //loaded in test_data-employees.js
        },
        loadTimeMatrixSyncControlFactory: function() {
            var self = this;
            var stateless_loader = GlobalCapacity.TimeMatrix.getStatelessLoader();
            var promise = new Promise(function(resolve, error_hanlder) {
                stateless_loader.runTimeMatrixSyncControlFactory(self.document_elem, function(sync_control_factory) {
                    self._sync_control_factory = sync_control_factory;
                    resolve(sync_control_factory);
                });
            });
            return promise;
        },
        loopScheduleSyncFactoryArray: function() {
            //array of TimeMatrixScheduleSyncControlFactory
            var schedule_sync_factory_array = this._sync_control_factory.timematrix_schedule_sync_control_factory;
            var schedule_sync_factory, timemtrix_schedule_sync_compare;
            console.log("schedule_sync_factory_array", schedule_sync_factory_array);
            for (var i = 0, len = schedule_sync_factory_array.length; i < len; i++) {
                schedule_sync_factory = schedule_sync_factory_array[i];
                timemtrix_schedule_sync_compare = new TimeMatrixScheduleSyncCompare(schedule_sync_factory, this);
                timemtrix_schedule_sync_compare.loopFieldportalTasks().then(function() {
                    timemtrix_schedule_sync_compare.updateUnmatchedTaskControlModels();
                });
            }

        },
        run: function() {
            var self = this;
            console.log("HERE");
            this.loadFieldPortal()
                .then(function(field_portal) {
                    return self.loadEmployeeSet();
                })
                .then(function(timematrix) {
                    return self.loadTimeMatrixSyncControlFactory();
                })
                .then(function() {
                    self.loopScheduleSyncFactoryArray();
                });
        },
    };

    var TimeMatrixScheduleSyncCompare = function(schedule_sync_factory, document_sync_compare) {
        this.document_sync_compare = document_sync_compare;
        this.schedule_sync_factory = schedule_sync_factory;
    };
    TimeMatrixScheduleSyncCompare.prototype = {
        getTaskControlCollection: function() {
            if (this._task_control_collection !== undefined) {
                return this._task_control_collection;
            }
            this._task_control_collection = this.schedule_sync_factory.task_control_factory.task_control_collection;
            return this._task_control_collection;
        },
        getScheduleIndex: function() {
            return this.schedule_sync_factory.schedule_index;
        },
        getReportParams: function() {
            if (this._report_params !== undefined) {
                return this._report_params;
            }
            this._report_params = this.schedule_sync_factory.timematrix_document.getScheduleParams(this.getScheduleIndex());
            return this._report_params;
        },
        loadFieldPortalJobCollection: function() {
            if (this._fieldportal_job_collection !== undefined) {
                return Promise.resolve(this._fieldportal_job_collection);
            }
            var load_promise = FieldPortal.loadFieldPortalJobCollection(this.getReportParams());

            load_promise.then(function(fieldportal_job_collection) {
                this._fieldportal_job_collection = fieldportal_job_collection;
            });

            return load_promise;
        },
        test_task_id: function(compare, task_id) {
            var fieldportal_job_id_array = compare.getFieldportalJobIndex().findTaskId(task_id);
            var task_control_id_array = compare.getTaskControlIndex().findTaskId(task_id);

            var task_match = new GlobalCapacity.TimeMatrix.SyncCompare.FieldportalJobIdArrayTaskMatch(fieldportal_job_id_array, task_control_id_array, compare);

            var best_match;
            for (var i = 0, len = fieldportal_job_id_array.length; i < len; i++) {
                best_match = task_match.getBestMatch(i);
                if (!best_match) {
                    continue;
                }

                //TODO: do some threshold
                console.log("THRESHOLD ", best_match.getMatchRank());
                console.log("THRESHOLD ", best_match.getMatch().matchesTaskAndSeq());

                if (best_match.getMatch().matchesTaskAndSeq()) {
                    compare.registerMatch(best_match);
                }
            }
        },
        loadCompare: function() {
            if (this._compare !== undefined) {
                return Promise.resolve(this._compare);
            }
            var self = this;
            var load_promise = new Promise(function(resolve, reject) {
                self.loadFieldPortalJobCollection().then(function(fieldportal_job_collection) {
                    self._compare = new GlobalCapacity.TimeMatrix.SyncCompare.TaskControlCollectionCompare(self.getTaskControlCollection(), fieldportal_job_collection);
                    resolve(self._compare);
                });
            });
            return load_promise;
        },
        loopFieldportalTasks: function() {
            var self = this;

            var promise_resolve;
            var return_promise = new Promise(function(resolve, reject) {
                promise_resolve = resolve; //to avoid nesting
            });

            //loop task_ids in fieldportal job index
            this.loadCompare().then(function(compare) {
                var fieldportal_task_ids = compare.getFieldportalJobIndex().getTaskIdArray();
                var task_id;
                for (var i = 0, len = fieldportal_task_ids.length; i < len; i++) {
                    task_id = fieldportal_task_ids[i];
                    self.test_task_id(compare, task_id);
                }
                promise_resolve(compare);
            });
            return return_promise;
        },
        updateUnmatchedTaskControlModels: function() {
            var promise_resolve;
            var return_promise = new Promise(function(resolve, reject) {
                promise_resolve = resolve; //to avoid nesting
            });

            //Set unmatched task_control_models (for view)
            this.loadCompare().then(function(compare) {
                var unmatched_task_controls = compare.task_control_queue.getQueueArray();
                var task_control_model;
                for (var i = 0, len = unmatched_task_controls.length; i < len; i++) {
                    task_control_model = compare.getTaskControl(unmatched_task_controls[i]);
                    task_control_model.set('needs_sync', true);
                    task_control_model.set('task_matched', false);
                }
                promise_resolve();
            });
            return return_promise;
        }

    };

    return TimeMatrixDocumentSyncCompare;

})();