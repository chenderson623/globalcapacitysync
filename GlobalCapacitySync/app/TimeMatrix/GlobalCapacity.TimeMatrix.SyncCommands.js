

if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

GlobalCapacity.TimeMatrix.SyncCompare = (function() {
    "use strict";

    var SyncCommands = {
    };


    SyncCommands.Command = function() {
        this.enabled = true;
        this.executed = false;
    };
    SyncCommands.Command.prototype = {
        execute: function() {
            if(!this.enabled) {
                return;
            }

            this.executed = true;
        },
        undo: function() {
            if(!this.executed) {
                return;
            }
        },
        bind: function() {

        }
    };


    return SyncCommands;

})();