var fieldportal_jobs = [{
    "workorder_id": 45600,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171990",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45601,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4170251",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45602,
    "JobType": "Work Order",
    "ContractRefNum": "29220863",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45603,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171131",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45604,
    "JobType": "Work Order",
    "ContractRefNum": "2917794", //was 29177941
    "SchedSeq": "4",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45605,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172208",
    "SchedSeq": "2", //was 1
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45606,
    "JobType": "Work Order",
    "ContractRefNum": "29221048",
    "SchedSeq": "1",
    "AssignedTechName": "Charles Hughes",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45607,
    "JobType": "Work Order",
    "ContractRefNum": "29216863",
    "SchedSeq": "1",
    "AssignedTechName": "Charles Hughes",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45608,
    "JobType": "Work Order",
    "ContractRefNum": "29216868",
    "SchedSeq": "2",
    "AssignedTechName": "Chris Barnes", //was Charles Hughes
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45609,
    "JobType": "Work Order",
    "ContractRefNum": "29158929",
    "SchedSeq": "3",
    "AssignedTechName": "Chris Barnes",
    "AssignedDate": "2011-10-05"
},



{
    "workorder_id": 45609,  //duplicated
    "JobType": "Work Order",
    "ContractRefNum": "29158929",
    "SchedSeq": "3",
    "AssignedTechName": "Chris Barnes",
    "AssignedDate": "2011-10-05"
},

{
    "workorder_id": 45609, //duplicated
    "JobType": "Work Order",
    "ContractRefNum": "29158929",
    "SchedSeq": "3",
    "AssignedTechName": "Chris Barnes",
    "AssignedDate": "2011-10-05"
},




 {
    "workorder_id": 45610,
    "JobType": "Work Order",
    "ContractRefNum": "29103120",
    "SchedSeq": "6",
    "AssignedTechName": "Chris Barnes",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45611,
    "JobType": "Work Order",
    "ContractRefNum": "29092035",
    "SchedSeq": "2",
    "AssignedTechName": "Chuck Oti",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45612,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171009",
    "SchedSeq": "1",
    "AssignedTechName": "Chuck Oti",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45613,
    "JobType": "Work Order",
    "ContractRefNum": "29165914",
    "SchedSeq": "4",
    "AssignedTechName": "Chuck Oti",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45614,
    "JobType": "Work Order",
    "ContractRefNum": "29004258",
    "SchedSeq": "3",
    "AssignedTechName": "Chuck Oti",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45615,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4168688",
    "SchedSeq": "1",
    "AssignedTechName": "Chuck Oti",
    "AssignedDate": "2011-10-05"
}, 


{  //these three are duplicated
    "workorder_id": 45616,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172408",
    "SchedSeq": "1",
    "AssignedTechName": "Chuck Oti", //changed
    "AssignedDate": "2011-10-05"
}, 

{
    "workorder_id": 45616,  //this is the closest match
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172408",
    "SchedSeq": "1",
    "AssignedTechName": "Craig MiddleBrook",
    "AssignedDate": "2011-10-05"
},

{
    "workorder_id": 45616,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172408",
    "SchedSeq": "2", //changed
    "AssignedTechName": "Craig MiddleBrook",
    "AssignedDate": "2011-10-05"
},




{
    "workorder_id": 45617,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4155349",
    "SchedSeq": "1",
    "AssignedTechName": "Craig MiddleBrook",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45618,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4170208",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45619,
    "JobType": "Work Order",
    "ContractRefNum": "29233337",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45620,
    "JobType": "Work Order",
    "ContractRefNum": "29205895",
    "SchedSeq": "3",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45621,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172097",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45622,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4159829",
    "SchedSeq": "1",
    "AssignedTechName": "Delon DeSouza",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45623,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171771",
    "SchedSeq": "1",
    "AssignedTechName": "Delon DeSouza",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45624,
    "JobType": "Advanced Services",
    "ContractRefNum": "29270937",
    "SchedSeq": "1",
    "AssignedTechName": "Delon DeSouza",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45625,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171273",
    "SchedSeq": "1",
    "AssignedTechName": "Delon DeSouza",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45626,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4168652",
    "SchedSeq": "3",
    "AssignedTechName": "Delon DeSouza",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45627,
    "JobType": "Work Order",
    "ContractRefNum": "29204901",
    "SchedSeq": "4",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45628,
    "JobType": "Work Order",
    "ContractRefNum": "29227241",
    "SchedSeq": "2",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45629,
    "JobType": "Work Order",
    "ContractRefNum": "29158462",
    "SchedSeq": "2",
    "AssignedTechName": "Earl Jones ",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45630,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4158268",
    "SchedSeq": "1",
    "AssignedTechName": "Earl Jones ",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45631,
    "JobType": "Work Order",
    "ContractRefNum": "29154252",
    "SchedSeq": "2",
    "AssignedTechName": "Earl Jones ",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45632,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4163828",
    "SchedSeq": "1",
    "AssignedTechName": "Earl Jones ",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45633,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171089",
    "SchedSeq": "1",
    "AssignedTechName": "Earl Jones ",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45634,
    "JobType": "Work Order",
    "ContractRefNum": "29240498",
    "SchedSeq": "1",
    "AssignedTechName": "Joe Paragas",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45635,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4165932",
    "SchedSeq": "2",
    "AssignedTechName": "Joe Paragas",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45636,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4167209",
    "SchedSeq": "1",
    "AssignedTechName": "John Cunningham",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45637,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4166812",
    "SchedSeq": "2",
    "AssignedTechName": "John Cunningham",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45638,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4135032",
    "SchedSeq": "2",
    "AssignedTechName": "John Cunningham",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45639,
    "JobType": "Work Order",
    "ContractRefNum": "29157549",
    "SchedSeq": "3",
    "AssignedTechName": "John Cunningham",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45640,
    "JobType": "Work Order",
    "ContractRefNum": "29195348",
    "SchedSeq": "2",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45641,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171368",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45642,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4169571",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45643,
    "JobType": "Work Order",
    "ContractRefNum": "29199247",
    "SchedSeq": "3",
    "AssignedTechName": "Larry Holt",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45644,
    "JobType": "Advanced Services",
    "ContractRefNum": "29268646",
    "SchedSeq": "1",
    "AssignedTechName": "Larry Holt",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45645,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4155909",
    "SchedSeq": "3",
    "AssignedTechName": "Larry Holt",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45646,
    "JobType": "Work Order",
    "ContractRefNum": "29188688",
    "SchedSeq": "2",
    "AssignedTechName": "Larry Holt",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45647,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4168211",
    "SchedSeq": "1",
    "AssignedTechName": "Loc Tran",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45648,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172089",
    "SchedSeq": "1",
    "AssignedTechName": "Loc Tran",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45649,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4170052",
    "SchedSeq": "2",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45650,
    "JobType": "Work Order",
    "ContractRefNum": "29113683",
    "SchedSeq": "3",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45651,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4158289",
    "SchedSeq": "3",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45652,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4160948",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45653,
    "JobType": "Advanced Services",
    "ContractRefNum": "29251885",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45654,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4166008",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45655,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4165991",
    "SchedSeq": "2",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45656,
    "JobType": "Work Order",
    "ContractRefNum": "29236272",
    "SchedSeq": "1",
    "AssignedTechName": "Manuel Chacon",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45657,
    "JobType": "Work Order",
    "ContractRefNum": "29239644",
    "SchedSeq": "1",
    "AssignedTechName": "Manuel Chacon",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45658,
    "JobType": "Work Order",
    "ContractRefNum": "29239101",
    "SchedSeq": "2",
    "AssignedTechName": "Manuel Chacon",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45659,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4169790",
    "SchedSeq": "1",
    "AssignedTechName": "Manuel Chacon",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45660,
    "JobType": "Work Order",
    "ContractRefNum": "29180681",
    "SchedSeq": "3",
    "AssignedTechName": "Mario Encarnacion",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45661,
    "JobType": "Work Order",
    "ContractRefNum": "29231237",
    "SchedSeq": "1",
    "AssignedTechName": "Mario Encarnacion",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45662,
    "JobType": "Work Order",
    "ContractRefNum": "29231280",
    "SchedSeq": "2",
    "AssignedTechName": "Mario Encarnacion",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45663,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171888",
    "SchedSeq": "1",
    "AssignedTechName": "Mario Encarnacion",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45664,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4170252",
    "SchedSeq": "2",
    "AssignedTechName": "Mario Encarnacion",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45665,
    "JobType": "Work Order",
    "ContractRefNum": "29113646",
    "SchedSeq": "4",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45666,
    "JobType": "Work Order",
    "ContractRefNum": "29158470",
    "SchedSeq": "2",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45667,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4169708",
    "SchedSeq": "1",
    "AssignedTechName": "Paul Niedzwiecki",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45668,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4169909",
    "SchedSeq": "1",
    "AssignedTechName": "Paul Niedzwiecki",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45669,
    "JobType": "Work Order",
    "ContractRefNum": "29238254",
    "SchedSeq": "1",
    "AssignedTechName": "Paul Niedzwiecki",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45670,
    "JobType": "Work Order",
    "ContractRefNum": "29216684",
    "SchedSeq": "2",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45671,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4165953",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45672,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4173249",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45673,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4169990",
    "SchedSeq": "1",
    "AssignedTechName": "Raymond Tong",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45674,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171770",
    "SchedSeq": "1",
    "AssignedTechName": "Raymond Tong",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45675,
    "JobType": "Work Order",
    "ContractRefNum": "29184135",
    "SchedSeq": "4",
    "AssignedTechName": "Raymond Tong",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45676,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171091",
    "SchedSeq": "1",
    "AssignedTechName": "Raymond Tong",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45677,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4162208",
    "SchedSeq": "1",
    "AssignedTechName": "Rey Radlin",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45678,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171012",
    "SchedSeq": "1",
    "AssignedTechName": "Rey Radlin",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45679,
    "JobType": "Work Order",
    "ContractRefNum": "29230793",
    "SchedSeq": "1",
    "AssignedTechName": "Robert Markley",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45680,
    "JobType": "Work Order",
    "ContractRefNum": "29063846",
    "SchedSeq": "4",
    "AssignedTechName": "Robert Markley",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45681,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4158533",
    "SchedSeq": "4",
    "AssignedTechName": "Robert Markley",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45682,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171470",
    "SchedSeq": "1",
    "AssignedTechName": "Rodney Bowman",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45683,
    "JobType": "Work Order",
    "ContractRefNum": "29205930",
    "SchedSeq": "2",
    "AssignedTechName": "Rodney Bowman",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45684,
    "JobType": "Work Order",
    "ContractRefNum": "29219594",
    "SchedSeq": "3",
    "AssignedTechName": "Rodney Bowman",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45685,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171668",
    "SchedSeq": "1",
    "AssignedTechName": "Ronald Gabin",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45686,
    "JobType": "Work Order",
    "ContractRefNum": "29223471",
    "SchedSeq": "2",
    "AssignedTechName": "Ronald Gabin",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45687,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4170688",
    "SchedSeq": "1",
    "AssignedTechName": "Ronald Gabin",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45688,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172450",
    "SchedSeq": "1",
    "AssignedTechName": "Ronald Gabin",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45689,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172093",
    "SchedSeq": "1",
    "AssignedTechName": "Ronald Gabin",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45690,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4163028",
    "SchedSeq": "3",
    "AssignedTechName": "Savva Savvas",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45691,
    "JobType": "Work Order",
    "ContractRefNum": "29229502",
    "SchedSeq": "2",
    "AssignedTechName": "Scott Edens",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45692,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4169989",
    "SchedSeq": "1",
    "AssignedTechName": "Scott Edens",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45693,
    "JobType": "Work Order",
    "ContractRefNum": "29216257",
    "SchedSeq": "2",
    "AssignedTechName": "Sean Maloney",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45694,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4172395",
    "SchedSeq": "1",
    "AssignedTechName": "Sean Maloney",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45695,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4162088",
    "SchedSeq": "2",
    "AssignedTechName": "Sean Maloney",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45696,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4171272",
    "SchedSeq": "1",
    "AssignedTechName": "Sean Maloney",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45697,
    "JobType": "Advanced Services",
    "ContractRefNum": "29267674",
    "SchedSeq": "1",
    "AssignedTechName": "Sunil Maharjan",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45698,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4162268",
    "SchedSeq": "2",
    "AssignedTechName": "Sunil Maharjan",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45699,
    "JobType": "Advanced Services",
    "ContractRefNum": "29267990",
    "SchedSeq": "1",
    "AssignedTechName": "Sunil Maharjan",
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45700,
    "JobType": "Work Order",
    "ContractRefNum": "29240521",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45701,
    "JobType": "Work Order",
    "ContractRefNum": "29238203",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}, {
    "workorder_id": 45702,
    "JobType": "Trouble Ticket",
    "ContractRefNum": "4168569",
    "SchedSeq": "1",
    "AssignedTechName": null,
    "AssignedDate": "2011-10-05"
}];
