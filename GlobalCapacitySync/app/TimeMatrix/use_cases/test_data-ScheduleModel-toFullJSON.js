var schedule_model_data = {
    "schedule_date": "2011-10-05T07:00:00.000Z",
    "timeslots_available": ["8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM", "# of Installs*"],
    "limit_params": null,
    "techs_available": [],
    "tech_schedule_collection": [{
        "tech_alias": "S-Aki Porter",
        "tech_model": {
            "tech_alias": "S-Aki Porter"
        },
        "timeslot_collection": [{
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "90713",
                "zip_code_suffix": "314",
                "clli_code": "LNBHCAXS",
                "task_type": "TT",
                "task_id": "4171990",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171990",
                "task_status": "EARMARKED",
                "time_range": "10:22-11:42",
                "start_time": "10:22",
                "end_time": "11:42",
                "service_type": "Repair Premise"
            }, {
                "zip_code": "90814",
                "zip_code_suffix": "182",
                "clli_code": "LNBHCAXT",
                "task_type": "TT",
                "task_id": "4170251",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4170251",
                "task_status": "CLEARED",
                "time_range": "10:30-11:45",
                "start_time": "10:30",
                "end_time": "11:45",
                "service_type": "Repair - Sharing"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "92806",
                "zip_code_suffix": "202",
                "clli_code": "ANHMCA12",
                "task_type": "WO",
                "task_id": "29220863",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29220863",
                "task_status": "EARMARKED",
                "time_range": "12:46-15:06",
                "start_time": "12:46",
                "end_time": "15:06",
                "service_type": "Install-BondedT1"
            }]
        }]
    }, {
        "tech_alias": "S-Brian Skiba",
        "tech_model": {
            "tech_alias": "S-Brian Skiba"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "60007",
                "zip_code_suffix": null,
                "clli_code": null,
                "task_type": "block",
                "task_id": "NA - Conference Call",
                "task_status": "TENTATIVE",
                "time_range": "08:00- 09:00",
                "start_time": "08:00",
                "end_time": "09:00",
                "service_type": "Not Avlbl",
                "task_seq": null,
                "task_url": null
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "60563",
                "zip_code_suffix": "283",
                "clli_code": "NPVLILNA",
                "task_type": "TT",
                "task_id": "4171131",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171131",
                "task_status": "CLEARED",
                "time_range": "10:00-11:00",
                "start_time": "10:00",
                "end_time": "11:00",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "60134",
                "zip_code_suffix": "172",
                "clli_code": "GENVILGN",
                "task_type": "WO",
                "task_id": "29177941",
                "task_seq": "4",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29177941",
                "task_status": "EARMARKED",
                "time_range": "12:00-14:00",
                "start_time": "12:00",
                "end_time": "14:00",
                "service_type": "Consumer Install"
            }]
        }, {
            "timeslot": "3 PM",
            "task_collection": [{
                "zip_code": "60077",
                "zip_code_suffix": null,
                "clli_code": "SKOKILSK",
                "task_type": "TT",
                "task_id": "4172208",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172208",
                "task_status": "EARMARKED",
                "time_range": "15:09-17:09",
                "start_time": "15:09",
                "end_time": "17:09",
                "service_type": "Repair - Sharing"
            }]
        }]
    }, {
        "tech_alias": "S-Charles Hughes",
        "tech_model": {
            "tech_alias": "S-Charles Hughes"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "19044",
                "zip_code_suffix": "222",
                "clli_code": "WLGRPAWG",
                "task_type": "WO",
                "task_id": "29221048",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29221048",
                "task_status": "CLEARED",
                "time_range": "09:30-11:00",
                "start_time": "09:30",
                "end_time": "11:00",
                "service_type": "Install TeleXtend"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "19477",
                "zip_code_suffix": null,
                "clli_code": "AMBLPAAM",
                "task_type": "WO",
                "task_id": "29216863",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29216863",
                "task_status": "CLEARED",
                "time_range": "11:30-13:15",
                "start_time": "11:30",
                "end_time": "13:15",
                "service_type": "Install TeleXtend"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "19477",
                "zip_code_suffix": null,
                "clli_code": "AMBLPAAM",
                "task_type": "WO",
                "task_id": "29216868",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29216868",
                "task_status": "CLEARED",
                "time_range": "13:30-14:00",
                "start_time": "13:30",
                "end_time": "14:00",
                "service_type": "Install TeleXtend"
            }]
        }]
    }, {
        "tech_alias": "S-Chris Barnes",
        "tech_model": {
            "tech_alias": "S-Chris Barnes"
        },
        "timeslot_collection": [{
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "20036",
                "zip_code_suffix": "160",
                "clli_code": "WASHDCMT",
                "task_type": "WO",
                "task_id": "29158929",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29158929",
                "task_status": "CLEARED",
                "time_range": "12:00-13:15",
                "start_time": "12:00",
                "end_time": "13:15",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "20018",
                "zip_code_suffix": "370",
                "clli_code": "WASHDCBK",
                "task_type": "WO",
                "task_id": "29103120",
                "task_seq": "6",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29103120",
                "task_status": "CLEARED",
                "time_range": "13:45-14:15",
                "start_time": "13:45",
                "end_time": "14:15",
                "service_type": "CDS - Install TeleXtend"
            }]
        }]
    }, {
        "tech_alias": "S-Chuck Oti",
        "tech_model": {
            "tech_alias": "S-Chuck Oti"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "07014",
                "zip_code_suffix": null,
                "clli_code": "PSSCNJPS",
                "task_type": "WO",
                "task_id": "29092035",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29092035",
                "task_status": "CLEARED",
                "time_range": "09:00-10:00",
                "start_time": "09:00",
                "end_time": "10:00",
                "service_type": "Install TeleXtend"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "07110",
                "zip_code_suffix": "341",
                "clli_code": "NTLYNJNU",
                "task_type": "TT",
                "task_id": "4171009",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171009",
                "task_status": "CLEARED",
                "time_range": "10:15-11:15",
                "start_time": "10:15",
                "end_time": "11:15",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "07470",
                "zip_code_suffix": "690",
                "clli_code": "LTFLNJLF",
                "task_type": "WO",
                "task_id": "29165914",
                "task_seq": "4",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29165914",
                "task_status": "CLEARED",
                "time_range": "12:00-13:30",
                "start_time": "12:00",
                "end_time": "13:30",
                "service_type": "CDS - Install Soho"
            }, {
                "zip_code": "07801",
                "zip_code_suffix": "180",
                "clli_code": "DOVRNJDO",
                "task_type": "WO",
                "task_id": "29004258",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29004258",
                "task_status": "EARMARKED",
                "time_range": "12:00-13:00",
                "start_time": "12:00",
                "end_time": "13:00",
                "service_type": "CDS - Install TeleXtend"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "07866",
                "zip_code_suffix": null,
                "clli_code": "DOVRNJDO",
                "task_type": "TT",
                "task_id": "4168688",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4168688",
                "task_status": "EARMARKED",
                "time_range": "13:00-14:00",
                "start_time": "13:00",
                "end_time": "14:00",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Craig MiddleBrook",
        "tech_model": {
            "tech_alias": "S-Craig MiddleBrook"
        },
        "timeslot_collection": [{
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "90061",
                "zip_code_suffix": "281",
                "clli_code": "LSANCA13",
                "task_type": "TT",
                "task_id": "4172408",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172408",
                "task_status": "CLEARED",
                "time_range": "10:30-11:45",
                "start_time": "10:30",
                "end_time": "11:45",
                "service_type": "Repair - Sharing"
            }]
        }, {
            "timeslot": "2 PM",
            "task_collection": [{
                "zip_code": "90602",
                "zip_code_suffix": "139",
                "clli_code": "WHTRCAXF",
                "task_type": "TT",
                "task_id": "4155349",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4155349",
                "task_status": "EARMARKED",
                "time_range": "14:31-16:11",
                "start_time": "14:31",
                "end_time": "16:11",
                "service_type": "Repair TeleXtend"
            }]
        }]
    }, {
        "tech_alias": "S-David Martinez",
        "tech_model": {
            "tech_alias": "S-David Martinez"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "90621",
                "zip_code_suffix": "362",
                "clli_code": "BNPKCA11",
                "task_type": "TT",
                "task_id": "4170208",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4170208",
                "task_status": "CLEARED",
                "time_range": "09:30-10:30",
                "start_time": "09:30",
                "end_time": "10:30",
                "service_type": "Repair Premise"
            }, {
                "zip_code": "92646",
                "zip_code_suffix": "490",
                "clli_code": "HNBHCAXH",
                "task_type": "WO",
                "task_id": "29233337",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29233337",
                "task_status": "EARMARKED",
                "time_range": "09:08-10:23",
                "start_time": "09:08",
                "end_time": "10:23",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "92868",
                "zip_code_suffix": "362",
                "clli_code": "ORNGCA14",
                "task_type": "WO",
                "task_id": "29205895",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29205895",
                "task_status": "EARMARKED",
                "time_range": "12:00-13:45",
                "start_time": "12:00",
                "end_time": "13:45",
                "service_type": "CDS - Install Soho"
            }]
        }, {
            "timeslot": "2 PM",
            "task_collection": [{
                "zip_code": "90806",
                "zip_code_suffix": null,
                "clli_code": "LNBHCAXG",
                "task_type": "TT",
                "task_id": "4172097",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172097",
                "task_status": "EARMARKED",
                "time_range": "14:27-15:42",
                "start_time": "14:27",
                "end_time": "15:42",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Delon DeSouza",
        "tech_model": {
            "tech_alias": "S-Delon DeSouza"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "10022",
                "zip_code_suffix": "558",
                "clli_code": "NYCMNY56",
                "task_type": "TT",
                "task_id": "4159829",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4159829",
                "task_status": "CLEARED",
                "time_range": "09:00-10:00",
                "start_time": "09:00",
                "end_time": "10:00",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "10017",
                "zip_code_suffix": "870",
                "clli_code": "NYCMNY56",
                "task_type": "TT",
                "task_id": "4171771",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171771",
                "task_status": "CLEARED",
                "time_range": "10:00-11:00",
                "start_time": "10:00",
                "end_time": "11:00",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "10004",
                "zip_code_suffix": null,
                "clli_code": null,
                "task_type": "block",
                "task_id": "NA - Vehicle Maintenance",
                "task_status": "TENTATIVE",
                "time_range": "11:00- 11:45",
                "start_time": "11:00",
                "end_time": "11:45",
                "service_type": "Not Avlbl",
                "task_seq": null,
                "task_url": null
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "10005",
                "zip_code_suffix": "163",
                "clli_code": null,
                "task_type": "ON",
                "task_id": "29270937",
                "task_seq": "1",
                "task_url": "http://status-int.oss.covad.com:15558/csp/overview?oid=29270937",
                "task_status": "EARMARKED",
                "time_range": "12:49-14:19",
                "start_time": "12:49",
                "end_time": "14:19",
                "service_type": "Advance Services - Repair"
            }, {
                "zip_code": "10036",
                "zip_code_suffix": "652",
                "clli_code": "NYCMNY42",
                "task_type": "TT",
                "task_id": "4171273",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171273",
                "task_status": "CLEARED",
                "time_range": "12:15-13:15",
                "start_time": "12:15",
                "end_time": "13:15",
                "service_type": "Repair - Sharing"
            }]
        }, {
            "timeslot": "2 PM",
            "task_collection": [{
                "zip_code": "10012",
                "zip_code_suffix": "391",
                "clli_code": "NYCMNYVS",
                "task_type": "TT",
                "task_id": "4168652",
                "task_seq": "3",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4168652",
                "task_status": "CLEARED",
                "time_range": "14:00-14:15",
                "start_time": "14:00",
                "end_time": "14:15",
                "service_type": "Repair TeleXtend"
            }]
        }]
    }, {
        "tech_alias": "S-Derrick Conner",
        "tech_model": {
            "tech_alias": "S-Derrick Conner"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "19102",
                "zip_code_suffix": "300",
                "clli_code": "PHLAPALO",
                "task_type": "WO",
                "task_id": "29204901",
                "task_seq": "4",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29204901",
                "task_status": "EARMARKED",
                "time_range": "08:29-09:59",
                "start_time": "08:29",
                "end_time": "09:59",
                "service_type": "Bonded SDSL"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "19406",
                "zip_code_suffix": "293",
                "clli_code": "KGPRPAKP",
                "task_type": "WO",
                "task_id": "29227241",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29227241",
                "task_status": "EARMARKED",
                "time_range": "12:00-14:00",
                "start_time": "12:00",
                "end_time": "14:00",
                "service_type": "Bonded SDSL"
            }]
        }]
    }, {
        "tech_alias": "S-Earl Jones",
        "tech_model": {
            "tech_alias": "S-Earl Jones"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "20815",
                "zip_code_suffix": "369",
                "clli_code": "CHCHMDBE",
                "task_type": "WO",
                "task_id": "29158462",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29158462",
                "task_status": "CLEARED",
                "time_range": "09:00-10:00",
                "start_time": "09:00",
                "end_time": "10:00",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "20036",
                "zip_code_suffix": "520",
                "clli_code": "WASHDCMT",
                "task_type": "TT",
                "task_id": "4158268",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4158268",
                "task_status": "EARMARKED",
                "time_range": "10:15-11:56",
                "start_time": "10:15",
                "end_time": "11:56",
                "service_type": "Repair Premise"
            }, {
                "zip_code": "20815",
                "zip_code_suffix": "355",
                "clli_code": "CHCHMDBE",
                "task_type": "WO",
                "task_id": "29154252",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29154252",
                "task_status": "CLEARED",
                "time_range": "10:00-12:00",
                "start_time": "10:00",
                "end_time": "12:00",
                "service_type": "Bonded SDSL"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "20036",
                "zip_code_suffix": "383",
                "clli_code": "WASHDCMT",
                "task_type": "TT",
                "task_id": "4163828",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4163828",
                "task_status": "EARMARKED",
                "time_range": "12:00-13:11",
                "start_time": "12:00",
                "end_time": "13:11",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "20016",
                "zip_code_suffix": "280",
                "clli_code": "WASHDCWL",
                "task_type": "TT",
                "task_id": "4171089",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171089",
                "task_status": "CLEARED",
                "time_range": "13:00-14:00",
                "start_time": "13:00",
                "end_time": "14:00",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Joe Paragas",
        "tech_model": {
            "tech_alias": "S-Joe Paragas"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "94111",
                "zip_code_suffix": "312",
                "clli_code": "SNFCCA01",
                "task_type": "WO",
                "task_id": "29240498",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29240498",
                "task_status": "CLEARED",
                "time_range": "09:30-10:00",
                "start_time": "09:30",
                "end_time": "10:00",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "94015",
                "zip_code_suffix": "401",
                "clli_code": "COLACA01",
                "task_type": "TT",
                "task_id": "4165932",
                "task_seq": "2",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4165932",
                "task_status": "EARMARKED",
                "time_range": "11:11-13:11",
                "start_time": "11:11",
                "end_time": "13:11",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-John Cunningham",
        "tech_model": {
            "tech_alias": "S-John Cunningham"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "10016",
                "zip_code_suffix": "660",
                "clli_code": "NYCMNY30",
                "task_type": "TT",
                "task_id": "4167209",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4167209",
                "task_status": "CLEARED",
                "time_range": "09:00-09:30",
                "start_time": "09:00",
                "end_time": "09:30",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "10032",
                "zip_code_suffix": "435",
                "clli_code": "NYCMNYWA",
                "task_type": "TT",
                "task_id": "4166812",
                "task_seq": "2",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4166812",
                "task_status": "CLEARED",
                "time_range": "10:30-11:30",
                "start_time": "10:30",
                "end_time": "11:30",
                "service_type": "Repair TeleXtend"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "10001",
                "zip_code_suffix": "320",
                "clli_code": "NYCMNY36",
                "task_type": "TT",
                "task_id": "4135032",
                "task_seq": "2",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4135032",
                "task_status": "EARMARKED",
                "time_range": "12:23-13:30",
                "start_time": "12:23",
                "end_time": "13:30",
                "service_type": "Repair Premise"
            }, {
                "zip_code": "10118",
                "zip_code_suffix": "640",
                "clli_code": "NYCMNY36",
                "task_type": "WO",
                "task_id": "29157549",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29157549",
                "task_status": "CLEARED",
                "time_range": "12:15-14:15",
                "start_time": "12:15",
                "end_time": "14:15",
                "service_type": "New Install - TeleSoho T3"
            }]
        }]
    }, {
        "tech_alias": "S-Kris Greeley",
        "tech_model": {
            "tech_alias": "S-Kris Greeley"
        },
        "timeslot_collection": [{
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "94501",
                "zip_code_suffix": "572",
                "clli_code": "ALMDCA11",
                "task_type": "WO",
                "task_id": "29195348",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29195348",
                "task_status": "EARMARKED",
                "time_range": "10:56-13:26",
                "start_time": "10:56",
                "end_time": "13:26",
                "service_type": "CDS - Install Soho"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "94607",
                "zip_code_suffix": "270",
                "clli_code": "OKLDCA03",
                "task_type": "TT",
                "task_id": "4171368",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171368",
                "task_status": "EARMARKED",
                "time_range": "13:53-15:53",
                "start_time": "13:53",
                "end_time": "15:53",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "3 PM",
            "task_collection": [{
                "zip_code": "94607",
                "zip_code_suffix": "160",
                "clli_code": "OKLDCA03",
                "task_type": "TT",
                "task_id": "4169571",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4169571",
                "task_status": "EARMARKED",
                "time_range": "15:53-17:53",
                "start_time": "15:53",
                "end_time": "17:53",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Larry Holt",
        "tech_model": {
            "tech_alias": "S-Larry Holt"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "92507",
                "zip_code_suffix": "281",
                "clli_code": "RVSDCA01",
                "task_type": "WO",
                "task_id": "29199247",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29199247",
                "task_status": "CLEARED",
                "time_range": "09:00-10:00",
                "start_time": "09:00",
                "end_time": "10:00",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "92507",
                "zip_code_suffix": "281",
                "clli_code": "29199247",
                "task_type": "ON",
                "task_id": "29268646",
                "task_seq": "1",
                "task_url": "http://status-int.oss.covad.com:15558/csp/overview?oid=29268646",
                "task_status": "CLEARED",
                "time_range": "10:01-11:30",
                "start_time": "10:01",
                "end_time": "11:30",
                "service_type": "Advanced Services - Install 3"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "92376",
                "zip_code_suffix": "468",
                "clli_code": "RILTCA11",
                "task_type": "TT",
                "task_id": "4155909",
                "task_seq": "3",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4155909",
                "task_status": "EARMARKED",
                "time_range": "12:21-13:21",
                "start_time": "12:21",
                "end_time": "13:21",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "92408",
                "zip_code_suffix": null,
                "clli_code": "SNBRCAXK",
                "task_type": "WO",
                "task_id": "29188688",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29188688",
                "task_status": "EARMARKED",
                "time_range": "13:42-14:42",
                "start_time": "13:42",
                "end_time": "14:42",
                "service_type": "CDS - Install Soho"
            }]
        }]
    }, {
        "tech_alias": "S-Loc Tran",
        "tech_model": {
            "tech_alias": "S-Loc Tran"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "94556",
                "zip_code_suffix": "253",
                "clli_code": "MORGCA12",
                "task_type": "TT",
                "task_id": "4168211",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4168211",
                "task_status": "EARMARKED",
                "time_range": "09:18-10:25",
                "start_time": "09:18",
                "end_time": "10:25",
                "service_type": "Repair - Sharing"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "94580",
                "zip_code_suffix": "303",
                "clli_code": "SNLNCA11",
                "task_type": "TT",
                "task_id": "4172089",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172089",
                "task_status": "EARMARKED",
                "time_range": "12:14-13:51",
                "start_time": "12:14",
                "end_time": "13:51",
                "service_type": "Repair TeleXtend"
            }]
        }]
    }, {
        "tech_alias": "S-London Drummond",
        "tech_model": {
            "tech_alias": "S-London Drummond"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "22209",
                "zip_code_suffix": "246",
                "clli_code": "ARTNVAAR",
                "task_type": "TT",
                "task_id": "4170052",
                "task_seq": "2",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4170052",
                "task_status": "EARMARKED",
                "time_range": "08:50-10:01",
                "start_time": "08:50",
                "end_time": "10:01",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "22314",
                "zip_code_suffix": "193",
                "clli_code": "ALXNVAAX",
                "task_type": "WO",
                "task_id": "29113683",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29113683",
                "task_status": "CLEARED",
                "time_range": "09:00-10:30",
                "start_time": "09:00",
                "end_time": "10:30",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "22203",
                "zip_code_suffix": "182",
                "clli_code": "ARTNVAAR",
                "task_type": "TT",
                "task_id": "4158289",
                "task_seq": "3",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4158289",
                "task_status": "CLEARED",
                "time_range": "11:00-13:45",
                "start_time": "11:00",
                "end_time": "13:45",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "22209",
                "zip_code_suffix": "246",
                "clli_code": "ARTNVAAR",
                "task_type": "TT",
                "task_id": "4160948",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4160948",
                "task_status": "EARMARKED",
                "time_range": "13:57-15:38",
                "start_time": "13:57",
                "end_time": "15:38",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "2 PM",
            "task_collection": [{
                "zip_code": "22201",
                "zip_code_suffix": "268",
                "clli_code": null,
                "task_type": "ON",
                "task_id": "29251885",
                "task_seq": "1",
                "task_url": "http://status-int.oss.covad.com:15558/csp/overview?oid=29251885",
                "task_status": "CLEARED",
                "time_range": "14:30-15:00",
                "start_time": "14:30",
                "end_time": "15:00",
                "service_type": "VOIP Install Covad"
            }]
        }, {
            "timeslot": "3 PM",
            "task_collection": [{
                "zip_code": "22202",
                "zip_code_suffix": null,
                "clli_code": "ARTNVACY",
                "task_type": "TT",
                "task_id": "4166008",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4166008",
                "task_status": "EARMARKED",
                "time_range": "15:21-16:32",
                "start_time": "15:21",
                "end_time": "16:32",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "4 PM",
            "task_collection": [{
                "zip_code": "22202",
                "zip_code_suffix": null,
                "clli_code": "ARTNVACY",
                "task_type": "TT",
                "task_id": "4165991",
                "task_seq": "2",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4165991",
                "task_status": "EARMARKED",
                "time_range": "16:32-17:43",
                "start_time": "16:32",
                "end_time": "17:43",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Manuel Chacon",
        "tech_model": {
            "tech_alias": "S-Manuel Chacon"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "94103",
                "zip_code_suffix": "200",
                "clli_code": "SNFCCA21",
                "task_type": "WO",
                "task_id": "29236272",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29236272",
                "task_status": "EARMARKED",
                "time_range": "08:22-09:42",
                "start_time": "08:22",
                "end_time": "09:42",
                "service_type": "Consumer Install"
            }]
        }, {
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "94105",
                "zip_code_suffix": null,
                "clli_code": "SNFCCA21",
                "task_type": "WO",
                "task_id": "29239644",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29239644",
                "task_status": "EARMARKED",
                "time_range": "09:42-11:32",
                "start_time": "09:42",
                "end_time": "11:32",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "94133",
                "zip_code_suffix": "155",
                "clli_code": "SNFCCA01",
                "task_type": "WO",
                "task_id": "29239101",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29239101",
                "task_status": "EARMARKED",
                "time_range": "11:46-13:06",
                "start_time": "11:46",
                "end_time": "13:06",
                "service_type": "CDS - Install Soho"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "94111",
                "zip_code_suffix": "142",
                "clli_code": "SNFCCA01",
                "task_type": "TT",
                "task_id": "4169790",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4169790",
                "task_status": "EARMARKED",
                "time_range": "13:06-14:26",
                "start_time": "13:06",
                "end_time": "14:26",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Mario Encarnacion",
        "tech_model": {
            "tech_alias": "S-Mario Encarnacion"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "91740",
                "zip_code_suffix": "386",
                "clli_code": "GLNDCAXF",
                "task_type": "WO",
                "task_id": "29180681",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29180681",
                "task_status": "CLEARED",
                "time_range": "07:30-08:45",
                "start_time": "07:30",
                "end_time": "08:45",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "91740",
                "zip_code_suffix": "416",
                "clli_code": "GLNDCAXF",
                "task_type": "WO",
                "task_id": "29231237",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29231237",
                "task_status": "CLEARED",
                "time_range": "09:00-10:00",
                "start_time": "09:00",
                "end_time": "10:00",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "91790",
                "zip_code_suffix": "285",
                "clli_code": "BLPKCAXF",
                "task_type": "WO",
                "task_id": "29231280",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29231280",
                "task_status": "CLEARED",
                "time_range": "10:15-11:15",
                "start_time": "10:15",
                "end_time": "11:15",
                "service_type": "CDS - Install Soho"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "92335",
                "zip_code_suffix": "623",
                "clli_code": "FNTACA11",
                "task_type": "TT",
                "task_id": "4171888",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171888",
                "task_status": "EARMARKED",
                "time_range": "12:04-13:04",
                "start_time": "12:04",
                "end_time": "13:04",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "91764",
                "zip_code_suffix": "520",
                "clli_code": "CCMNCAXF",
                "task_type": "TT",
                "task_id": "4170252",
                "task_seq": "2",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4170252",
                "task_status": "EARMARKED",
                "time_range": "13:28-14:28",
                "start_time": "13:28",
                "end_time": "14:28",
                "service_type": "Repair TeleXtend"
            }]
        }]
    }, {
        "tech_alias": "S-Mel Lindsay",
        "tech_model": {
            "tech_alias": "S-Mel Lindsay"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "20015",
                "zip_code_suffix": "201",
                "clli_code": "WASHDCWL",
                "task_type": "WO",
                "task_id": "29113646",
                "task_seq": "4",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29113646",
                "task_status": "EARMARKED",
                "time_range": "08:23-10:53",
                "start_time": "08:23",
                "end_time": "10:53",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "20016",
                "zip_code_suffix": "411",
                "clli_code": "WASHDCWL",
                "task_type": "WO",
                "task_id": "29158470",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29158470",
                "task_status": "EARMARKED",
                "time_range": "10:53-13:53",
                "start_time": "10:53",
                "end_time": "13:53",
                "service_type": "New Install - TeleSoho T3"
            }]
        }]
    }, {
        "tech_alias": "S-Paul Niedzwiecki",
        "tech_model": {
            "tech_alias": "S-Paul Niedzwiecki"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "91320",
                "zip_code_suffix": "120",
                "clli_code": "NWPKCAXF",
                "task_type": "TT",
                "task_id": "4169708",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4169708",
                "task_status": "CLEARED",
                "time_range": "09:30-10:00",
                "start_time": "09:30",
                "end_time": "10:00",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "91362",
                "zip_code_suffix": "383",
                "clli_code": "THOKCAXF",
                "task_type": "TT",
                "task_id": "4169909",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4169909",
                "task_status": "CLEARED",
                "time_range": "10:15-11:30",
                "start_time": "10:15",
                "end_time": "11:30",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "91367",
                "zip_code_suffix": "220",
                "clli_code": "CNPKCA01",
                "task_type": "WO",
                "task_id": "29238254",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29238254",
                "task_status": "EARMARKED",
                "time_range": "11:28-13:28",
                "start_time": "11:28",
                "end_time": "13:28",
                "service_type": "EU Copper - 1"
            }]
        }]
    }, {
        "tech_alias": "S-Philip Flores",
        "tech_model": {
            "tech_alias": "S-Philip Flores"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "94102",
                "zip_code_suffix": null,
                "clli_code": "SNFCCA04",
                "task_type": "WO",
                "task_id": "29216684",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29216684",
                "task_status": "EARMARKED",
                "time_range": "09:04-12:04",
                "start_time": "09:04",
                "end_time": "12:04",
                "service_type": "Install TeleXtend"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "94110",
                "zip_code_suffix": "100",
                "clli_code": "SNFCCA04",
                "task_type": "TT",
                "task_id": "4165953",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4165953",
                "task_status": "EARMARKED",
                "time_range": "12:04-15:04",
                "start_time": "12:04",
                "end_time": "15:04",
                "service_type": "Repair LPVA Voice"
            }]
        }, {
            "timeslot": "5 PM",
            "task_collection": [{
                "zip_code": "95405",
                "zip_code_suffix": "479",
                "clli_code": "SNRSCA01",
                "task_type": "TT",
                "task_id": "4173249",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4173249",
                "task_status": "EARMARKED",
                "time_range": "17:56-19:56",
                "start_time": "17:56",
                "end_time": "19:56",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Raymond Tong",
        "tech_model": {
            "tech_alias": "S-Raymond Tong"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "90068",
                "zip_code_suffix": null,
                "clli_code": "HLWDCA01",
                "task_type": "TT",
                "task_id": "4169990",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4169990",
                "task_status": "EARMARKED",
                "time_range": "08:30-10:10",
                "start_time": "08:30",
                "end_time": "10:10",
                "service_type": "Repair LPVA Voice"
            }]
        }, {
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "90212",
                "zip_code_suffix": "313",
                "clli_code": "BVHLCA01",
                "task_type": "TT",
                "task_id": "4171770",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171770",
                "task_status": "CLEARED",
                "time_range": "09:30-11:00",
                "start_time": "09:30",
                "end_time": "11:00",
                "service_type": "Repair TeleXtend"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "90057",
                "zip_code_suffix": "290",
                "clli_code": "LSANCA06",
                "task_type": "WO",
                "task_id": "29184135",
                "task_seq": "4",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29184135",
                "task_status": "EARMARKED",
                "time_range": "12:45-14:55",
                "start_time": "12:45",
                "end_time": "14:55",
                "service_type": "Vendor Meet - Install/MPOE"
            }]
        }, {
            "timeslot": "3 PM",
            "task_collection": [{
                "zip_code": "90018",
                "zip_code_suffix": "112",
                "clli_code": "LSANCA38",
                "task_type": "TT",
                "task_id": "4171091",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171091",
                "task_status": "EARMARKED",
                "time_range": "15:21-16:28",
                "start_time": "15:21",
                "end_time": "16:28",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Rey Radlin",
        "tech_model": {
            "tech_alias": "S-Rey Radlin"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "10310",
                "zip_code_suffix": "194",
                "clli_code": "NYCRNYNS",
                "task_type": "TT",
                "task_id": "4162208",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4162208",
                "task_status": "CLEARED",
                "time_range": "09:30-10:30",
                "start_time": "09:30",
                "end_time": "10:30",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "11224",
                "zip_code_suffix": "241",
                "clli_code": "NYCKNYAU",
                "task_type": "TT",
                "task_id": "4171012",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171012",
                "task_status": "CLEARED",
                "time_range": "11:30-12:30",
                "start_time": "11:30",
                "end_time": "12:30",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Robert Markley",
        "tech_model": {
            "tech_alias": "S-Robert Markley"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "19805",
                "zip_code_suffix": "740",
                "clli_code": "WLMGDEWL",
                "task_type": "WO",
                "task_id": "29230793",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29230793",
                "task_status": "CLEARED",
                "time_range": "08:30-09:30",
                "start_time": "08:30",
                "end_time": "09:30",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "08081",
                "zip_code_suffix": "956",
                "clli_code": "WLTWNJ02",
                "task_type": "WO",
                "task_id": "29063846",
                "task_seq": "4",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29063846",
                "task_status": "CLEARED",
                "time_range": "10:30-11:30",
                "start_time": "10:30",
                "end_time": "11:30",
                "service_type": "Install TeleXtend"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "08638",
                "zip_code_suffix": "480",
                "clli_code": "ENVLNJEW",
                "task_type": "TT",
                "task_id": "4158533",
                "task_seq": "4",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4158533",
                "task_status": "CLEARED",
                "time_range": "12:45-14:15",
                "start_time": "12:45",
                "end_time": "14:15",
                "service_type": "Vendor Meet - Repair/MPOE"
            }]
        }]
    }, {
        "tech_alias": "S-Rodney Bowman",
        "tech_model": {
            "tech_alias": "S-Rodney Bowman"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "21136",
                "zip_code_suffix": "123",
                "clli_code": "RSTWMDRS",
                "task_type": "TT",
                "task_id": "4171470",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171470",
                "task_status": "CLEARED",
                "time_range": "09:15-10:00",
                "start_time": "09:15",
                "end_time": "10:00",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "20770",
                "zip_code_suffix": "354",
                "clli_code": "CLPKMDBW",
                "task_type": "WO",
                "task_id": "29205930",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29205930",
                "task_status": "CLEARED",
                "time_range": "11:15-13:00",
                "start_time": "11:15",
                "end_time": "13:00",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "21090",
                "zip_code_suffix": "102",
                "clli_code": "FPATMDFR",
                "task_type": "WO",
                "task_id": "29219594",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29219594",
                "task_status": "EARMARKED",
                "time_range": "12:00-13:11",
                "start_time": "12:00",
                "end_time": "13:11",
                "service_type": "New Install - TeleSoho T3"
            }]
        }]
    }, {
        "tech_alias": "S-Ronald Gabin",
        "tech_model": {
            "tech_alias": "S-Ronald Gabin"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "94022",
                "zip_code_suffix": "370",
                "clli_code": "LSATCA11",
                "task_type": "TT",
                "task_id": "4171668",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171668",
                "task_status": "CLEARED",
                "time_range": "09:30-10:45",
                "start_time": "09:30",
                "end_time": "10:45",
                "service_type": "Repair Premise"
            }, {
                "zip_code": "94022",
                "zip_code_suffix": "370",
                "clli_code": "LSATCA11",
                "task_type": "WO",
                "task_id": "29223471",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29223471",
                "task_status": "EARMARKED",
                "time_range": "09:30-10:53",
                "start_time": "09:30",
                "end_time": "10:53",
                "service_type": "Install TeleXtend"
            }]
        }, {
            "timeslot": "11 AM",
            "task_collection": [{
                "zip_code": "95113",
                "zip_code_suffix": "232",
                "clli_code": "SNJSCA02",
                "task_type": "TT",
                "task_id": "4170688",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4170688",
                "task_status": "EARMARKED",
                "time_range": "11:25-13:02",
                "start_time": "11:25",
                "end_time": "13:02",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "95126",
                "zip_code_suffix": "182",
                "clli_code": "SNJSCA02",
                "task_type": "TT",
                "task_id": "4172450",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172450",
                "task_status": "EARMARKED",
                "time_range": "13:02-14:09",
                "start_time": "13:02",
                "end_time": "14:09",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "2 PM",
            "task_collection": [{
                "zip_code": "94538",
                "zip_code_suffix": "330",
                "clli_code": "FRMTCA12",
                "task_type": "TT",
                "task_id": "4172093",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172093",
                "task_status": "EARMARKED",
                "time_range": "14:39-15:46",
                "start_time": "14:39",
                "end_time": "15:46",
                "service_type": "Repair - Sharing"
            }]
        }]
    }, {
        "tech_alias": "S-Savva Savvas",
        "tech_model": {
            "tech_alias": "S-Savva Savvas"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "11731",
                "zip_code_suffix": "620",
                "clli_code": "CMMKNYCM",
                "task_type": "TT",
                "task_id": "4163028",
                "task_seq": "3",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4163028",
                "task_status": "CLEARED",
                "time_range": "09:00-10:30",
                "start_time": "09:00",
                "end_time": "10:30",
                "service_type": "Vendor Meet - Repair/MPOE"
            }]
        }]
    }, {
        "tech_alias": "S-Scott Edens",
        "tech_model": {
            "tech_alias": "S-Scott Edens"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "91101",
                "zip_code_suffix": "214",
                "clli_code": "PSDNCA11",
                "task_type": "WO",
                "task_id": "29229502",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29229502",
                "task_status": "EARMARKED",
                "time_range": "08:46-08:50",
                "start_time": "08:46",
                "end_time": "08:50",
                "service_type": "EU Copper - 2"
            }]
        }, {
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "91010",
                "zip_code_suffix": null,
                "clli_code": "MNRVCAXG",
                "task_type": "TT",
                "task_id": "4169989",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4169989",
                "task_status": "EARMARKED",
                "time_range": "09:36-11:32",
                "start_time": "09:36",
                "end_time": "11:32",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "92399",
                "zip_code_suffix": null,
                "clli_code": null,
                "task_type": "block",
                "task_id": "NA - GTO",
                "task_status": "TENTATIVE",
                "time_range": "12:12- 16:15",
                "start_time": "12:12",
                "end_time": "16:15",
                "service_type": "Not Avlbl",
                "task_seq": null,
                "task_url": null
            }]
        }]
    }, {
        "tech_alias": "S-Sean Maloney",
        "tech_model": {
            "tech_alias": "S-Sean Maloney"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "91352",
                "zip_code_suffix": "572",
                "clli_code": "PACMCAXF",
                "task_type": "WO",
                "task_id": "29216257",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29216257",
                "task_status": "CLEARED",
                "time_range": "09:00-10:00",
                "start_time": "09:00",
                "end_time": "10:00",
                "service_type": "Install TeleXtend"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "91342",
                "zip_code_suffix": "310",
                "clli_code": "SYLMCAXF",
                "task_type": "TT",
                "task_id": "4172395",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172395",
                "task_status": "CLEARED",
                "time_range": "10:15-10:45",
                "start_time": "10:15",
                "end_time": "10:45",
                "service_type": "Repair Premise"
            }, {
                "zip_code": "91352",
                "zip_code_suffix": "154",
                "clli_code": "NHWDCA01",
                "task_type": "TT",
                "task_id": "4162088",
                "task_seq": "2",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4162088",
                "task_status": "EARMARKED",
                "time_range": "10:26-11:26",
                "start_time": "10:26",
                "end_time": "11:26",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "93551",
                "zip_code_suffix": null,
                "clli_code": "PLDLCA01",
                "task_type": "TT",
                "task_id": "4171272",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171272",
                "task_status": "EARMARKED",
                "time_range": "12:37-13:37",
                "start_time": "12:37",
                "end_time": "13:37",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Sunil Maharjan",
        "tech_model": {
            "tech_alias": "S-Sunil Maharjan"
        },
        "timeslot_collection": [{
            "timeslot": "8 AM",
            "task_collection": [{
                "zip_code": "10128",
                "zip_code_suffix": "320",
                "clli_code": "29218850",
                "task_type": "ON",
                "task_id": "29267674",
                "task_seq": "1",
                "task_url": "http://status-int.oss.covad.com:15558/csp/overview?oid=29267674",
                "task_status": "EARMARKED",
                "time_range": "08:28-10:28",
                "start_time": "08:28",
                "end_time": "10:28",
                "service_type": "Advanced Services - Install 3"
            }]
        }, {
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "10019",
                "zip_code_suffix": "744",
                "clli_code": "NYCMNY50",
                "task_type": "TT",
                "task_id": "4162268",
                "task_seq": "2",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4162268",
                "task_status": "CLEARED",
                "time_range": "09:00-11:15",
                "start_time": "09:00",
                "end_time": "11:15",
                "service_type": "Repair - EuCopper"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "10001",
                "zip_code_suffix": "300",
                "clli_code": null,
                "task_type": "ON",
                "task_id": "29267990",
                "task_seq": "1",
                "task_url": "http://status-int.oss.covad.com:15558/csp/overview?oid=29267990",
                "task_status": "EARMARKED",
                "time_range": "12:17-14:47",
                "start_time": "12:17",
                "end_time": "14:47",
                "service_type": "Advanced Services - Install 3"
            }]
        }]
    }, {
        "tech_alias": "S-Will Jackson",
        "tech_model": {
            "tech_alias": "S-Will Jackson"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "94925",
                "zip_code_suffix": "120",
                "clli_code": "LRKSCA11",
                "task_type": "WO",
                "task_id": "29240521",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29240521",
                "task_status": "CLEARED",
                "time_range": "09:00-11:00",
                "start_time": "09:00",
                "end_time": "11:00",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "94965",
                "zip_code_suffix": "143",
                "clli_code": "SSLTCA11",
                "task_type": "WO",
                "task_id": "29238203",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29238203",
                "task_status": "EARMARKED",
                "time_range": "10:24-13:41",
                "start_time": "10:24",
                "end_time": "13:41",
                "service_type": "Install-BT1/4.5"
            }]
        }, {
            "timeslot": "2 PM",
            "task_collection": [{
                "zip_code": "94589",
                "zip_code_suffix": "264",
                "clli_code": "VLLJCA01",
                "task_type": "TT",
                "task_id": "4168569",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4168569",
                "task_status": "EARMARKED",
                "time_range": "14:37-15:44",
                "start_time": "14:37",
                "end_time": "15:44",
                "service_type": "Repair - Sharing"
            }]
        }]
    }]
}