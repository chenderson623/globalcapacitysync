
var schedule_model_data = {
    "schedule_date": "2011-10-05T07:00:00.000Z",
    "timeslots_available": ["8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM", "# of Installs*"],
    "limit_params": null,
    "techs_available": null,
    "tech_schedule_collection": [  {
        "tech_alias": "S-Chuck Oti",
        "tech_model": {
            "tech_alias": "S-Chuck Oti"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "07014",
                "zip_code_suffix": null,
                "clli_code": "PSSCNJPS",
                "task_type": "WO",
                "task_id": "29092035",
                "task_seq": "2",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29092035",
                "task_status": "CLEARED",
                "time_range": "09:00-10:00",
                "start_time": "09:00",
                "end_time": "10:00",
                "service_type": "Install TeleXtend"
            }]
        }, {
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "07110",
                "zip_code_suffix": "341",
                "clli_code": "NTLYNJNU",
                "task_type": "TT",
                "task_id": "4171009",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171009",
                "task_status": "CLEARED",
                "time_range": "10:15-11:15",
                "start_time": "10:15",
                "end_time": "11:15",
                "service_type": "Repair Premise"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "07470",
                "zip_code_suffix": "690",
                "clli_code": "LTFLNJLF",
                "task_type": "WO",
                "task_id": "29165914",
                "task_seq": "4",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29165914",
                "task_status": "CLEARED",
                "time_range": "12:00-13:30",
                "start_time": "12:00",
                "end_time": "13:30",
                "service_type": "CDS - Install Soho"
            }, {
                "zip_code": "07801",
                "zip_code_suffix": "180",
                "clli_code": "DOVRNJDO",
                "task_type": "WO",
                "task_id": "29004258",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29004258",
                "task_status": "EARMARKED",
                "time_range": "12:00-13:00",
                "start_time": "12:00",
                "end_time": "13:00",
                "service_type": "CDS - Install TeleXtend"
            }]
        }, {
            "timeslot": "1 PM",
            "task_collection": [{
                "zip_code": "07866",
                "zip_code_suffix": null,
                "clli_code": "DOVRNJDO",
                "task_type": "TT",
                "task_id": "4168688",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4168688",
                "task_status": "EARMARKED",
                "time_range": "13:00-14:00",
                "start_time": "13:00",
                "end_time": "14:00",
                "service_type": "Repair Premise"
            }]
        }]
    }, {
        "tech_alias": "S-Craig MiddleBrook",
        "tech_model": {
            "tech_alias": "S-Craig MiddleBrook"
        },
        "timeslot_collection": [{
            "timeslot": "10 AM",
            "task_collection": [{
                "zip_code": "90061",
                "zip_code_suffix": "281",
                "clli_code": "LSANCA13",
                "task_type": "TT",
                "task_id": "4172408",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172408",
                "task_status": "CLEARED",
                "time_range": "10:30-11:45",
                "start_time": "10:30",
                "end_time": "11:45",
                "service_type": "Repair - Sharing"
            }]
        }, {
            "timeslot": "2 PM",
            "task_collection": [{
                "zip_code": "90602",
                "zip_code_suffix": "139",
                "clli_code": "WHTRCAXF",
                "task_type": "TT",
                "task_id": "4155349",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4155349",
                "task_status": "EARMARKED",
                "time_range": "14:31-16:11",
                "start_time": "14:31",
                "end_time": "16:11",
                "service_type": "Repair TeleXtend"
            }]
        }]
    }, {
        "tech_alias": "S-David Martinez",
        "tech_model": {
            "tech_alias": "S-David Martinez"
        },
        "timeslot_collection": [{
            "timeslot": "9 AM",
            "task_collection": [{
                "zip_code": "90621",
                "zip_code_suffix": "362",
                "clli_code": "BNPKCA11",
                "task_type": "TT",
                "task_id": "4170208",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4170208",
                "task_status": "CLEARED",
                "time_range": "09:30-10:30",
                "start_time": "09:30",
                "end_time": "10:30",
                "service_type": "Repair Premise"
            }, {
                "zip_code": "92646",
                "zip_code_suffix": "490",
                "clli_code": "HNBHCAXH",
                "task_type": "WO",
                "task_id": "29233337",
                "task_seq": "1",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29233337",
                "task_status": "EARMARKED",
                "time_range": "09:08-10:23",
                "start_time": "09:08",
                "end_time": "10:23",
                "service_type": "New Install - TeleSoho T3"
            }]
        }, {
            "timeslot": "12 PM",
            "task_collection": [{
                "zip_code": "92868",
                "zip_code_suffix": "362",
                "clli_code": "ORNGCA14",
                "task_type": "WO",
                "task_id": "29205895",
                "task_seq": "3",
                "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29205895",
                "task_status": "EARMARKED",
                "time_range": "12:00-13:45",
                "start_time": "12:00",
                "end_time": "13:45",
                "service_type": "CDS - Install Soho"
            }]
        }, {
            "timeslot": "2 PM",
            "task_collection": [{
                "zip_code": "90806",
                "zip_code_suffix": null,
                "clli_code": "LNBHCAXG",
                "task_type": "TT",
                "task_id": "4172097",
                "task_seq": "1",
                "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172097",
                "task_status": "EARMARKED",
                "time_range": "14:27-15:42",
                "start_time": "14:27",
                "end_time": "15:42",
                "service_type": "Repair Premise"
            }]
        }]
    }]
};