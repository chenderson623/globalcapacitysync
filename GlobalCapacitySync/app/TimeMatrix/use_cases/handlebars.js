taks = {
    "zip_code": "07801",
    "zip_code_suffix": "180",
    "clli_code": "DOVRNJDO",
    "task_type": "WO",
    "task_id": "29004258",
    "task_seq": "3",
    "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29004258",
    "task_status": "EARMARKED",
    "time_range": "12:00-13:00",
    "start_time": "12:00",
    "end_time": "13:00",
    "service_type": "CDS - Install TeleXtend"
};


/*jshint multistr: true */
var task_template = ' \
    <div class="task"> \
        <div class="zip-code">{{zip_code}}{{#if zip_code_suffix}}-{{zip_code_suffix}}{{/if}}</div> \
        <div class="clli-code">{{clli_code}}</div> \
        <div class="task-link"><a target="_new" href="{{task_url}}">{{task_type}}{{task_id}}-{{task_seq}}</a></div> \
        <div class="task_status">{{task_status}}</div> \
        <div class="scheduled-time">({{time_range}})</div> \
        <div class="service-type">{{service_type}}</div> \
    </div>';