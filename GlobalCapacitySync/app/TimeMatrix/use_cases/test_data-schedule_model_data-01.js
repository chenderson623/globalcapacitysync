var schedule_model_data = {
    "schedule_date": "2011-10-05T07:00:00.000Z",
    "timeslots_available": ["8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM", "# of Installs*"],
    "limit_params": null,
    "techs_available": [],
    "tech_schedule_collection": [{
            "tech_alias": "S-Chuck Oti",
            "tech_model": {
                "tech_alias": "S-Chuck Oti"
            },
            "timeslot_collection": [{
                    "timeslot": "9 AM",
                    "task_collection": []
                }, {
                    "timeslot": "10 AM",
                    "task_collection": [{
                            "zip_code": "07110",
                            "zip_code_suffix": "341",
                            "clli_code": "NTLYNJNU",
                            "task_type": "TT",
                            "task_id": "4171009",
                            "task_seq": "1",
                            "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4171009",
                            "task_status": "CLEARED",
                            "time_range": "10:15-11:15",
                            "start_time": "10:15",
                            "end_time": "11:15",
                            "service_type": "Repair Premise"
                        }]
                }, {
                    "timeslot": "12 PM",
                    "task_collection": [{
                            "zip_code": "07801",
                            "zip_code_suffix": "180",
                            "clli_code": "DOVRNJDO",
                            "task_type": "WO",
                            "task_id": "29004258",
                            "task_seq": "3",
                            "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29004258",
                            "task_status": "EARMARKED",
                            "time_range": "12:00-13:00",
                            "start_time": "12:00",
                            "end_time": "13:00",
                            "service_type": "CDS - Install TeleXtend"
                        }]
                }, {
                    "timeslot": "1 PM",
                    "task_collection": []
                }]
        }, {
            "tech_alias": "S-Craig MiddleBrook",
            "tech_model": {
                "tech_alias": "S-Craig MiddleBrook"
            },
            "timeslot_collection": [{
                    "timeslot": "10 AM",
                    "task_collection": [{
                            "zip_code": "07801",
                            "zip_code_suffix": "180",
                            "clli_code": "DOVRNJDO",
                            "task_type": "WO",
                            "task_id": "29004258",
                            "task_seq": "4",
                            "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29004258",
                            "task_status": "EARMARKED",
                            "time_range": "12:00-13:00",
                            "start_time": "12:00",
                            "end_time": "13:00",
                            "service_type": "CDS - Install TeleXtend"
                        },
                        {
                            "zip_code": "90061",
                            "zip_code_suffix": "281",
                            "clli_code": "LSANCA13",
                            "task_type": "TT",
                            "task_id": "4172408",
                            "task_seq": "1",
                            "task_url": "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id=4172408",
                            "task_status": "CLEARED",
                            "time_range": "10:30-11:45",
                            "start_time": "10:30",
                            "end_time": "11:45",
                            "service_type": "Repair - Sharing"
                        }
                    ]
                }, {
                    "timeslot": "2 PM",
                    "task_collection": [{
                            "zip_code": "92868",
                            "zip_code_suffix": "362",
                            "clli_code": "ORNGCA14",
                            "task_type": "WO",
                            "task_id": "29205895",
                            "task_seq": "3",
                            "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29205895",
                            "task_status": "EARMARKED",
                            "time_range": "12:00-13:45",
                            "start_time": "12:00",
                            "end_time": "13:45",
                            "service_type": "CDS - Install Soho"
                        }]
                }]
        }, {
            "tech_alias": "S-David Martinez",
            "tech_model": {
                "tech_alias": "S-David Martinez"
            },
            "timeslot_collection": [{
                    "timeslot": "9 AM",
                    "task_collection": []
                }, {
                    "timeslot": "12 PM",
                    "task_collection": [{
                            "zip_code": "92868",
                            "zip_code_suffix": "362",
                            "clli_code": "ORNGCA14",
                            "task_type": "WO",
                            "task_id": "29205895",
                            "task_seq": "4",
                            "task_url": "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId=29205895",
                            "task_status": "EARMARKED",
                            "time_range": "12:00-13:45",
                            "start_time": "12:00",
                            "end_time": "13:45",
                            "service_type": "CDS - Install Soho"
                        }]
                }, {
                    "timeslot": "2 PM",
                    "task_collection": []
                }]
        }]
};
