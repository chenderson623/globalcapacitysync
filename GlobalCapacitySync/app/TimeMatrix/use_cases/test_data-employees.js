var employee_data =[{
    "name": "Alex Shperlin",
    "region": "Chicago, IL",
    "manager": "Paul Smith",
    "covad_alias": "S-Alex Shperlin"
}, {
    "name": "Anthony Oriola",
    "region": "Washington, DC",
    "manager": "Paul Smith",
    "covad_alias": "S-Anthony Oriola"
}, {
    "name": "Anthony Smith",
    "region": "Washington, DC",
    "manager": "Paul Smith",
    "covad_alias": "S-Anthony Smith"
}, {
    "name": "Ben Osifeso",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Ben Osifeso"
}, {
    "name": "Bill McGill",
    "region": null,
    "manager": null,
    "covad_alias": null
}, {
    "name": "Brian Nelson",
    "region": "Chicago, IL",
    "manager": "Paul Smith",
    "covad_alias": "S-Brian Nelson"
}, {
    "name": "Cesar Ariza",
    "region": "Orlando, FL",
    "manager": "Paul Smith",
    "covad_alias": "S-Cesar Ariza"
}, {
    "name": "Charles Hughes",
    "region": "Philadelphia, PA",
    "manager": "Paul Smith",
    "covad_alias": "S-Charles Hughes"
}, {
    "name": "Chris Barnes",
    "region": "Washington, DC",
    "manager": "Paul Smith",
    "covad_alias": "S-Chris Barnes"
}, {
    "name": "Chuck Oti",
    "region": "New York, NY",
    "manager": "Paul Smith",
    "covad_alias": "S-Chuck Oti"
}, {
    "name": "Craig MiddleBrook",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Craig MiddleBrook"
}, {
    "name": "Dave Cleland",
    "region": "Miami, FL",
    "manager": "Paul Smith",
    "covad_alias": "S-Dave Cleland"
}, {
    "name": "David Holt",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-David Holt"
}, {
    "name": "David Martinez",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-David Martinez1"
}, {
    "name": "Delon DeSouza",
    "region": "New York, NY",
    "manager": "Paul Smith",
    "covad_alias": "S-Delon DeSouza"
}, {
    "name": "DeShawn Nolan",
    "region": "Chicago, IL",
    "manager": "Paul Smith",
    "covad_alias": "S-DeShawn Nolan"
}, {
    "name": "Earl Jones ",
    "region": "Washington, DC",
    "manager": "Paul Smith",
    "covad_alias": "S-Earl Jones"
}, {
    "name": "Ernest Trull",
    "region": "Chicago, IL",
    "manager": "Paul Smith",
    "covad_alias": "S-Ernest Trull"
}, {
    "name": "Fred Behn",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Fred Behn"
}, {
    "name": "Jamel Young",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Jamel Young"
}, {
    "name": "James Wisniewski",
    "region": "Washington, DC",
    "manager": "Paul Smith",
    "covad_alias": "S-James Wisniewski"
}, {
    "name": "Joe Paragas",
    "region": "San Francisco, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Joe Paragas"
}, {
    "name": "John Cunningham",
    "region": "New York, NY",
    "manager": "Paul Smith",
    "covad_alias": "S-John Cunningham"
}, {
    "name": "John Wolfington",
    "region": "San Francisco, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-John Wolfington"
}, {
    "name": "Jorge Lopez",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Jorge Lopez"
}, {
    "name": "Jose Canales",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Jose Canales"
}, {
    "name": "Jose Carrillo",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Jose Carillo"
}, {
    "name": "Juan Martinez",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Juan Martinez"
}, {
    "name": "Kiwuan Miles",
    "region": "Chicago, IL",
    "manager": "Paul Smith",
    "covad_alias": "S-Kiwuan Miles"
}, {
    "name": "Larry Holt",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Larry Holt"
}, {
    "name": "Leo Silva",
    "region": "San Diego, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Leo Silva"
}, {
    "name": "Loc Tran",
    "region": "San Francisco, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Loc Tran"
}, {
    "name": "Manuel Chacon",
    "region": "San Francisco, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Manuel Chacon"
}, {
    "name": "Mario Encarnacion",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Mario Encarnacion"
}, {
    "name": "Paul Mifflin",
    "region": "Chicago, IL",
    "manager": "Paul Smith",
    "covad_alias": "S-Paul Mifflin"
}, {
    "name": "Paul Niedzwiecki",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Paul Niedzwiecki"
}, {
    "name": "Raymond Tong",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Raymond Tong"
}, {
    "name": "Rey Radlin",
    "region": "New York, NY",
    "manager": "Paul Smith",
    "covad_alias": "S-Rey Radlin"
}, {
    "name": "Robert Markley",
    "region": "Philadelphia, PA",
    "manager": "Paul Smith",
    "covad_alias": "S-Robert Markley"
}, {
    "name": "Roderick Lewis",
    "region": "New York, NY",
    "manager": "Paul Smith",
    "covad_alias": "S-Roderick Lewis"
}, {
    "name": "Rodney Bowman",
    "region": "Washington, DC",
    "manager": "Paul Smith",
    "covad_alias": "S-Rodney Bowman"
}, {
    "name": "Ronald Gabin",
    "region": "San Francisco, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Ronald Gabin"
}, {
    "name": "Savva Savvas",
    "region": "New York, NY",
    "manager": "Paul Smith",
    "covad_alias": "S-Savva Savvas"
}, {
    "name": "Scott Edens",
    "region": "Los Angeles, CA",
    "manager": "Paul Smith",
    "covad_alias": "S-Scott Edens"
}, {
    "name": "Sean Maloney",
    "region": "Boston, MA",
    "manager": "Paul Smith",
    "covad_alias": "S-Sean Maloney"
}, {
    "name": "Sunil Maharjan",
    "region": "New York, NY",
    "manager": "Paul Smith",
    "covad_alias": "S-Sunil Maharjan"
}, {
    "name": "Tomas Nieves",
    "region": "New York, NY",
    "manager": "Paul Smith",
    "covad_alias": "S-Tomas Nieves"
}, {
    "name": "Vic Cotto",
    "region": "Cleveland, OH",
    "manager": "Paul Smith",
    "covad_alias": "S-Victor Cotto"
}];
