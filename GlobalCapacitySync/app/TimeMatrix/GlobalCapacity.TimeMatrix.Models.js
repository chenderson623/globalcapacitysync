
if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

Models = {};

Models.Schedule = {
    limit_params: {}, //obj of limits. ie {regions: ['ATL','BOS']}
    schedule_date: null, //string
    techs_available: [], //array of strings
    timeslots_available: [], //array of strings
    tech_schedules: {} //obj with tech names as keys, Models.TechSchedule as values
};

Models.TechSchedule = {
    timeslots: {} //obj with timeslot labels as keys, array of Model.Task as values
};

var schedule = {
    limit_params: {},
    schedule_date: '2014-03-22',
    techs_available: ['tech_a', 'tech_b', 'tech_c'],
    timeslots_available: ['8AM', '9AM', '10AM'],
    tech_schedules: {
        'tech_a': {
            '8AM': [{task_id:4556},{task_id: 767545}],
            '9AM': [{task_id: 5777575}]
        },
        'tech_b' : {
            '9AM' : [{task_id: 65464}]
        }
    }
};