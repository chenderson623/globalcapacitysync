if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

if (typeof GlobalCapacity.TimeMatrix.Backbone.Factories == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix.Backbone.Factories is required.';
}

GlobalCapacity.TimeMatrix.Controls = (function() {
    /*jshint multistr: true */
    'use strict';
    /**
     *
     *
     * ============================MODELS==============================================
     */
    /**
     *
     * ScheduleControlModel:
     */
    var ScheduleControlModel = Backbone.Model.extend({
        defaults: {
            number_unsynced_tasks: false, //false until compare is run
            schedule_compared: false,
            state: 'Un-Synced'
        },
        initialize: function() {
            //properties
            this.schedule_model = null; //GlobalCapacity.TimeMatrix.Backbone.Models.ScheduleModel
            this.task_control_collection = new TaskControlCollection();
            this.task_control_collection.schedule_control_model = this;
            this.tech_control_collection = new TechControlCollection();
            this.tech_control_collection.schedule_control_model = this;
        },
        getTechControlCollection: function() {
            return this.tech_control_collection;
        },
        getTaskControlCollection: function() {
            return this.task_control_collection;
        },
    });
    /**
     *
     * TaskControlModel:
     */
    var TaskControlModel = Backbone.Model.extend({
        defaults: {
            task_matched: null, //t/f, or null if not attempted
            task_selectable: true,
            task_selected: true,
            in_process: false,
            needs_sync: false,
            synced: false, //TODO: listener on this property that can bubble up to ScheduleControlModel
            message: ''
        },
        initialize: function() {
            //properties
            this.timematrix_task = null; //GlobalCapacity.TimeMatrix.Backbone.Models.TaskModel
            this.fieldportal_job = null;
            this.tech_control_model = null;
            this.sync_compare_match_item = null;
        },
        setSyncCompareMatch: function(sync_compare_match_item) {
            this.sync_compare_match_item = sync_compare_match_item;
            this.trigger("sync_compare_match_item:set");
        },
        setFieldPortalJob: function(fieldportal_job) {
            this.fieldportal_job = fieldportal_job;
            //Set states:
            this.set('task_matched', true);
            this.set('task_selectable', false); //TODO: there could be other reasons to select a task
            this.set('task_selected', false);
            this.set('needs_sync', false);
            //Trigger event
            this.trigger("fieldportal_job:set");
        },
        getFieldPortalJob: function() {
            return this.fieldportal_job;
        },
        getTechSchedule: function() {
            return this.timematrix_task.getTimeslot().getTechSchedule();
        },
        getTechControlCollection: function() {
            return this.collection.getTechControlCollection();
        },
        getTechControlModel: function() {
            if(this.tech_control_model === null) {
                var tech_schedule = this.getTechSchedule();
                var tech_model = tech_schedule.tech_model;
                var tech_control_models = this.getTechControlCollection().findTechControlForTechModel(tech_model);
                if(tech_control_models.length > 0) {
                    this.tech_control_model = tech_control_models[0];
                }
            }
            return this.tech_control_model;
        },
    });

    /**
     *
     * TaskControlCollection:
     */
    var TaskControlCollection = Backbone.Collection.extend({
        model: TaskControlModel,
        schedule_control_model: null,
        getTechControlCollection: function() {
            return this.schedule_control_model.getTechControlCollection();
        }
    });

    /**
     *
     * TechControlModel:
     */
    var TechControlModel = Backbone.Model.extend({
        defaults: {
            tech_matched: null, //t/f, or null if not attempted
            tech_selectable: false,
            tech_selected: false
        },
        initialize: function() {
            //properties
            this.timematrix_tech = null; //GlobalCapacity.TimeMatrix.Backbone.Models.TechModel
            this.fieldportal_tech = null;
        },
        setFieldportalTech: function(fieldportal_tech) {
            this.fieldportal_tech = fieldportal_tech;
            this.set('tech_matched', true);
            this.trigger('set:fieldportal_tech');
        },
        getFieldportalTech: function() {
            return this.fieldportal_tech;
        },
        setUnmatched: function() {
            this.set('tech_matched', false);
        }
    });

    /**
     *
     * TechControlCollection:
     */
    var TechControlCollection = Backbone.Collection.extend({
        model: TechControlModel,
        schedule_control_model: null,
        findTechControlForTechModel: function(timematrix_tech) {
            return this.filter(function(tech_control_model){
                return tech_control_model.timematrix_tech === timematrix_tech;
            });
        }
    });

    /**
     *
     * FieldportalTechControlModel:
     */
    var FieldportalTechControlModel = Backbone.Model.extend({
        defaults: {
            tech_name: null
        }
    });

    /**
     *
     *
     * ============================VIEWS===================================================
     */
    /**
     *
     * TechControlView:
     */
    var TechControlView = Backbone.View.extend({
        tagName: 'div',
        className: 'tech-name-control',
        events: {
            //'click .tech-match-link': 'openMatchTech'
        },
        initialize: function() {
            this.render();
            this.model.bind("set:fieldportal_tech", this.afterSetFieldportalTech, this);
            this.model.bind("change:tech_matched", this.afterSetTechMatched, this);
        },
        render: function() {
            //nothing at first
            return this;
        },
        afterSetFieldportalTech: function() {
            var html = '<div class="tech-name">' + this.model.fieldportal_tech.get('tech_name') + '<\/div>';
            this.$el.html(html);
        },
        afterSetTechMatched: function() {
            if (this.model.get('tech_matched') === false) {
                var html = '<div class="tech-name unmatched">(tech alias unmatched)<\/div>';
                html += '<div class="tech-match-link"><a href="#">assign tech<\/a><\/div>';
                this.$el.html(html);
            }
        },
        openMatchTech: function() {
            alert('openTechMatch');
            console.log('THIS', this);
        }
    });
    /**
     *
     * TechMessageView:
     */
    var TechMessageView = Backbone.View.extend({
        tagName: 'div',
        className: 'tech-message',
        initialize: function() {
            this.render();
            this.model.bind("change:tech_message", this.afterSetTechMessage, this);
        },
        render: function() {
            return this;
        },
        afterSetTechMessage: function() {}
    });

    /**
     *
     * TechSelectView:
     */
    var TechSelectView = Backbone.View.extend({
        tagName: 'div',
        className: 'tech-select',
        events: {
            'change input.tech_select': 'changeTechSelect'
        },
        initialize: function() {
            this.render();
            this.model.bind("change:tech_selectable", this.afterSetTechSelectable, this);
            this.model.bind("change:tech_selected", this.afterSetTechSelected, this);
        },
        $tech_select: null,
        render: function() {
            this.$tech_select = $('<input name="tech_select" type="checkbox" value="selected"/>');
            this.setTechSelected();
            this.setTechSelectable();
            this.$el.append(this.$tech_select);
            return this;
        },
        setTechSelectable: function() {
            this.$tech_select.attr("disabled",!this.model.get('tech_selectable'));
        },
        setTechSelected: function() {
            this.$tech_select.prop("checked",this.model.get('tech_selected'));
        },
        changeTechSelect: function() {
            this.model.set('tech-selected', this.$tech_select.prop("checked"));
        }
    });

    /*
     * Note: this view object will not hold a model.
     * The menu view is regenerated on each menu click
     * model is passed to changeContext
     */
    var TaskContextMenuView = Backbone.View.extend({
        tagName: 'div',
        className: 'tm-task-context-menu',
        template: ' \
          <ul class="dropdown-menu" role="menu"> \
              <li class="dropdown-header">TimeMatrix Task <span class="task-type"></span> <span class="task-id"></span></li> \
              <li class="divider dynamic-items"></li> \
              <li class="divider static-items"></li> \
              <li><a tabindex="-1" class="open-task">Open Task</a></li> \
          </ul>',
        $dynamic_divider: null,
        $static_divider: null,
        render: function() { //should only be called once
            this.$el.empty();
            var $template = $(this.template);
            this.$el.append($template);
            //cache some items:
            this.$dynamic_divider = $template.find('li.divider.dynamic-items');
            this.$static_divider = $template.find('li.divider.static-items');
            return this.el;
        },
        static_events: {
            'click a.open-task': 'openTask'
        },
        //----------------------------------------
        //    Static event handlers
        //----------------------------------------
        openTask: function() {
            var task_control_model = this.task_control_model;

            GlobalCapacity.TimeMatrix.getOSS(function(oss){
                oss.getGlobalCapacityTaskDocuments(function(task_documents){
                    task_documents.loadTaskDocumentWindowController(function(window_controller){
try{
                        var controller = new window_controller();
                        controller.emitter.onAny(function(){console.log("On ANY TaskDocumentWindowController", this, arguments);});
                        console.log("WINDOW CONTROLLER", controller);
                        var task_url = 'http://testtimematrix.local.dev/TT4166632.htm';
                        //var task_url = task_control_model.timematrix_task.task_url;
                        controller.openTaskDocumentWindow(task_url);
                        controller.getTaskData(function(task_data){
                            console.log("TASK DATA: ", task_data);
                            controller.closeChildWindow();
                        });
} catch(e) {console.log(e.toSource());}
                    });
                });
            });
        },
        events: { //gets cleared and filled in changeContext()
        },
        //----------------------------------------
        //    Preset menu event handlers
        //----------------------------------------
        clickOpenInPortal: function() {
            console.log("clickOpenInPortal");
        },
        clickSyncTask: function() {
            console.log("clickSyncTask", this.task_control_model);
        },
        task_control_model: null,
        changeContext: function(task_control_model) {
            this.task_control_model = task_control_model;
            this.events = _.extend({}, this.static_events);
            this.undelegateEvents();

            var timematrix_task = task_control_model.timematrix_task;
            this.setTaskType(timematrix_task.get('task_type'));
            this.setTaskId(timematrix_task.get('task_id'));

            this.clearDynamicItems();

            //TODO calculate menu items here:
            this.insertClickItem('sync-this-task', 'Sync Task', 'clickSyncTask');

            this.setFieldportalJob(task_control_model);

            this.insertHeader('&nbsp;');
            this.insertClickItem('do-something', 'Do Something', function(){console.log("DO SOMETHING", this.task_control_model);});

            this.delegateEvents(this.events);
        },
        setTaskType: function(task_type) {
            this.$el.find('span.task-type').text(task_type);
        },
        setTaskId: function(task_id) {
            this.$el.find('span.task-id').text(task_id);
        },
        setFieldportalJob: function(task_control_model) {
            var fieldportal_job = task_control_model.getFieldPortalJob();
            if(!fieldportal_job) {
                return;
            }
            this.insertHeader('Portal Job ' + fieldportal_job.get('workorder_id'));
            this.insertClickItem('open-in-portal', 'Open in Portal', 'clickOpenInPortal');
            this.insertClickItem('report-in-portal', 'Report in Portal');
            this.insertClickItem('something-in-portal', 'Something in Portal', function(){console.log("SOMETHING IN PORTAL", this.task_control_model);}, 'disabled');
        },
        clearDynamicItems: function() {
            this.$dynamic_divider.nextUntil(this.$static_divider).each(function(li_item){
                $(this).remove();
            });
        },
        insertHeader: function(item_label){
            this.insertDynamicItem('<li class="dropdown-header">' + item_label + '</li>');
        },
        insertClickItem: function(item_class, item_label, callback, li_class) { //per backbone delegateEvents, callback can be a string
            this.insertDynamicItem('<li class="' + li_class + '"><a class="' + item_class + '" tabindex="-1">' + item_label + '</a></li>');
            this.events['click .' + item_class] = callback;
        },
        insertDynamicItem: function(item) {
            $(item).insertBefore(this.$static_divider);
        },
        insertDynamicItems: function(items_array) {
            for(var i=0, len=items_array.length; i<len;i++) {
                $(items_array[i]).insertBefore(this.$static_divider);
            }
        }
    });

    var TaskContextMenuFactory = { //global object to create / hold task context menu view.
        $task_context_menu_view: null,
        //create once, and only when needed
        getTaskContextMenuView: function() {
            if(this.$task_context_menu_view !== null) {
                return this.$task_context_menu_view;
            }
            //Create TaskContextMenuView and append to body
            this.$task_context_menu_view = new TaskContextMenuView();
            var el = this.$task_context_menu_view.render();
            $('body').append(el);
            return this.$task_context_menu_view;
        },
        getTaskContextMenu$El: function() { //TODO: add task_control_model as argument?
            return this.getTaskContextMenuView().$el;
        },

        fillContextMenu: function(task_control_model) {
            return this.getTaskContextMenuView().changeContext(task_control_model);
        }
    };


    /**
     *
     * TaskContextMenuButtonView:
     * model is task_control_model. This view handles re-delegating click event and holding task_control_model
     */
    var TaskContextMenuButtonView = Backbone.View.extend({
        tagName: 'div',
        className: 'task-context-menu',
        /*
         * No need to create $.contextmenu objects on each. 99% of the time, will not be used
         * Set up event to listen for click. On one click, undelegate this and create the $.contextmenu object
         */
        events: {
            'click': 'createContextMenu'
        },
        createContextMenu: function(e) {
            this.undelegateEvents(); //this view no longer handles click event. now a holder for the $.contextmenu event
            var self = this;
            //CREATE CONTEXTMENU
            var cmenu = this.$context_menu_button.contextmenu({
                target: TaskContextMenuFactory.getTaskContextMenu$El(),
                before: function(e) {
                    TaskContextMenuFactory.fillContextMenu(self.model);
                    return true;
                }
            });
            this.get$contextmenu().show(e);

            e.stopPropagation();
        },
        get$contextmenu: function() {
            //upon creation, $.contextmenu is stored in $el.data(context);
            return this.$context_menu_button.data('context');
        },
        initialize: function() {
        },
        $context_menu_button: null,
        render: function() {
            this.$context_menu_button = $('<button type="button" class="tm-task-context-menu-btn btn btn-default btn-sm"><span class="glyphicon glyphicon-align-justify"></span></button>');
            this.$el.append(this.$context_menu_button);
            return this.el;
        },
    });


    /**
     *
     * TaskSelectView:
     */
    var TaskSelectView = Backbone.View.extend({
        tagName: 'div',
        className: 'task-select',
        events: {
            'change input.task-select': 'changeTaskSelect'
        },
        initialize: function() {
            this.model.bind("change:task_selectable", this.setTaskSelectable, this);
            this.model.bind("change:task_selected", this.setTaskSelected, this);
        },
        $task_select: null,
        render: function() {
            this.$task_select = $('<input name="task_select" type="checkbox" value="selected"/>');
            this.setTaskSelected();
            this.setTaskSelectable();
            this.$el.append(this.$task_select);
            return this;
        },
        setTaskSelectable: function() {
            this.$task_select.attr("disabled",!this.model.get('task_selectable'));
        },
        setTaskSelected: function() {
            this.$task_select.prop("checked",this.model.get('task_selected'));
        },
        changeTaskSelect: function() {
            this.model.set('task-selected', this.$task_select.prop("checked"));
        }
    });

    /**
     *
     * TaskMessageView:
     */
    var TaskMessageView = Backbone.View.extend({
        tagName: 'div',
        className: 'task-message',
        initialize: function() {
            this.render();
            this.model.bind("change:message", this.afterSetTaskMessage, this);
        },
        render: function() {
            this.$el.html(this.model.get("message"));
            return this;
        },
        afterSetTaskMessage: function() {
            this.$el.html(this.model.get("message"));
        }
    });


    /**
     *
     * TaskInfoView:
     */
    var TaskInfoView = Backbone.View.extend({
        el: false, //we're going to attach this directory to the existing task-info element
        initialize: function() {
            //this.listenTo(this.model, "change", this.render);
            this.model.bind("change:task_matched", this.render, this);
            this.listenTo(this.model, "change:in_process", this.changeInProcess);
            this.render();
        },
        events: {
            "change .needs-sync-checkbox": "changeNeedsSync"
        },
        rendered: false,
        render: function() {
            if(this.rendered === true) {
                this.$el.empty();
            }
            this.renderNeedsSync();
            this.renderFieldPortalJob();
            return this;
        },
        changeNeedsSync: function(e) {
            var element = e.currentTarget;
            console.log("CHANGE NEEDS SYNC", arguments);
        },
        renderNeedsSync: function() {
            if(this.model.get('needs_sync') === true) {
                var $sync_control = $('<div class="needs-sync"><span class="needs-sync-message">Needs Sync</span> <span>Sync: </span><input class="needs-sync-checkbox" type="checkbox" checked="checked"/></div>');
                this.$el.append($sync_control);
            }
        },
        renderFieldPortalJob: function() {
            var fieldportal_job = this.model.getFieldPortalJob();
            if(!fieldportal_job) {
                return;
            }
            this.$('.needs-sync').remove();
            this.$el.append('<div class="fieldportal-job"><span class="fieldportal-label">Portal Job:</span> <span class="fieldportal-workorder-id">' + fieldportal_job.get('workorder_id') + '</span></div>');
        },
        changeInProcess: function() {
            if(this.model.get('in_process') === true) {
                this.$el.parent().showLoading();
            } else {
                this.$el.parent().hideLoading();
            }
        }
    });

    /**
     *
     *
     * ============================UI ITEMS================================================
     */
    var NavBarDocumentType = function(timematrix_document) {
        this.report_params = timematrix_document.getReportParameters();
        this.template = ' \
            <div class="document-type-controls"> \
                <h4>Time Matrix</h4> \
                <div>' + this.report_params.tech + ' ' + this.report_params.date_range + '</div> \
            <\/div>';
    };
    NavBarDocumentType.prototype = {
        getHtml: function() {
            return this.template;
        }
    };

    var NavBarTimematrixScheduleView = function(schedule_model) {
        this.schedule_model = schedule_model;
        this.template = ' \
            <h3>{{date_formatted}} Schedule</h3> \
            <div class="sidebar_info"> \
                <ul class="list-unstyled schedule-stats"> \
                    <li> \
                        <span class="act act-warning">{{number_of_techs}}</span> \
                        <strong>Techs</strong> \
                    </li> \
                    <li> \
                        <span class="act act-success">{{number_of_tasks}}</span> \
                        <strong>Tasks</strong> \
                    </li> \
                </ul> \
            </div> \
            ';
        this.view = null;
    };
    //TODO add additional menu items for schedule. Needs to be after this
    NavBarTimematrixScheduleView.prototype  = {
        get$El: function() {
            return this.getBackboneView().$el;
        },
        getBackboneView: function() {
            if(this.view !== null) {
                return this.view;
            }
            var self = this;
            var View = Backbone.View.extend({
                tagName: 'li',
                className: 'schedule',
                template: Handlebars.compile(self.template),
                initialize: function() {
                    this.render();
                },
                render: function() {
                    var data = this.model.toJSON();
                    var schedule_date = data.schedule_date;
                    var date_num = '0' + schedule_date.getUTCDate();
                    data.date_formatted = (schedule_date.getUTCMonth() + 1) + '/' + date_num.substr(-2) + '/' + schedule_date.getUTCFullYear();

                    var html = this.template(data);
                    this.$el.html(html);
                }
            });
            this.view = new View({model: this.schedule_model});
            return this.view;
        },

    };


    var NavBarTimematrixScheduleControlUnsyncedJobsItemView = function(schedule_control_model) {
        this.schedule_control_model = schedule_control_model;
        this.template = ' \
            <span class="act act-danger">{{number_unsynced_tasks}}</span> \
            <strong>Unsynced Jobs</strong> \
            ';
        this.view = null;
    };
    NavBarTimematrixScheduleControlUnsyncedJobsItemView.prototype  = {
        get$El: function() {
            return this.getBackboneView().$el;
        },
        getBackboneView: function() {
            if(this.view !== null) {
                return this.view;
            }
            var self = this;
            var View = Backbone.View.extend({
                tagName: 'li',
                className: 'unsynced',
                template: Handlebars.compile(self.template),
                initialize: function() {
                    this.render();
                },
                render: function() {
                    var data = this.model.toJSON();
                    var number_unsynced_tasks = data.number_unsynced_tasks;
                    if(number_unsynced_tasks === false) {
                        data.number_unsynced_tasks = '-';
                    }
                    var html = this.template(data);
                    this.$el.html(html);
                }
            });
            this.view = new View({model: this.schedule_control_model});
            return this.view;
        }
    };

    var NavBarTimematrixScheduleControlCompareMenuItemView = function(schedule_control_model) {
        this.schedule_control_model = schedule_control_model;
        this.view = null;
    };
    NavBarTimematrixScheduleControlCompareMenuItemView.prototype  = {
        get$El: function() {
            return this.getBackboneView().$el;
        },
        getBackboneView: function() {
            if(this.view !== null) {
                return this.view;
            }
            var self = this;
            var View = Backbone.View.extend({
                tagName: 'li',
                className: 'compare-action',
                events: {
                    'click': 'onClick'
                },
                initialize: function() {
                    this.render();
                },
                render: function() {
                    var html = '<a tabindex="-1">Compare to Portal</a>';
                    this.$el.html(html);
                },
                onClick: function() {
                    console.log("Compare Button Clicked");
                }
            });
            this.view = new View({model: this.schedule_control_model});
            return this.view;
        }
    };

    var NavBarTimematrixScheduleControlSyncMenuItemView = function(schedule_control_model) {
        this.schedule_control_model = schedule_control_model;
        this.view = null;
    };
    NavBarTimematrixScheduleControlSyncMenuItemView.prototype  = {
        get$El: function() {
            return this.getBackboneView().$el;
        },
        getBackboneView: function() {
            if(this.view !== null) {
                return this.view;
            }
            var self = this;
            var View = Backbone.View.extend({
                tagName: 'li',
                className: 'compare-action',
                events: {
                    'click': 'onClick'
                },
                initialize: function() {
                    this.render();
                },
                render: function() {
                    var html = '<a tabindex="-1">Sync All</a>';
                    this.$el.html(html);
                },
                onClick: function() {
                    console.log("Sync Button Clicked");
                }
            });
            this.view = new View({model: this.schedule_control_model});
            return this.view;
        }
    };


    var NavBarTimematrixScheduleControlView = function(schedule_control_model) {

    };
    NavBarTimematrixScheduleControlView.prototype  = {

    };

    //note: this is independent of sync
    var NavBarTimematrixDropdown = function(timematrix_document) {
        this.timematrix_document = timematrix_document;
        this.template = ' \
            <a data-toggle="dropdown" class="dropdown-toggle" ><span class="glyphicon glyphicon-th"></span> Time Matrix <b class="caret"></b></a> \
            <ul class="dropdown-menu"> \
                <li><a tabindex="-1">TimeMatrix Menu Item 1</a></li> \
                <li><a tabindex="-1">TimeMatrix Menu Item 2</a></li> \
                <li class="divider end-section"></li> \
                <li><a tabindex="-1">Refresh Page</a></li> \
            </ul>';
        this.view = null;
        this.schedule_views = [];
    };
    NavBarTimematrixDropdown.prototype = {
        get$El: function() {
            return this.getBackboneView().$el;
        },
        getBackboneView: function() {
            if(this.view !== null) {
                return this.view;
            }
            var self = this;
            var View = Backbone.View.extend({
                tagName: 'li',
                className: 'dropdown',
                template: Handlebars.compile(self.template),
                initialize: function() {
                    this.render();
                },
                render: function() {
                    //var data = this.model.toJSON();
                    //var html = this.template(data);
                    var html = this.template();
                    this.$el.html(html);
                }
            });
            this.view = new View();
            return this.view;
        },
        addSchedule: function(index, schedule_model) {
            this.schedule_views[index] = new NavBarTimematrixScheduleView(schedule_model);
            var dropdown_$el = this.get$El().find('ul.dropdown-menu');
            $('<li class="divider schedule-section"></li>').insertBefore(dropdown_$el.find('li.end-section'));
            this.schedule_views[index].get$El().insertBefore(dropdown_$el.find('li.end-section'));
        },
        addScheduleControl: function(index, schedule_control_model) {
            var schedule_view = this.schedule_views[index];
            var unsynced_jobs_stat = new NavBarTimematrixScheduleControlUnsyncedJobsItemView(schedule_control_model);
            schedule_view.get$El().find('ul.schedule-stats').append(unsynced_jobs_stat.get$El());

            var compare_schedule_menu_item = new NavBarTimematrixScheduleControlCompareMenuItemView(schedule_control_model);
            compare_schedule_menu_item.get$El().insertAfter(schedule_view.get$El());

            var sync_schedule_menu_item = new NavBarTimematrixScheduleControlSyncMenuItemView(schedule_control_model);
            sync_schedule_menu_item.get$El().insertAfter(compare_schedule_menu_item.get$El());
        }
    };

    /**
     *
     *
     * ============================FACTORIES================================================
     */
    /**
     *
     * TechControlCollectionFactory:
     */
    var TechControlCollectionFactory = function(schedule_control_model) {
        this.schedule_control_model = schedule_control_model;
        this.tech_control_collection = this.schedule_control_model.getTechControlCollection();
    };
    TechControlCollectionFactory.prototype = {
        fillTechControlModelsFromScheduleModel: function(schedule_model) {
            var self = this;
            var tech_schedule_collection = schedule_model.getTechScheduleCollection();
            tech_schedule_collection.each(function(tech_schedule){
                var tech = tech_schedule.getTech();
                self.createTechControlModel(tech);
            });
        },
        createTechControlModel: function(timematrix_tech_model) {
            var tech_control_model = this.tech_control_collection.add({});
            tech_control_model.timematrix_tech = timematrix_tech_model;
            return tech_control_model;
        },
        createTechControlView: function(tech_control_model, timematrix_tech_view) {
            var tech_control_view = new TechControlView({model: tech_control_model});
            timematrix_tech_view.assign('.tech-info', tech_control_view);
        },
        createTechSelectView: function(tech_control_model, timematrix_tech_view) {
            var tech_select_view = new TechSelectView({model: tech_control_model});
            timematrix_tech_view.$('.tech-alias').prepend(tech_select_view.$el);
        },
        createTechMessageView: function(tech_control_model, timematrix_tech_view) {
            var tech_message_view = new TechMessageView({model: tech_control_model});
            timematrix_tech_view.assign('.tech-message', tech_message_view);
        },
        addFromTimematrixViewFactory: function(timematrix_tech_view, timematrix_tech_model, timematrix_view_factory) {
            var tech_control_model = this.createTechControlModel(timematrix_tech_model);
            this.createTechControlView(tech_control_model, timematrix_tech_view);
            this.createTechSelectView(tech_control_model, timematrix_tech_view);
            this.createTechMessageView(tech_control_model, timematrix_tech_view);
        },
        loadTechControlCollectionWithEmployeeSet: function(employee_set) {
            var tech_control_employee_loader = new TechControlModelEmployeeLoader(this.tech_control_collection, employee_set);
            tech_control_employee_loader.loadTechControlCollectionWithEmployees();
        },
        loadTechControlCollectionWithEmployeesFromOSS: function(oss, callback) {
            var employee_set_loader = oss.getEmployeeSetLoader();
            var self = this;
            employee_set_loader.loadEmployees(function(employee_set){
                self.loadTechControlCollectionWithEmployeeSet.call(self, employee_set);
                if(callback) {
                    callback(employee_set);
                }
            });
        }
    };

    /**
     *
     * TechControlModelEmployeeLoader:
     */
    var TechControlModelEmployeeLoader = function(tech_control_collection, employee_set) {
        this.tech_control_collection = tech_control_collection;
        this.employee_set = employee_set;
    };
    TechControlModelEmployeeLoader.prototype = {
        loadTechControlModelWithEmployee: function(tech_control_model) {
            var tech_alias = tech_control_model.timematrix_tech.get('tech_alias');
            var tech_name = this.employee_set.getNameForGlobalCapacityAlias(tech_alias);
            if(tech_name) {
                //TODO: make this a proper model
                tech_control_model.setFieldportalTech(new FieldportalTechControlModel({tech_name: tech_name}));
            } else {
                tech_control_model.setUnmatched();
            }
        },
        loadTechControlCollectionWithEmployees: function(){
            this.tech_control_collection.each(this.loadTechControlModelWithEmployee.bind(this));
        }
    };


    /**
     *
     * TaskControlCollectionFactory:
     */
    var TaskControlCollectionFactory = function(schedule_control_model) {
        this.schedule_control_model = schedule_control_model;
        this.task_control_collection = this.schedule_control_model.getTaskControlCollection();
    };
    TaskControlCollectionFactory.prototype = {
        fillTaskControlModelsFromScheduleModel: function(schedule_model) {
            var self = this;
            var tech_schedule_collection = schedule_model.getTechScheduleCollection();
            tech_schedule_collection.each(function(tech_schedule){
                var timeslot_collection = tech_schedule.getTimeslotCollection();
                timeslot_collection.each(function(timeslot){
                    timeslot.getTaskCollection().each(function(task){
                        self.createTaskControlModel(task);
                    });
                });
            });
        },
        createTaskControlModel: function(timematrix_task_model) {
            var task_control_model = this.task_control_collection.add({});
            task_control_model.timematrix_task = timematrix_task_model;
            return task_control_model;
        },
        createTaskSelectView: function(task_control_model, timematrix_task_view) {
            var task_select_view = new TaskSelectView({model: task_control_model});
            timematrix_task_view.$el.find('.task-link').prepend(task_select_view.$el);
            task_select_view.render();
        },
        createTaskContextMenuButtonView: function(task_control_model, timematrix_task_view) {
            var task_context_menu_button_view = new TaskContextMenuButtonView({model: task_control_model});
            timematrix_task_view.$el.find('.task-controls').append(task_context_menu_button_view.$el);
            task_context_menu_button_view.render();
        },
        createTaskMessageView: function(task_control_model, timematrix_task_view) {
            var task_message_view = new TaskMessageView({model: task_control_model});
            //timematrix_task_view.assign('.task-controls', task_message_view);
            timematrix_task_view.$el.find('.task-controls').append(task_message_view.$el);
            task_message_view.render();
        },
        createTaskInfoView: function(task_control_model, timematrix_task_view) {
            var task_info_view = new TaskInfoView({
                model: task_control_model,
                el: timematrix_task_view.$('.task-info')
            });
        },
        addFromTimematrixViewFactory: function(timematrix_task_view, timematrix_task_model, timematrix_view_factory) {
            var task_control_model = this.createTaskControlModel(timematrix_task_model);
            this.createTaskSelectView(task_control_model, timematrix_task_view);
            this.createTaskMessageView(task_control_model, timematrix_task_view);
            this.createTaskContextMenuButtonView(task_control_model, timematrix_task_view);
            this.createTaskInfoView(task_control_model, timematrix_task_view);
        }
    };

    /**
     *
     *
     * ============================TimeMatrixSyncControlFactory=======================================
     * This is a progressive enhancement to ScheduleBackboneCollectionFactory
     */
    var TimeMatrixSyncControlFactory = function(schedule_backbone_collection_factory) {
        this.schedule_backbone_collection_factory = schedule_backbone_collection_factory;
        //Internal:
        this.schedule_collection = null;
        //array of TimeMatrixScheduleSyncControlFactory:
        this.timematrix_schedule_sync_control_factory = [];
    };
    TimeMatrixSyncControlFactory.prototype = {
        _on_schedule_model_create: function(schedule_model, timematrix_document, schedule_index) {
            this.timematrix_schedule_sync_control_factory[schedule_index] = new TimeMatrixScheduleSyncControlFactory(schedule_model, timematrix_document, schedule_index);
            //set it up
            this.timematrix_schedule_sync_control_factory[schedule_index].setupFactories();
            //run it
            this.timematrix_schedule_sync_control_factory[schedule_index].generateScheduleComponentsModels();
        },
        setupFactories: function() {
            var schedule_date_cell_view_factory = new GlobalCapacity.TimeMatrix.Backbone.Factories.ScheduleDateCellBackboneViewFactory(this.schedule_backbone_collection_factory);
            this.schedule_backbone_collection_factory.onScheduleModelCreate(this._on_schedule_model_create.bind(this));
        },

        generateScheduleCollection: function() {
            this.schedule_collection = this.schedule_backbone_collection_factory.getScheduleCollection();
        },
        generateUIControls: function(ui) {
            //Document-type info piece
            var timematrix_document = this.schedule_backbone_collection_factory.timematrix_document;
            var document_type_control = new NavBarDocumentType(timematrix_document);
            ui.setMiddleItem(document_type_control.getHtml()); //TODO: change to get$El
            ui.setMiddleDivider();

            //Timematrix dropdown
            //TODO remove collection factory from here
            var timematrix_dropdown_control = new NavBarTimematrixDropdown(timematrix_document);
            ui.setMiddleItemLi(timematrix_dropdown_control.get$El());
            var schedule_collection = this.schedule_backbone_collection_factory.getScheduleCollection();
            var schedule;
            for(var i=0, len=schedule_collection.length;i<len;i++) {
                timematrix_dropdown_control.addSchedule(i,schedule_collection.at(i));
                timematrix_dropdown_control.addScheduleControl(i,this.timematrix_schedule_sync_control_factory[i].schedule_control_model);
            }
        }
    };

    /**
     *
     *
     * ============================TimeMatrixScheduleSyncControlFactory=======================================
     *
     */
    var TimeMatrixScheduleSyncControlFactory = function(schedule_model, timematrix_document, schedule_index) {
        this.schedule_model = schedule_model;
        this.timematrix_document = timematrix_document;
        this.schedule_index = schedule_index;
        //Internal:
        this.schedule_model_components_factory = null;
        this.schedule_model_components_views_factory = null;
        //From setupFactories():
        this.schedule_control_model = null;
        this.tech_control_factory = null;
        this.task_control_factory = null;
    };

    TimeMatrixScheduleSyncControlFactory.prototype = {
        getTimeMatrixDocument: function() {
            return this.timematrix_document;
        },
        getTimeMatrix: function() {
            return this.getTimeMatrixDocument().timematrix;
        },
        getScheduleModelComponentsFactory: function() {
            if(this.schedule_model_components_factory === null) {
                this.schedule_model_components_factory = new GlobalCapacity.TimeMatrix.Backbone.Factories.ScheduleModelComponentsFactory(this.schedule_model, this.timematrix_document, this.schedule_index);
            }
            return this.schedule_model_components_factory;
        },
        getScheduleModelComponentsViewsFactory: function() {
            if(this.schedule_model_components_views_factory === null) {
                this.schedule_model_components_views_factory = new GlobalCapacity.TimeMatrix.Backbone.Factories.ScheduleModelComponentsViewsFactory(this.getScheduleModelComponentsFactory());
            }
            return this.schedule_model_components_views_factory;
        },
        setupFactories: function() {
            var timematrix = this.getTimeMatrix();
            var views_factory = this.getScheduleModelComponentsViewsFactory();

            //===============Schedule Control Model==========================================
            this.schedule_control_model = new ScheduleControlModel();

            //===============Tech Control Factory============================================
            this.tech_control_factory = new TechControlCollectionFactory(this.schedule_control_model);
            views_factory.on_tech_view.push(this.tech_control_factory.addFromTimematrixViewFactory.bind(this.tech_control_factory));

            //===============Task Control Factory============================================
            this.task_control_factory = new TaskControlCollectionFactory(this.schedule_control_model);
            views_factory.on_task_view.push(this.task_control_factory.addFromTimematrixViewFactory.bind(this.task_control_factory));
        },
        generateScheduleComponentsModels: function(callback) {
            this.getScheduleModelComponentsFactory().generateScheduleComponentsModels();
            //===========After Processors=====================================================
            var self = this;
            this.getTimeMatrix().getOSS(function run_employee_set_loader(oss){
                try{
                    self.tech_control_factory.loadTechControlCollectionWithEmployeesFromOSS(oss, function(employee_set){
                        if(callback) callback(self);
                    });
                } catch (err) {
                    console.log(err.toSource());
                }
            });
        },
    };

    return {
        ScheduleControlModel: ScheduleControlModel,
        TaskControlCollectionFactory: TaskControlCollectionFactory,
        TechControlCollectionFactory: TechControlCollectionFactory,
        TimeMatrixSyncControlFactory: TimeMatrixSyncControlFactory,
        TimeMatrixScheduleSyncControlFactory: TimeMatrixScheduleSyncControlFactory
    };
})();