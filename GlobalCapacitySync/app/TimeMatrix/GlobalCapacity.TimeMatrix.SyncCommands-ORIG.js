

if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

GlobalCapacity.TimeMatrix.SyncCompare = (function() {
    "use strict";

    var SyncCommands = {
    };


var covad_MatrixSyncronizeCommand_Get=function(matrix_job){
    var self = this;

    this.matrix_job  = matrix_job;
    this.task_window = null;

    this.dom_element = null;
    this.done        = false;

    this.callback_func = null;

    this.getMatrixJob = function() {
        return self.matrix_job;
    };

    this.appendCommandToDom=function(dom_element){
        self.dom_element = dom_element;
        dom_element.appendChild(document.createTextNode("Needs Upload:"));
        dom_element.appendChild(document.createElement("br"));

        var button       = document.createElement('button');
        button.innerHTML = 'Get Task';

        //button.command_obj=self;
        dom_element.command_obj = self;

        $(button).click(function(e){
                if(e.target){
                        targ = e.target;
                } else if (e.srcElement){
                        targ = e.srcElement;
                }
                //Note that targ.command_obj is this object. Set 8 lines above
                targ.parentNode.command_obj.execute();
         });
        dom_element.appendChild(button);
    }

    this.execute=function(callback_func,callback_opts){
        self.callback_func = callback_func;
        self.callback_opts = callback_opts;
        var window_obj = getCommandWindow();
        //TODO move this. moved temporarily to be able to skip
        self.done                  = true;
        window_obj.getCreateJobWindow(self);
    }

    this.taskAddedFromPopup = function(job_id) {
        self.dom_element.innerHTML = "Job Added: " + job_id + " ";
        self.done                  = true;

        var link    = document.createElement('a');
        var job_url = fieldportal_root_url + '/fieldportal/action_popup.php?m=/Clients/CovadDSL/&c=JobCovadDSL&a=edit&job_id='+job_id;
        link.setAttribute('href', job_url);
        link.innerHTML        = 'Open Queue Job';
        link.style.fontStyle  = 'italic';
        link.style.whiteSpace = 'nowrap';
        link.style.fontSize   = '0.8em';


        $(link).click(function(e){
                if(e.preventDefault){
                    e.preventDefault();
                } else {
                    e.returnValue=false;
                }
                if(e.target){
                        targ=e.target;
                } else if (e.srcElement){
                        targ=e.srcElement;
                }
                //Note that targ.command_obj is this object. Set 8 lines above
                var task_link=targ.getAttribute('href');
                window.open(task_link,"queuetask","width=750,height=700,toolbar=no,scrollbars=yes,location=yes,status=yes");
                return false;
         });

        self.dom_element.appendChild(link);



        if(self.callback_func){
            self.callback_func();
        }

    };

}


var covad_MatrixSyncronizeCommand_Delete=function(queue_job){
    var self=this;

    this.queue_job=queue_job;

    this.dom_element=null;

    this.appendCommandToDom=function(dom_element){
        self.dom_element=dom_element;

        var button=document.createElement('button');
        button.innerHTML='Delete';

        //button.command_obj=self;
        dom_element.command_obj=self;

        $(button).click(function(e){
                if(e.target){
                        targ=e.target;
                } else if (e.srcElement){
                        targ=e.srcElement;
                }
                //Note that targ.command_obj is this object. Set 8 lines above
                targ.parentNode.command_obj.execute();
         });

        dom_element.appendChild(button);

    }

    this.execute=function(callback_func,callback_opts){
        self.callback_func = callback_func;
        self.clientCancelJob();
        self.done = true;
    }

    this.notify = function() {
        if(self.dom_element){
            self.dom_element.innerHTML = self.response.message;
        }
        if(self.callback_func){
            self.callback_func();
        }
    };

    this.clientCancelJob=function(){
        $.ajax({
            url:      fieldportal_covad_url + "ajax_covad_cancel_matrix_job.php?nd="+new Date().getTime(),
            data:     {job_id:self.queue_job.workorder_id},
            type:     "GET",
            dataType: "jsonp",
            success: function (data,textStatus){
                self.response=data;
                self.notify();
            },
            complete: function(xhr,textStatus) {
            },
            error: function( xhr,textStatus,errorThrown) {
                if(xhr.status!=200){
                    alert("Error, Data request could not be completed.");
                    return;
                }
                if(textStatus=="parsererror"){
                    alert("Error, Data response has errors.");
                    return;
                }
                alert("Error, Unknown error: " + textStatus);
            }

        });
    }
}

var covad_MatrixSyncronizeCommand_Move=function(matrix_job,queue_job){
    var self = this;

    this.matrix_job = matrix_job;
    this.queue_job  = queue_job;

    this.response    = null;
    this.dom_element = null;
    this.done        = false;

    this.callback_func = null;

    this.appendCommandToDom = function(dom_element){
        self.dom_element = dom_element;
        var move_label   = 'Move to ' + self.matrix_job.task_employee + ':';
        dom_element.appendChild(document.createTextNode(move_label));
        dom_element.appendChild(document.createElement("br"));

        dom_element.appendChild(document.createTextNode('(from ' + self.queue_job.AssignedTechName + ')'));

        var button       = document.createElement('button');
        button.innerHTML = 'Move';

        //button.command_obj=self;
        dom_element.command_obj = self;

        $(button).click(function(e){
                if(e.target){
                        targ=e.target;
                } else if (e.srcElement){
                        targ=e.srcElement;
                }
                //Note that targ.command_obj is this object. Set 8 lines above
                targ.parentNode.command_obj.execute();
         });

        dom_element.appendChild(button);


    }
    this.execute=function(callback_func){
        self.callback_func = callback_func;
        self.moveJobInQueue();
        self.done = true;
    }

    this.notify = function() {
        if(self.dom_element){
            self.dom_element.innerHTML = self.response.message;
        }
        if(self.callback_func){
            self.callback_func();
        }
    };

    this.moveJobInQueue=function(){
        $.ajax({
            url:      fieldportal_covad_url + "ajax_covad_move_matrix_job.php?nd="+new Date().getTime(),
            data:     {job_id:queue_job.workorder_id,AssignedTechName:self.matrix_job.task_employee},
            type:     "GET",
            dataType: "jsonp",
            success: function (data,textStatus){
                self.response = data;
                self.notify();
            },
            complete: function(xhr,textStatus) {
            },
            error: function( xhr,textStatus,errorThrown) {
                if(xhr.status!=200){
                    alert("Error, Data request could not be completed.");
                    return;
                }
                if(textStatus=="parsererror"){
                    alert("Error, Data response has errors.");
                    return;
                }
                alert("Error, Unknown error: " + textStatus);
            }

        });
    }
}

var covad_MatrixSyncronizeCommand_Ok = function(matrix_job,queue_job){
    var self = this;

    this.queue_job  = queue_job;
    this.matrix_job = matrix_job;

    this.appendCommandToDom = function(dom_element){
        dom_element.appendChild(document.createTextNode('Queue Id: ' + self.queue_job.workorder_id + ' '));

        var link    = document.createElement('a');
        var job_url = fieldportal_root_url + 'fieldportal/action_popup.php?m=/Clients/CovadDSL/&c=JobCovadDSL&a=edit&job_id='+self.queue_job.workorder_id;
        link.setAttribute('href', job_url);
        link.innerHTML        = 'Open Queue Job';
        link.style.fontStyle  = 'italic';
        link.style.whiteSpace = 'nowrap';
        link.style.fontSize   = '0.8em';


        $(link).click(function(e){
                if(e.preventDefault){
                    e.preventDefault();
                } else {
                    e.returnValue=false;
                }
                if(e.target){
                        targ=e.target;
                } else if (e.srcElement){
                        targ=e.srcElement;
                }
                //Note that targ.command_obj is this object. Set 8 lines above
                var task_link=targ.getAttribute('href');
                window.open(task_link,"queuetask","width=750,height=700,toolbar=no,scrollbars=yes,location=yes,status=yes");
                return false;
         });

        dom_element.appendChild(link);

        dom_element.appendChild(document.createElement("br"));
        dom_element.appendChild(document.createTextNode('Timeslot: ' + self.queue_job.AssignedTime));
        dom_element.appendChild(document.createElement("br"));

        var matrix_start_hour=matrix_job.start_time.split(':')[0];
        var timeslot_error='OK';

        if(queue_job.AssignedTime=="12-4"){
            if(matrix_start_hour<12) timeslot_error='Matrix timeslot is earlier. Should be checked.';
            if(matrix_start_hour>=16) timeslot_error='Matrix timeslot is later. Should be checked.';

        } else if(queue_job.AssignedTime=="9-1") {
            if(matrix_start_hour>=13) timeslot_error='Matrix timeslot is later. Should be checked.';

        } else if(queue_job.AssignedTime=="9AM") {
            if(matrix_start_hour!='09' && matrix_start_hour!='08') timeslot_error='Matrix timeslot is exact time. Should be checked.';

        } else if(queue_job.AssignedTime=="1PM") {
            if(matrix_start_hour!='12'&& matrix_start_hour!='13') timeslot_error='Matrix timeslot is exact time. Should be checked.';

        } else if(queue_job.AssignedTime=="4-7"){
            if(matrix_start_hour<16) timeslot_error='Matrix timeslot is earlier. Should be checked.';

        } else {
            timeslot_error='Invalid Timeslot';
        }

        timeslot_error_span=document.createElement('span');
        if(timeslot_error!='OK') {
           var start_time_cell = $(dom_element.parentNode).find('.start_time')[0];
            start_time_cell.style.color='red';
            var end_time_cell = $(dom_element.parentNode).find('.end_time')[0];
            end_time_cell.style.color='red';

            timeslot_error_span.style.color='red';
            timeslot_error_span.style.fontStyle='italic';
            timeslot_error_span.style.whiteSpace='nowrap';

        } else {

            timeslot_error_span.style.color='green';
        }
        timeslot_error_span.appendChild(document.createTextNode(timeslot_error));
        dom_element.appendChild(timeslot_error_span);


    }
}

var covad_MatrixSyncronizeCommand_Null=function(){
    var self=this;


    this.appendCommandToDom=function(dom_element){
        dom_element.appendChild(document.createTextNode(String.fromCharCode(160)));
    }
}

var covad_MatrixSyncronizeCommand_BadEmployee=function(){
    var self=this;


    this.appendCommandToDom=function(dom_element){
        dom_element.appendChild(document.createTextNode("Employee Not Found"));
        dom_element.style.color='red';
    }
}


var covad_MatrixSyncronizeCommand_CantMove=function(matrix_job,queue_job){
    var self=this;

    this.queue_job=queue_job;
    this.matrix_job=matrix_job;

    this.appendCommandToDom=function(dom_element){
        dom_element.appendChild(document.createTextNode("Time matrix tech does not match, but jobs is already closed in the portal by " + queue_job.AssignedTechName + "."));
        dom_element.style.color='red';
    }
}

var covad_MatrixSyncronizeCommand_CantDelete=function(matrix_job,queue_job){
    var self=this;

    this.queue_job=queue_job;
    this.matrix_job=matrix_job;

    this.appendCommandToDom=function(dom_element){
        dom_element.appendChild(document.createTextNode("This job is not in time matrix, but jobs is already closed in the portal."));
        dom_element.style.color='red';
    }
}

    return SyncCommands;

})();