var PubSub = function() {

        var empty_func = function(){};
        /**
         *
         *
         * ============================Pub/Sub===================================================
         */
        this._subscriptions = {};
        this._publish = function(topic) { //can also pass args
            var args = Array.prototype.slice.call(arguments);
            var event={type: topic};
            args[0] = event; //make the first argument the event
            if (this._subscriptions[topic] !== undefined) {
                for (var i = 0, len = this._subscriptions[topic].length; i < len; i++) {
                    this._subscriptions[topic][i].apply(this, args);
                }
            }
            //if (this._subscriptions.ALL !== undefined) {
            //    for (var j = 0, all_len = this._subscriptions.ALL.length; j < all_len; j++) {
            //        this._subscriptions.ALL[j].apply(this, args);
            //    }
            //}
        };
        this.subscribe = function(topic, callback) {
            if (!this._subscriptions[topic]) {
                this._subscriptions[topic] = [];
            }
            var index = this._subscriptions[topic].length;
            this._subscriptions[topic][index] = callback;
            return [topic, index]; //This is the 'handle' that can be used to unsubscribe
        };
        this.subscribeToAll = function(callback) {
            return this.subscribe('ALL', callback);
        };
        this.unsubscribe = function(handle) {
            var topic = handle[0];
            var index = handle[1];
            if(this._subscriptions[topic] && this._subscriptions[topic][index]) {
                this._subscriptions[topic][index]=empty_func;
            }
        };
        //This is to run a callback once, even if the event has already occured
        this.after_ready = [];
        this.event_log = {};
        this.subscribeToAll(this.saveEventLog);
        this.saveEventLog = function(event) {
            var args = Array.prototype.slice.call(arguments);
            //args.shift(); //remove the event arg
            var event_type = event.type;
            if(!self.event_log[event_type]) {
                //only logging the first one
                self.event_log[event_type] = args;
            }
        };
        this.onceAfterReady = function(topic, callback) {

        };

};
