//if (typeof Backbone == 'undefined') {
//    throw 'Backbone is required.';
//}
//if (typeof Handlebars == 'undefined') {
//    throw 'Handlebars is required.';
//}

if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

if (typeof GlobalCapacity.TimeMatrix.Backbone == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix.Backbone.Models is required.';
}


GlobalCapacity.TimeMatrix.Backbone.Views = (function() {

    //TODO: implement this: http://ianstormtaylor.com/assigning-backbone-subviews-made-even-cleaner/

    /**
     *
     *
     * ============================TEMPLATES==============================================
     */
    /*jshint multistr: true */
    var task_template = ' \
            <div class="task-controls"></div> \
            <div class="zip-code">{{zip_code}}{{#if zip_code_suffix}}-{{zip_code_suffix}}{{/if}}<\/div> \
            <div class="clli-code">{{clli_code}}<\/div> \
            <div class="task-link"> \
                <a target="_new" href="{{task_url}}">{{task_type}}{{task_id}}-{{task_seq}}<\/a> \
            <\/div> \
            <div class="task-status">{{task_status}}<\/div> \
            <div class="scheduled-time">({{time_range}})<\/div> \
            <div class="service-type">{{service_type}}<\/div> \
            <div class="task-info"></div>';
    var task_handlebars_template = Handlebars.compile(task_template);

    var block_template = ' \
            <div class="task-type">{{task_id}}<\/div> \
            <div class="scheduled-time">({{time_range}})<\/div> \
            <div class="service-type">{{service_type}}<\/div>';
    var block_handlebars_template = Handlebars.compile(block_template);

    var tech_template = ' \
            <div class="tech-controls"><\/div> \
            <div class="tech-message"><\/div> \
            <div class="tech-alias">{{tech_alias}}<\/div> \
            <div class="tech-info"><\/div>';
    var tech_handlebars_template = Handlebars.compile(tech_template);

    var schedule_date_template = ' \
        Schedule Date (rewritten): <span class="date-formatted">{{date_formatted}}</span> <span class="day-of-week">{{day_of_the_week}}</span>\
    ';
    var schedule_date_handlebars_template = Handlebars.compile(schedule_date_template);

    var tmp = '<div class="task-count">Tasks: <span class="task-count">{{task_count}}<\/span><\/div>';

    var tech_control_name_template = '({{tech_name}}) <a href="">some action<\/a>';
    var tech_control_name_handlebars_template = Handlebars.compile(tech_control_name_template);

    var tech_control_noname_template = 'tech undefined <a href="">define action<\/a>';
    var tech_control_noname_handlebars_template = Handlebars.compile(tech_control_noname_template);


    /**
     *
     *
     * ============================VIEWS=================================================
     */
    /**
     *
     * TechCellView:
     */
    var TechCellView = Backbone.View.extend({
        tagName: 'div',
        className: 'tech-container',
        template: tech_handlebars_template,
        tech_control: null,
        initialize: function() {
            this.render();
        },
        render: function() {
            var html = this.template(this.model.toJSON());
            this.$el.html(html);
            //this.employeeControl();
            return this;
        },
        /*employeeControl: function() {
            if (this.model.get('tech_name')) {
                this.tech_control = new TechControlNameView({
                    model: this.model
                });
                this.tech_control.render();
                this.$el.find('.tech-control').html(this.tech_control.$el);
            } else {
                this.tech_control = new TechControlNoNameView({
                    model: this.model
                });
                this.tech_control.render();
                this.$el.find('.tech-control').html(this.tech_control.$el);
            }
        }*/
    });

    var DispatchTaskView = Backbone.View.extend({
        tagName: 'div',
        className: 'task dispatch',
        template: task_handlebars_template,
        initialize: function() {
            this.render();
        },
        render: function() {
            var html = this.template(this.model.toJSON());
            this.$el.html(html);
            this.classTaskStatus();
            return this;
        },
        classTaskStatus: function() {
            if (this.model.get('task_status') === "CLEARED") {
                this.$el.addClass('cleared');
            }
        }
    });
    var BlockTaskView = Backbone.View.extend({
        tagName: 'div',
        className: 'block',
        template: block_handlebars_template,
        initialize: function() {
            //throw "STOP";
            //console.log('initialize');
            this.render();
        },
        render: function() {
            var data = this.model.toJSON();
            data.task_id = data.task_id.replace("NA - ", "");
            var html = this.template(data);
            this.$el.html(html);
        }
    });
    var ScheduleDateCellView = Backbone.View.extend({
        daysOfWeek: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
        tagName: 'div',
        className: 'schedule-date-cell',
        template: schedule_date_handlebars_template,
        initialize: function() {
            this.render();
        },
        render: function() {
            var data = this.model.toJSON();
            var schedule_date = data.schedule_date;
            var date_num = '0' + schedule_date.getUTCDate();
            data.date_formatted = (schedule_date.getUTCMonth() + 1) + '/' + date_num.substr(-2) + '/' + schedule_date.getUTCFullYear();
            data.day_of_the_week = this.daysOfWeek[schedule_date.getUTCDay()];
            var html = this.template(data);
            this.$el.html(html);
        }
    });
    return {
        TechCellView: TechCellView,
        DispatchTaskView: DispatchTaskView,
        BlockTaskView: BlockTaskView,
        ScheduleDateCellView: ScheduleDateCellView
    };


})();