if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

if (typeof GlobalCapacity.TimeMatrix.Backbone == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix.Backbone is required.';
}


GlobalCapacity.TimeMatrix.Backbone.Factories = (function() {

    var ScheduleBackboneCollectionFactory = function ScheduleBackboneCollectionFactory(timematrix_document) {
        this.timematrix_document = timematrix_document;
        //Creates:
        this._schedule_collection = null;
        //Triggers:
        this.on_schedule_model_create = [];
    };
    ScheduleBackboneCollectionFactory.prototype = {
        getTimeMatrixDocument: function() {
            return this.timematrix_document;
        },
        _get_schedule_model_data: function(schedule_table) {
            schedule_data = schedule_table.getScheduleDataMap();
            var schedule_model_data = {
                schedule_date: schedule_table.getScheduleDateObj(),
                timeslots_available: schedule_data.timeslot_column_index
            };
            return schedule_model_data;
        },
        _on_schedule_model_create: function(schedule_model, index) {
            for (var i = 0, len = this.on_schedule_model_create.length; i < len; i++) {
                this.on_schedule_model_create[i].call(this, schedule_model, this.timematrix_document, index);
            }
        },
        _create_schedule_collection: function() {
            this._schedule_collection = new GlobalCapacity.TimeMatrix.Backbone.Models.ScheduleCollection();
            var schedule_tables = this.timematrix_document.getSchedules(); //array of GlobalCapacity.TimeMatrix.DOM.ScheduleTable
            var schedule_model;
            for (var i = 0, len = schedule_tables.length; i < len; i++) {
                schedule_model = this._schedule_collection.add(this._get_schedule_model_data(schedule_tables[i]));
                this._on_schedule_model_create(schedule_model, i);
            }
        },
        onScheduleModelCreate: function(callback) {
            this.on_schedule_model_create.push(callback);
        },
        getScheduleCollection: function() {
            if(!this._schedule_collection) {
                this._create_schedule_collection();
            }
            return this._schedule_collection;
        }
    };

    /**
     *
     * ================Model Factory=========================================
     * This is all tied to schedule_maps_iterator - depends on timematrix_document and schedule_index
     */
    var ScheduleModelComponentsFactory = function(schedule_model, timematrix_document, schedule_index) {
        this.schedule_model = schedule_model;
        this.timematrix_document = timematrix_document;
        this.schedule_index = schedule_index;

        //Generated:
        this.schedule_maps_iterator = this.getScheduleMapsIterator();
        this.task_model_index = {};

        //Callbacks:
        this.on_tech_schedule = [];
        this.on_tech = [];
        this.on_timeslot_collection = [];
        this.after_timeslot = [];
        this.on_task = [];

        //State:
        this.current_tech_schedule_model = null;
        this.current_tech_model = null;
        this.current_timeslot_collection = null;
        this.current_timeslot = null;
        this.current_task = null;

        //=============Setup Callbacks===============================
        this.schedule_maps_iterator.on_schedule_row.push(this.onScheduleRow.bind(this));
        this.schedule_maps_iterator.on_timeslot.push(this.onTimeslot.bind(this));
        this.schedule_maps_iterator.after_timeslot.push(this.afterTimeslot.bind(this));
        this.schedule_maps_iterator.on_task.push(this.onTask.bind(this));
    };
    ScheduleModelComponentsFactory.prototype = {
        getTimeMatrixDocument: function() {
            return this.timematrix_document;
        },
        getScheduleMapsIterator: function() {
            return this.getTimeMatrixDocument().getScheduleMapsIterator(this.schedule_index);
        },
        createTechSchedule: function() {
            var tech_schedule_model = this.schedule_model.getTechScheduleCollection().add({
                tech_alias: this.schedule_maps_iterator.getTech()
            });
            this.current_tech_schedule_model = tech_schedule_model;
            for (var i = 0, len = this.on_tech_schedule.length; i < len; i++) {
                this.on_tech_schedule[i](this);
            }
            return tech_schedule_model;
        },
        createTechModel: function(tech_schedule) {
            var tech_model = tech_schedule.getTech();
            this.current_tech_model = tech_model;
            for (var i = 0, len = this.on_tech.length; i < len; i++) {
                this.on_tech[i](tech_model, this);
            }
            return tech_model;
        },
        createTimeslotCollection: function(tech_schedule) {
            var timeslot_collection = tech_schedule.getTimeslotCollection();
            this.current_timeslot_collection = timeslot_collection;
            for (var i = 0, len = this.on_timeslot_collection.length; i < len; i++) {
                this.on_timeslot_collection[i](this);
            }
            return timeslot_collection;
        },
        onScheduleRow: function(schedule_maps_iterator) {
            var tech_schedule_model = this.createTechSchedule();
            this.createTechModel(tech_schedule_model);
            this.createTimeslotCollection(tech_schedule_model);
        },
        getTaskDataArray: function(schedule_maps_iterator) {
            var task_array = schedule_maps_iterator.getTimeslotTaskArray();
            if (!task_array || !task_array.length || task_array.length === 0) {
                return false;
            }
            return task_array;
        },
        checkTimeslotHasTasks: function(schedule_maps_iterator) {
            var task_array = this.getTaskDataArray(schedule_maps_iterator);
            return task_array !== false;
        },
        checkTimeslotHasDispatchTasks: function(task_array) {
            var has_dispatch_tasks = false;
            var task_data;
            for (var i = 0, len = task_array.length; i < len; i++) {
                task_data = task_array[i];
                if (task_data.task_type !== 'block') {
                    has_dispatch_tasks = true;
                    break;
                }
            }
            return has_dispatch_tasks;
        },
        onTimeslot: function(schedule_maps_iterator) {
            this.current_timeslot = null;
            if (this.checkTimeslotHasTasks(schedule_maps_iterator)) {
                //Create TimeslotModel
                var timeslot_text = schedule_maps_iterator.getTimeslot();
                this.current_timeslot = this.current_timeslot_collection.add({
                    timeslot: timeslot_text
                });
            }
        },
        afterTimeslot: function(schedule_maps_iterator) {
            if(!this.current_timeslot) {
                return;
            }
            for (var i = 0, len = this.after_timeslot.length; i < len; i++) {
                this.after_timeslot[i](this.current_timeslot, this);
            }
        },
        indexTaskModel: function(task_model) {
            if (!this.task_model_index[task_model.get('task_type')]) {
                this.task_model_index[task_model.get('task_type')] = {};
            }
            var task_type_index = this.task_model_index[task_model.get('task_type')];
            if (!task_type_index[task_model.get('task_id')]) {
                task_type_index[task_model.get('task_id')] = [];
            }
            task_type_index[task_model.get('task_id')].push(task_model);
        },
        onTask: function(schedule_maps_iterator) {
            //Create TaskModel
            var task_collection = this.current_timeslot.getTaskCollection();
            var task_data = schedule_maps_iterator.getTimeslotTaskData();
            var task = task_collection.add(task_data);
            if(task.get('task_type') === 'block') {
                task.dispatch_task = false;
            }
            this.indexTaskModel(task);
            for (var i = 0, len = this.on_task.length; i < len; i++) {
                this.on_task[i](task, this);
            }
        },
        generateScheduleComponentsModels: function(callback) {
            this.schedule_maps_iterator.iterateSchedule();
            if(callback) callback(this.schedule_model);
        }
    };


    /**
     *
     * ================Views Factory=========================================
     */
    var ScheduleModelComponentsViewsFactory = function(schedule_model_components_factory) {
        this.schedule_model_components_factory = schedule_model_components_factory;

        this.on_tech_view = [];
        this.on_task_view = [];

        //=============Setup Callbacks To Model Factory=============================
        this.schedule_model_components_factory.on_tech.push(this.onTechModel.bind(this));
        this.schedule_model_components_factory.on_task.push(this.onTaskModel.bind(this));
        this.schedule_model_components_factory.after_timeslot.push(this.afterTimeslot.bind(this));
    };
    ScheduleModelComponentsViewsFactory.prototype = {

        onTechModel: function(tech_model, schedule_model_components_factory) {
            //DOMTableWrapper.Cell
            var tech_cell = schedule_model_components_factory.schedule_maps_iterator.getTechCell();
            tech_cell.empty();
            var tech_view = new GlobalCapacity.TimeMatrix.Backbone.Views.TechCellView({model: tech_model});
            $(tech_cell.getElem()).html(tech_view.el);

            for (var i = 0, len = this.on_tech_view.length; i < len; i++) {
                this.on_tech_view[i](tech_view, tech_model, this);
            }
        },
        onTaskModel: function(task_model, schedule_model_components_factory) {
        },
        createBlockTaskView: function(task_model) {
            var view = new GlobalCapacity.TimeMatrix.Backbone.Views.BlockTaskView({model: task_model});
            return view;
        },
        createDispatchTaskView: function(task_model) {
            var task_view = new GlobalCapacity.TimeMatrix.Backbone.Views.DispatchTaskView({model: task_model});

            for (var i = 0, len = this.on_task_view.length; i < len; i++) {
                this.on_task_view[i](task_view, task_model, this);
            }

            return task_view;
        },
        createTaskView: function(task_model) {
            if(task_model.isDispatchTask()) {
                return this.createDispatchTaskView(task_model);
            } else {
                return this.createBlockTaskView(task_model);
            }
        },
        afterTimeslot: function(timeslot_model, schedule_model_components_factory) {
            //DOMTableWrapper.Cell
            var timeslot_cell = schedule_model_components_factory.schedule_maps_iterator.getTimeslotCell();
            var $timeslot_cell = $(timeslot_cell.getElem());
            timeslot_cell.empty();

            var task_collection = timeslot_model.getTaskCollection();
            var task_view;
            var task_num = 1;
            var self = this;
            task_collection.each(function add_task_views_for_timeslot(task_model){
                if(task_num > 1) {
                    //add a divider
                    $timeslot_cell.append('<hr class="task-divider">');
                }
                task_view = self.createTaskView(task_model);
                $timeslot_cell.append(task_view.el);
                task_num++;
            });

        }
    };

    var ScheduleDateCellBackboneViewFactory = function(schedule_backbone_collection_factory) {
        this.schedule_backbone_collection_factory = schedule_backbone_collection_factory;

        //=============Setup Callbacks To Collection Factory=============================
        schedule_backbone_collection_factory.onScheduleModelCreate(this.createScheduleDateCellView);
    };
    ScheduleDateCellBackboneViewFactory.prototype = {
        createScheduleDateCellView: function(schedule_model, timematrix_document, schedule_index) {
            //scope of 'this' is ScheduleBackboneCollectionFactory
            var date_cell_view = new GlobalCapacity.TimeMatrix.Backbone.Views.ScheduleDateCellView({model: schedule_model});
            var schedule_table = timematrix_document.getSchedule(schedule_index);
            var date_row_obj = schedule_table.schedule_table_dom.getDateRow();
            var date_cell = date_row_obj.getCell(0);

            $(date_cell.getElem()).html(date_cell_view.el);
        }
    };
    return {
        ScheduleBackboneCollectionFactory: ScheduleBackboneCollectionFactory,
        ScheduleModelComponentsFactory: ScheduleModelComponentsFactory,
        ScheduleModelComponentsViewsFactory: ScheduleModelComponentsViewsFactory,
        ScheduleDateCellBackboneViewFactory: ScheduleDateCellBackboneViewFactory
    };


})();