'use strict';
if(typeof DOMTableWrapper == 'undefined') {
	throw 'DOMTableWrapper is required.';
}
if(typeof DOMElements == 'undefined') {
	throw 'DOMElements is required.';
}


/*
DOM hierarchy:

	Document:
		ScheduleTables:
			ScheduleTable:
				DateRow
				TimeslotHeadingsRow
				ScheduleRows
					ScheduleRow:
						TechCell
						TimeslotCells
							TimeslotCell
								TaskTables

*/

if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

GlobalCapacity.TimeMatrix.DOM = (function() {

	/**
	 *
	 *
	 * ============================Document (main entry point)=======================================
	 */

	var Document = function Document(document_elem) {
		this.elem = document_elem;
        //Internal:
		this.content_div = null;
		this.parameters_elem = null;
		this.schedule_table_elems_array = null;
		this.schedule_tables = [];
    };
    Document.prototype = {
        isTimeMatrixDocument: function() {
            try {
                this.getParametersElem();
            } catch(err) {
                console.log(err.toSource());
                return false;
            }
            return true;
        },

		getContentDiv: function getContentDiv() {
			//content div is first div child which has attribute align=center
			if (this.content_div === null) {
				var child_elems = this.elem.childNodes;
				for (var i = 0, len = child_elems.length; i < len; i++) {
					if (child_elems[i].nodeName === 'DIV' && child_elems[i].align === 'center') {
						this.content_div = child_elems[i];
					}
				}
			}
            if(!this.content_div) {
                throw "Content Div not found";
            }
			return this.content_div;
		},

		getParametersElem: function getParametersElem() {
			if(this.parameters_elem === null) {
				var parameters_table = this.getContentDiv().querySelector('table[name="tableData"]');
				if (parameters_table === null) {
					throw "Cannot find paramters table";
				}
				var parameters_elems = parameters_table.getElementsByTagName('h5');
				if (parameters_elems.length === 0) {
					throw "Cannot find parameters element";
				}
				this.parameters_elem = parameters_elems[0];
			}
			return this.parameters_elem;
		},

		getScheduleTableElemsArray: function getScheduleTableElemsArray() {
			//schedule tables are all the tables in content_div except the first one
			if(this.schedule_table_elems_array === null) {
				this.schedule_table_elems_array = [];
				var child_elems = this.getContentDiv().childNodes;
				for (var i = 0, len = child_elems.length; i < len; i++) {
					if (child_elems[i].nodeName === 'TABLE') {
						this.schedule_table_elems_array.push(child_elems[i]);
					}
				}
				//remove the first table:
				this.schedule_table_elems_array.shift();
			}
			return this.schedule_table_elems_array;
		},

		countScheduleTables: function countScheduleTables() {
			return this.getScheduleTableElemsArray().length;
		},

		getScheduleTable: function getScheduleTable(schedule_table_index) {
			if(this.schedule_tables[schedule_table_index] === undefined) {
				var table_elem = this.getScheduleTableElemsArray()[schedule_table_index];
				if( table_elem === undefined) {
					throw "Schedule Table Index " + schedule_table_index + " is not valid";
				}
				this.schedule_tables[schedule_table_index] = new ScheduleTable(table_elem);
			}
			return this.schedule_tables[schedule_table_index];
		}
	};

	/**
	 *
	 *
	 * ============================Schedule Table=================================================
	 */

	var ScheduleTable = function ScheduleTable(table_elem) {
		if (!table_elem.nodeName || table_elem.nodeName !== "TABLE") {
			throw "Need to pass a table element";
		}
		this.elem = table_elem;
		/**
		 *
		 * options:
		 */
		this.schedule_rows_begin = 5;
		this.schedule_rows_end = -6;
		this.date_row_index = 0;
		this.timeslot_row_index = 1;
		/**
		 *
		 * calculated:
		 */
		this.table_wrapper = new DOMTableWrapper.Table(this.elem);
		this.date_row = null;
		this.date_text = null;
        this.date_obj = null;
		this.timeslot_headings_row = null;
		this.timeslot_headings_array = null;
		this.schedule_dom_map = null;

		/**
		 * @returns {DOMTableWrapper.Row}
		 */
		this.getDateRow = function getDateRow() {
			if(this.date_row === null) {
				this.date_row = this.table_wrapper.getRow(this.date_row_index);
			}
			return this.date_row;
		};

		/**
		 * Returns text of date cell
		 * @return {string}
		 */
		this.getDateText = function getDateText() {
			if(this.date_text === null) {
				var cell_obj = this.getDateRow().getCell(0);
				var regexp = /Schedule Date :\s(.+?)\s/i;
				var result = DOMElements.Filters.getElementRegexpResult(cell_obj.getElem(), regexp);
				if(!result[1]) {
					this.date_text = null;
				} else {
					this.date_text = result[1];
				}
			}
			return this.date_text;
		};

        this.getDateObj = function getDateObj() {
            if(this.date_obj === null) {
                var date_text = this.getDateText();
                var date_parts = date_text.split('/');
                this.date_obj = new Date(0,0,0,0,0,0,0);
                this.date_obj.setUTCFullYear(parseInt(date_parts[2]));
                this.date_obj.setUTCMonth(parseInt(date_parts[0]) - 1);
                this.date_obj.setUTCDate(parseInt(date_parts[1]));
            }
            return this.date_obj;
        };

		/**
		 * Returns DOMTableWrapper.Row obj
		 * @return {DOMTableWrapper.Row}
		 */
		this.getTimeslotHeadingsRow = function getTimeslotHeadingsRow() {
			if(this.timeslot_headings_row === null) {
				this.timeslot_headings_row = this.table_wrapper.getRow(this.timeslot_row_index);
			}
			return this.timeslot_headings_row;
		};

		/**
		 * @return {array} of string
		 */
		this.getTimeslotHeadings = function getTimeslotHeadings() {
			if(this.timeslot_headings_array === null) {
				this.timeslot_headings_array = this.getTimeslotHeadingsRow().map(DOMElements.Filters.getElementCleanText);
				//remove the first one:
				this.timeslot_headings_array.shift();
			}
			return this.timeslot_headings_array;
		};

		/**
		 * @return {DOMTableWrapper.Table}
		 */
		this.getTableWrapper = function getTableWrapper() {
			return this.table_wrapper;
		};

		this.getScheduleDOMMap = function getScheduleDOMMap() {
			if(this.schedule_dom_map === null) {
				var factory = new ScheduleTable_DOM_Map_Factory(this.getTableWrapper());
				factory.schedule_rows_begin = this.schedule_rows_begin;
				factory.schedule_rows_end = this.schedule_rows_end;
				this.schedule_dom_map = factory.getScheduleDOMMap();
				this.schedule_dom_map.timeslot_column_index = this.getTimeslotHeadings();
			}
			return this.schedule_dom_map;
		};
	};

	/**
	 *
	 *
	 * ============================Schedule DOM Map Factory===============================================
	 */

    /**
     * Generates dom_map from DOMTableWrapper.Table object
     * Also does some preliminary styling to the table
     * uses DOMTableWrapper.TableProcessor
     * @param {DOMTableWrapper.Table} table_wrapper
     */
    function ScheduleTable_DOM_Map_Factory(table_wrapper) {
        this.table_wrapper = table_wrapper;
        this.schedule_rows_begin =0;
        this.schedule_rows_end = 0;

        var schedule_dom_map = {
            timeslot_column_index: [], //array[timeslot cells length] of string
            schedule_row_index: [], //array[schedule_rows.length] of string
            schedule_rows: [], //array of TableWrapper.Row
            tech_cells: [], //array[schedule_rows.length] of TableWrapper.Cell
            timeslot_cells: [] ////array[schedule_rows.length] of array[occupied timeslot cells] of TableWrapper.Cell
        };

        var current_row = 0;

        var RowCellsProcess_ScheduleRow_TechCells = {
            cell_begin: 0,
            cell_end: 1,
            beforeCells : function(cell_wrappers_array, row_wrapper){},
            processCell: function(cell_wrapper) {
				//preliminary styleing:
                cell_wrapper.addClass('tech-cell');

                var tech_name = cell_wrapper.getCleanText();
                schedule_dom_map.schedule_row_index.push(tech_name);
                schedule_dom_map.tech_cells.push(cell_wrapper);
            },
            afterCells: function(){},
        };

        var RowCellsProcess_ScheduleRow_TimeslotCells = {
            cell_begin: 1,
            cell_end: -1,
            timeslot_cells: null,
            col_index: 0,
            beforeCells : function(cell_wrappers_array, row_wrapper){
                this.timeslot_cells = [];
                this.col_index = -1;
            },
            processCell: function(cell_wrapper) {
                this.col_index++;
                cell_wrapper.addClass('timeslot-cell');
                var cell_elem = cell_wrapper.getElem();
                if(this.isEmpty(cell_elem)) {
                    cell_wrapper.addClass('empty');
                    cell_wrapper.empty();
                } else {
                    cell_wrapper.addClass('occupied');
                    this.removeBgColors(cell_elem);
                    this.timeslot_cells[this.col_index] = cell_wrapper;
                }
            },
            afterCells: function(){
                schedule_dom_map.timeslot_cells[current_row] = this.timeslot_cells;
            },
            removeBgColors: function(cell_elem) {
                var row_elems = cell_elem.getElementsByTagName('tr');
                for(var i=0,len=row_elems.length;i<len;i++) {
                    row_elems[i].bgColor = null;
                }
            },
            isEmpty: function(cell_elem) {
                var table_elem = cell_elem.children[0];
                var table_rows = table_elem.rows;
                return table_rows.length <2;
            }
        };

        var TableRowsProcess_ScheduleRows = {
            cells_processes: [
                RowCellsProcess_ScheduleRow_TechCells,
                RowCellsProcess_ScheduleRow_TimeslotCells
            ],
            row_begin:0,
            row_end: 0,
            current_row: 0,
            beforeRows : function(row_wrappers_array, table_wrapper) {
                this.current_row = -1;
            },
            processRow : function(row_wrapper) {
                this.current_row++;
                current_row = this.current_row;

                schedule_dom_map.schedule_rows[current_row] = row_wrapper;

                row_wrapper.addClass('schedule-row');
                if(this.current_row % 2 === 0) {
                    row_wrapper.addClass('even');
                } else {
                    row_wrapper.addClass('odd');
                }
            },
            afterRows: function() {
            }
        };

        this.getScheduleDOMMap = function() {
            //can only be run once:
            if(schedule_dom_map.schedule_row_index.length === 0) {
                var table_processor = new DOMTableWrapper.TableProcessor(this.table_wrapper);
                TableRowsProcess_ScheduleRows.row_begin = this.schedule_rows_begin;
                TableRowsProcess_ScheduleRows.row_end = this.schedule_rows_end;
                table_processor.process(TableRowsProcess_ScheduleRows);
            }
            return schedule_dom_map;
        };

    }

	/**
	 *
	 *
	 * ============================Return Values===============================================
	 */

	return {
		Document: Document
	};

})();