//if (typeof Backbone == 'undefined') {
//    throw 'Backbone is required.';
//}
if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

/**
 * Models:
 *
 * ScheduleCollection
 *   ScheduleModel
 *     TechScheduleCollection
 *       TechScheduleModel
 *         TechModel
 *         TimeslotCollection
 *           TimeslotModel
 *             TaskCollection
 *               TaskModel
 */

if (typeof GlobalCapacity.TimeMatrix.Backbone == 'undefined') {
    GlobalCapacity.TimeMatrix.Backbone = {};
}


GlobalCapacity.TimeMatrix.Backbone.Models = (function() {

    /**
     *
     *
     * ============================Extend Backbone=================================================
     * fullExtend: a better way to extend Bacbone objects. From: https://coderwall.com/p/xj81ua
     */
    (function(Model) {
        'use strict';
        // Additional extension layer for Models
        Model.fullExtend = function(protoProps, staticProps) {
            // Call default extend method
            var extended = Model.extend.call(this, protoProps, staticProps);
            // Add a usable super method for better inheritance
            extended._super = this.prototype;
            // Apply new or different defaults on top of the original
            if (protoProps.defaults) {
                for (var k in this.prototype.defaults) {
                    if (!extended.prototype.defaults[k]) {
                        extended.prototype.defaults[k] = this.prototype.defaults[k];
                    }
                }
            }
            return extended;
        };

    })(Backbone.Model);

    (function(View) {
        'use strict';
        // Additional extension layer for Views
        View.fullExtend = function(protoProps, staticProps) {
            // Call default extend method
            var extended = View.extend.call(this, protoProps, staticProps);
            // Add a usable super method for better inheritance
            extended._super = this.prototype;
            // Apply new or different events on top of the original
            if (protoProps.events) {
                for (var k in this.prototype.events) {
                    if (!extended.prototype.events[k]) {
                        extended.prototype.events[k] = this.prototype.events[k];
                    }
                }
            }
            return extended;
        };

        //from: http://ianstormtaylor.com/assigning-backbone-subviews-made-even-cleaner/
        View.prototype.assign = function (selector, view) {
            var selectors;
            if (_.isObject(selector)) {
                selectors = selector;
            }
            else {
                selectors = {};
                selectors[selector] = view;
            }
            if (!selectors) return;
            _.each(selectors, function (view, selector) {
                view.setElement(this.$(selector)).render();
            }, this);
        };


    })(Backbone.View);

    /**
     *
     *
     * ============================MODELS=================================================
     */

    /**
     *
     * TaskModel:
     */
    var TaskModel = Backbone.Model.extend({
        defaults: {
            zip_code: null,
            zip_code_suffix: null,
            clli_code: null,
            task_type: null,
            task_id: null,
            task_seq: null,
            task_url: null,
            task_status: null,
            time_range: null,
            start_time: null,
            end_time: null,
            service_type: null
        },
        getTimeslot: function() {
            return this.collection.getTimeslot();
        },
        dispatch_task: true,
        isDispatchTask: function() {
            return this.dispatch_task;
        }
    });

    /**
     *
     * TaskCollection:
     */
    var TaskCollection = Backbone.Collection.extend({
        model: TaskModel,
        timeslot: null,
        getTimeslot: function() {
            return this.timeslot;
        },
        importFullJSON: function(json_data) {
            var task_model;
            for(var i=0, len = json_data.length; i<len; i++) {
                task_model = this.add(json_data[i]);
            }
        }
    });

    /**
     *
     * BlockTaskModel:
     */
    var BlockTaskModel = Backbone.Model.extend({
        defaults: {
            zip_code: null,
            zip_code_suffix: null,
            clli_code: null,
            task_type: null,
            task_id: null,
            task_seq: null,
            task_url: null,
            task_status: null,
            time_range: null,
            start_time: null,
            end_time: null,
            service_type: null
        }
    });

    /**
     *
     * TechModel:
     */
    var TechModel = Backbone.Model.extend({
        defaults: {
            tech_alias: null
        }
    });

    /**
     *
     * TimeslotModel:
     */
    var TimeslotModel = Backbone.Model.extend({
        defaults: {
            timeslot: null
        },
        initialize: function() {
            //properties
            task_collection = null;  //TaskCollection
        },
        getTaskCollection: function() {
            if(!this.task_collection) {
                this.task_collection = new TaskCollection();
                this.task_collection.timeslot = this;
            }
            return this.task_collection;
        },
        getTasksCount: function() {
            return this.getTaskCollection().length;
        },
        getTechSchedule: function() {
            return this.collection.getTechSchedule();
        },
        toFullJSON: function() {
            var json = this.toJSON();
            json.task_collection = this.task_collection.toJSON();
            return json;
        },
        importFullJSON: function(json_data) {
            this.set('timeslot', json_data.timeslot);
            var task_collection = this.getTaskCollection();
            task_collection.importFullJSON(json_data.task_collection);
        }
    });

    /**
     *
     * TimeslotCollection:
     */
    var TimeslotCollection = Backbone.Collection.extend({
        model: TimeslotModel,
        tech_schedule: null, //parent
        getTechSchedule: function() {
            return this.tech_schedule;
        },
        toFullJSON: function() {
            var json = [];
            this.each(function(timeslot_model){
                json.push(timeslot_model.toFullJSON());
            });
            return json;
        },
        importFullJSON: function(json_data) {
            var timeslot_model;
            for(var i=0, len = json_data.length; i<len; i++) {
                timeslot_model = this.add({});
                timeslot_model.importFullJSON(json_data[i]);
            }
        }
    });

    /**
     *
     * TechScheduleModel:
     */
    var TechScheduleModel = Backbone.Model.extend({
        defaults: {
            tech_alias: null
        },
        initialize: function() {
            //properties
            this.tech_model = null; //TechModel
            this.timeslot_collection = null; //TimeslotCollection
        },
        setTech: function(tech_model) {
            this.tech_model = tech_model;
        },
        getTech: function() {
            if(!this.tech_model) {
                this.tech_model = new TechModel({tech_alias: this.get('tech_alias')});
            }
            return this.tech_model;
        },
        getTimeslotCollection: function() {
            if(!this.timeslot_collection) {
                this.timeslot_collection = new TimeslotCollection();
                this.timeslot_collection.tech_schedule = this;
            }
            return this.timeslot_collection;
        },
        getSchedule: function() {
            return this.collection.getSchedule();
        },
        toFullJSON: function() {
            var json = this.toJSON();
            json.tech_model = this.tech_model.toJSON();
            json.timeslot_collection = this.timeslot_collection.toFullJSON();
            return json;
        },
        importFullJSON: function(json_data) {
            this.set('tech_alias', json_data.tech_alias);
            this.getTech(); //initialize tech model
            var timeslot_collection = this.getTimeslotCollection();
            timeslot_collection.importFullJSON(json_data.timeslot_collection);
        }
    });

    /**
     *
     * TechScheduleCollection:
     */
    var TechScheduleCollection = Backbone.Collection.extend({
        model: TechScheduleModel,
        schedule: null, //parent
        getSchedule: function() {
            return this.schedule;
        },
        toFullJSON: function() {
            var json = [];
            this.each(function(tech_schedule_model){
                json.push(tech_schedule_model.toFullJSON());
            });
            return json;
        },
        importFullJSON: function(json_data) {
            var tech_schedule_model;
            for(var i=0, len = json_data.length; i<len; i++) {
                tech_schedule_model = new TechScheduleModel();
                tech_schedule_model.importFullJSON(json_data[i]);
                this.add(tech_schedule_model);
            }
        }
    });

    /**
     *
     * ScheduleModel:
     */
    var ScheduleModel = Backbone.Model.extend({
        defaults: {
            limit_params: {}, //object of limits. ie {regions: ['ATL','BOS']}
            schedule_date: null, //Date
            techs_available: [], //array of strings
            timeslots_available: [], //array of strings
            number_of_techs: 0,
            number_of_tasks: 0
        },
        initialize: function() {
            this.tech_schedule_collection = new TechScheduleCollection();
            this.tech_schedule_collection.schedule = this;
            this.tech_schedule_collection.bind("add", function(tech_schedule_model, tech_schedule_collection, options){
                //'this' is ScheduleModel
                var techs_available = this.get('techs_available');
                techs_available.push(tech_schedule_model.get('tech_alias'));
                this.set('techs_available', techs_available);
                this.set('number_of_techs', this.get('number_of_techs')+1);
                tech_schedule_model.getTimeslotCollection().bind("add", function(timeslot_model){
                    //TODO: can probably bind the below directly to timeslot_model
                    //TODO: timealot_model.on("increment_number_of_tasks")
                    timeslot_model.getTaskCollection().bind("add", function(task_model){
                        this.set('number_of_tasks', this.get('number_of_tasks')+1);
                    }, this);
                }, this);
            }, this);
        },
        getTechScheduleCollection: function() {
            return this.tech_schedule_collection;
        },
        getISODate: function() {
            var iso_string = this.get('schedule_date').toISOString();
            var parts = iso_string.split('T');
            return parts[0];
        },
        toFullJSON: function() {
            var json = this.toJSON();
            json.tech_schedule_collection = this.tech_schedule_collection.toFullJSON();
            return json;
        },
        importFullJSON: function(json_data) {
            for(var key in this.defaults) {
                if(this.defaults.hasOwnProperty(key) && json_data.hasOwnProperty(key)) {
                    switch(key) {
                        case 'schedule_date':
                            this.set(key, new Date(json_data[key]));
                            break;
                        default:
                            this.set(key, json_data[key]);
                    }
                }
            }
            this.tech_schedule_collection.importFullJSON(json_data.tech_schedule_collection);
        }
    });

    /**
     *
     * ScheduleCollection:
     */
    var ScheduleCollection = Backbone.Collection.extend({
        model: ScheduleModel,
        toFullJSON: function() {
            var json = [];
            this.each(function(schedule_model){
                json.push(schedule_model.toFullJSON());
            });
            return json;
        },
    });

    return {
        ScheduleCollection: ScheduleCollection
     };


})();