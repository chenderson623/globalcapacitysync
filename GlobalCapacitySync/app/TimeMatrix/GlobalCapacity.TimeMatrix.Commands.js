if (typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}


GlobalCapacity.TimeMatrix.Commands = GlobalCapacity.TimeMatrix.Commands || {};


/**
 *
 *
 */
GlobalCapacity.TimeMatrix.Commands.WindowSyncAll = (function() {
    'use strict';
    var SyncAll = function(oss, task_control_collection) {
        this.oss = oss;
        this.task_control_collection = task_control_collection;
        this.stop = false;
        this.emitter = oss.newEmitter();

        //loaded below:
        this.task_documents = null;

    };
    SyncAll.prototype = {
        execute: function() {
            var self = this;
            this.count = -1;

            //preload stuff:
            self.oss.getGlobalCapacityTaskDocuments(function(task_documents) {
                self.task_documents = task_documents;
                var load_commands = task_documents.loadCommands();
                task_documents.loadTaskDocumentWindowController(function(window_controller) {
                    self.window_controller = window_controller;
                    load_commands.then(function(){
                        self.emitter.emit('start');
                        self.open_next();
                    });
                });
            });
        },
        quit: function() {
            this.stop = true;
        },
        open_window: function(task_model, callback) {
            var controller = new this.window_controller();
            console.log("WINDOW CONTROLLER", controller);
            this.emitter.emit('open_window', task_model);

            var task_url = 'http://testtimematrix.local.dev/TT4166632.htm';
            //var task_url = task_control_model.timematrix_task.task_url;
            controller.openChildWindow(task_url);
            var self = this;
            controller.getTaskData(function(task_data) {
                self.emitter.emit('task_data', task_data);
                controller.closeChildWindow();
                callback();
            });
        },

        open_next: function() {
            console.log("OPEN NEXT", this);
            this.count++;
            var task_control_model, task_model;
            if (this.stop !== true && this.count < this.task_control_collection.length) {
                task_control_model = this.task_control_collection.at(this.count);

                task_model = task_control_model.timematrix_task;
                var sync_window_command = new GlobalCapacity.TaskDocuments.Commands.OpenTaskDocumentWindowController(task_model);
                sync_window_command.emitter.on('close_window', this.open_next.bind(this));
                sync_window_command.emitter.on('task_data', this.handle_task_data.bind(this, sync_window_command, task_control_model));
                sync_window_command.execute();
                //TODO: remove
                if (this.count > 4) {
                    this.stop = true;
                }
            }
            this.emitter.emit('done');
        },

        handle_task_data: function(sync_window_command, task_control_model, task_data) {
            var save_new_task_command = new GlobalCapacity.TaskDocuments.Commands.SaveNewTask(task_data);
            save_new_task_command.emitter.on('saved', function(){
                sync_window_command.close_window();

                //TODO: replace with real
                task_control_model.setFieldPortalJob(new Backbone.Model({workorder_id: 12345}));
            });
            save_new_task_command.execute();
        }

    };

    return SyncAll;
})();
