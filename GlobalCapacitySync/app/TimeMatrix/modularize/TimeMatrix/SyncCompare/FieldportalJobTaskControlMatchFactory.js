(function(){
    //plain object.
    //no dependencies.
    FieldportalJobTaskControlMatchFactory = {
        fieldportal_log: null,
        matchTaskId: function(timematrix_task, fieldportal_job) {
            return parseInt(timematrix_task.get('task_id')) === parseInt(fieldportal_job.get('ContractRefNum'));
        },
        matchTaskType: function(timematrix_task, fieldportal_job) {
            var match_job_type = FieldPortal.JobTypesForAbbrev.getJobType(timematrix_task.get('task_type'));
            return match_job_type === fieldportal_job.get('JobType');
        },
        hasSchedSeq: function(fieldportal_job) {
            var fieldportal_seq = parseInt(fieldportal_job.get('SchedSeq'));
            if (fieldportal_seq === 0) {
                return false;
            }
            return true;
        },
        matchTaskSeq: function(timematrix_task, fieldportal_job) {
            if (!SyncCompare.FieldportalJobTaskControlMatchFactory.hasSchedSeq(fieldportal_job)) {
                return null;
            }
            return parseInt(timematrix_task.get('task_seq')) === parseInt(fieldportal_job.get('SchedSeq'));
        },
        matchTech: function(task_control_model, fieldportal_job) {
            var tech_control = task_control_model.getTechControlModel();
            var task_fieldportal_tech = tech_control.getFieldportalTech();
            if (!task_fieldportal_tech) {
                return null;
            }
            return task_fieldportal_tech.get('tech_name') === fieldportal_job.get('AssignedTechName');
        },
        matchDate: function(timematrix_task, fieldportal_job) {
            var timeslot = timematrix_task.getTimeslot();
            var tech_schedule = timeslot.getTechSchedule();
            var schedule_model = tech_schedule.getSchedule();
            var iso_date = schedule_model.getISODate();
            return iso_date === fieldportal_job.get('AssignedDate');
        },
        //TODO implement:
        markFieldportalJobWasMoved: function(timematrix_task, fieldportal_job) {
            //determines if job was moved to fieldportal job was moved to tech
            //only meaninful if tech does not match
            if (SyncCompare.FieldportalJobTaskControlMatchFactory.fieldportal_log) {

            }
        },
        markFieldportalJobWasCreated: function(timematrix_task, fieldportal_job) {
            //determines if job was created in fieldportal by tech
            //only meaninful if tech does not match. don't want to match it as it might not be right

        },
        getMatch: function(task_control_model, fieldportal_job) {
            var match = new SyncCompare.FieldportalJobTaskControlMatch();
            var timematrix_task = task_control_model.timematrix_task;
            match.task_id = SyncCompare.FieldportalJobTaskControlMatchFactory.matchTaskId(timematrix_task, fieldportal_job);
            match.task_type = SyncCompare.FieldportalJobTaskControlMatchFactory.matchTaskType(timematrix_task, fieldportal_job);
            match.task_seq = SyncCompare.FieldportalJobTaskControlMatchFactory.matchTaskSeq(timematrix_task, fieldportal_job);
            match.tech = SyncCompare.FieldportalJobTaskControlMatchFactory.matchTech(task_control_model, fieldportal_job);
            if (!match.tech) {
                match.fieldportal_moved = SyncCompare.FieldportalJobTaskControlMatchFactory.markFieldportalJobWasMoved(task_control_model, fieldportal_job);
                match.fieldportal_created = SyncCompare.FieldportalJobTaskControlMatchFactory.markFieldportalJobWasCreated(task_control_model, fieldportal_job);
            }
            match.date = SyncCompare.FieldportalJobTaskControlMatchFactory.matchDate(timematrix_task, fieldportal_job);
            return match;
        }
    };
    return FieldportalJobTaskControlMatchFactory;
})();