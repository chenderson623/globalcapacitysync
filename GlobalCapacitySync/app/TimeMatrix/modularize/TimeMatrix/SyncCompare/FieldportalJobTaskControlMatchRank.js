(function(){

    SyncCompare.FieldportalJobTaskControlMatchRank = {
        ranks: {
            task_id: 100000,
            task_type: 10000,
            date: 1000,
            task_seq: 100,
            tech: 10,
            fieldportal_moved: -10000, //cannot match if job was moved to fieldportal_job and task_control tech does not match
            fieldportal_created: -100000 //cannot match if job was created by tech in fieldportal_job and task_control tech does not match
        },
        rank: function(match) {
            var rank = 0;
            for (var match_type in SyncCompare.FieldportalJobTaskControlMatchRank.ranks) {
                if (match[match_type] === true) {
                    rank += SyncCompare.FieldportalJobTaskControlMatchRank.ranks[match_type];
                }
            }
            return rank;
        }
    };
    
})();