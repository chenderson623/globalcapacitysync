(function(){

    require("IDQueue");
    require("FieldportalJobIdIndex");

    SyncCompare.TaskControlCollectionCompare = function(task_control_collection, fieldportal_job_collection) {
        this.task_control_collection = task_control_collection;
        this.fieldportal_job_collection = fieldportal_job_collection;
        //Internal:
        this.fieldportal_job_index = null;
        this.fieldportal_job_queue = null;
        this.task_control_index = null;
        this.task_control_queue = null;
    };
    SyncCompare.TaskControlCollectionCompare.prototype = {
        getFieldportalJobIndex: function() {
            if (this.fieldportal_job_index === null) {
                this.fieldportal_job_index = new GlobalCapacity.TimeMatrix.SyncCompare.FieldportalJobIdIndex();
                this.fieldportal_job_queue = new GlobalCapacity.TimeMatrix.SyncCompare.IDQueue();
                var fieldportal_job;
                for (var i = 0, len = this.fieldportal_job_collection.size(); i < len; i++) {
                    fieldportal_job = this.fieldportal_job_collection.at(i);
                    this.fieldportal_job_index.add(fieldportal_job, i);
                    this.fieldportal_job_queue.add(i);
                }
            }
            return this.fieldportal_job_index;
        },
        getFieldportalJobQueue: function() {
            if (this.fieldportal_job_queue === null) {
                this.getFieldportalJobIndex(); //built together
            }
            return this.fieldportal_job_queue;
        },
        getFieldportalJobQueueArray: function() {
            return this.getFieldportalJobQueue().getQueueArray();
        },
        getTaskControlIndex: function() {
            if (this.task_control_index === null) {
                this.task_control_index = new GlobalCapacity.TimeMatrix.SyncCompare.TaskControlTaskIdIndex();
                this.task_control_queue = new GlobalCapacity.TimeMatrix.SyncCompare.IDQueue();
                var task_control;
                for (var i = 0, len = this.task_control_collection.size(); i < len; i++) {
                    task_control = this.task_control_collection.at(i);
                    this.task_control_index.add(task_control, i);
                    this.task_control_queue.add(i);
                }
            }
            return this.task_control_index;
        },
        getTaskControlQueue: function() {
            if (this.task_control_queue === null) {
                this.getTaskControlIndex();
            }
            return this.task_control_queue;
        },
        /*
         *
         * =======TimeMatrix Methods=============
         */
        getTaskControl: function(task_control_id) {
            return this.task_control_collection.at(task_control_id);
        },
        timematrixHasTaskId: function(task_id) {
            return this.getTaskControlIndex().findTaskId(task_id).length > 0;
        },
        timematrixHasUnmatchedTaskId: function(task_id) {
            return this.getUnmatchedTaskControlIds(task_id).length > 0;
        },
        getUnmatchedTaskControlIds: function(task_id) {
            var task_control_id_array = this.getTaskControlIndex().findTaskId(task_id);
            var return_array = [];
            for (var i = 0, len = task_control_id_array.length; i < len; i++) {
                if (!this.getTaskControlQueue().hasID(task_control_id_array[i])) {
                    continue;
                }
                return_array.push(task_control_id_array[i]);
            }
            return return_array;
        },
        /*
         *
         * =======Fieldportal Methods=============
         */
        getFieldportalJob: function(fieldportal_job_id) {
            return this.fieldportal_job_collection.at(fieldportal_job_id);
        },
        fieldportalHasTaskId: function(task_id) {
            return this.getFieldportalJobIndex().findTaskId(task_id).length > 0;
        },
        fieldportalHasUnmatchedTaskId: function(task_id) {
            return this.getUnmatchedFieldportalJobIds(task_id).length > 0;
        },
        getUnmatchedFieldportalJobIds: function(task_id) {
            var fieldportal_job_id_array = this.getFieldportalJobIndex().findTaskId(task_id);
            var return_array = [];
            for (var i = 0, len = fieldportal_job_id_array.length; i < len; i++) {
                if (!this.getFieldportalJobQueue().hasID(fieldportal_job_id_array[i])) {
                    continue;
                }
                return_array.push(fieldportal_job_id_array[i]);
            }
            return return_array;
        },
        getTaskIdsWithManyFieldportalJobIds: function() {
            return this.getFieldportalJobIndex().getTaskIdsWithMultipleJobs();
        },
        /*
         *
         * =======Main Matching Methods=============
         */
        registerMatch: function(match_item) {
            console.log("registerMatch", match_item.getMatchRank());
            console.log("MATCH ITEM", match_item);
            this.fieldportal_job_queue.removeIdFromQueue(match_item.fieldportal_job_collection_id);
            this.task_control_queue.removeIdFromQueue(match_item.task_control_collection_id);
            //TODO: added this to match. not sure this is the right place yet
            var task_control_model = this.getTaskControl(match_item.task_control_collection_id);
            var fieldportal_job = this.getFieldportalJob(match_item.fieldportal_job_collection_id);
            task_control_model.setFieldPortalJob(fieldportal_job);
            task_control_model.setSyncCompareMatch(match_item);
        },
    };

})();