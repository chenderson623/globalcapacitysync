(function(){

    SyncCompare.FieldportalJobTaskControlMatch = function() {
        this.task_id = false;
        this.task_seq = false;
        this.task_type = false;
        this.tech = false;
        this.date = false;
        //TODO: use these?
        //fieldportal job was moved to this tech
        this.fieldportal_moved = false; //cannot match if job was moved to fieldportal_job and task_control tech does not match
        //fieldportal job was created by this tech
        this.fieldportal_created = false; //cannot match if job was created by tech in fieldportal_job and task_control tech does not match
    };
    SyncCompare.FieldportalJobTaskControlMatch.prototype = {
        matchesTask: function() {
            return this.task_id && this.task_type;
        },
        //TODO: how to use these with fieldportal_moved? how to use with MatchRank?
        matchesTaskAndSeq: function() {
            return this.matchesTask() && this.task_seq;
        },
        matchesThruTech: function() {
            return this.matchesTaskAndSeq() && this.tech;
        }
    };

    
})();