(function(){

    SyncCompare.FieldportalJobTaskControlMatchItem = function(task_control_model, fieldportal_job, fieldportal_job_collection_id, task_control_collection_id) {
        //Tasks to match
        this.task_control_model = task_control_model;
        this.fieldportal_job = fieldportal_job;
        //Need indexes to re-find in collections
        this.fieldportal_job_collection_id = fieldportal_job_collection_id;
        this.task_control_collection_id = task_control_collection_id;
        //These are overrideable
        this.match_rank_method = SyncCompare.FieldportalJobTaskControlMatchRank.rank;
        this.match_method = SyncCompare.FieldportalJobTaskControlMatchFactory.getMatch;
        //Internal:
        this.match = null;
        this.match_rank = null;
    };
    SyncCompare.FieldportalJobTaskControlMatchItem.prototype = {
        getMatch: function() {
            if (this.match === null) {
                this.match = this.match_method(this.task_control_model, this.fieldportal_job);
            }
            return this.match;
        },
        getMatchRank: function() {
            if (this.match_rank === null) {
                this.match_rank = this.match_rank_method(this.getMatch());
            }
            return this.match_rank;
        },
        toString: function() {
            var fieldportal_tech = this.task_control_model.getTechControlModel().getFieldportalTech();
            var fieldportal_tech_name = '';
            if (fieldportal_tech) {
                fieldportal_tech_name = fieldportal_tech.get('tech_name');
            }
            var return_str = "Task ID: tc:" + this.task_control_model.timematrix_task.get('task_id') + " fp:" + this.fieldportal_job.get('ContractRefNum');
            return_str += "\nTaskSeq: tc:" + this.task_control_model.timematrix_task.get('task_seq') + " fp:" + this.fieldportal_job.get('SchedSeq');
            return_str += "\nTech: tc:" + fieldportal_tech_name + " fp:" + this.fieldportal_job.get('AssignedTechName');
            return return_str;
        }
    };
    
})();