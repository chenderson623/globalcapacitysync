(function(){

    SyncCompare.MatchedScoresArrayManyToManyFilter = function(matched_scores_array) {
        this.matched_scores_array = matched_scores_array;
        this.reduced_matched_scores_array = null;
    };
    SyncCompare.MatchedScoresArrayManyToManyFilter.prototype = {
        cloneArray: function(array_to_clone) {
            return JSON.parse(JSON.stringify(array_to_clone));
        },
        _get_best_tc_match_row_index: function(tc_index) {
            var scores = this.reduced_matched_scores_array;
            var max = 0;
            var max_score_index = 0;
            var score;
            for (var fp = 0, len = scores.length; fp < len; fp++) {
                score = scores[fp][tc_index];
                if (score > max) {
                    max = score;
                    max_score_index = fp;
                }
            }
            return max_score_index;
        },
        _null_all_tc_rows_except: function(tc_index, fp_index) {
            var scores = this.reduced_matched_scores_array;
            for (var fp = 0, len = this.reduced_matched_scores_array.length; fp < len; fp++) {
                if (fp !== fp_index) {
                    this.reduced_matched_scores_array[fp][tc_index] = null;
                }
            }
        },
        _filter_best_tc_score: function(tc_index) {
            var best_fp = this._get_best_tc_match_row_index(tc_index);
            this._null_all_tc_rows_except(tc_index, best_fp);
        },
        _get_non_null_column: function(row_index) {
            var row = this.reduced_matched_scores_array[row_index];
            for (var col = 0, len = row.length; col < len; col++) {
                if (row[col] !== null) {
                    return col;
                }
            }
            return null;
        },
        reduce: function() {
            this.reduced_matched_scores_array = this.cloneArray(this.matched_scores_array);
            var tc_len = this.reduced_matched_scores_array[0].length;
            for (var tc = 0; tc < tc_len; tc++) {
                this._filter_best_tc_score(tc);
            }
        },
        getBestMatchIndex: function(fieldportal_job_id_array_index) {
            if (this.reduced_matched_scores_array === null) {
                this.reduce();
            }
            return this._get_non_null_column(fieldportal_job_id_array_index);
        }
    };

    
})();