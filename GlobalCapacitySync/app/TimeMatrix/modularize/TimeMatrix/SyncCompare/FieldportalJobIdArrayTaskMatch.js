(function(){

    FieldportalJobIdArrayTaskMatch = function(fieldportal_job_id_array, task_control_id_array, compare) {
        this.fieldportal_job_id_array = fieldportal_job_id_array;
        this.task_control_id_array = task_control_id_array;
        this.compare = compare;
        //Internal:
        this.ranked_matches_array = null; //[fieldportal_job_id_array_index][task_control_id_array_index] = match item
        this.ranked_match_scores_array = null; //[fieldportal_job_id_array_index][task_control_id_array_index] = match score
        this.many_to_many_filter = null;
    };
    FieldportalJobIdArrayTaskMatch.prototype = {
        _generate_ranked_match_for_fieldportal: function(fieldportal_job_id_array_index) {
            if (!this.ranked_matches_array[fieldportal_job_id_array_index]) {
                this.ranked_matches_array[fieldportal_job_id_array_index] = [];
            }
            var fieldportal_job = this.compare.getFieldportalJob(this.fieldportal_job_id_array[fieldportal_job_id_array_index]);
            var task_control, match_item;
            for (var i = 0, len = this.task_control_id_array.length; i < len; i++) {
                task_control = this.compare.getTaskControl(this.task_control_id_array[i]);
                match_item = new GlobalCapacity.TimeMatrix.SyncCompare.FieldportalJobTaskControlMatchItem(task_control, fieldportal_job, this.fieldportal_job_id_array[fieldportal_job_id_array_index], this.task_control_id_array[i]);
                this.ranked_matches_array[fieldportal_job_id_array_index][i] = match_item;
            }
        },
        _generate_ranked_matches: function() {
            this.ranked_matches_array = [];
            for (var i = 0, len = this.fieldportal_job_id_array.length; i < len; i++) {
                this._generate_ranked_match_for_fieldportal(i);
            }
        },
        _generate_ranked_match_scores: function() {
            this.ranked_match_scores_array = [];
            var ranked_matches = this.getRankedMatchesArray();
            var tc_array;
            for (var fp = 0, fplen = ranked_matches.length; fp < fplen; fp++) {
                this.ranked_match_scores_array[fp] = [];
                tc_array = ranked_matches[fp];
                for (var tc = 0, tclen = tc_array.length; tc < tclen; tc++) {
                    this.ranked_match_scores_array[fp][tc] = tc_array[tc].getMatchRank();
                }
            }
        },
        getManyToManyFilter: function() {
            if (this.many_to_many_filter === null) {
                this.many_to_many_filter = new SyncCompare.MatchedScoresArrayManyToManyFilter(this.getRankedMatchScoresArray());
            }
            return this.many_to_many_filter;
        },
        getRankedMatch: function(fieldportal_job_id_array_index, task_control_id_array_index) {
            var matches = this.getRankedMatchesArray();
            if (!matches[fieldportal_job_id_array_index]) {
                throw "Bad fieldportal_job_id_array_index";
            }
            if (!matches[fieldportal_job_id_array_index][task_control_id_array_index]) {
                throw "Bad task_control_id_array_index";
            }
            return matches[fieldportal_job_id_array_index][task_control_id_array_index];
        },
        getRankedMatchesArray: function() {
            if (this.ranked_matches_array === null) {
                this._generate_ranked_matches();
            }
            return this.ranked_matches_array;
        },
        getRankedMatchScoresArray: function() {
            if (this.ranked_match_scores_array === null) {
                this._generate_ranked_match_scores();
            }
            return this.ranked_match_scores_array;
        },
        getBestMatch: function(fieldportal_job_id_array_index) {
            if (!this.getRankedMatchScoresArray()[fieldportal_job_id_array_index]) {
                throw "Bad fieldportal_job_id_array_index";
            }
            if (this.fieldportal_job_id_array.length > 1) {
                //do a many-to-many match
                var best_match_tc_index = this.getManyToManyFilter().getBestMatchIndex(fieldportal_job_id_array_index);
                if (best_match_tc_index === null) {
                    return null;
                }
                return this.getRankedMatch(fieldportal_job_id_array_index, best_match_tc_index);
            }
            //TODO: this should probably be in its own function
            var scores = this.getRankedMatchScoresArray()[fieldportal_job_id_array_index];
            var max = 0;
            var max_score_index = 0;
            var score;
            for (var i = 0, len = scores.length; i < len; i++) {
                score = scores[i];
                if (score > max) {
                    max = score;
                    max_score_index = i;
                }
            }
            return this.getRankedMatchesArray()[fieldportal_job_id_array_index][max_score_index];
        }
    };
    return FieldportalJobIdArrayTaskMatch
})();