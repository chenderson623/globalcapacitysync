(function(){

    FieldportalJobIdIndex = function() {
        this.fieldportal_job_id_index = {};
    };
    FieldportalJobIdIndex.prototype = {
        add: function(fieldportal_job, index) {
            var task_id = fieldportal_job.get('ContractRefNum');
            if (!this.fieldportal_job_id_index[task_id]) {
                this.fieldportal_job_id_index[task_id] = [];
            }
            this.fieldportal_job_id_index[task_id].push(index);
        },
        findTaskId: function(task_id) {
            task_id = parseInt(task_id);
            if (!this.fieldportal_job_id_index[task_id]) {
                return [];
            }

            if (!this.fieldportal_job_id_index[task_id].length) {
                //either not an array or is empty
                return [];
            }
            //if still here, we have an array with length >= 1
            return this.fieldportal_job_id_index[task_id];
        },
        findFieldportalJobIds: function(task_id, task_seq, task_type, fieldportal_job_collection) {
            var return_array = [];
            var fieldportal_job_id_array = this.findTaskId(task_id);
            var search_job_type = FieldPortal.JobTypesForAbbrev.getJobType(task_type);
            var fieldportal_job, fieldportal_job_id;
            for (var i = 0, len = fieldportal_job_id_array.length; i < len; i++) {
                fieldportal_job_id = fieldportal_job_id_array[i];
                fieldportal_job = fieldportal_job_collection.at(fieldportal_job_id);
                if (parseInt(fieldportal_job.get('SchedSeq')) === parseInt(task_seq) && fieldportal_job.get('JobType') === search_job_type) {
                    return_array.push(fieldportal_job_id); //theoretically there could be more than one
                }
            }
            return return_array;
        },
        getTaskIdsWithMultipleJobs: function() {
            var return_array = [];
            for (var task_id in this.fieldportal_job_id_index) {
                if (this.fieldportal_job_id_index[task_id].length > 1) {
                    return_array.push(task_id);
                }
            }
            return return_array;
        },
        getTaskIdArray: function() {
            var return_array = [];
            for (var task_id in this.fieldportal_job_id_index) {
                return_array.push(task_id);
            }
            return return_array;
        }
    };
    return FieldportalJobIdIndex;
})();