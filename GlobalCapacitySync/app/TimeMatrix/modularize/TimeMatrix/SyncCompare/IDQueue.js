(function(){

    SyncCompare.IDQueue = function() {
        this.id_queue = []; //always use getQueueArray to access this as some values may have been nullified
    };
    SyncCompare.IDQueue.prototype = {
        add: function(id) {
            //only add if does not exist
            if (this.findID(id) === -1) {
                this.id_queue.push(id);
            }
        },
        findID: function(id) {
            return this.id_queue.indexOf(id);
        },
        hasID: function(id) {
            return this.findID(id) > -1;
        },
        removeIdFromQueue: function(id) {
            var index = this.findID(id);
            if (index > -1) {
                this.id_queue[index] = null;
            }
        },
        getQueueArray: function() {
            return this.id_queue.filter(function(id) {
                return id !== null;
            });
        }
    };
    
    
})();