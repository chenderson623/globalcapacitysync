(function(){

    TaskControlTaskIdIndex = function() {
        this.task_control_task_id_index = {};
    };
    TaskControlTaskIdIndex.prototype = {
        add: function(task_control_model, index) {
            var task_id = task_control_model.timematrix_task.get('task_id');
            if (!this.task_control_task_id_index[task_id]) {
                this.task_control_task_id_index[task_id] = [];
            }
            this.task_control_task_id_index[task_id].push(index);
        },
        findTaskId: function(task_id) {
            task_id = parseInt(task_id);
            if (!this.task_control_task_id_index[task_id]) {
                return [];
            }

            if (!this.task_control_task_id_index[task_id].length) {
                //either not an array or is empty
                return [];
            }
            //if still here, we have an array with length >= 1
            return this.task_control_task_id_index[task_id];
        },
        findTaskControlIds: function(task_id, task_seq, task_type, task_control_collection) {
            var return_array = [];
            var task_control_id_array = this.findTaskId(task_id);
            var task_model, task_control_id;
            for (var i = 0, len = task_control_id_array.length; i < len; i++) {
                task_control_id = task_control_id_array[i];
                task_model = task_control_collection.at(task_control_id).timematrix_task;
                if (parseInt(task_model.get('task_seq')) === parseInt(task_seq) && task_model.get('task_type') === task_type) {
                    return_array.push(task_control_id); //theoretically there could be more than one
                }
            }
            return return_array;
        },
        getTaskIdArray: function() {
            var return_array = [];
            for (var task_id in this.task_control_task_id_index) {
                return_array.push(task_id);
            }
            return return_array;
        }
    };


})();