
        TaskTable.data_prototype = {
            zip_code: null,
            zip_code_suffix: null,
            clli_code: null,
            task_type: null,
            task_id: null,
            task_seq: null,
            task_url: null,
            task_status: null,
            time_range: null,
            start_time: null,
            end_time: null,
            service_type: null
        };

/*
<table width="90" border="0">
    <tbody>
        <tr>
            <td>
                {{zip_code}}{{zip_code_suffix}} {{clli_code}}<br>
            </td>
        </tr>
        <tr>
            <td>
                <a target="_new" href="{{task_url}}">
                {{task_type}}{{task_id}}-{{task_seq}}<br>
                </a>
            </td>
        </tr>
        <tr>
            <td>
                {{task_status}}<br>({{start_time}}-{{end_time}})
                <br>
            </td>
        </tr>
        <tr>
            <td>
                {{service_type}}<br>
            </td>
        </tr>
    </tbody>
</table>
*/

function createTableElem() {
    var table = document.createElement('table');
    return table;
}

function createTrElem(table_elem) {
    var row = table_elem.insertRow(-1);
    var cell = row.insertCell(-1);
    return cell;
}

function createZipClliNode(data) {
    var content = data.zip_code + "-" + data.zip_code_suffix + " " + data.clli_code;
    var cell = createTrElem();
    cell.innerHTML = content;
}

//===============================

function TaskTable_Std(data) {
    this.data = data;
    this.table_elem = document.createElement('table');
}

TaskTable_Std.prototype = {
    createNodeCell: function() {
        var row = this.table_elem.insertRow(-1);
        var cell = row.insertCell(-1);
        return cell;
    },
    setNodeElem: function(content) {
        var elem = this.createNodeCell();
        elem.innerHTML = content;
        return elem;
    },
    getTableElem: function() {
        var zip_clli_node = this.setNodeElem(data.zip_code + "-" + data.zip_code_suffix + " " + data.clli_code);
        var task_link_node = this.setNodeElem('<a target="_new" href="' + this.data.task_url + '">' + this.data.task_type + this.data.task_id + '-' + this.data.task_seq + '</a>');
        var status_and_time_node = this.setNodeElem(this.data.task_status + ' ' + '(' + this.data.start_time + '-' + this.data.end_time + ')');
        var service_type_node = this.setNodeElem(this.data.service_type);
        return this.table_elem;
    }

};