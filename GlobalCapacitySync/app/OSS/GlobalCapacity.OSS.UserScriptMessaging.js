/*
 *
 * This is one-way communication. This makes request calls to UserScriptSide
 *
 * UserScriptMessaging Object is either a request object, or a response object.
 * Request object keeps track of requests, and also responds to broken connection
 *
 * No initialization is needed. This script will assume that UserScriptSide is already there
 *
 * Request side postMessage and waits for aknowledge. should be almost instantaneous, but set timeout, say 1000
 * Example communications:
 * request: {
 *       get: "SessionData",
 *       from: "PageSide",
 *       to: "UserScriptSide",
 *       request_id: #####
 *   }
 *
 *   aknowledge: {
 *       aknowledge: request_id,
 *       from: "UserScriptSide",
 *       to: "PageSide"
 *   }
 *
 *   response: {
 *       response: request_id,
 *       type: "get:SessionData"
 *       from: "UserScriptSide",
 *       to: "PageSide",
 *       success: true,
 *       message: "",  //if success = false, this is an error message
 *       data: {session_data}
 *   }
 *
 */

if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.OSS == 'undefined') {
    throw 'GlobalCapacity.OSS is required.';
}

GlobalCapacity.OSS.UserScriptMessaging = function(oss) {
    var self = this;
    this.oss = oss;
    this.debug = false;
    this.user_script_messaging_channel = window;
    this.self_name = "PageSide";
    this.user_script_name = "UserScriptSide";
    this.aknowledge_timeout = 1000;
    this.ping_limit = 20;
    var requests = {};
    var awaiting_aknowledge = {};

    //************************Public Methods*************************************
    this.getSessionData = function(success, error) {
        var message_obj = {
            action: "getSessionData",
            from: self.self_name,
            to: self.user_script_name
        };
        registerRequest(message_obj, success, error);
        sendRequest(message_obj);
    };
    this.logIn = function(username, password, success, error) {
        var message_obj = {
            action: "logIn",
            username: username,
            password: password,
            from: self.self_name,
            to: self.user_script_name
        };
        registerRequest(message_obj, success, error);
        sendRequest(message_obj);
    };
    this.logOut = function(success, error) {
        var message_obj = {
            action: "logOut",
            from: self.self_name,
            to: self.user_script_name
        };
        registerRequest(message_obj, success, error);
        sendRequest(message_obj);
    };
    this.setValue = function(key, value, success, error) {
        makePostRequest(key, value, success, error);
    };
    this.getValue = function(key, success, error) {
        makeGetRequest(key, success, error);
    };
    //************************Ready******************************************
    var ready = false;
    var on_ready = [];
    this.onReady = function(callback) {
        if (ready === true) {
            callback();
            return;
        }
        on_ready.push(callback);
    };
    var setReady = function() {
        if (self.debug) console.log('PageSide:READY');
        ready = true;
        for (var i = 0, len = on_ready.length; i < len; i++) {
            on_ready[i]();
        }
        on_ready = [];
    };
    var pings = {};
    var ping_interval;
    var ping_count = 0;
    var stopPing = function() {
        clearInterval(ping_interval);
    };

    var ping = function() {
        ping_count++;
        if (ping_count >= self.ping_limit) {
            stopPing();
            throw self.user_script_name + " never responded to pings";
        }
        if (self.debug) console.log("PageSide:PING " + ping_count);
        var request_id = self.createNewRequestId();
        var ping = {
            ping: request_id,
            from: self.self_name,
            to: self.user_script_name
        };
        pings[request_id] = true;
        self.user_script_messaging_channel.postMessage(JSON.stringify(ping), '*');
    };

    var receivePong = function(response_message) {
        var request_id = response_message.pong;
        if (self.debug) console.log("PageSide:RECEIVE PONG", request_id, pings[request_id]);
        if (pings[request_id]) { //belongs to this object
            stopPing();
            setReady(); //means we're ready
        }
    };
    //************************Aknowledge*************************************
    var waitAknowledge = function(message_obj) {
        var request_id = message_obj.request_id;
        awaiting_aknowledge[request_id] = message_obj;
        setTimeout(function() {
            timeoutAknowledge(request_id);
        }, self.aknowledge_timeout);
    };
    var clearAknowledge = function(request_id) {
        if (awaiting_aknowledge[request_id]) {
            awaiting_aknowledge[request_id] = null;
        }
    };
    var timeoutAknowledge = function(request_id) {
        if (awaiting_aknowledge[request_id]) {
            //check if request had an error callback, otherwise throw
            var error = "Request " + request_id + " timed out. Something went wrong with the User Script side";
            if (requests[request_id].error && typeof requests[request_id].error === 'function') {
                requests[request_id].error(error);
            } else {
                throw error;
            }
        }
    };
    //************************Requests*************************************
    this.createNewRequestId = function() {
        var request_time = new Date().getTime();
        var rand = Math.floor(Math.random() * 1000) + 1;
        return request_time + "_" + rand;
    };

    var registerRequest = function(message_obj, success, error) {
        var request_id = self.createNewRequestId();
        message_obj.request_id = request_id;
        var request = {
            message: message_obj,
            success: success,
            error: error
        };
        requests[request_id] = request;
    };

    var makeGetRequest = function(get_name, success, error) {
        var message_obj = {
            get: get_name,
            from: self.self_name,
            to: self.user_script_name
        };
        registerRequest(message_obj, success, error);
        sendRequest(message_obj);
    };
    var makePostRequest = function(post_name, data, success, error) {
        var message_obj = {
            post: post_name,
            data: data,
            from: self.self_name,
            to: self.user_script_name
        };
        registerRequest(message_obj, success, error);
        sendRequest(message_obj);
    };
    var sendRequest = function(message_obj) {
        waitAknowledge(message_obj);
        if (self.debug) console.log('PageSide:sendRequest: ', message_obj);
        self.user_script_messaging_channel.postMessage(JSON.stringify(message_obj), '*');
    };
    //************************Process Incoming Messages*****************************
    var processResponse = function(response_message) {
        var request_id = response_message.response;
        if (!requests[request_id]) {
            throw "Request " + request_id + " is not registered";
        }
        //This will have our callbacks:
        var request = requests[request_id];
        //Check if already received
        if (request.received) {
            throw "Request " + request_id + " has already been received";
        }
        //Mark it received
        request.received = true;
        //Store the response
        request.response = response_message;
        var is_success = response_message.success;
        if (is_success && typeof request.success === 'function') {
            if(typeof response_message.data !== 'object') {
                response_message.data = JSON.parse(response_message.data);
            }
            request.success(response_message.data);
        }
        if (!is_success) {
            if (typeof request.error === 'function') {
                request.error(response_message.message);
            } else {
                throw received_message.message;
            }
        }
    };
    var recievePostMessage = function(event) {
        var received_message;
        try {
            received_message = JSON.parse(event.data);
        } catch (e) {
            // Do nothing
        }
        if (self.debug) console.log("PageSide:receivePostMessage:", received_message);

        //only need to listen to messages to this
        if (!received_message.to || received_message.to !== self.self_name) return;
        if (received_message.pong) {
            return receivePong(received_message);
        }
        if (received_message.aknowledge) {
            return clearAknowledge(received_message.aknowledge);
        }
        if (received_message.response) {
            return processResponse(received_message);
        }
    };
    //
    // Setup listener
    self.user_script_messaging_channel.addEventListener('message', recievePostMessage, false);
    //Setup ping to see when responder is ready:
    ping_interval = setInterval(ping, 300);
};