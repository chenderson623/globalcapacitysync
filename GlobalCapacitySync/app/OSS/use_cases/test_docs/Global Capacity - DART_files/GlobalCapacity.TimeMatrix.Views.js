
if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

GlobalCapacity.TimeMatrix.Views = (function() {
    function TaskTable_Std(data) {
        this.data = data;
        this.table_elem = null;
    }
    TaskTable_Std.prototype = {
        createNodeCell: function() {
            var row = this.table_elem.insertRow(-1);
            var cell = row.insertCell(-1);
            return cell;
        },
        setNodeElem: function(content) {
            var elem = this.createNodeCell();
            elem.innerHTML = content;
            return elem;
        },
        getTableElem: function() {
            if (!this.table_elem) {
                var data = this.data;
                this.table_elem = document.createElement('table');
                var zip_clli_node = this.setNodeElem(data.zip_code + "-" + data.zip_code_suffix + " " + data.clli_code);
                var task_link_node = this.setNodeElem('<a target="_new" href="' + this.data.task_url + '">' + this.data.task_type + this.data.task_id + '-' + this.data.task_seq + '</a>');
                var status_and_time_node = this.setNodeElem(this.data.task_status + ' ' + '(' + this.data.start_time + '-' + this.data.end_time + ')');
                var service_type_node = this.setNodeElem(this.data.service_type);
            }
            return this.table_elem;
        }
    };
    /**
     *
     *
     * ============================Return Values===============================================
     */
    return {
        TaskTable_Std: TaskTable_Std
    };
})();