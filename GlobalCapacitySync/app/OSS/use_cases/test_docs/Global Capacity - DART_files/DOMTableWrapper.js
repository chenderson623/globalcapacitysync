//if(typeof DOMElements == 'undefined') {
//    throw 'DOMElements is required.';
//}


DOMTableWrapper = (function() {

    /*===============Table==============================================================*/
    function Table(table_elem) {
        this.elem = table_elem;
        this.row_wrappers_array = null;
    }

    Table.prototype = {
        getElem: function() {
            if (!this.elem || this.elem.rows === undefined) {
                throw "Bad Table Element";
            }
            return this.elem;
        },
        getRowsHtmlCollection: function() {
            return this.getElem().rows;
        },
        map: function(map_func) {
            var map = Array.prototype.map;
            return map.call(this.getRowsHtmlCollection(), map_func);
        },
        getRows: function(slice_begin, slice_end) {
            if (this.row_wrappers_array === null) {
                var self = this;
                this.row_wrappers_array = this.map(function createRowElem(row_elem) {
                    return new Row(row_elem, self);
                });
            }
            if (slice_begin !== undefined && parseInt(slice_begin) >= 0) {
                return this.getRowsSlice(slice_begin, slice_end);
            }
            return this.row_wrappers_array;
        },
        getRowsSlice: function(slice_begin, slice_end) {
            if (slice_end !== undefined && !isNaN(parseInt(slice_end))) {
                return this.getRows().slice(parseInt(slice_begin), parseInt(slice_end));
            } else {
                return this.getRows().slice(parseInt(slice_begin));
            }
        },
        countRows: function() {
            return this.getRows().length;
        },
        getRow: function(index) {
            if (this.getRows()[index] !== undefined) {
                return this.getRows()[index];
            }
            return null;
        }
    };

    /*===============Row==============================================================*/

    function Row(row_elem, table_wrapper) {
        this.elem = row_elem;
        this.table_wrapper = table_wrapper;
        this.cell_wrappers_array = null;
    }
    Row.prototype = {
        getElem: function() {
            if (!this.elem || this.elem.cells === undefined) {
                throw "Bad Row Element";
            }
            return this.elem;
        },
        getCellsHtmlCollection: function() {
            return this.getElem().cells;
        },
        map: function(map_func) {
            var map = Array.prototype.map;
            return map.call(this.getCellsHtmlCollection(), map_func);
        },
        getCells: function(slice_begin, slice_end) {
            if (this.cell_wrappers_array === null) {
                this.cell_wrappers_array = this.map(function createCellElem(cell_elem) {
                    return new Cell(cell_elem);
                });
            }
            if (slice_begin !== undefined && parseInt(slice_begin) >= 0) {
                return this.getCellsSlice(slice_begin, slice_end);
            }
            return this.cell_wrappers_array;
        },
        getCellsSlice: function(slice_begin, slice_end) {
            if (slice_end !== undefined && !isNaN(parseInt(slice_end))) {
                return this.getCells().slice(parseInt(slice_begin), parseInt(slice_end));
            } else {
                return this.getCells().slice(parseInt(slice_begin));
            }
        },
        countCells: function() {
            return this.getCells().length;
        },
        getCell: function(index) {
            if (this.getCells()[index] !== undefined) {
                return this.getCells()[index];
            }
            return null;
        },
        getRowIndex: function() {
            return this.getElem().rowIndex;
        },
        getTableWrapper: function() {
            return this.table_wrapper;
        },
        addClass: function(class_name) {
            this.elem.classList.add(class_name);
        }
    };

    /*===============Cell==============================================================*/

    function Cell(cell_elem, row_wrapper) {
        this.elem = cell_elem;
        this.row_wrapper = row_wrapper;
    }

    Cell.prototype = {
        getElem: function() {
            if (!this.elem || this.elem.cellIndex === undefined) {
                throw "Bad Cell Element";
            }
            return this.elem;
        },
        getCellIndex: function() {
            return this.getElem().cellIndex;
        },
        getRowWrapper: function() {
            return this.row_wrapper;
        },
        addClass: function(class_name) {
            this.elem.classList.add(class_name);
        },
        getCleanText: function() {
            return DOMElements.Filters.getElementCleanText(this.elem);
        },
        empty: function() {
            while (this.elem.firstChild) {
                this.elem.removeChild(this.elem.firstChild);
            }
        }
    };

    /*===============TableProcessor======================================================*/
    function TableProcessor(table_wrapper) {
        this.table_wrapper = table_wrapper;
    }

    TableProcessor.prototype = {
        process: function(table_rows_process) {
            var row_wrapper_array = this.table_wrapper.getRows(table_rows_process.row_begin, table_rows_process.row_end);
            var rows_process = table_rows_process;

            var before_result = table_rows_process.beforeRows(row_wrapper_array, this.table_wrapper);
            if (before_result === false) { //exit, returning false
                return false;
            }
            if (typeof before_result === "object" && before_result.processRow !== undefined) { //if object is returned, assume it is a new process object
                rows_process = before_result;
            }
            var row_result;
            for (var i = 0, len = row_wrapper_array.length; i < len; i++) {
                row_result = rows_process.processRow(row_wrapper_array[i]);
                if (row_result === false) {
                    break; //breaks out of loop, going to afterRows
                }
                //run any cells_processes, or the return result of processRow
                if (toString.call(row_result) === "[object Array]") {
                    //if an array was returned, use instead of table_rows_processes.cells_processes
                    this.doCellProcesses(row_result, row_wrapper_array[i]);
                } else {
                    this.doCellProcesses(table_rows_process.cells_processes, row_wrapper_array[i]);
                }
            }
            table_rows_process.afterRows(); //always call the original process, not rows_process
        },
        doCellProcesses: function(cells_processes, row_wrapper) {
            if (toString.call(cells_processes) !== "[object Array]") {
                //if its not an array, return
                return null;
            }
            var row_cells_processor = new RowProcessor(row_wrapper);
            for (var i = 0, len = cells_processes.length; i < len; i++) {
                row_cells_processor.process(cells_processes[i]);
            }
        }
    };

    /*===============RowProcessor======================================================*/

    function RowProcessor(row_wrapper) {
        this.row_wrapper = row_wrapper;
    }

    RowProcessor.prototype = {
        process: function(row_cells_process) {
            var cell_objs = this.row_wrapper.getCells(row_cells_process.cell_begin, row_cells_process.cell_end);
            var cells_process = row_cells_process;

            var before_result = row_cells_process.beforeCells(cell_objs, this.row_wrapper);
            if (before_result === false) { //exit, returning false
                return false;
            }
            if (typeof before_result === "object" && before_result.processRow !== undefined) { //if object is returned, assume it is a new process object
                cells_process = before_result;
            }
            var cell_result;
            for (var i = 0, len = cell_objs.length; i < len; i++) {
                cell_result = cells_process.processCell(cell_objs[i]);
                if (cell_result === false) {
                    break; //breaks out of loop, going to afterCells
                }
            }
            row_cells_process.afterCells(); //always call the original process, not row_cells_process
        },

    };

    /*===============TableProcessPrototype======================================================*/

    var TableRowsProcessPrototype = {
        row_begin: null,
        row_end: null,
        cells_processes: [], //processes to run on each row.
        beforeRows: function(row_wrappers_array, table_wrapper) {
            //run by TableProcessor before entering row loop.
            //If returns false, TableProcessor will exit, returning false.
            //If anything but null is returned, it will be assumed to be a new process object
        },
        processRow: function(row_wrapper) {
            //run by TableProcessor on each row.
            //If returns false, TableProcessor breaks out of loop
            //in TableProcessor, this.cells_processes is run after this
            //If this returns an array, TableProcessor will use that instead of this.cells_processes
        },
        afterRows: function() {
            //run by TableProcessor after row loop. Return value not processed
            //It will be this object called, even if another object was used by processRow (set by beforeRows)
        }
    };

    /*===============RowProcessPrototype======================================================*/

    var RowCellsProcessPrototype = {
        cell_begin: null,
        cell_end: null,
        beforeCells: function(cell_objs_array, row_wrapper) {
            //run by RowProcessor before entering cell loop.
            //If returns false, RowProcessor will exit, returning false.
            //If anything but null is returned, it will be assumed to be a new process object
        },
        processCell: function(cell_wrapper) {
            //run by RowProcessor on each cell.
            //If returns false, RowProcessor breaks out of loop
        },
        afterCells: function() {
            //run by RowProcessor after cell loop. Return value not processed
            //It will be this object called, even if another object was used by processRow (set by beforeRows)
        }
    };

    return {
        Table: Table,
        Row: Row,
        Cell: Cell,
        TableProcessor: TableProcessor,
        RowProcessor: RowProcessor,
        TableRowsProcessPrototype: TableRowsProcessPrototype,
        RowCellsProcessPrototype: RowCellsProcessPrototype
    };

})();