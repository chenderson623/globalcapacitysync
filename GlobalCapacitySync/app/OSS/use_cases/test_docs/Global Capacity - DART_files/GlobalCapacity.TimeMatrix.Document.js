
if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

GlobalCapacity.TimeMatrix.Document = (function() {
    /**
     *
     *
     * ============================Document (main entry point)=======================================
     */
    //TODO: make this prototype
    var Document = function(timematrix_dom_obj, timematrix) {
        this.timematrix = timematrix;
        this.timematrix_dom_obj = timematrix_dom_obj;
        this.report_parameters = null;
        this.schedules = null;
        this.schedule_maps_iterators = [];

        this.getDocumentType = function() {
            return "TimeMatrix";
        };

        this.isTimeMatrixDocument = function() {
            return this.timematrix_dom_obj.isTimeMatrixDocument();
        };

        this.getReportParameters = function() {
            if(this.report_parameters === null) {
                var elem = this.timematrix_dom_obj.getParametersElem();
                var parser = new GlobalCapacity.TimeMatrix.Parsers.ReportParametersNode(elem);
                this.report_parameters = parser.parse();
            }
            return this.report_parameters;
        };

        this.countSchedules = function() {
            return this.getSchedules().length;
        };
        this.getSchedules = function() {
            if (this.schedules === null) {
                if (!this.timematrix_dom_obj) {
                    throw "No document dom is set";
                }
                this.schedules = [];
                var schedules_count = this.timematrix_dom_obj.countScheduleTables();
                var schedule_table_dom;
                for (var i = 0; i < schedules_count; i++) {
                    schedule_table_dom = this.timematrix_dom_obj.getScheduleTable(i);
                    this.schedules.push(new Schedule(schedule_table_dom));
                }
            }
            return this.schedules;
        };
        this.getSchedule = function(schedule_index) {
            if (this.schedules === null) {
                this.getSchedules();
            }
            if (this.schedules[schedule_index] === undefined) {
                throw "Schedule index " + schedule_index + " is not valid";
            }
            return this.schedules[schedule_index];
        };
        this.getScheduleParams = function(schedule_index) {
            var return_obj = {};
            var report_params = this.getReportParameters();
            var schedule_date = this.getSchedule(schedule_index).ISODateString();

            return_obj.report_tech = report_params.tech;
            return_obj.report_region = report_params.region;
            return_obj.report_dates = [schedule_date];

            return return_obj;
        };
        this.getScheduleMapsIterator = function(schedule_index) {
            if(typeof this.schedule_maps_iterators[schedule_index] === 'undefined') {
                this.schedule_maps_iterators[schedule_index] = new ScheduleMapsIterator(this.getSchedule(schedule_index));
            }
            return this.schedule_maps_iterators[schedule_index];
        };
        /**
         * ======================Controls Methods======================================
         *
         */
        this.createSyncControlsFactory = function(callback) {
            console.log("TIMEMATRIX DOCUMENT createSyncControls");
            var stateless_loader = this.timematrix.getStatelessLoader();
            stateless_loader.runTimeMatrixSyncControlFactory(this, function(sync_control_factory){
                if(callback) {
                    callback(sync_control_factory);
                }
                console.timeEnd('INSIDE TimeMatrix.Document.createSyncControls ', sync_control_factory);
            });
        };
    };
    /**
     *
     *
     * ============================Schedule=========================================================
     */
    var Schedule = function(schedule_table_dom) {
        this.schedule_table_dom = schedule_table_dom;
        this.task_map = null;
        this.tech_map = null;

        this.getScheduleDateObj = function() {
            return this.schedule_table_dom.getDateObj();
        };
        this.ISODateString = function(){
            function pad(n){return n<10 ? '0'+n : n;}
            var date_obj = this.getScheduleDateObj();
             return date_obj.getUTCFullYear()+'-' +
                  pad(date_obj.getUTCMonth()+1)+'-' +
                  pad(date_obj.getUTCDate());
        };
        this.getScheduleDOMMap = function() {
            if (!this.schedule_table_dom) {
                throw "No schedule dom is set";
            }
            return this.schedule_table_dom.getScheduleDOMMap();
        };
        this.getScheduleTaskMap = function() {
            if(this.task_map === null) {
                var task_map_factory = new Schedule_Task_Map_Factory(this.getScheduleDOMMap());
                //at the same time we will split task tables:
                task_map_factory.remapTaskTables = rewriteTimeslotCell_MultipleTasks;
                this.task_map = task_map_factory.getScheduleTaskMap();
            }
            return this.task_map;
        };
        this.getScheduleDataMap = function() {
            var dom_map = this.getScheduleDOMMap();
            var data_obj = {
                timeslot_column_index: dom_map.timeslot_column_index,
                schedule_row_index: dom_map.schedule_row_index,
                task_data: this.getScheduleTaskMap().task_data
            };
            return data_obj;
        };
        this.getScheduleTechMap = function() {
            if(this.tech_map === null) {
                var tech_map_factory = new Schedule_Tech_Map_Factory(this.getScheduleDOMMap());
                this.tech_map = tech_map_factory.getScheduleTechMap();
            }
            return this.tech_map;
        };
        this.getScheduleDataJSONString = function() {
            return JSON.stringify(this.getScheduleDataMap());
        };
        this.getScheduleDataIndex = function() {
            return ScheduleDataIndex(this.getScheduleTaskMap().task_data);
        };
        this.countTasks = function() {
            var data_index = this.getScheduleDataIndex();
            return data_index.length;
        };
        this.countTechs = function() {
            var data_obj = this.getScheduleTaskMap();
            return data_obj.task_data.length;
        };
    };
    /**
     *
     *
     * ============================ScheduleDataIndex=========================================================
     * Takes 3 dim array from Schedule.getScheduleDataMap (or Schedule.getScheduleTaskMap().task_tables) and creates a linear index
     */
    var ScheduleDataIndex = function(task_array) {
        var schedule_index = [];
        var indexTaskData = function(task_data, row, col) {
            for(var task=0, task_len=task_data.length; task<task_len; task++) {
                schedule_index.push([row, col, task]);
            }
        };
        var indexScheduleRow = function(schedule_row, row) {
            for(var col=0, col_len = schedule_row.length; col < col_len; col++) {
                if(!schedule_row[col]) {
                    continue;
                }
                indexTaskData(schedule_row[col], row, col);
            }
        };
        for(var row=0, row_len = task_array.length; row<row_len; row++) {
            indexScheduleRow(task_array[row],row);
        }
        return schedule_index;
    };
    /**
     *
     *
     * ============================Schedule_Task_Map_Factory=======================================
     */
    /**
     * Generates task_map from dom_map
     * schedule_dom_map generated by GlobalCapacity.TimeMatrix.DOM.ScheduleTable
     * @param {GlobalCapacity.TimeMatrix.DOM.ScheduleTable.schedule_dom_map} schedule_dom_map
     */
    function Schedule_Task_Map_Factory(schedule_dom_map) {
        this.schedule_dom_map = schedule_dom_map;

        var schedule_task_map = {
            task_data : [],
            task_count: [], //This is not an accurate task count. Includes blocks
            task_tables : []
        };

        this.parseTimeslotCell = function parseTimeslotCell(timeslot_cell_wrapper, cell_index, schedule_row_index) {
            var table_elems = timeslot_cell_wrapper.getElem().getElementsByTagName('TABLE');
            schedule_task_map.task_tables[schedule_row_index][cell_index] = table_elems;
            var task_array;
            var parser;
            for (var i = 0, len = table_elems.length; i < len; i++) {
                parser = new GlobalCapacity.TimeMatrix.Parsers.TasksTable(table_elems[i]);
                //most times, this will pass only one, so wrap it in ternary:
                task_array = (task_array) ? task_array.concat(parser.parse()) : parser.parse();
            }
            schedule_task_map.task_data[schedule_row_index][cell_index] = task_array;
            schedule_task_map.task_count[schedule_row_index] += task_array.length;
        };
        this.remapTaskTables = null; //this can be set to do some task table remapping
        this.iterateTimeslotCells = function iterateTimeslotCells(timeslot_cells_row, schedule_row_index) {
            var timeslot_cell;
            for (var cell_index in timeslot_cells_row) {
                timeslot_cell = timeslot_cells_row[cell_index];
                this.parseTimeslotCell(timeslot_cell, cell_index, schedule_row_index);
                if(typeof this.remapTaskTables === 'function') {
                    this.remapTaskTables(timeslot_cell, schedule_task_map, cell_index, schedule_row_index);
                }
            }
        };
        this.iterateScheduleTimeslotRows = function iterateScheduleRows() {
            var timeslot_cells_row;
            for (var i = 0, len = this.schedule_dom_map.timeslot_cells.length; i < len; i++) {
                timeslot_cells_row = this.schedule_dom_map.timeslot_cells[i];
                schedule_task_map.task_data[i] = [];
                schedule_task_map.task_tables[i] = [];
                schedule_task_map.task_count[i] = 0;
                this.iterateTimeslotCells(timeslot_cells_row, i);
            }
        };
        this.getScheduleTaskMap = function() {
            if (schedule_task_map.task_data.length === 0) {
                this.iterateScheduleTimeslotRows();
            }
            return schedule_task_map;
        };
    }
    /**
     *
     *
     * ======================== rewriteTimeslotCell_MultipleTasks =======================================
     */
    /**
     * Used by Schedule_Task_Map_Factory to split TaskTables into one per task
     * @param  {DOM Element} timeslot_cell
     * @param  {Array} task_array
     * @return {[Array of DOM Elements]}
     */
    function rewriteTimeslotCell_MultipleTasks(timeslot_cell_wrapper, schedule_task_map, cell_index, schedule_row_index) {
        var task_array = schedule_task_map.task_data[schedule_row_index][cell_index];
        if (task_array.length < 2) {
            return;
        }

        //clear the timslot cell:
        timeslot_cell_wrapper.empty();
        //add new task table elements
        var task_table_elems = [];
        var task_table_template;
        for (var i = 0, len = task_array.length; i < len; i++) {
            task_table_template = new GlobalCapacity.TimeMatrix.Views.TaskTable_Std(task_array[i]);
            task_table_elems[i] = task_table_template.getTableElem();
            timeslot_cell_wrapper.getElem().appendChild(task_table_elems[i]);
        }
        schedule_task_map.task_tables[schedule_row_index][cell_index] = task_table_elems;
    }

    /**
     *
     *
     * ============================Schedule_Tech_Map_Factory=======================================
     */
    /**
     * Generates tech_map from dom_map
     * schedule_dom_map generated by GlobalCapacity.TimeMatrix.DOM.ScheduleTable
     * @param {GlobalCapacity.TimeMatrix.DOM.ScheduleTable.schedule_dom_map} schedule_dom_map
     */
    function Schedule_Tech_Map_Factory(schedule_dom_map) {
        this.schedule_dom_map = schedule_dom_map;

        var schedule_tech_map = {
            tech_aliases : [],
            tech_objects : [],
            tech_cells : []
        };

        var tech_object_proto = {
            tech_alias: null,
            task_count: 0,
            tech_name: null,
            employee_id: null
        };

        this.iterateScheduleTimeslotRows = function iterateScheduleRows() {
            var tech_cell, tech_object, tech_alias;
            for (var i = 0, len = this.schedule_dom_map.tech_cells.length; i < len; i++) {
                tech_cell = this.schedule_dom_map.tech_cells[i];
                tech_alias = tech_cell.getCleanText();
                tech_object = Object.create(tech_object_proto);
                tech_object.tech_alias = tech_alias;
                schedule_tech_map.tech_cells.push(tech_cell.getElem());
                schedule_tech_map.tech_aliases.push(tech_alias);
                schedule_tech_map.tech_objects.push(tech_object);
            }
        };
        this.getScheduleTechMap = function() {
            if (schedule_tech_map.tech_cells.length === 0) {
                this.iterateScheduleTimeslotRows();
            }
            return schedule_tech_map;
        };
    }

    function ScheduleMapsIterator(schedule_document) {
        this.schedule_document = schedule_document;
        this.schedule_dom_map = this.schedule_document.getScheduleDOMMap();
        this.schedule_task_map = this.schedule_document.getScheduleTaskMap();
        this.schedule_data_map = this.schedule_document.getScheduleDataMap();

        this.on_schedule_row = [];
        this.onScheduleRow = function() {
            for(var i=0, len = this.on_schedule_row.length; i<len; i++) {
                this.on_schedule_row[i](this);
            }
        };

        this.after_schedule_row = [];
        this.afterScheduleRow = function() {
            for(var i=0, len = this.after_schedule_row.length; i<len; i++) {
                this.after_schedule_row[i](this);
            }
        };

        this.on_timeslot = [];
        this.onTimeslot = function() {
            for(var i=0, len = this.on_timeslot.length; i<len; i++) {
                this.on_timeslot[i](this);
            }
        };

        this.after_timeslot = [];
        this.afterTimeslot = function() {
            for(var i=0, len = this.after_timeslot.length; i<len; i++) {
                this.after_timeslot[i](this);
            }
        };

        this.on_task = [];
        this.onTask = function() {
            for(var i=0, len = this.on_task.length; i<len; i++) {
                this.on_task[i](this);
            }
        };

        this.getScheduleRow = function(schedule_row_pos) {
            if(!schedule_row_pos) {
                schedule_row_pos = this.current_schedule_row_pos;
            }
        };

        this.getTech = function(schedule_row_pos) {
            if(!schedule_row_pos) {
                schedule_row_pos = this.current_schedule_row_pos;
            }
            return this.schedule_data_map.schedule_row_index[schedule_row_pos];
        };
        this.getTechCell = function(schedule_row_pos) {
            if(!schedule_row_pos) {
                schedule_row_pos = this.current_schedule_row_pos;
            }
            return this.schedule_dom_map.tech_cells[schedule_row_pos];
        };

        this.getTimeslot = function(timeslot_pos) {
            if(!timeslot_pos) {
                timeslot_pos = this.current_timeslot_pos;
            }
            return this.schedule_data_map.timeslot_column_index[timeslot_pos];
        };

        this.getTimeslotCell = function(timeslot_pos, schedule_row_pos) {
            if(!timeslot_pos) {
                timeslot_pos = this.current_timeslot_pos;
            }
            if(!schedule_row_pos) {
                schedule_row_pos = this.current_schedule_row_pos;
            }
            if(!this.schedule_dom_map.timeslot_cells[schedule_row_pos] || !this.schedule_dom_map.timeslot_cells[schedule_row_pos][timeslot_pos]) {
                return null;
            }
            return this.schedule_dom_map.timeslot_cells[schedule_row_pos][timeslot_pos];
        };

        this.getTimeslotTaskArray = function(timeslot_pos, schedule_row_pos) {
            if(!timeslot_pos) {
                timeslot_pos = this.current_timeslot_pos;
            }
            if(!schedule_row_pos) {
                schedule_row_pos = this.current_schedule_row_pos;
            }
            if(!this.schedule_data_map.task_data[schedule_row_pos] || !this.schedule_data_map.task_data[schedule_row_pos][timeslot_pos]) {
                return null;
            }
            return this.schedule_data_map.task_data[schedule_row_pos][timeslot_pos];
        };

        this.getTimeslotTaskData = function(timeslot_task_pos, timeslot_pos, schedule_row_pos) {
            if(!timeslot_task_pos) {
                timeslot_task_pos = this.current_timeslot_task_pos;
            }
            var task_array = this.getTimeslotTaskArray(timeslot_pos, schedule_row_pos);
            if(!task_array || !task_array[timeslot_task_pos]) {
                return null;
            }
            return task_array[timeslot_task_pos];
        };

        this.current_timeslot_task_pos = 0;
        this.iterateTasks = function() {
            var task_array = this.getTimeslotTaskArray();
            if(!task_array) {
                return;
            }
            for(var i=0, len = task_array.length; i<len; i++) {
                this.current_timeslot_task_pos = i;
                this.onTask();
            }
        };

        this.current_timeslot_pos = 0;
        this.iterateTimeslots = function() {
            var timeslot_index = this.schedule_data_map.timeslot_column_index;
            for(var i=0, len = timeslot_index.length; i<len; i++) {
                this.current_timeslot_pos = i;
                this.onTimeslot();
                this.iterateTasks();
                this.afterTimeslot();
            }
        };

        this.current_schedule_row_pos = 0;
        this.iterateScheduleRows = function() {
            var schedule_row_index = this.schedule_data_map.schedule_row_index;
            for(var i=0, len = schedule_row_index.length; i<len; i++) {
                this.current_schedule_row_pos = i;
                this.onScheduleRow();
                this.iterateTimeslots();
                this.afterScheduleRow();
            }
        };

        this.iterateSchedule = function() {
            this.iterateScheduleRows();
        };
    }


    return Document;
})();