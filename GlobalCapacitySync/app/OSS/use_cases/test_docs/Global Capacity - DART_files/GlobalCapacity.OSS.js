//if (typeof $LAB === 'undefined') {
//    throw "LABjs is required";
//}
if (CSrms === undefined) {
    throw "CSrms is required";
}
var GlobalCapacity = GlobalCapacity || {};
/**
 * GlobalCapacity.OSS is a service locator
 */
GlobalCapacity.OSS = (function() {
    "use strict";
    var OSS = {
        debug: true,
        libs: {
            jquery: 'jquery-1.11.0/jquery.min.js',
            emitter: 'EventEmitter2-0.4.14/lib/eventemitter2.js',
            channel: 'jschannel/src/jschannel.js'
        },
        getCSSStack: function(){
            return [
                CSrms.getAppUrl('OSS/css/oss-resets.css'),
                CSrms.getGeboUrl('bootstrap/css/bootstrap.min.css'),
                CSrms.getGeboUrl('lib/qtip2/jquery.qtip.min.css'),
                CSrms.getGeboUrl('img/flags/flags.css'),
                CSrms.getGeboUrl('img/splashy/splashy.css'),

                CSrms.getAppUrl('OSS/css/gebo-style-lite.css'),
                CSrms.getAppUrl('OSS/css/oss-page-controls.css'),
                CSrms.getAppUrl('OSS/css/login-dropdown.css'),
                CSrms.getAppUrl('TimeMatrix/css/oss-timematrix.css')
            ];
        },
        loadCSSStack: function() {
            var script_loader = this.getScriptLoader();
            var css_stack = this.getCSSStack();
            for(var i=0, len = css_stack.length; i<len; i++) {
                script_loader.inject_css_file(css_stack[i]);
            }
        },
        /**
         * Not required, but takes care of some initial loading.
         * If run without this, you need to take care of initialization and waiting for these
         * @param  {Function} callback
         * @return {null}
         */
        init: function(callback) {
            var timeout = setTimeout(function() {
                throw "OSS.init did not initialize";
            }, 4000);
            var checklist = {
                script_loader: false,
                emitter: false,
                session_factory: false,
            };

            var self = this;
            var ready = function(item) {
                if (self.debug) console.log('OSS.init::READY', item);
                checklist[item] = true;
                var all_true = true;
                for (var key in checklist) {
                    if (checklist[key] !== true) {
                        all_true = false;
                        break;
                    }
                }
                if (all_true && !callback.called) {
                    callback.called = true;
                    clearTimeout(timeout);
                    callback();
                }
            };
            this.getScriptLoader().loadScriptLoader(function() {
                ready('script_loader');
                self.getEmitter(ready.bind(self, 'emitter'));
                var session_factory = self.getSessionFactory();
                session_factory.createSessionProvider(ready.bind(self, 'session_factory'));
            });

        },

        urls: null,
        getUrls: function getUrls() {
            if (this.urls === null) {
                this.urls = new OSSURLs(this);
            }
            return this.urls;
        },
        script_loader: null,
        getScriptLoader: function() {
            if (this.script_loader === null) {
                this.script_loader = new OSSScriptLoader();
                this.script_loader.debug = this.debug;
            }
            return this.script_loader;
        },
        loadScript: function(script, callback) {
            this.getScriptLoader().loadScripts([script], callback);
        },
        loadScripts: function(scripts, callback) {
            this.getScriptLoader().loadScripts(scripts, callback);
        },
        loadCSS: function(href) {
            this.getScriptLoader().inject_css_file(href);
        },
        emitter: null,
        newEmitter: function(options) {
            //Warning: this assumes emitter is loaded. Since we call init(), this ia a good assumption
            if(!options) {
                options = {};
            }
            options.wildcard = options.wildcard || true;
            options.delimiter = options.delimiter || '::';
            options.newListener = options.newListener || false;
            options.maxListeners = options.maxListeners || true;
            return new EventEmitter2(options);
        },
        getEmitter: function(callback) {
            if (this.emitter === null) {
                var self = this;
                this.loadScript(CSrms.getLibsUrl(this.libs.emitter), function() {
                    self.emitter = new EventEmitter2({
                        wildcard: true,
                        delimiter: 'x',
                        newListener: false,
                        maxListeners: 20
                    });
                    callback(self.emitter);
                    if (self.debug) {
                        self.emitter.onAny(function() {
                            console.log('Emitter::onAny', this.event);
                        });
                    }
                });
                return;
            }
            callback(this.emitter);
            return this.emitter;
        },
        emit: function(event) {
            var all_args = arguments;
            var self = this;
            this.getEmitter(function(emitter) {
                emitter.emit.apply(emitter, all_args);
            });
        },
        once: function(event, listener) {
            this.getEmitter(function(emitter) {
                emitter.once(event, listener);
            });
        },
        /**
         *
         * ---------OSS.UserScriptMessaging---------
         *
         */
        oss_user_script_messaging: null,
        setUserScriptMessaging: function setUserScriptMessaging(user_script_messaging) {
            this.oss_user_script_messaging = user_script_messaging;
        },
        createUserScriptMessaging: function createUserScriptMessaging(callback) {
            if (this.oss_user_script_messaging !== null) {
                throw "user Script Messaging is already created";
            }
            var self = this;
            this.loadScript(CSrms.getAppUrl('OSS/GlobalCapacity.OSS.UserScriptMessaging.js'), function() {
                self.oss_user_script_messaging = new GlobalCapacity.OSS.UserScriptMessaging(self);
                if (callback) callback(self.oss_user_script_messaging);
            });
        },
        getUserScriptMessaging: function getUserScriptMessaging(callback) { //can call this without callback if you know it's already loaded
            if (this.oss_user_script_messaging !== null) {
                if (callback) callback.call(this, this.oss_user_script_messaging);
                return this.oss_user_script_messaging;
            }
            this.createUserScriptMessaging(callback);
            return null;
        },
        /**
         *
         * ---------OSS.UI---------
         *
         */
        oss_ui: null,
        setOSSUI: function(ui) {
            this.oss_ui = ui;
        },
        createOSSUI: function(callback) {
            if (this.oss_ui !== null) {
                throw "UI is already created";
            }
            var self = this;
            var scripts = [];
            if (typeof jQuery === 'undefined') scripts.push(CSrms.getLibsUrl(this.libs.jquery));
            scripts.push(CSrms.getAppUrl('OSS/GlobalCapacity.OSS.UI.js'));
            this.loadScripts(scripts, function() {
                self.oss_ui = GlobalCapacity.OSS.UI;
                self.oss_ui.oss = self;
                self.oss_ui.debug = self.debug;
                if (callback) callback(self.oss_ui);
            });
        },
        getOSSUI: function(callback) { //can call this without callback if you know it's already loaded
            if (this.oss_ui !== null) {
                if (callback) callback(this.oss_ui);
                return this.oss_ui;
            }
            this.createOSSUI(callback);
            return null;
        },
        /**
         *
         * ---------OSS.ControlsFactory---------
         * Should be able to have multiple instances per page load
         * No getter. Controls are not stored here. Creator is here as a convenience
         */
        createOSSControlsFactory: function(document_factory, callback) {
            var self = this;
            var scripts = [];
            scripts.push(CSrms.getGeboUrl('bootstrap/js/bootstrap.min.js'));
            scripts.push(CSrms.getLibsUrl('bootstrap-contextmenu/bootstrap-contextmenu-leftclick.js'));
            scripts.push(CSrms.getAppUrl('OSS/GlobalCapacity.OSS.ControlsFactory.js'));
            this.loadScripts(scripts, function() {
                var controls_factory = new GlobalCapacity.OSS.ControlsFactory(self, document_factory);
                controls_factory.debug = self.debug;
                if (callback) callback(controls_factory);
            });
        },
        /**
         *
         * ---------GlobalCapacity.TaskDocuments---------
         *
         */
        globalcapacity_taskdocuments: null,
        setGlobalCapacityTaskDocuments: function(globalcapacity_taskdocuments_obj) {
            this.globalcapacity_taskdocuments = globalcapacity_taskdocuments_obj;
        },
        createGlobalCapacityTaskDocuments: function(callback) {
            if (this.globalcapacity_taskdocuments !== null) {
                throw "Task Documents is already created";
            }
            var self = this;
            this.loadScript(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.js'), function() {
                self.globalcapacity_taskdocuments = GlobalCapacity.TaskDocuments;
                self.globalcapacity_taskdocuments.oss = self;
                self.globalcapacity_taskdocuments.debug = self.debug;
                if (callback) callback(self.globalcapacity_taskdocuments);
            });
        },
        getGlobalCapacityTaskDocuments: function(callback) { //can call this without callback if you know it's already loaded
            if (this.globalcapacity_taskdocuments !== null) {
                if (callback) callback(this.globalcapacity_taskdocuments);
                return this.globalcapacity_taskdocuments;
            }
            this.createGlobalCapacityTaskDocuments(callback);
            return null;
        },
        /**
         *
         * ---------GlobalCapacity.TimeMatrix---------
         *
         */
        globalcapacity_timematrix: null,
        setGlobalCapacityTimeMatrix: function(globalcapacity_timematrix_obj) {
            this.globalcapacity_timematrix = globalcapacity_timematrix_obj;
        },
        createGlobalCapacityTimeMatrix: function(callback) {
            if (this.globalcapacity_timematrix !== null) {
                throw "Time Matrix is already created";
            }
            var self = this;
            this.loadScript(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.js'), function() {
                self.globalcapacity_timematrix = GlobalCapacity.TimeMatrix;
                self.globalcapacity_timematrix.debug = self.debug;
                if (callback) callback(self.globalcapacity_timematrix);
            });
        },
        getGlobalCapacityTimeMatrix: function(callback) { //can call this without callback if you know it's already loaded
            if (this.globalcapacity_timematrix !== null) {
                if (callback) callback(this.globalcapacity_timematrix);
                return this.globalcapacity_timematrix;
            }
            this.createGlobalCapacityTimeMatrix(callback);
            return null;
        },
        loadGlobalCapacityTimeMatrix: function() {
            if(this.globalcapacity_timematrix !== null) {
                return new Promise.resolve(this.globalcapacity_timematrix);
            }
            var self = this;
            var promise = new Promise(function(resolve, error_hanlder) {
                self.getGlobalCapacityTimeMatrix(function() {
                    resolve(self.globalcapacity_timematrix);
                });
            });
            return promise;
        },

        /**
         *
         * ---------FieldPortal---------
         *
         */
        fieldportal: null,
        setFieldPortal: function(fieldportal_obj) {
            this.fieldportal = fieldportal_obj;
        },
        createFieldPortal: function(callback) {
            if (this.fieldportal !== null) {
                throw "Fieldportal is already created";
            }
            var self = this;
            this.loadScript(CSrms.getAppUrl('FieldPortal/FieldPortal.js'), function() {
                self.fieldportal = FieldPortal;

                //TODO: this should be put into FieldPortal.js
                var scripts = [];
                if(typeof CSV === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('CSV.js.3.3.1/csv.min.js'));
                }
                if(typeof jQuery === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('jquery-1.11.0/jquery.min.js'));
                }
                if(typeof _ === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('underscore-1.6.0/underscore.js'));
                }
                if(typeof Backbone === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('backbone-1.1.2/backbone.js'));
                }
                if(typeof Handlebars === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('handlebars-1.3.0/handlebars.js'));
                }
                scripts.push(CSrms.getAppUrl('FieldPortal/FieldPortal.Backbone.Dispatch.js'));
                scripts.push(CSrms.getAppUrl('FieldPortal/FieldPortal.JobCollectionFactory.js'));

                self.loadScripts(scripts, function() {
                });

                if (callback) callback(self.fieldportal);
            });
        },
        getFieldPortal: function(callback) { //can call this without callback if you know it's already loaded
            if (this.fieldportal !== null) {
                if (callback) callback(this.fieldportal);
                return this.fieldportal;
            }
            this.createFieldPortal(callback);
            return null;
        },
        loadFieldPortal: function() {
            if(FieldPortal !== undefined) {
                return new Promise.resolve(FieldPortal);
            }
            var self = this;
            var promise = new Promise(function(resolve, error_hanlder) {
                self.getFieldPortal(function(field_portal) {
                    resolve(field_portal);
                });
            });
            return promise;
        },
        /**
         *
         * ---------Initializers---------
         *
         */
        employee_set_loader: null,
        getEmployeeSetLoader: function() {
            if (this.employee_set_loader === null) {
                this.employee_set_loader = new OSSEmployeeSetLoader(this);
            }
            return this.employee_set_loader;
        },
        createDocumentFactory: function(document_body_elem, document_url) {
            return new OSSDocumentFactory(this, document_body_elem, document_url);
        },
        //
        //----SessionFactory:
        //Assume only one session factory.
        //Create it and make it available so we don't have to use callbacks
        //Use createSessionFactory, or initialize your own and set it to this.session_factory
        session_factory: null,
        createSessionFactory: function() {
            if (this.session_factory !== null) {
                throw "Session Factory is already created";
            }
            this.session_factory = new OSSSessionFactory(this);
            return this.session_factory;
        },
        getSessionFactory: function() {
            if (this.session_factory === null) {
                this.createSessionFactory();
            }
            return this.session_factory;
        },
        /**
         *
         * ---------Initializers---------
         *
         */
        loadUserOSSCredentials: function() {
            var promise = new Promise(function(resolve){
                resolve({
                    username: 'chenderson',
                    password: '1234'
                });
            });
            return promise;
        }
    };

    /**
     *
     *END OSS
     *====================================================================================================
     *
     *
     *
     */

    var OSSScriptLoader = function OSSScriptLoader() {
        this.debug = true;
        //this.basket_scriptloader_filepath = "basket.js-0.4.0/dist/basket.full.min.js";
        //this.LABjs_scriptloader_filepath = "LABjs-2.0.3/LAB.min.js";
        this.LABjs_scriptloader_filepath = "LABjs-2.0.3/LAB-debug.min.js";
        this.scriptloader_method = this.inject_scripts;
        //this.scriptloader_method = this.load_via_LABjs;
    };

    OSSScriptLoader.prototype = {
        inject_css_file: function inject_css_file(href) {
            var cssElement = document.createElement('link');
            cssElement.rel = 'stylesheet';
            cssElement.type = 'text/css';
            cssElement.href = href;
            document.getElementsByTagName('head')[0].appendChild(cssElement);
        },
        /**
         *
         * --------Script Injection Methods--------
         */
        inject_script: function inject_script(src, callback, async) {
            if (typeof async === 'undefined') {
                async = false;
            }
            var script_elem = document.createElement('script');
            script_elem.src = src;
            //CJH trying this: always set async: if (async === true) {
                //script_elem.async = true;
                script_elem.async = async;
            //}

            script_elem.onreadystatechange = script_elem.onload = function() {
                var state = script_elem.readyState;
                if (callback && !callback.done && (!state || /loaded|complete/.test(state))) {
                    callback.done = true;
                    callback();
                }
            };
            document.getElementsByTagName('head')[0].appendChild(script_elem);
        },
        inject_scripts: function inject_scripts(src_array, callback, async) {
            var loader_callback = false;
            for (var i = 0, len = src_array.length; i < len; i++) {
                if (i === len - 1) {
                    loader_callback = callback;
                }
                this.inject_script(src_array[i], loader_callback, async);
            }
        },
        /**
         *
         * --------basket.js Methods--------
         */
        basketjs: null,
        load_basketjs: function load_basketjs(callback) {
            if (this.basketjs !== null) {
                callback();
                return;
            }
            var self = this;
            this.inject_script(CSrms.getLibsUrl(this.basket_scriptloader_filepath), function() {
                self.basketjs = basket;
                callback.call(self);
            });
        },
        load_via_basketjs: function load_via_basketjs(scripts_array, callback) {
            if (this.basketjs === null) {
                //load basketjs and re-call this function
                this.load_basketjs(this.load_via_basketjs.bind(this, scripts_array, callback));
                return;
            }
            if (this.debug) {
                this.basketjs.clear();
            }
            this.basketjs.require.apply(
                this, scripts_array.map(function create_array_of_backet_objects(script) {
                    return {
                        url: script
                    };
                })
            ).then(function() {
                callback();
            }, function() {
                console.log("BASKET ERROR", arguments);
            });
        },
        /**
         *
         * ---------LABjs Methods---------
         */
        LABjs: null,
        load_LABjs: function load_LABjs(callback) {
            if (this.LABjs !== null) {
                callback();
                return;
            }
            var self = this;
            this.inject_script(CSrms.getLibsUrl(this.LABjs_scriptloader_filepath), function() {
                self.LABjs = $LAB;
                callback.call(self);
            });
        },
        load_via_LABjs: function load_via_LABjs(scripts_array, callback) {
            if (this.LABjs === null) {
                //load LABjs and re-call this function
                this.load_LABjs(this.load_via_LABjs.bind(this, scripts_array, callback));
                return;
            }
            if (this.debug) {
                this.LABjs.setOptions({
                    Debug: true
                }).script(scripts_array).wait(callback);
            } else {
                this.LABjs.script(scripts_array).wait(callback);
            }
        },

        /**
         *
         * ---------Main Public Methods---------
         */
        loadScriptLoader: function loadScriptLoader(callback) {
            switch (this.scriptloader_method) {
                case this.inject_scripts:
                    callback();
                    break;
                case this.load_via_basketjs:
                    this.load_basketjs(callback);
                    break;
                case this.load_via_LABjs:
                    this.load_LABjs(callback);
                    break;
                default:
                    throw script_loader_type + " is not a known script loader";
            }
        },
        loadScripts: function load_scripts(scripts, callback) {
            this.scriptloader_method(scripts, callback);
        },
        setScriptLoaderMethod: function(script_loader_type) {
            switch (script_loader_type) {
                case 'inject':
                    this.scriptloader_method = this.inject_scripts;
                    break;
                case 'basketjs':
                    this.scriptloader_method = this.load_via_basketjs;
                    break;
                case 'LABjs':
                    this.scriptloader_method = this.load_via_LABjs;
                    break;
                default:
                    throw script_loader_type + " is not a known script loader";
            }
        }

    };

    /**
     *
     * ===============Document Factory======================================================
     *
     */
    var OSSDocumentFactory = function(oss, document_body_elem, document_url) {
        this.oss = oss;
        this.document_body_elem = document_body_elem;
        this.document_url = document_url;
        //Generated element:
        this.document_obj = null;
    };
    OSSDocumentFactory.prototype = {
        triggerCreatedEvent: function() {
            this.oss.emit("OSS.DocumentObj::created", this.document_obj);
        },
        setDocumentObj: function(document_obj) {
            this.document_obj = document_obj;
            this.triggerCreatedEvent();
        },
        /**
         *
         * ---------Document Type---------
         *
         */
        getDocumentType: function(callback) {
            //if document obect if loaded, use that:
            if (this.document_obj) {
                callback(this.document_obj.getDocumentType());
                return;
            }

            var self = this;
            //try url first:
            this.getDocumentTypeByURL(function(document_type) {
                if (!document_type) {
                    //try by creating document object
                    self.getDocumentTypeByDocument(function(document_type) {
                        callback.call(self, document_type);
                    });
                } else {
                    callback.call(self, document_type);
                }
            });
        },
        /**
         *
         * ---------Document Type By URL---------
         *
         */
        getDocumentTypeByURL: function(callback) {
            var urls = this.oss.getUrls();
            var document_type = null;
            try {
                document_type = urls.getDocumentTypeByUrl(this.document_url);
            } catch (err) {
                //swallow the error
            }
            if (callback) {
                callback(document_type);
            }
            return document_type;
        },
        /**
         *
         * ---------Document Type By Document---------
         *
         */
        getDocumentTypeByDocument: function(callback) {
            this.createDocumentObj(function(document_obj) {
                if (!document_obj) {
                    callback(null);
                } else {
                    callback(document_obj.getDocumentType());
                }
            });
        },

        _create_timematrix_document: function(callback) {
            var self = this;
            this.oss.getGlobalCapacityTimeMatrix(function after_timematrix_loaded(globalcapacity_timematrix) {
                globalcapacity_timematrix.createDocumentObj(self.document_body_elem, function test_is_timemetrix(new_document_obj) {
                    console.log("INSDIE CREATE", new_document_obj);
                    var is_timematrix = new_document_obj.isTimeMatrixDocument();
                    if (is_timematrix) {
                        self.setDocumentObj(new_document_obj);
                        callback.call(self, self.document_obj);
                        return self.document_obj;
                    } else {
                        callback.call(self, null);
                        return null;
                    }
                });
            });
        },

        _create_task_document: function(callback) {
            var self = this;
            this.oss.getGlobalCapacityTaskDocuments(function test_task_type(globalcapacity_taskdocuments) {
                var document_type = globalcapacity_taskdocuments.getTaskType(self.document_body_elem);
                if (!document_type) {
                    //is not a known task document
                    //returning null
                    callback(null);
                } else {
                    globalcapacity_taskdocuments.createDocumentObj(self.document_body_elem, function is_task_document(new_document_obj) {
                        self.setDocumentObj(new_document_obj);
                        callback.call(self, self.document_obj);
                        return self.document_obj;
                    });
                }
            });
        },

        _create_document_object: function(document_type, callback) {

            switch (document_type) {
                case "Work Order":
                case "Trouble Ticket":
                case "Offnet Task":
                    this._create_task_document(callback);
                    break;
                case "TimeMatrix":
                    this._create_timematrix_document(callback);
                    break;
                default:
                    throw "unknown document type: " + document_type;
            }

        },

        /**
         *
         * ---------Document Obj---------
         * This is the main method of the factory
         */
        createDocumentObj: function(callback) {
            if (!callback) {
                callback = function() {};
            }
            if (this.document_obj !== null) {
                callback(this.document_obj);
                return this.document_obj;
            }
            //see if we can get document type by url:
            var document_type = this.getDocumentTypeByURL();
            if (document_type) {
                this._create_document_object(document_type, callback);
                return;
            }

            //otherwise, shoot in the dark
            //test taskdocuments first (most frequently used):
            var self = this;
            this._create_task_document(function test_task_document(taskdocument_obj) {
                if (!taskdocument_obj) {
                    //it's not a task document. maybe its a timematrix
                    self._create_timematrix_document(function test_timematrix(timematrix_document_obj) {
                        if (!timematrix_document_obj || !timematrix_document_obj.isTimeMatrixDocument()) {
                            throw "Document type cannot be determined";
                        }
                        //its a imematrix document, return it
                        callback(timematrix_document_obj);
                    });
                } else {
                    //return taskdocument_obj;
                    callback(taskdocument_obj);
                }
            });
        },
        getDocumentObj: function getDocumentObj(callback) {
            if (this.document_obj === null) {
                this.createDocumentObj(callback);
            } else {
                callback(this.document_obj);
            }
            return this.document_obj;
        }
    };

    /**
     *
     * ===============Session Factory=====================================================
     *
     */
    var OSSSessionFactory = function(oss, session_provider_type) {
        this.session_provider_types = ['user_script_messaging', 'standard'];
        if (!session_provider_type) {
            session_provider_type = 'standard';
        }
        this.oss = oss;
        this.session_provider_type = session_provider_type;
        //Generated:
        this.session_provider = null;
    };
    OSSSessionFactory.prototype = {
        triggerCreatedEvent: function() {
            this.oss.emit("OSS.Session::created", this);
        },
        setSessionProvider: function(session_provider) {
            this.session_provider = session_provider;
            this.triggerCreatedEvent();
        },


        getSessionProvider: function(callback) {
            if (this.session_provider === null) {
                this.createSessionProvider(callback);
                return;
            }
            if(callback) {
                callback(this.session_provider);
            }
            return this.session_provider;
        },

        createSessionProvider: function(callback) {
            //in this case, the session provider is simply UserScriptMessaging
            var self = this;
            switch (this.session_provider_type) {
                case 'standard':
                    var session_provider = new StandardSessionProvider();
                    self.setSessionProvider(session_provider);
                    callback(session_provider);
                    break;
                case 'user_script_messaging':
                    this.oss.getUserScriptMessaging(function(user_script_requestor) {
                        self.setSessionProvider(user_script_requestor);
                        callback(user_script_requestor);
                    });
                    break;
                default:
                    throw this.session_provider_type + " is not a valid session provider type";
            }
        }

    };

    var StandardSessionProvider = function() {
        this.fieldportal_session_url = 'http://synergy.fieldportal.net/fieldportal/ajax_session-CORS.php';
        //Internal:
        this.session_data = null;
    };
    StandardSessionProvider.prototype = {
        setSessionData: function(session_data) {
            this.session_data = session_data;
            //TODO: emit an event here
        },
        getSessionData: function(success, error) {
            if (this.session_data !== null) {
                success(this.session_data);
                return;
            }
            var httpRequest = new XMLHttpRequest();
            httpRequest.withCredentials = true;
            var self = this;
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        var session_data = JSON.parse(httpRequest.responseText);
                        self.setSessionData(session_data);
                        success(session_data);
                    } else {
                        error("Error in getSessionData");
                    }
                }
            };
            httpRequest.open('GET', this.fieldportal_session_url, true);
            httpRequest.send();
        },
        logIn: function(username, password, success, error) {
            var data = 'submit=Login&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);
            var httpRequest = new XMLHttpRequest();
            httpRequest.withCredentials = true;
            var self = this;
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        var session_data = JSON.parse(httpRequest.responseText);
                        self.setSessionData(session_data);
                        success(session_data);
                    } else {
                        error("Error in LogIn");
                    }
                }
            };
            httpRequest.open('POST', this.fieldportal_session_url);
            httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            httpRequest.send(data);
        },
        logOut: function(success, error) {
            var data = 'submit=Logout';
            var httpRequest = new XMLHttpRequest();
            httpRequest.withCredentials = true;
            var self = this;
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        var session_data = JSON.parse(httpRequest.responseText);
                        self.setSessionData(session_data);
                        success(session_data);
                    } else {
                        error("Error in logOut");
                    }
                }
            };
            httpRequest.open('POST', this.fieldportal_session_url);
            httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            httpRequest.send(data);
        }
    };

    /**
     *
     * ===============URLs====================================================================
     *
     */
    var OSSURLs = function(oss) {
        this.oss = oss;
        this.document_types = {
            timematrix: "TimeMatrix",
            work_order: "Work Order",
            trouble_ticket: "Trouble Ticket",
            offnet_task: "Offnet Task"
        };
        this.timematrix_url = "http://reports.oss.covad.com:10080/spreport/timeslot/TimeSlotOutput.jsp";
        this.trouble_ticket_url = "http://reports.oss.covad.com:10080/ossreport/ttbrowser/ShowTicketDetails.jsp?problem_id={{task_id}}";
        this.work_order_url = "http://mobile.oss.covad.com:17555/iworeport/iwo/IwoOutput.jsp?referrer=INTERNAL&worklog=Y&clientOrderId={{task_id}}";
        this.offnet_task_url = "http://status-int.oss.covad.com:15558/csp/details?oid={{task_id}}";
        var document_url_tests = {
            timematrix: [{
                host: "reports.oss.covad.com",
                port: "10080",
                file: "TimeSlotOutput.jsp"
            }, {
                host: "testtimematrix.local.dev",
                directory: "/TimeMatrixes/"
            }],
            work_order: [{
                host: "mobile.oss.covad.com",
                port: "17555"
            }, {
                host: "testtimematrix.local.dev",
                directory: "/TestTimeMatrix/WorkOrders/"
            }, {
                host: "testtimematrix.local.dev",
                directory: "/WorkOrders/"
            }],
            trouble_ticket: [{
                host: "reports.oss.covad.com",
                port: "10080",
                file: "ShowTicketDetails.jsp"
            }, {
                host: "testtimematrix.local.dev",
                directory: "/TestTimeMatrix/TroubleTickets/"
            }, {
                host: "testtimematrix.local.dev",
                directory: "/TroubleTickets/"
            }],
            offnet_task: [{
                host: "status-int.oss.covad.com",
                port: "15558"
            }, {
                host: "testtimematrix.local.dev",
                directory: "/TestTimeMatrix/OffnetTasks/"
            }]
        };
        /**
         *
         * ---------Document Type By URL---------
         *
         */
        this.getDocumentTypeByUrl = function(url) {
            if (!url) {
                url = document.location;
            }
            var url_parts = parseUri(document.location);
            var document_type = match_url_parts(url_parts);
            if (!document_type) {
                throw "Document type not found for " + url.toString();
            }
            return this.document_types[document_type];
        };
        var match_url_parts = function(url_parts) {
            var url_test;
            for (var document_type in document_url_tests) {
                if (!document_url_tests.hasOwnProperty(document_type)) {
                    continue;
                }
                url_test = document_url_tests[document_type];
                for (var i = 0, len = url_test.length; i < len; i++) {
                    if (test_url_parts_match(url_parts, url_test[i])) {
                        return document_type;
                    }
                }
            }
            return null;
        };
        var test_url_parts_match = function(search_parts, defined_parts) {
            for (var defined_part in defined_parts) {
                if (!defined_parts.hasOwnProperty(defined_part)) {
                    continue;
                }
                //Any mismatch will return false
                if (!search_parts[defined_part]) {
                    return false;
                }
                if (search_parts[defined_part] != defined_parts[defined_part]) {
                    return false;
                }
            }
            return true;
        };
        /**
         *
         * ---------Document URL Methods---------
         *
         */
        this.getWorkOrderUrl = function(task_id) {
            return this.work_order_url.replace('{{task_id}}', task_id);
        };
        this.getTroubleTicketUrl = function(task_id) {
            return this.trouble_ticket_url.replace('{{task_id}}', task_id);
        };
        this.getOffNetTaskUrl = function(task_id) {
            return this.offnet_task_url.replace('{{task_id}}', task_id);
        };
    };
    /**
     *
     * ===============URL Functions===========================================================
     *
     */
    var parseUri = function(str) {
        var o = parseUri.options,
            m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
            uri = {},
            i = 14;
        while (i--) uri[o.key[i]] = m[i] || "";
        uri[o.q.name] = {};
        uri[o.key[12]].replace(o.q.parser, function($0, $1, $2) {
            if ($1) uri[o.q.name][$1] = $2;
        });
        return uri;
    };
    parseUri.options = {
        strictMode: false,
        key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
        q: {
            name: "queryKey",
            parser: /(?:^|&)([^&=]*)=?([^&]*)/g
        },
        parser: {
            strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
            loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
        }
    };

    /**
     *
     * ===============EmployeeSetLoader===========================================================
     *
     */
    var OSSEmployeeSetLoader = function(oss) {
        this.oss = oss;
    };
    OSSEmployeeSetLoader.prototype = {
        _load_FieldPortal: function(callback) {
            this.oss.getFieldPortal(function(field_portal) {
                callback.call(this, field_portal);
            });
        },
        _load_EmployeeSet: function(callback) {
            this._load_FieldPortal(function(field_portal) {
                callback(field_portal.getFieldPortalEmployeeSet());
            });
        },
        loadEmployees: function(callback) {
            this._load_EmployeeSet(function(employee_set) {
                employee_set.loadEmployees(callback.bind(this, employee_set));
            });
        }
    };


    return OSS;
})();