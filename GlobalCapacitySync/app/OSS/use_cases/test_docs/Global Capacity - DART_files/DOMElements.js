
DOMElements = (function() {

    Tools = {
        getNextSibling: function(elem, filter) {
            var nextElem = elem.nextSibling;
            while (nextElem) {
                if (!filter || filter(nextElem)) {
                    return nextElem;
                }
                nextElem = nextElem.nextSibling;
            }
            return null;
        },
        tableFilter: function(elem) {
            switch (elem.nodeName.toUpperCase()) {
                case 'TABLE':
                    return true;
                default:
                    return false;
            }
        },
        divFilter: function(elem) {
            switch (elem.nodeName.toUpperCase()) {
                case 'DIV':
                    return true;
                default:
                    return false;
            }
        },
        getChildNodesByTagName : function(element, tag_name) {
            var children = element.children;
            var filtered = [];
            for (var i = 0; i < children.length; i++) {
                child = children[i];
                if (children[i].nodeName === tag_name) {
                    filtered.push(children[i]);
                }
            }
            return filtered;
        },

        getTextNodeValueArray : function(element) {
            var child_nodes = element.childNodes;
            var text_values = [];
            var child_node,node_value;
            for (var i = 0, len = child_nodes.length; i < len; i++) {
                child_node = child_nodes[i];
                if (child_node.nodeName !== '#text') {
                    continue;
                }
                node_value = child_node.nodeValue.trim();
                if (node_value === '') {
                    continue;
                }
                text_values[text_values.length] = node_value;
            }
            return text_values;
        }

    };

    Filters = {
        clean_text: function(text) {
            //trim . strip newlines . strip extra whitespace
            return text.replace(/^\s+|\s+$/g, '').replace(/\r?\n|\r/g, ' ').replace(/\s+/g, ' ');
        },
        rtrim_colon: function(text) {
            return text.replace(/:+$/g, '');
        },
        getElementCleanText: function(elem) {
            if (!elem || elem.textContent === undefined) {
                return null;
            }
            var text = elem.textContent;
            return Filters.clean_text(text);
        },
        getElementRegexpResult: function(elem, regexp) {
            if (!elem || elem.textContent === undefined) {
                return null;
            }
            var text = elem.textContent;
            var result = regexp.exec(text);
            return result;
        },
        getElementTextTrimColon: function(elem) {
            var text = this.getElementCleanText(elem);
            return Filters.rtrim_colon(text);
        },
    };


    var ChildElementMap = function ChildElementMap(parent_elem, tag_name) {
        this.parent_elem = parent_elem;
        this.element_map = parent_elem.getElementsByTagName(tag_name);
        this.text_map = null;
    };

    ChildElementMap.prototype = {
        getTextMap: function() {
            if (this.text_map === null) {
                this.text_map = [];
                for (var i = 0, len = this.element_map.length; i < len; i++) {
                    this.text_map.push(Filters.getElementCleanText(this.element_map[i]));
                }
            }
            return this.text_map;
        },
        findByText: function(text) {
            if (this.text_map === null) {
                this.getTextMap();
            }
            for (var i = 0, len = this.text_map.length; i < len; i++) {
                if (this.text_map[i] === text) {
                    return this.element_map[i];
                }
            }
            return null;
        },
        findByRegExp: function(re) {
            if (this.text_map === null) {
                this.getTextMap();
            }
            for (var i = 0, len = this.text_map.length; i < len; i++) {
                if (re.test(this.text_map[i])) {
                    return this.element_map[i];
                }
            }
            return null;
        }
    };


    return {
        Tools: Tools,
        Filters: Filters,
        ChildElementMap: ChildElementMap,
    };

})();