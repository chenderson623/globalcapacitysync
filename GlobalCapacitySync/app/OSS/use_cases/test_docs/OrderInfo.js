function showCustomerInfo(cinfo) {
  document.write("<h5>CUSTOMER INFO</h5>");
  document.write("<table width='100%' cellpadding='5' border='1' cellspacing='0'>");
  document.write("<tr><th class='info'>Partner<br>+ WO</th><td>"+pairValue(cinfo.partner)
   +(cinfo.clarityid? "<br>Clarity <a href='link?clarityid="+cinfo.clarityid+"'>#"+cinfo.clarityid+"</a>":"")
   +(cinfo.wo? (cinfo.clarityid?" / ":"<br>")+"WO <a href='link?wo="+cinfo.wo+"'>#"+cinfo.wo+"</a>":"")
   +(cinfo.ccn?"<br>CCN <a href='link?ccn="+cinfo.ccn+"'>"+cinfo.ccn+"</a>":"")+(cinfo.singledispatch? "<br>Single Dispatch":"")
   +"</td></tr>");
  document.write("<tr><th>Site<br>Info</th><td>"+cinfo.clientfirst+" "+cinfo.clientlast
   +(cinfo.companyname?" / "+cinfo.companyname:"")+"<br>"+cinfo.sitestreet
   +(cinfo.siteunit?", "+cinfo.siteunit:"")
   +"<br>"+cinfo.sitecity+", "+cinfo.sitestate+" "+cinfo.sitezip+"<br>");
  document.write((cinfo.contactphone?"&nbsp;<i>Site: </i>"
   +cinfo.sitephone+"<br>&nbsp;<i>Primary:</i> "+cinfo.contactphone:"&nbsp;<i>Site/Primary:</i> "+cinfo.sitephone)
   +(cinfo.altcontactphone?"<br>&nbsp;<i>Alternate:</i> "+cinfo.altcontactphone:"")
   +(cinfo.email?"<br>&nbsp;<i>Email:</i> <a href='mailto:"+cinfo.email+"'>"+cinfo.email+"</a><br>":"")
   +"</td></tr>");
  if (cinfo.region) document.write("<tr><th>Region</th><td>"+cinfo.region+"</td></tr>");
  document.write("</table>");
}

function showOrderInfo(oinfo) {
  document.write("<h5>ORDER INFO</h5>");
  document.write("<table width='100%' cellpadding='5' border='1' cellspacing='0'>"); document.write("<tr><th class='info'>Service<br>Info</th><td>"+pairValue(oinfo.service));
  if (oinfo.sowid) {
     sowid= pairKey(oinfo.sowid);
     sow= pairValue(oinfo.sowid);
    document.write("<br><a href='link?sowid="+sowid+"'>SOW "+sowid+": "+sow+"</a>");
  }
  document.write("<hr><i>Instructions/Notes: </i><br>"+oinfo.notes.replace(/\n/g,"<br>").replace(/https?:\/\/[^ \t\r\n<]*/g,"<a href='$&'>$&</a>")+"</td></tr>");
  if (oinfo.equipment) {
   document.write("<tr><th>Equipment<br>Info</th><td>");
  //document.write("<i>Customer Supplied:<br></i>"oinfo.customerequip.replace(/<br>/g,"\n")+"<hr>");
   if ("shift" in oinfo.equipment) {
     for(var i=0; i<oinfo.equipment.length; ++i) {
       writeEquipment(oinfo.equipment[i],oinfo.username[i],oinfo.password[i],oinfo.wansubnet[i],oinfo.wancidr[i],oinfo.lansubnet[i],oinfo.lancidr[i])
     }
   } else {
     writeEquipment(oinfo.equipment,oinfo.username,oinfo.password,oinfo.wansubnet,oinfo.wancidr,oinfo.lansubnet,oinfo.lancidr)
   }
  }
  document.write("</td></tr></table>");
}

function writeEquipment(eq,usr,pwd,wansn,wancidr,lansn,lancidr) {
      document.write("<u>"+pairValue(eq)+"</u><br>");
      if(usr) document.write("&nbsp;<i>U/P:</i>"+usr+"/"+pwd+"<br>");
      if(wansn) document.write("&nbsp;<i>WAN:</i>"+wansn+"/"+wancidr+"<br>");
      if(lansn) document.write("&nbsp;<i>LAN:</i>"+lansn+"/"+lancidr+"<br>");
}

function passCustomerInfo(cinfo) {
  document.write("<input type='hidden' name='servicecategoryid' value='"+(cinfo.singledispatch?0:1)+"'>"); // repurposed for singledispatch
  if(cinfo.clarityid) document.write("<input type='hidden' name='clarityid' value='"+cinfo.clarityid+"'>");
  if(cinfo.wo) document.write("<input type='hidden' name='wo' value='"+parseInt(cinfo.wo)+"'>");
  if(cinfo.ccn) document.write("<input type='hidden' name='covadcircuitnumber' value='"+cinfo.ccn+"'>");
  document.write("<input type='hidden' name='customerid' value='"+parseInt(pairKey(cinfo.partner))+"'>");
  if(/:[0-9]/.test(pairKey(cinfo.partner))) document.write("<input type='hidden' name='affiliateid' value='"+parseInt(pairKey(cinfo.partner).split(':')[1])+"'>");
  document.write("<input type='hidden' name='first' value='"+htmlEncode(cinfo.clientfirst)+"'>");
  document.write("<input type='hidden' name='last' value='"+htmlEncode(cinfo.clientlast)+"'>");
  if(cinfo.companyname)document.write("<input type='hidden' name='company' value='"+htmlEncode(cinfo.companyname)+"'>");
  document.write("<input type='hidden' name='street' value='"+htmlEncode(cinfo.sitestreet)+"'>");
  if(cinfo.siteunit)document.write("<input type='hidden' name='unit' value='"+htmlEncode(cinfo.siteunit)+"'>");
  document.write("<input type='hidden' name='city' value='"+htmlEncode(cinfo.sitecity)+"'>");
  document.write("<input type='hidden' name='state' value='"+cinfo.sitestate+"'>");
  document.write("<input type='hidden' name='zip' value='"+cinfo.sitezip+"'>");
  document.write("<input type='hidden' name='telephone' value='"+cinfo.sitephone+"'>");
  document.write("<input type='hidden' name='primarytelephone' value='"+cinfo.contactphone+"'>");
  document.write("<input type='hidden' name='alttelephone' value='"+cinfo.altcontactphone+"'>");
  document.write("<input type='hidden' name='email' value='"+htmlEncode(cinfo.email)+"'>");
  //document.write("<input type='hidden' name='customerequip' value='"+htmlEncode(oinfo.customerequip)+"'>");
}

function passOrderInfo(oinfo) {
  var n=0;
  //document.write("<input type='hidden' name='servicecategoryid' value='"+parseInt(pairKey(oinfo.ordercategory))+"'>"); //moved to passCustomerInfo to use for singledispatch
  document.write("<input type='hidden' name='dispatchserviceid' value='"+parseInt(pairKey(oinfo.service))+"'>");
  document.write("<input type='hidden' name='notes' value='"+htmlEncode(oinfo.notes)+"'>");
  if(oinfo.sowid) document.write("<input type='hidden' name='sowid' value='"+pairKey(oinfo.sowid)+"'>");
  if (oinfo.equipment) {
    var n=0;
    if ("shift" in oinfo.equipment) {
     for(var i=0; i<oinfo.equipment.length; ++i) {
       ++n;
       passEquipment(n,oinfo.equipment[i],oinfo.username[i],oinfo.password[i],oinfo.wansubnet[i],oinfo.wancidr[i],oinfo.lansubnet[i],oinfo.lancidr[i])
     }
    } else {
     passEquipment(++n,oinfo.equipment,oinfo.username,oinfo.password,oinfo.wansubnet,oinfo.wancidr,oinfo.lansubnet,oinfo.lancidr)
    }
  }
  document.write("<input type='hidden' name='orderedequipment.size' value='"+n+"'>");
}

function passEquipment(n,eq,usr,pwd,wansn,wancidr,lansn,lancidr) {
     document.write("<input type='hidden' name='"+n+".orderedequipmentid' value='"+pairKey(eq)+"'>");
     if (usr) document.write("<input type='hidden' name='"+n+".orderedcpeusername' value='"+usr+"'>");
     if (pwd) document.write("<input type='hidden' name='"+n+".orderedcpepassword' value='"+pwd+"'>");
     if (wansn) document.write("<input type='hidden' name='"+n+".orderedipaddress' value='"+wansn+"/"+wancidr+"'>");
     if (lansn) document.write("<input type='hidden' name='"+n+".orderedsubnetmask' value='"+lansn+"/"+lancidr+"'>");
}

function pairValue(obj) {
  return obj[pairKey(obj)];
}

function pairKey(obj) {
  for(var k in obj) {
    return k;
  }
}

function saveInfo(cinfo,oinfo) {
  var order1a={}
  order1a.contactinfo= cinfo
  var order2a={}
  order2a.equip= oinfo
  setCookie('order1a',JSON.stringify(order1a))
  setCookie('order2a',JSON.stringify(order2a))
}
