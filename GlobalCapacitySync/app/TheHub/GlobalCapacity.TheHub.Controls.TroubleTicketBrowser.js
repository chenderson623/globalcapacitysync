'use strict';
if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TheHub == 'undefined') {
    throw 'GlobalCapacity.TheHub is required.';
}

if (typeof GlobalCapacity.TheHub.Controls == 'undefined') {
    throw 'GlobalCapacity.TheHub is required.';
}

GlobalCapacity.TheHub.Controls.TroubleTicketBrowser = (function() {
    /*jshint multistr: true */
    'use strict';

var watch_elem = document.getElementById('m_content');
var report_viewer_elem;

function lookForTT() {
    GM_log('look for TT');

    report_viewer_elem = watch_elem.querySelector('#VisibleReportContentm_sqlRsWebPart_ctl00_ReportViewer_ctl09');

    var table_elems = report_viewer_elem.querySelectorAll('table');
    for(var i=0, len=table_elems.length; i<len; i++) {
        GM_log(table_elems[i]);
        if(table_elems[i].lang) GM_log('HAS LANG');
        GM_log(table_elems[i].rows[0].cells[0].textContent);
        if(table_elems[i].rows[0].cells[0].textContent == 'Trouble Ticket Browser') {
            GM_log("IS TT!!!");
        }
    }

}



    var WatchForTroubleTicket = {
        // Need to set these two:
        watch_elem: null,
        callback: null,

        watch_added_elem_id: 'VisibleReportContentm_sqlRsWebPart_ctl00_ReportViewer_ctl09',
        mutation_observer: null,

        trouble_ticket: null,

        nextParentTable: function(elem) {
            var parent = elem.parentElement;
            while( parent.tagName !== 'TABLE' && parent.parentElement) {
                parent = parent.parentElement;
            }
            return parent;
        },

        getParentTable: function(elem, count) {
            var parent = elem;
            while(count > 0 && parent) {
                console.log("PARENT", parent);
                parent = this.nextParentTable(parent);
                count--;
            }
            return parent;
        },

        lookForTT: function(target) {
            console.log('look for TT');

            var table_elems = target.querySelectorAll('table');
            for(var i = 0, len = table_elems.length; i < len; i++) {
                if(table_elems[i].rows[0].cells[0].textContent == 'Trouble Ticket Browser') {
                    console.log("IS TT!!!", table_elems[i]);
                    return this.getParentTable(table_elems[i], 1);
                }
            }
            return false;
        },

        createMutationObserver: function(watch_added_elem_id, found_callback) {
            var self = this;

            return new MutationObserver(function(mutations) {
              mutations.forEach(function(mutation) {
                console.log("MUTATION TARGET", mutation.target);
                if(!mutation.addedNodes) return;
                if(mutation.addedNodes.length > 0) {
                    if(mutation.target.id == watch_added_elem_id) {
                        setTimeout(function(){

                            var tt = self.lookForTT(mutation.target);
                            if(tt) {
                                self.trouble_ticket = tt;
                                self.disconnectMutationObserver();
                                found_callback(tt);
                                return;
                            }


                        }, 500);
                    }

                }
              });
            });
        },

        disconnectMutationObserver: function() {
            if(this.mutation_observer) {
                this.mutation_observer.disconnect();
                this.mutation_observer = null;
            }
        },

        watch: function(watch_elem, callback) {
            this.watch_elem = watch_elem;
            this.callback   = callback;

            this.disconnectMutationObserver();

            // see if it's there already:
            var target = this.watch_elem.querySelector('#' + this.watch_added_elem_id);
            if(target) {
                var tt = this.lookForTT(target);
                if(tt) {
                    this.trouble_ticket = tt;
                    callback(tt);
                    return;
                }
            }

            this.mutation_observer = this.createMutationObserver(this.watch_added_elem_id, callback);
            this.mutation_observer.observe(this.watch_elem, {childList:true, subtree: true});
        }
    };

    return {
        WatchForTroubleTicket: WatchForTroubleTicket
    };
})();
