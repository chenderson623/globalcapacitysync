'use strict';
if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TheHub == 'undefined') {
    throw 'GlobalCapacity.TheHub is required.';
}

GlobalCapacity.TheHub.Controls = (function() {
    /*jshint multistr: true */
    'use strict';

    /**
     *
     *
     * ============================Nav Items================================================
     */
    var NavBarDocumentType = function(thehub_model) {
        this.thehub_model = thehub_model;

        this.template = ' \
            <div class="document-type-controls"> \
                <h4>The Hub</h4> \
                <div>' + this.thehub_model.get('thehub_action') + '</div> \
            <\/div>';
    };
    NavBarDocumentType.prototype = {
        getHtml: function() {
            return this.template;
        }
    };


    var SyncControlFactory = function(document_object, thehub_model) {
        this.document_object = document_object;
        this.thehub_model      = thehub_model;

        this.jobdetails_controls_factory = null;
    };
    SyncControlFactory.prototype = {
        generateUIControls: function(ui) {
            //Document-type info piece
            this.document_type_control = new NavBarDocumentType(this.thehub_model);
            ui.setMiddleItem(this.document_type_control.getHtml());
            ui.setMiddleDivider();

            var self = this;

            if(this.thehub_model.get('thehub_action') == 'Trouble Ticket Browser') {
                var load_troubleticketbrowser_controls = self.document_object.thehub.loadTroubleTicketBrowserControls();

                load_troubleticketbrowser_controls.then(function(troubleticketbrowser_controls){

                    troubleticketbrowser_controls.WatchForTroubleTicket.watch(self.document_object.thehub_dom_obj.getMContentElement(), function(result){
                        console.log("WATCH RESULT", result);

//TODO: This is test:
  var task_documents = GlobalCapacity.TaskDocuments;
  task_documents.createTroubleTicketDocument(result, function(tt_document){
      console.log('Task ID',tt_document.getTaskId());
      console.log('Order Status',tt_document.getOrderStatus());

      console.log(tt_document.getSummaryData());
      console.log(tt_document.getEndUserData());
      console.log(tt_document.getWorkOrderData());
      console.log(tt_document.getTaskData());

  });





                    });

                });

            }
        }
    };

    return {
        SyncControlFactory: SyncControlFactory
    };
})();
