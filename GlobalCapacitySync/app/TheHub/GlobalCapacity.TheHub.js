'use strict';
var GlobalCapacity = GlobalCapacity || {};
if (typeof GlobalCapacity.TheHub !== 'undefined') {
    throw 'GlobalCapacity.TheHub already defined.';
}

GlobalCapacity.TheHub = (function() {
    /**
     *
     *
     * ============================TheHub (main entry point)=======================================
     */
    var TheHub = {
        /**
         *
         * =============Standard Module Stuff==================================
         */
        oss: null,
        task_documents: null,
        autoload: true,
        debug: false,
        getOSS: function getOSS(callback) {
            if(this.oss === null) {
                if(typeof GlobalCapacity.OSS !== 'undefined') {
                    this.oss =GlobalCapacity.OSS;
                } else {
                    //autoload it ourselves
                    //TODO: trigger warning that we are autoloading OSS
                    var self = this;
                    //OSS path must be absolute
                    this.inject_script('/public/GlobalCapacity/OSS/GlobalCapacity.OSS.js', function after_oss_load(){
                        self.oss = GlobalCapacity.OSS;
                        GlobalCapacity.OSS.getScriptLoader().setScriptLoaderMethod('LABjs');
                        callback.call(self, self.oss);
                    });
                    return;
                }
            }
            callback.call(this, this.oss);
        },
        //convenience function to allow us to load OSS programatically (probably only for testing)
        inject_script: function inject_script(src, callback) {
            if(this.autoload === false) {
                throw "Cannot autoload scripts";
            }
            var script_elem = document.createElement('script');
            script_elem.src = src;
            script_elem.onreadystatechange = script_elem.onload = function() {
                var state = script_elem.readyState;
                if (callback && !callback.done && (!state || /loaded|complete/.test(state))) {
                    callback.done = true;
                    callback();
                }
            };
            document.getElementsByTagName('head')[0].appendChild(script_elem);
        },
        loadScripts: function loadScripts(scripts, callback) {
            if(this.autoload === false) {
                throw "Cannot autoload scripts";
            }
            this.getOSS(function(oss){
                oss.loadScripts(scripts, callback);
            });
        },

        /**
         *
         *
         * ============================Loaders=============================================================
         */
        //alias for createTheHubDocument
        createDocumentObj: function(document_elem, callback) {
            this.createTheHubDocument(document_elem, callback);
        },

        createTheHubDocument: function(document_elem, callback) {
            var self = this;
            this.loadTheHubDocument(function(){
                self.oss.getGlobalCapacityTaskDocuments(function(task_documents){
                    self.task_documents = task_documents;

                    self.createTheHubDOM(document_elem, function(thehub_dom){
                        var thehub_document = new GlobalCapacity.TheHub.Document(thehub_dom, self);
                        callback.call(self, thehub_document);
                    });

                });
            });
        },

        loadTheHubDocument: function(callback) {
            if(typeof GlobalCapacity.TheHub.Document !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableWrapper === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableWrapper.js'));
            }
            //scripts.push(CSrms.getAppUrl('TheHub/GlobalCapacity.TheHub.Views.js'));
            //scripts.push(CSrms.getAppUrl('TheHub/GlobalCapacity.TheHub.Parsers.js'));
            scripts.push(CSrms.getAppUrl('TheHub/GlobalCapacity.TheHub.Document.js'));
            this.loadScripts(scripts, callback);
        },

        createTheHubDOM: function(thehub_document_elem, callback) {
            var self = this;
            this.loadTheHubDOM(function(){
                var thehub_dom_obj;
                thehub_dom_obj = new GlobalCapacity.TheHub.DOM(thehub_document_elem);
                callback(thehub_dom_obj);
            });
        },

        loadTheHubDOM: function(callback) {
            if(typeof GlobalCapacity.TheHub.DOM !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableWrapper === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableWrapper.js'));
            }
            scripts.push(CSrms.getAppUrl('TheHub/GlobalCapacity.TheHub.DOM.js'));
            this.loadScripts(scripts, callback);
        },

        loadControls: function(callback) {
            if(GlobalCapacity.TheHub.Controls !== undefined) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.TheHub.Controls);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];

                if(typeof jQuery === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('jquery-1.11.0/jquery.min.js'));
                }
                if(typeof _ === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('underscore-1.6.0/underscore.js'));
                }
                if(typeof Backbone === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('backbone-1.1.2/backbone.js'));
                    scripts.push(CSrms.getLibsUrl('backbone-forms-20141202/distribution/backbone-forms.min.js'));
                }
                if(typeof Handlebars === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('handlebars-1.3.0/handlebars.js'));
                }
                if(typeof Sortable === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('sortable-0.8.0/sortable.js'));
                    self.getOSS(function(oss){
                        oss.loadCSS(CSrms.getLibsUrl('sortable-0.8.0/css/sortable-theme-minimal.css'));
                    });
                }

                scripts.push(CSrms.getAppUrl('TheHub/GlobalCapacity.TheHub.Controls.js'));
                scripts.push(CSrms.getAppUrl('TheHub/GlobalCapacity.TheHub.Backbone.Models.js'));
                self.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.TheHub.Controls);
                });
            });

            if(callback){
                promise.then(function(controls_obj){
                    callback(controls_obj);
                });
            }

            return promise;
        },

        loadTroubleTicketBrowserControls: function(callback) {
            if(GlobalCapacity.TheHub.Controls && GlobalCapacity.TheHub.Controls.TroubleTicketBrowser !== undefined) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.TheHub.Controls.TroubleTicketBrowser);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];

                scripts.push(CSrms.getAppUrl('TheHub/GlobalCapacity.TheHub.Controls.TroubleTicketBrowser.js'));

                self.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.TheHub.Controls.TroubleTicketBrowser);
                });
            });

            if(callback){
                promise.then(function(controls_obj){
                    callback(controls_obj);
                });
            }

            return promise;
        },

    };


    return TheHub;
})();