'use strict';
if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TheHub == 'undefined') {
    throw 'GlobalCapacity.TheHub is required.';
}

GlobalCapacity.TheHub.Document = (function() {
    /**
     *
     *
     * ============================Document (main entry point)=======================================
     */
    //TODO: make this prototype
    var Document = function(thehub_dom_obj, thehub) {
        this.thehub = thehub;
        this.thehub_dom_obj = thehub_dom_obj;
        this.thehub_model = null;

        this.getDocumentType = function() {
            return "TheHub";
        };

        this.isTheHubDocument = function() {
            return this.thehub_dom_obj.isTheHubDocument();
        };

        this.isTroubleTicketBrowser = function() {
            return this.thehub_dom_obj.isTroubleTicketBrowser();
        };


        /**
         * ======================Controls Methods======================================
         *
         */
        this.getTheHubModel = function() {
            if(this.thehub_model === null) {
                this.thehub_model = new GlobalCapacity.TheHub.Backbone.Models.TheHubModel();

                if(this.isTroubleTicketBrowser()) {
                    this.thehub_model.set('thehub_action', 'Trouble Ticket Browser');
                }
            }
            return this.thehub_model;
        };

        this.createSyncControlsFactory = function(callback) {
            var self = this;
            var load_controls = this.thehub.loadControls();

            this.thehub.task_documents.loadParseChild(function(parse_child){
                var parent = window.opener || window.parent;
                if(parent !== window) {
                    var child = new GlobalCapacity.TaskDocuments.ParseChild(document);

                    // override ParseChild.getTaskDocument:
                    child.getTaskDocument = function(callback) {
                        console.log("getTaskDocumentXXX");
                    }

                    child.getTaskDocumentWindowChannel(function(taskdocument_window_channel) {});
                }
                load_controls.then(function(controls){
                    self.SyncControlFactory = new controls.SyncControlFactory(self, self.getTheHubModel());
                    callback(self.SyncControlFactory);
                });


            });

/*
            load_controls.then(function(controls){
                self.SyncControlFactory = new controls.SyncControlFactory(self, self.getTheHubModel());
                callback(self.SyncControlFactory);
            });
*/
        };

    };

    /**
     *
     *
     * ============================Schedule=========================================================
     */
    var Schedule = function(schedule_table_dom) {
        this.schedule_table_dom = schedule_table_dom;

        this.getScheduleDOMMap = function() {
            if (!this.schedule_table_dom) {
                throw "No schedule dom is set";
            }
            return this.schedule_table_dom.getScheduleDOMMap();
        };

    };

    function ScheduleMapsIterator(schedule_document) {
        this.schedule_document = schedule_document;
        this.on_schedule_row = [];

        this.schedule_dom_map = this.schedule_document.getScheduleDOMMap();
        this.schedule_task_map = this.schedule_document.getScheduleTaskMap();
        this.schedule_data_map = this.schedule_document.getScheduleDataMap();

    }

    return Document;
})();