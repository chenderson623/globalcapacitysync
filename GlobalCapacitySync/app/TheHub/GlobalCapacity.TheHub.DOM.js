'use strict';
if(typeof DOMTableWrapper == 'undefined') {
	throw 'DOMTableWrapper is required.';
}
if(typeof DOMElements == 'undefined') {
	throw 'DOMElements is required.';
}

if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TheHub == 'undefined') {
    throw 'GlobalCapacity.TheHub is required.';
}

GlobalCapacity.TheHub.DOM = (function() {

	/**
	 *
	 *
	 * ============================Document (main entry point)=======================================
	 */

	var DOM = function DOM(document_elem) {

		this.elem = document_elem;
        //Internal:
		this.content_div = null;
		this.parameters_elem = null;
		this.schedule_table_elems_array = null;
		this.schedule_tables = [];
    };
    DOM.prototype = {
        isTheHubDocument: function() {
            var mcontent_elem = this.getMContentElement();
            if(mcontent_elem) {
                return true;
            }
            return false;
        },
        isTroubleTicketBrowser: function() {
            return document.title = 'Reporting Services - Trouble Ticket Browser';
        },
        getMContentElement: function() {
            return this.elem.querySelector('#m_content');
        }
    };


	/**
	 *
	 *
	 * ============================Return Values===============================================
	 */

	return DOM;

})();