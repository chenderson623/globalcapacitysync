'use strict';
//if (typeof Backbone == 'undefined') {
//    throw 'Backbone is required.';
//}
if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TheHub == 'undefined') {
    throw 'GlobalCapacity.TheHub is required.';
}

if (typeof GlobalCapacity.TheHub.Backbone == 'undefined') {
    GlobalCapacity.TheHub.Backbone = {};
}

GlobalCapacity.TheHub.Backbone.Models = (function() {


    // 2016-06-04 NOTE: this is the same as in Timemartix
    /**
     *
     *
     * ============================Extend Backbone=================================================
     * fullExtend: a better way to extend Bacbone objects. From: https://coderwall.com/p/xj81ua
     */
    (function(Model) {
        'use strict';
        // Additional extension layer for Models
        Model.fullExtend = function(protoProps, staticProps) {
            // Call default extend method
            var extended = Model.extend.call(this, protoProps, staticProps);
            // Add a usable super method for better inheritance
            extended._super = this.prototype;
            // Apply new or different defaults on top of the original
            if (protoProps.defaults) {
                for (var k in this.prototype.defaults) {
                    if (!extended.prototype.defaults[k]) {
                        extended.prototype.defaults[k] = this.prototype.defaults[k];
                    }
                }
            }
            return extended;
        };

    })(Backbone.Model);

    (function(View) {
        'use strict';
        // Additional extension layer for Views
        View.fullExtend = function(protoProps, staticProps) {
            // Call default extend method
            var extended = View.extend.call(this, protoProps, staticProps);
            // Add a usable super method for better inheritance
            extended._super = this.prototype;
            // Apply new or different events on top of the original
            if (protoProps.events) {
                for (var k in this.prototype.events) {
                    if (!extended.prototype.events[k]) {
                        extended.prototype.events[k] = this.prototype.events[k];
                    }
                }
            }
            return extended;
        };

        //from: http://ianstormtaylor.com/assigning-backbone-subviews-made-even-cleaner/
        View.prototype.assign = function (selector, view) {
            var selectors;
            if (_.isObject(selector)) {
                selectors = selector;
            }
            else {
                selectors = {};
                selectors[selector] = view;
            }
            if (!selectors) return;
            _.each(selectors, function (view, selector) {
                view.setElement(this.$(selector)).render();
            }, this);
        };


    })(Backbone.View);

    /**
     *
     *
     * ============================MODELS=================================================
     */

    /**
     *
     * TaskModel:
     */
    var TheHubModel = Backbone.Model.extend({
        defaults: {
            thehub_action: "",
        },
    });

    return {
        TheHubModel: TheHubModel
     };


})();