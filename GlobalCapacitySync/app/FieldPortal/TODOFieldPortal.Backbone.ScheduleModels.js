if (typeof Backbone === 'undefined') {
    throw 'Backbone is required.';
}

//TODO: no work done on this. can mirror GlobalCapacity schedule models

FieldPortal.Backone.ScheduleModeuls = (function() {

    //===== ScheduleModel =====

    var ScheduleModel = Backbone.Model.extend({
        defaults: {
            date: null,
            tech_schedules: null  //TechScheduleCollection
        },
        initialize: function() {
            this.set('tech_schedules', new TechScheduleCollection());
            this.get('tech_schedules').schedule = this;
        }
    });

    //===== ScheduleCollection =====

    var ScheduleCollection = Backbone.Collection.extend({
        model: ScheduleModel
    });
    ScheduleCollection.prototype.getSchedule = function(date_string, create_if_not_found) {
        if (typeof create_if_not_found === 'undefined')
            create_if_not_found = false;
        var retval = null;
        retval = this.each(function(schedule) {
            if (schedule.get('date_string') === date_string) {
                return schedule;
            }
        });
        if (!retval && create_if_not_found) {
            retval = this.createNewSchedule(date_string);
        }
        return retval;
    };
    ScheduleCollection.prototype.createNewSchedule = function(date_string) {
        var schedule = new ScheduleModel({date_string: date_string});
        this.add(schedule);
        return schedule;
    };
    ScheduleCollection.prototype.createNewTaskCollection = function() {

    };
    ScheduleCollection.fromJSON = function(json_object, task_model) {
        var schedule_collection = new ScheduleCollection();
        var schedule = schedule_collection.getSchedule(json_object.date_string, true);
        var tech_schedules_json = json_object.tech_schedules;

        var tech_schedule_json, tasks_json, task_json;
        var tech_schedule;

        var schedule_index = null;
        var task_index = null;

        for (schedule_index in tech_schedules_json) {
            tech_schedule_json = tech_schedules_json[schedule_index];
            tasks_json = tech_schedule_json.tasks;

            tech_schedule = schedule.get('tech_schedules').getTechSchedule(tech_schedule_json.tech_name, true);
            for (task_index in tasks_json) {

                task_json = tasks_json[task_index];
                //task      = tech_schedule.get('tasks').add(task_json);
                //console.log(task_json);
                task = new task_model(task_json);
                tech_schedule.get('tasks').add(task);
            }
        }
        return schedule_collection;
    };
    ScheduleCollection.prototype.getDatesArray = function() {
        var dates_array = [];

        this.each(function(schedule_model) {
            dates_array[dates_array.length] = schedule_model.getISODate();
        });

        return dates_array;
    };
    ScheduleCollection.prototype.eachTask = function(callback) {

        var tech_schedules, tasks;
        this.each(function(schedule) {

            tech_schedules = schedule.get('tech_schedules');

            tech_schedules.each(function(tech_schedule) {
                tasks = tech_schedule.get('tasks');
                tasks.each(function(task) {
                    callback(task);
                });
            });
        });

    };
    //===== TechScheduleModel =====

    var TechScheduleModel = Backbone.Model.extend({
        defaults: {
            tech_name: null,
            tasks: null  //TaskCollection
        },
        initialize: function() {
            this.set('tasks', new TaskCollection(null, {tech_schedule: this}));
        }
    });
    TechScheduleModel.prototype.getSchedule = function() {
        return this.collection.schedule;
    };

    //===== TechScheduleCollection =====

    var TechScheduleCollection = Backbone.Collection.extend({
        schedule: null,
        model: TechScheduleModel
    });
    TechScheduleCollection.prototype.getTechSchedule = function(tech_name, create_if_not_found) {
        if (typeof create_if_not_found === 'undefined')
            create_if_not_found = false;
        var retval = null;
        retval = this.each(function(tech_schedule) {
            if (tech_schedule.get('tech_name') === tech_name) {
                return tech_schedule;
            }
        });
        if (!retval && create_if_not_found) {
            retval = this.createNewTechSchedule(tech_name);
        }
        return retval;
    };
    TechScheduleCollection.prototype.createNewTechSchedule = function(tech_name) {
        var tech_schedule = new TechScheduleModel({tech_name: tech_name});
        this.add(tech_schedule);
        return tech_schedule;
    };




    //===== TaskModel =====
    //Generic Task model meant to be extended
    var TaskModel = Backbone.Model.extend({
        task_dom_obj: null,
        defaults: {} //Extend this with cleitn specific data
    });
    TaskModel.prototype.getTechSchedule = function() {
        return this.collection.tech_schedule;
    };
    TaskModel.prototype.getSchedule = function() {
        return this.collection.tech_schedule.getSchedule();
    };
    TaskModel.prototype.scrollIntoView = function() {
        if (this.task_dom_obj !== null) {
            this.task_dom_obj.scrollIntoView();
        }
    };

    //===== TaskCollection =====

    var TaskCollection = Backbone.Collection.extend({
        tech_schedule: null,
        initialize: function(models, options) {
            if (typeof options === 'object' && typeof options.tech_schedule !== 'undefined') {
                this.tech_schedule = options.tech_schedule;
            }
        }
        //model         : TaskModel  //pass a model type
    });


    //Return Public Objects:
    return {
        ScheduleCollection: ScheduleCollection,
        TaskCollection: TaskCollection,
        TaskModel: TaskModel
    };

})();