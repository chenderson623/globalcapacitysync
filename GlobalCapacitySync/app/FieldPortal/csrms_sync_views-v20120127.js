if (typeof jQuery === 'undefined') {
    throw 'jQuery is required.';
}
if (typeof Backbone === 'undefined') {
    throw 'Backbone is required.';
}
if (typeof Handlebars === 'undefined') {
    throw 'Handlebars is required.';
}

var CSrms = CSrms || {};


CSrms.SyncViews = (function() {

    //===========CommandIndexView===========================

    var CommandIndexView = Backbone.View.extend({
        className: 'command-index',
        render: function() {
            var $el = $(this.el);

            var print_tech_name = this.model.getTechName();

            //can access this.model.collection here. don't know how to get 'previous' though

            if (typeof this.options.parent_view === 'object') {
                if (this.options.parent_view.getPreviousSibling()) {
                    var prev_tech_name = this.options.parent_view.getPreviousSibling().model.getTechName();
                    if (prev_tech_name === print_tech_name) {
                        print_tech_name = '';
                    }
                }
            }

            var contents = print_tech_name;

            $el.append(contents);
            return this;
        }
    });

    //===========CommandControlView===========================

    var CommandControlView = Backbone.View.extend({
        className: "command-control",
        initialize: function() {
            _.bindAll(this, "render", "setEnabled");
            this.model.bind("change:enabled", this.setEnabled, this);
            this.model.bind("change:done", this.setDone, this);
            this.model.bind("change:in_progress", this.setInProgress, this);
        },
        events: {
            "change :checkbox": "toggleEnabled"
        },
        render: function() {
            var content = ' \
				\
				<div class="control-title">Upload Task</div>\
				<div class="control-enable"><input type="checkbox" class="control-enable-input" name="command_enabled" /></div>\
				<div style="clear:both;"></div>\
				\
				<div class="command_enable_message"></div>\
				<div class="command_button" style="width:100%;text-align:center;"><a href="#">Upload Now</a></div>\
			';
            $(this.el).append(content);
            this.setEnabled();
            return this;
        }
    });
    CommandControlView.prototype.toggleEnabled = function() {
        this.model.toggleEnabled();
    };
    CommandControlView.prototype.setEnabled = function() {
        if (this.model.get('enabled')) {
            this.$(":checkbox").attr('checked', 'checked');
            this.$(".command_enable_message").toggleClass('included');
            this.$(".command_enable_message").text(this.options.enabled_text);
        } else {
            this.$(":checkbox").attr('checked', '');
            this.$(".command_enable_message").toggleClass('not_included');
            this.$(".command_enable_message").text(this.options.not_enabled_text);
        }
    };
    CommandControlView.prototype.options = {
        enabled_text: '',
        not_enabled_text: 'Not Included'
    };
    CommandControlView.prototype.setDone = function() {
        var content = 'DONE';
        $(this.el).html(content);
    };
    CommandControlView.prototype.setInProgress = function() {
        this.options.parent_view.scrollIntoView();
        var content = 'IN PROGRESS';
        var $el = $(this.el);
        $el.html(content);
    };


    //===========ClientTaskView===========================

    var ClientTaskView = Backbone.View.extend({
        className: 'client-task'
                //override this:
                /*
                 render: function() {
                 $el = $(this.el);

                 $el.append(this.model.get('task_id') + "<br />" + this.model.get('service_type'));
                 return this;
                 }
                 */
    });


    //===========QueueJobView===========================

    var QueueJobView = Backbone.View.extend({
        className: 'queue-job',
        render: function() {
            var $el = $(this.el);
            $el.append("No Queue Job Found");
            $el.addClass('not-found');
            //console.log($el);
            return this;
        }

    });

    //===========CommandItemView===========================
    /**
     * Need to pass options.client_task_view_class
     */
    var CommandItemView = Backbone.View.extend({
        tagName: 'li',
        className: 'sync-command-item',
        initialize: function(options) {
            this.previous_sibling = options.previous_sibling;
            //console.log(this.options.client_task_view_class);
            this.command_index_view = new CommandIndexView({model: this.model, parent_view: this});
            this.client_task_view = new options.client_task_view_class({model: this.model.getClientTask(), parent_view: this});
            this.queue_job_view = new QueueJobView();
            this.command_control_view = new CommandControlView({model: this.model, parent_view: this});
        },
        render: function() {
            var $el = $(this.el);
            $el.append(this.command_index_view.render().el);
            $el.append(this.client_task_view.render().el);
            $el.append(this.queue_job_view.render().el);
            $el.append(this.command_control_view.render().el);
            return this;
        }

    });
    CommandItemView.prototype.getPreviousSibling = function() {
        return this.previous_sibling;
    };
    CommandItemView.prototype.scrollIntoView = function() {
        var $el = $(this.el);
        if (typeof $el.scrollintoview === 'function') {
            $el.next().scrollintoview();
        }
    };




    //===========CommandListView===========================

    var CommandListView = Backbone.View.extend({
        tagName: 'ul',
        className: 'sync-command-list',
        initialize: function(options) {
            var self = this;
            this._item_views = [];

            var last_view = null;
            var view;
            this.collection.each(function(control_model) {
                view = new CommandItemView({
                    client_task_view_class: options.client_task_view_class,
                    previous_sibling: last_view,
                    model: control_model
                });
                self._item_views.push(view);
                last_view = view;
            });
        },
        render: function() {
            var self = this;
            $(this.el).empty();
            _(this._item_views).each(function(view) {
                $(self.el).append(view.render().el);
            });
            return this;
        }
    });


    //===========SyncControlRunView===========================

    var SyncControlRunView = Backbone.View.extend({
        className: 'sync-control',
        initialize: function() {
            //_.bindAll(this,"render","setEnabled");
            //this.model.bind("change:num_commands",this.setEnabled,this);
            var _this = this;

            var countdown_content = this.model.get('num_commands') + ' Commands';
            this.countdownElement = $('<span>' + countdown_content + '</span>');

            this.model.bind("change:num_undone", this.updateCountdown, this);


        },
        events: {
            "click .run-commands-button": "runCommands"
        },
        render: function() {
            $(this.el).append(this.runButtonElement);
            $(this.el).append(this.countdownElement);
            return this;
        }
    });
    SyncControlRunView.prototype.runButtonElement = $('<input type="button" class="run-commands-button" value="RunXX" />');
    SyncControlRunView.prototype.countdownElement = null;
    SyncControlRunView.prototype.updateCountdown = function() {
        var num_commands = this.model.get('num_commands');
        var num_undone = this.model.get('num_undone');
        if (num_undone === 0) {
            this.countdownElement.html('Done');
        } else {
            this.countdownElement.html(num_undone + '/' + num_commands);
        }
    };
    SyncControlRunView.prototype.runCommands = function() {
        //console.log('RUN');
        this.model.run();
    };


    //===============ModalView======================
    function ModalView($controlpanel_container) {

        var options = {
            width: '900',
            height: '650',
            closed: 'true'
        };
        var mb_class = "containerPlus draggable resizable";
        var mb_options = "{buttons:'m,c', skin:'default', width:'" + options.width + "', height:'" + options.height + "', closed:'" + options.closed + "', iconized:'false', title:'Fieldportal Sync'}";

        $controlpanel_container.addClass(mb_class + ' ' + mb_options);
        //$controlpanel_container.attr('style',mb_style);

        $controlpanel_container.buildContainers({
            containment: "document",
            //elementsPath     : FieldPortal.getModulesUrl("Clients/CovadDSL/greasemonkey_scripts/elements/"),
            elementsPath: "/fieldportal.com/csrms-libs/mb.components/containerPlus-2.5.1/elements/",
            maintainOnWindow: true,
            onCreate: function(o) {
            },
            onResize: function(o) {
                o.mb_switchFixedPosition();
            },
            onClose: function(o) {
            },
            onCollapse: function(o) {
            },
            onIconize: function(o) {
            },
            onDrag: function(o) {
            },
            onRestore: function(o) {
            }
        });

        this.getJqueryElement = function() {
            return $controlpanel_container;
        };
    }

    //Return Public Objects:
    return {
        CommandItemView: CommandItemView,
        CommandListView: CommandListView,
        ClientTaskView: ClientTaskView,
        SyncControlRunView: SyncControlRunView,
        ModalView: ModalView
    };


})();