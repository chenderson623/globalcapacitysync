if(typeof Backbone == 'undefined') {
	throw 'Backbone is required.';
}

if(typeof FieldPortal.Backbone == 'undefined') {
	FieldPortal.Backbone = {};
}

FieldPortal.Backbone.Dispatch = (function(){

	//===== Dispatch.Job =====

	var Job = Backbone.Model.extend({
		url: CSrms.getAPIUrl('dispatch_queue/jobs?545643645654645'),
		defaults: {
			workorder_id: null,
			AssignedDate: null,
			AssignedTechName: null,
			AssignedTime: null,
			AssignedWorkOrder: null,
			ContractRefNum: null,
			SchedSeq: null,
			LocationCity: null,
			LocationState: null,
			LocationZip: null
		},

    initialize: function () {
      var that = this;
      // Hook into jquery
      // Use withCredentials to send the server cookies
      // The server must allow this through response headers
      $.ajaxPrefilter( function( options, originalOptions, jqXHR ) {
        options.xhrFields = {
          withCredentials: true
        };
        // If we have a csrf token send it through with the next request
        if(typeof that.get('_csrf') !== 'undefined') {
          jqXHR.setRequestHeader('X-CSRF-Token', that.get('_csrf'));
        }
      });
    },

	});


	//===== Dispatch.JobCollection =====

	var JobCollection = Backbone.Collection.extend({
		model: Job,
		url: "" //needs to be set
	});


	//===== Dispatch.Set =====
	// Enables us to load a parameter-based set of dispatch jobs
	var Set = Backbone.Model.extend({
		defaults: {
			report_dates: [],
			report_tech: null,
			report_region: null,
			url: ''
		},

		initialize: function(){
			this.dispatch_collection = new Collection();
		},

		fetch: function(options) {
			options = _.extend(options,{
				data: {report_dates:this.get('report_dates').join(',')},
				url: this.get('url')
			});
			return this.dispatch_collection.fetch(options);
		}
	});
	Set.prototype.dispatch_collection = null;

	//Return Public Objects:
	return {
		Job: Job,
		JobCollection: JobCollection,
		Set: Set
	};

})();