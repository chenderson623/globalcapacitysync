if(typeof FieldPortal == 'undefined') {
    throw 'FieldPortal is required.';
}

FieldPortal.JobCollectionFactory = function() {
}
FieldPortal.JobCollectionFactory.prototype = {
    stringify: function(report_params) {
        var stringified = '';
        if (report_params.report_tech) {
            if (stringified !== '') {
                stringified += '&';
            }
            stringified += 'report_tech=' + encodeURIComponent(report_params.report_tech);
        }
        if (report_params.report_region) {
            if (stringified !== '') {
                stringified += '&';
            }
            stringified += 'report_region=' + encodeURIComponent(report_params.report_region);
        }
        if (report_params.report_dates && report_params.report_dates.length) {
            if (stringified !== '') {
                stringified += '&';
            }
            stringified += 'report_dates=' + encodeURIComponent(report_params.report_dates.join(','));
        }
        return stringified;
    },
    loadJobsCSV: function(report_params) {
        var self = this;

        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                url: "http://synergy.fieldportal.net/api/dispatch_queue/jobs",
                data: self.stringify(report_params),
                type: "GET",
                dataType: "text",
                success: function(data, textStatus) {
                    resolve(data);
                },
                complete: function(xhr, textStatus) {},
                error: function(xhr, textStatus, errorThrown) {
                    if (xhr.status != 200) {
                        alert("Error, Data request could not be completed.");
                        return;
                    }
                    if (textStatus == "parsererror") {
                        alert("Error, Data response has errors.");
                        return;
                    }
                    alert("Error, Unknown error: " + textStatus);
                }
            });
        });
        return promise;
    },
    loadJobCollectionFromCSV: function(csv_text) {
        var job_collection = new FieldPortal.Backbone.Dispatch.JobCollection();
        var csv = new CSV(csv_text, {
            header: true
        });

        csv.forEach(function(object) {
            job_collection.add(object);
        });
        return job_collection;
    },
    loadJobCollectionFromAPI: function(report_params) {
        var self = this;
        var promise = new Promise(function(resolve, reject) {
            var load_promise = self.loadJobsCSV(report_params);
            load_promise.then(function(csv) {
                var job_collection = self.loadJobCollectionFromCSV(csv);
                resolve(job_collection);
            });
        });
        return promise;
    }
};