
FieldPortal = (function() {

    var FieldPortal = {
        oss: null,
        getOSS: function getOSS() {
            if(this.oss === null) {
                if(typeof GlobalCapacity.OSS !== 'undefined') {
                    this.oss =GlobalCapacity.OSS;
                } else {
                    throw("GlobalCapacity.OSS is not available");
                }
            }
            return this.oss;
        },
        loadScripts: function loadScripts(scripts, callback) {
            this.getOSS(function(oss){
                oss.loadScripts(scripts, callback);
            });
        },
        employeeset: null,
        getFieldPortalEmployeeSet: function() {
            if (this.employeeset === null) {
                this.employeeset = new EmployeeSet(this);
            }
            return this.employeeset;
        },
        loadFieldPortalJobCollection: function(params) {
            //TODO: do script loading if not defined:
            //if(FieldPortal.JobCollectionFactory === undefined)
            //var self = this;
            //var load_promise = new Promise(function(resolve, reject){
            //    self.loadScripts();
            //});
            //return load_promise;
            var factory = new FieldPortal.JobCollectionFactory();
            return factory.loadJobCollectionFromAPI(params);
        }
    };

    FieldPortal.JobTypes = {
        job_types: ['Trouble Ticket', 'Work Order', 'Advanced Services'],
    };

    FieldPortal.JobTypesForAbbrev = {
        job_types: {
            TT: "Trouble Ticket",
            WO: "Work Order",
            ON: "Advanced Services"
        },
        getJobType: function(job_type_abbrev) {
            if(this.job_types[job_type_abbrev] === undefined) {
                throw job_type_abbrev + " is not a valid job type";
            }
            return this.job_types[job_type_abbrev];
        }
    };

    var EmployeeSet = function EmployeeSet(fieldportal_module) {
        this.fieldportal_module = fieldportal_module;
        this.employees = [];
    };
    EmployeeSet.prototype = {
        loadEmployees: function(callback) {
            var self = this;
            $.ajax({
                //TODO make this a CSrms url
                url: "http://synergy.fieldportal.net/fieldportal/modules/Clients/CovadDSL/ajax_employees_in_covad_project.php?nd=" + new Date().getTime(),
                type: "GET",
                dataType: "jsonp",
                success: function(data, textStatus) {
                    self.employees = data;
                    callback(self);
                },
                complete: function(xhr, textStatus) {},
                error: function(xhr, textStatus, errorThrown) {
                    if (xhr.status != 200) {
                        alert("Error, Data request could not be completed.");
                        return;
                    }
                    if (textStatus == "parsererror") {
                        alert("Error, Data response has errors.");
                        return;
                    }
                    alert("Error, Unknown error: " + textStatus);
                }
            });
        },
        count: function() {
            return this.employees.length;
        },
        getNameForGlobalCapacityAlias: function(globalcapacity_alias) {
            for (var index in this.employees) {
                if (this.employees[index].covad_alias == globalcapacityh_alias) {
                    return this.employees[index].name;
                }
            }
            return false;
        }
    };


    return FieldPortal;
})();