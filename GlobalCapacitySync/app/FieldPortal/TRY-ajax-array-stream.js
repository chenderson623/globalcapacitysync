
var old_way = {
    "138061": {
        "workorder_id": "138061",
        "AssignedTechName": "Alex Shperlin",
        "AssignedDate": "2014-06-18",
        "ContractRefNum": null,
        "SchedSeq": "0",
        "AssignedTime": null,
        "LocationCity": null,
        "LocationState": null,
        "LocationZip": null,
        "AssignedWorkOrder": "119854",
        "job_date": "06\/18\/2014"
    },
    "137927": {
        "workorder_id": "137927",
        "AssignedTechName": "Alex Shperlin",
        "AssignedDate": "2014-06-18",
        "ContractRefNum": "31260248",
        "SchedSeq": "2",
        "AssignedTime": "12-4",
        "LocationCity": "Chicago",
        "LocationState": "IL",
        "LocationZip": "60606",
        "AssignedWorkOrder": "119876",
        "job_date": "06\/18\/2014"
    },
    "137926": {
        "workorder_id": "137926",
        "AssignedTechName": "Alex Shperlin",
        "AssignedDate": "2014-06-18",
        "ContractRefNum": "31269442",
        "SchedSeq": "1",
        "AssignedTime": "9-1",
        "LocationCity": "Chicago",
        "LocationState": "IL",
        "LocationZip": "60603",
        "AssignedWorkOrder": "119855",
        "job_date": "06\/18\/2014"
    }
};


//TO:

var new_way = {
    "queue_jobs": {
        "field_indexes": [
            "workorder_id",
            "AssignedTechName",
            "AssignedDate",
            "ContractRefNum",
            "SchedSeq",
            "AssignedTime",
            "LocationCity",
            "LocationState",
            "LocationZip",
            "AssignedWorkOrder",
            "job_date"
        ],
        "jobs": [
            ["138061","Alex Shperlin","2014-06-18",null,"0",null,null,null,null,"119854","06\/18\/2014"],
            ["137927","Alex Shperlin","2014-06-18","31260248","2","12-4","Chicago","IL","60606","119876","06\/18\/2014"],
            ["137926","Alex Shperlin","2014-06-18","31269442","1","9-1","Chicago","IL","60603","119855","06\/18\/2014"],
        ]
    }
};

var Objectize = function(field_indexes) {
    this.field_indexes = field_indexes;

    this.getObject = function(job_array) {
        var new_object = {};
        for(var i=0, len=this.field_indexes.length; i < len; i++) {
            new_object[this.field_indexes[i]] = job_array[i];
        }
        return new_object;
    };
};

var queue_jobs = new_way.queue_jobs;
var objectize = new Objectize(queue_jobs.field_indexes);
var object = objectize.getObject(queue_jobs.jobs[0]);
