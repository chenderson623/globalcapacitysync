if (typeof jQuery === 'undefined') {
    throw 'jQuery is required.';
}
if (typeof Backbone === 'undefined') {
    throw 'Backbone is required.';
}
//if(typeof Handlebars == 'undefined') {
//	throw 'Handlebars is required.';
//}

var CSrms = CSrms || {};


CSrms.SyncControl = (function() {


    //===========SyncControlModel===========================

    var SyncControlModel = Backbone.Model.extend({
        defaults: function() {
            return {
                enabled: false,
                in_progess: false,
                done: false,
                message: ''
            };
        }
    });
    SyncControlModel.prototype.client_task = null;
    SyncControlModel.prototype.setClientTask = function(client_task) {
        this.client_task = client_task;
    };
    SyncControlModel.prototype.getClientTask = function() {
        return this.client_task;
    };
    SyncControlModel.prototype.queue_job = null;
    SyncControlModel.prototype.setQueueJob = function(queue_job) {
        this.queue_job = queue_job;
    };
    SyncControlModel.prototype.getQueueJob = function() {
        return this.queue_job;
    };
    SyncControlModel.prototype.schedule_date = null;
    SyncControlModel.prototype.setScheduleDate = function(schedule_date) {
        this.schedule_date = schedule_date;
    };
    SyncControlModel.prototype.getScheduleDate = function() {
        return this.schedule_date;
    };
    SyncControlModel.prototype.tech_name = null;
    SyncControlModel.prototype.setTechName = function(tech_name) {
        this.tech_name = tech_name;
    };
    SyncControlModel.prototype.getTechName = function() {
        return this.tech_name;
    };
    SyncControlModel.prototype.toggleEnabled = function() {
        this.set({enabled: !this.get("enabled")});
    };
    SyncControlModel.prototype.isEnabled = function() {
        return this.get('enabled');
    };
    SyncControlModel.prototype.setInProgress = function() {
        this.set({in_progress: true});
    };
    SyncControlModel.prototype.setDone = function() {
        this.set({in_progress: false, done: true});
    };
    SyncControlModel.prototype.doCommand = function(callback) {
        var _this = this;
        _this.setInProgress();
        setTimeout(function() {
            _this.setDone();
            callback();
        }, 600);
    };


    //===========SyncControlCollection===========================

    var SyncControlCollection = Backbone.Collection.extend({
        model: SyncControlModel
    });

    //===========SyncControlRun===========================

    var SyncControlRun = Backbone.Model.extend({
        defaults: function() {
            return {
                sync_control_collection: false,
                command_stack: [],
                num_commands: 0,
                num_undone: 0,
                done: false
            };
        },
        initialize: function() {
            var index = 0;
            var sync_control_model;
            var sync_control_collection = this.get('sync_control_collection');
            while (sync_control_model = sync_control_collection.at(index)) {
                //do some tests to sync_control_model
                //console.log(sync_control_model.isEnabled());
                //if(sync_control_model.isEnabled()) {
                this.get('command_stack').push(index);
                index++;
                //}
            }
            
            //console.log('Stack Done');
            var num_commands = this.get('command_stack').length;
            this.set({num_commands: num_commands, num_undone: num_commands});
        }
    });

    SyncControlRun.prototype.run = function() {
        this.nextCommand();
    };
    SyncControlRun.prototype.nextCommand = function() {
        var index = this.get('command_stack').shift();
        var sync_control_model = this.get('sync_control_collection').at(index);
        var _this = this;

        if (sync_control_model) {
            sync_control_model.doCommand(function() {
                sync_control_model.setDone();
                _this.set({num_undone: _this.get('command_stack').length});
                _this.nextCommand();
            });
        }

    };



    //Return Public Objects:
    return {
        SyncControlModel: SyncControlModel,
        SyncControlCollection: SyncControlCollection,
        SyncControlRun: SyncControlRun
    };

})();