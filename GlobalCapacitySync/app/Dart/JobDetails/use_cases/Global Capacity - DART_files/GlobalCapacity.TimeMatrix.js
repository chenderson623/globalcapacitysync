var GlobalCapacity = GlobalCapacity || {};
if (typeof GlobalCapacity.TimeMatrix !== 'undefined') {
    throw 'GlobalCapacity.TimeMatrix already defined.';
}

GlobalCapacity.TimeMatrix = (function() {
    /**
     *
     *
     * ============================TimeMatrix (main entry point)=======================================
     */
    TimeMatrix = {
        /**
         *
         * =============Standard Module Stuff==================================
         */
        oss: null,
        autoload: true,
        debug: false,
        getOSS: function getOSS(callback) {
            if(this.oss === null) {
                if(typeof GlobalCapacity.OSS !== 'undefined') {
                    this.oss =GlobalCapacity.OSS;
                } else {
                    //autoload it ourselves
                    //TODO: trigger warning that we are autoloading OSS
                    var self = this;
                    //OSS path must be absolute
                    this.inject_script('/public/GlobalCapacity/OSS/GlobalCapacity.OSS.js', function after_oss_load(){
                        self.oss = GlobalCapacity.OSS;
                        GlobalCapacity.OSS.getScriptLoader().setScriptLoaderMethod('LABjs');
                        callback.call(self, self.oss);
                    });
                    return;
                }
            }
            callback.call(this, this.oss);
        },
        //convenience function to allow us to load OSS programatically (probably only for testing)
        inject_script: function inject_script(src, callback) {
            if(this.autoload === false) {
                throw "Cannot autoload scripts";
            }
            var script_elem = document.createElement('script');
            script_elem.src = src;
            script_elem.onreadystatechange = script_elem.onload = function() {
                var state = script_elem.readyState;
                if (callback && !callback.done && (!state || /loaded|complete/.test(state))) {
                    callback.done = true;
                    callback();
                }
            };
            document.getElementsByTagName('head')[0].appendChild(script_elem);
        },
        loadScripts: function loadScripts(scripts, callback) {
            if(this.autoload === false) {
                throw "Cannot autoload scripts";
            }
            this.getOSS(function(oss){
                oss.loadScripts(scripts, callback);
            });
        },

        /**
         *
         *
         * ============================Loaders=============================================================
         */
        loadTimeMatrixDOM: function(callback) {
            if(typeof GlobalCapacity.TimeMatrix.DOM !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableWrapper === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableWrapper.js'));
            }
            scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.DOM.js'));
            this.loadScripts(scripts, callback);
        },

        createTimeMatrixDOM: function(timematrix_document_elem, callback) {
            var self = this;
            this.loadTimeMatrixDOM(function(){
                var timematrix_dom_obj;
                timematrix_dom_obj = new GlobalCapacity.TimeMatrix.DOM.Document(timematrix_document_elem);
                callback(timematrix_dom_obj);
            });
        },

        //alias for createTimeMatrixDocument
        createDocumentObj: function(document_elem, callback) {
            this.createTimeMatrixDocument(document_elem, callback);
        },

        loadTimeMatrixDocument: function(callback) {
            if(typeof GlobalCapacity.TimeMatrix.Document !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableWrapper === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableWrapper.js'));
            }
            scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.Views.js'));
            scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.Parsers.js'));
            scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.Document.js'));
            this.loadScripts(scripts, callback);
        },

        createTimeMatrixDocument: function(document_elem, callback) {
            var self = this;
            this.loadTimeMatrixDocument(function(){
                self.createTimeMatrixDOM(document_elem, function(timematrix_dom){
                    var timematrix_document = new GlobalCapacity.TimeMatrix.Document(timematrix_dom, self);
                    callback.call(self, timematrix_document);
                });
            });
        },

        loadBackboneFactory: function(callback) {
            if(GlobalCapacity.TimeMatrix.Backbone !== undefined && GlobalCapacity.TimeMatrix.Backbone.Factories !== undefined) {
                callback();
                return;
            }

            var scripts = [];
            if(typeof jQuery === 'undefined') {
                scripts.push(CSrms.getLibsUrl('jquery-1.11.0/jquery.min.js'));
            }
            if(typeof _ === 'undefined') {
                scripts.push(CSrms.getLibsUrl('underscore-1.6.0/underscore.js'));
            }
            if(typeof Backbone === 'undefined') {
                scripts.push(CSrms.getLibsUrl('backbone-1.1.2/backbone.js'));
            }
            if(typeof Handlebars === 'undefined') {
                scripts.push(CSrms.getLibsUrl('handlebars-1.3.0/handlebars.js'));
            }

            scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.Backbone.Models.js'));
            scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.Backbone.Views.js'));
            scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.Backbone.Factories.js'));
            this.loadScripts(scripts, callback);
        },

        loadControls: function(callback) {
            if(GlobalCapacity.TimeMatrix.Controls !== undefined) {
                callback();
                return;
            }
            var scripts = [];
            //contextmenu:
            scripts.push(CSrms.getLibsUrl('bootstrap-contextmenu/bootstrap-contextmenu-leftclick.js'));
            scripts.push(CSrms.getLibsUrl('jquery.showLoading/js/jquery.showLoading.js'));
            scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.Controls.js'));
            this.loadScripts(scripts, callback);
        },

        loadCommands: function(callback) {
            if(GlobalCapacity.TimeMatrix.Commands !== undefined) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.TimeMatrix.Commands);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];
                scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.Commands.js'));
                self.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.TimeMatrix.Commands);
                });
            });

            if(callback){
                promise.then(function(commands_obj){
                    callback(commands_obj);
                });
            }

            return promise;
        },

        loadSyncCompare: function(callback) {
            if(GlobalCapacity.TimeMatrix.SyncCompare !== undefined) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.TimeMatrix.SyncCompare);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];
                scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.SyncCompare.js'));
                this.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.TimeMatrix.SyncCompare);
                });
            });

            if(callback){
                promise.then(function(sync_compare){
                    callback(sync_compare);
                });
            }

            return promise;
        },

        loadDocumentSyncCompare: function(callback) {
            if(GlobalCapacity.TimeMatrix.SyncCompare && GlobalCapacity.TimeMatrix.SyncCompare.Document) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.TimeMatrix.SyncCompare.Document);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];
                if(GlobalCapacity.TimeMatrix.SyncCompare === undefined) {
                    scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.SyncCompare.js'));
                }
                scripts.push(CSrms.getAppUrl('TimeMatrix/GlobalCapacity.TimeMatrix.SyncCompare.Document.js'));
                self.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.TimeMatrix.SyncCompare.Document);
                });
            });

            if(callback){
                promise.then(function(sync_compare){
                    callback(sync_compare);
                });
            }

            return promise;
        },
        getStatelessLoader: function() {
            return new StatelessLoader(this);
        }
    };

    /**
     * Allow us to flatten out some callbacks in calling scripts
     * Also allow creation of objects with various starting objects
     * @param {GlobalCapacity.TimeMatrix} timematrix
     */
    var StatelessLoader = function(timematrix) {
        this.timematrix = timematrix;
    };
    StatelessLoader.prototype = {
        isElement: function(obj){
            return (
                typeof HTMLElement === "object" ? obj instanceof HTMLElement : //DOM2
                    obj && typeof obj === "object" && obj !== null && obj.nodeType === 1 && typeof obj.nodeName==="string"
            );
        },
        isTimeMatrixDocument: function(obj) {
            if(typeof GlobalCapacity.TimeMatrix.Document === 'undefined') return false;
            return obj instanceof GlobalCapacity.TimeMatrix.Document;
        },
        getTimeMatrixDocument: function(obj, callback) {
            if(this.isTimeMatrixDocument(obj)) {
                callback(obj);
                return obj;
            }
            if(obj.getTimeMatrixDocument !== undefined) {
                callback(obj.getTimeMatrixDocument());
                return obj.getTimeMatrixDocument();
            }
            if(this.isElement(obj)) {
                this.timematrix.createTimeMatrixDocument(obj, function(timematrix_document){
                    callback(timematrix_document);
                });
                return null;
            }
            throw "Cannot determine TimeMatrixDocument from passed object";
        },
        getScheduleCollectionFactory: function(obj, callback) {
            var self = this;
            this.getTimeMatrixDocument(obj, function(timematrix_document){
                self.timematrix.loadBackboneFactory(function(){
                    var schedule_backbone_collection_factory = new GlobalCapacity.TimeMatrix.Backbone.Factories.ScheduleBackboneCollectionFactory(timematrix_document);
                    if(callback) callback(schedule_backbone_collection_factory);
                });
            });
            return null;
        },
        getScheduleCollectionFactoryWithControls: function(obj, callback) {
            var self = this;
            this.getScheduleCollectionFactory(obj, function(schedule_backbone_collection_factory){
                self.timematrix.loadControls(function(){
                    if(callback) callback(schedule_backbone_collection_factory);
                });
            });
        },
        getTimeMatrixSyncControlFactory: function(obj, callback) {
            this.getScheduleCollectionFactoryWithControls(obj, function(schedule_backbone_collection_factory){
                var sync_control_factory = new GlobalCapacity.TimeMatrix.Controls.TimeMatrixSyncControlFactory(schedule_backbone_collection_factory);
                if(callback) callback(sync_control_factory);
            });
        },
        runTimeMatrixSyncControlFactory: function(obj, callback) {
            this.getTimeMatrixSyncControlFactory(obj, function(sync_control_factory) {
                sync_control_factory.setupFactories();
                //sync_control_factory.generateUIControls(); //run from calling script
                sync_control_factory.generateScheduleCollection();
                if(callback) callback(sync_control_factory);
            });
        }
    };

    return TimeMatrix;
})();