//if (typeof DOMElements == 'undefined') {
//    throw 'DOMElements is required.';
//}

if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TimeMatrix == 'undefined') {
    throw 'GlobalCapacity.TimeMatrix is required.';
}

GlobalCapacity.TimeMatrix.Parsers = (function() {

    function ReportParametersNode(element) {
        this.element = element;
        this.data = null;
    }
    ReportParametersNode.prototype = {
        parse: function() {
            this.data = {
                region: null,
                tech: null,
                date_range: null,
                report_business_day: null
            };
            var node_text = DOMElements.Filters.getElementCleanText(this.element);

            var region_regex = /This\sreport\sis\srun\sfor\s?"(FRU.+?)"\s?/;
            var region_result = region_regex.exec(node_text);
            if (region_result) {
                this.data.region = region_result[1].trim();
            }

            var tech_regex = /This\sreport\sis\srun\sfor\s?"(.+?)"\s?/;
            var tech_result = tech_regex.exec(node_text);
            if (tech_result) {
                this.data.tech = tech_result[1].trim();
            }

            var daterange_regex = /for\sthe\sDate\sRange\s?"(.+?)"\s?and/i;
            var daterange_result = daterange_regex.exec(node_text);
            if (daterange_result) {
                this.data.date_range = daterange_result[1].trim();
            }

            var business_day_regex = /Business\sDay\sincludes\s?"(.+?)"\s?</i;
            var business_day_result = business_day_regex.exec(node_text);
            if (business_day_result) {
                this.data.report_business_day = result[1].trim();
            }
            this.data.toString = function() {
                var tostring='';
                if(this.region) {
                    tostring+= "Region: " + this.region;
                }
                if(this.tech) {
                    if(tostring.length > 0) tostring+= " ";
                    tostring+= "Tech(s): " + this.tech;
                }
                if(this.date_range) {
                    if(tostring.length > 0) tostring+= " ";
                    tostring+= "Date Range: " + this.date_range;
                }
                if(this.report_business_day) {
                    if(tostring.length > 0) tostring+= " ";
                    tostring+= "Business Day: " + this.report_business_day;
                }
                return tostring;
            };
            return this.data;
        }
    };

    //TODO teardosn and compare DOM_tasks_table_data & DOM_tasks_table_data-BETTER
    function ZipClliNode(element) {
        this.element = element;
        this.data_prototype = {
            zip_code: null,
            zip_code_suffix: null,
            clli_code: null
        };
        this.task_array = [];
    }
    ZipClliNode.prototype = {
        parseTextNode: function(text_node) {
            var matches = text_node.match(/(\d\d\d\d\d)(\d*)?\s*(\w*)?/);
            var data = {
                zip_code: matches[1],
                zip_code_suffix: matches[2],
                clli_code: matches[3]
            };
            return data;
        },
        parse: function() {
            //Tasks are separated by br elements
            var text_nodes = DOMElements.Tools.getTextNodeValueArray(this.element);
            if (text_nodes.length === 0) {
                return this.task_array;
            }
            var text_node;
            for (var i = 0, len = text_nodes.length; i < len; i++) {
                text_node = text_nodes[i];
                this.task_array.push(this.parseTextNode(text_node));
            }
            return this.task_array;
        }
    };


    function TaskLinkNode(element) {
        this.element = element;
        this.data_prototype = {
            task_type: null,
            task_id: null,
            task_seq: null,
            task_url: null
        };
        this.task_array = [];
    }
    TaskLinkNode.prototype = {
        parseTextNode: function(text_node) {
            text_value = text_node.nodeValue.trim();
            if (text_value !== '') {
                var data = Object.create(this.data_prototype);
                data.task_type = 'block';
                data.task_id = text_value;

                this.task_array.push(data);
            }
        },
        parseAnchorNode: function(anchor_node) {
            var text_content = anchor_node.textContent;
            var url = anchor_node.href;

            var text_matches = text_content.match(/(\w\w)(\d+)-(\d+)/);
            var data = Object.create(this.data_prototype);
            data.task_type = text_matches[1];
            data.task_id = text_matches[2];
            data.task_seq = text_matches[3];
            data.task_url = url;
            this.task_array.push(data);

        },
        parse: function() {
            var child_nodes = this.element.childNodes; //must loop all nodes. could be text or <a> node, but need to keep in order

            var child_node;
            for (var i = 0, len = child_nodes.length; i < len; i++) {
                child_node = child_nodes[i];
                //check if its a text node:
                if (child_node.nodeName == '#text') {
                    this.parseTextNode(child_node);
                }
                if (child_node.nodeName == 'A') {
                    this.parseAnchorNode(child_node);
                }
            }
            return this.task_array;
        }
    };


    function StatusAndTimeNode(element) {
        this.element = element;
        this.data_prototype = {
            task_status: null,
            time_range: null,
            start_time: null,
            end_time: null
        };
        this.task_array = [];
    }
    StatusAndTimeNode.prototype = {

        parse: function() {
            //This one is really screwed up. has lots of line breaks and br's. best to strip all those and
            //leave a pattern STATUS(time)STATUS(time), split ) then split (
            var text_nodes = DOMElements.Tools.getTextNodeValueArray(this.element);
            //concat them all and remove whitespace:
            var status_text = text_nodes.join('', text_nodes);
            status_text = status_text.replace(/\s/, '');

            var status_array = status_text.split(')');
            var data, status_parts, time_range_parts;
            //last element is empty
            var len = status_array.length - 1;
            for (var s = 0; s < len; s++) {
                data = Object.create(this.data_prototype);
                status_parts = status_array[s].split('(');
                data.task_status = status_parts[0].trim();
                data.time_range = status_parts[1].trim();
                //some time ranges include date. remove the date
                data.time_range = data.time_range.replace(/\d\d\/\d\d\/\d\d/g, '');
                time_range_parts = data.time_range.split('-');
                data.start_time = time_range_parts[0].trim();
                data.end_time = time_range_parts[1].trim();

                this.task_array.push(data);
            }
            return this.task_array;
        }
    };

    function ServiceTypeNode(element) {
        this.element = element;
        this.data_prototype = {
            service_type: null
        };
        this.task_array = [];
    }
    ServiceTypeNode.prototype = {

        parse: function() {
            var text_nodes = DOMElements.Tools.getTextNodeValueArray(this.element);
            var text_node;
            for (var i = 0, len = text_nodes.length; i < len; i++) {
                text_node = text_nodes[i];
                this.task_array.push({
                    service_type: text_node
                });
            }
            return this.task_array;
        }
    };

    function TasksTable(element) {
        this.element = element;
        this.data_prototype = {
            zip_code: null,
            zip_code_suffix: null,
            clli_code: null,
            task_type: null,
            task_id: null,
            task_seq: null,
            task_url: null,
            task_status: null,
            time_range: null,
            start_time: null,
            end_time: null,
            service_type: null
        };
        this.task_array = null;

    }
    TasksTable.prototype = {
        initTaskArray: function(len) {
            this.task_array = [];
            for (var i = 0; i < len; i++) {
                this.task_array[i] = Object.create(this.data_prototype);
            }
        },

        mergeTaskData: function(task_data, node_data) {
            //accepting only the node_data keys that exist in task_data
            for (var key in node_data) {
                //skip inherited keys:
                if (!node_data.hasOwnProperty(key)) {
                    continue;
                }
                if (Object.getPrototypeOf(task_data).hasOwnProperty(key)) { //need to test on __proto__ for Object.create(d) object
                    task_data[key] = node_data[key];
                }
            }
            return task_data;
        },

        mergeNodeData: function(parseable_node) {
            var node_task_array = parseable_node.parse();
            if (this.task_array === null) {
                this.initTaskArray(node_task_array.length);
            }
            if (this.task_array.length !== node_task_array.length) {
                throw "There seems to be a mismatch in the task count";
            }
            for (var i = 0, len = node_task_array.length; i < len; i++) {
                this.mergeTaskData(this.task_array[i], node_task_array[i]);
            }
        },

        parse: function() {
            var table_rows = this.element.rows;
            this.mergeNodeData(new ZipClliNode(table_rows[0].cells[0]));
            this.mergeNodeData(new TaskLinkNode(table_rows[1].cells[0]));
            this.mergeNodeData(new StatusAndTimeNode(table_rows[2].cells[0]));
            this.mergeNodeData(new ServiceTypeNode(table_rows[3].cells[0]));
            return this.task_array;
        }

    };

    return {
        ReportParametersNode: ReportParametersNode,
        ZipClliNode: ZipClliNode,
        TaskLinkNode: TaskLinkNode,
        StatusAndTimeNode: StatusAndTimeNode,
        ServiceTypeNode: ServiceTypeNode,
        TasksTable: TasksTable
    };

})();