/*jshint multistr: true */


var GlobalCapacity = GlobalCapacity || {};
GlobalCapacity.OSS = GlobalCapacity.OSS || {};
GlobalCapacity.OSS.UI = (function() {
    var UI = {
        debug: false,
        oss: null,
        getOSS: function() {
            if(this.oss === null) {
                this.oss = GlobalCapacity.OSS;
            }
        },

        main_ui: null,
        //Default UI creator:
        createUI: function() {
            if(this.main_ui === null) { //only create if its not there
                this.main_ui = new NavBar();
            }
            return this.main_ui;
        },
        getUI: function() {
            if(this.main_ui === null) {
                throw "UI is not set up";
            }
            return this.main_ui;
        },
        getUI$Elem: function() {
            return this.getUI().get$Elem();
        },
        getUIElem: function() {
            return this.getUI().getElem();
        },
        attachToDOM: function() {
            document.body.appendChild(this.getUIElem());
            this.getUI().show();
        },
        login_ui: null,
        //Default Login UI:
        createLoginUI: function(session_provider) {
            if(this.login_ui === null) { //only create if its not there
                this.login_ui = new LoginDropdown(session_provider);
            }
            return this.login_ui;
        },
        getLoginUI: function() {
            if(this.login_ui === null) {
                throw "Login UI is not set up";
            }
            return this.login_ui;
        },

        message_box: null,
        getMessageBox: function() {
            if(this.message_box === null) {
                this.message_box = new MessageBox();
                document.body.appendChild(this.message_box.getUIElem());
            }

            return this.message_box;
        },

        /*====Init======*/
        init: function(session_factory) {
            var self = this;
            //Default UI setup. This is not nesessary to call. Can call/customize components as needed
            var ui = this.createUI(); //note that these are also cached within this object
            session_factory.getSessionProvider(function(session_provider){
                var login_ui = self.createLoginUI(session_provider);
                login_ui.init();
                ui.setRightItem(login_ui.get$Element());
            });
        }
    };
    /**
     *
     *
     * ============================NavBar=====================================================
     */
    var NavBar = function() {
        this.navbar_template = ' \
        <header> \
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation"> \
                <div class="navbar-inner"> \
                    <img class="brand pull-left navbar-brand-image" src="' + CSrms.getAppUrl('OSS/images/tci_logo.png') + '" alt="Synergy" /> \
                    <div class="container"> \
                        <ul class="nav navbar-nav" id="navbar-middle-menu"> \
                            <li class="divider-vertical hidden-sm hidden-xs"></li> \
                        </ul> \
                        <ul class="nav navbar-nav user_menu pull-right" id="navbar-right-menu"> \
                            <li class="divider-vertical hidden-sm hidden-xs"></li> \
                        </ul> \
                    </div> \
                </div> \
            </nav> \
        </header>';
        this.$navbar = null;
        this.$middle_menu = null;
        this.$right_menu = null;
    };
    NavBar.prototype = {
        get$Elem: function() {
            return this.get$NavBar();
        },
        getElem: function() {
            return this.get$NavBar()[0];
        },
        get$NavBar: function() {
            if(this.$navbar === null) {
                this.$navbar = $(this.navbar_template);
            }
            return this.$navbar;
        },
        get$RightMenu: function() {
            if(!this.$right_menu) {
                this.$right_menu = this.get$NavBar().find('#navbar-right-menu');
            }
            return this.$right_menu;
        },
        get$MiddleMenu: function() {
            if(!this.$middle_menu) {
                this.$middle_menu = this.get$NavBar().find('#navbar-middle-menu');
            }
            return this.$middle_menu;
        },

        show: function() {
            this.get$NavBar().slideDown();
        },
        hide: function() {
            this.get$NavBar().slideUp(function() {});
        },
        setRightItem: function(item) {
            var $li = $('<li></li>');
            $li.append(item);
            this.get$RightMenu().append($li);
        },
        setMiddleItem: function(item) {
            var $li = $('<li></li>');
            $li.append(item);
            this.get$MiddleMenu().append($li);
        },
        setMiddleDivider: function() {
            var $li = $('<li class="divider-vertical hidden-sm hidden-xs"></li>');
            this.get$MiddleMenu().append($li);
        },
        setMiddleItemLi: function(li_item) {
            this.get$MiddleMenu().append(li_item);
        }
    };

    /**
     *
     *
     * ============================LoginDropdown=====================================================
     */
    var LoginDropdown = function(session_provider) {
        this.session_provider = session_provider;

        this.login_template = ' \
        <ul id="user-drop-controls"> \
            <li class="user-drop-login"> \
                <a class="login-trigger" href="#">Log in <span>&#x25BC;</span></a> \
                <div style="display: none;" class="login-content"> \
                    <form class="login"> \
                        <div class="message"></div> \
                        <fieldset class="inputs"> \
                            <input id="username" name="username" placeholder="Your email address" required="" type="email" /> \
                            <input id="password" name="password" placeholder="Password" required="" type="text" /> \
                        </fieldset> \
                        <fieldset class="actions"> \
                            <input class="submit-button" value="Log in" type="submit"> \
                        </fieldset> \
                    </form> \
                </div> \
            </li> \
        </ul>';
        this.logout_form_template = ' \
        <form class="logout"> \
            <fieldset class="actions"> \
                <input class="submit-button" value="Log Out" type="submit"> \
            </fieldset> \
        </form>';
        this.$element = null;
        this.$trigger_elem = null;
        this.$login_content = null;
    };
    LoginDropdown.prototype = {
        get$Element: function() {
            if(this.$element === null) {
                this.$element = $(this.login_template);
            }
            return this.$element;
        },
        get$TriggerElem: function() {
            if(this.$trigger_elem === null) {
                this.$trigger_elem = this.get$Element().find('.login-trigger');
            }
            return this.$trigger_elem;
        },
        get$LoginContent: function() {
            if(this.$login_content === null) {
                this.$login_content = this.get$Element().find('.login-content');
            }
            return this.$login_content;
        },
        setLoggedIn: function(full_name) {
            this.get$TriggerElem().html(full_name + ' <span>&#x25BC;<\/span>');
            this.get$LoginContent().html(this.logout_form_template);
        },
        close: function() {
            this.get$TriggerElem().removeClass('active');
            this.get$LoginContent().slideUp();
            this.get$TriggerElem().find('span').html('&#x25BC;');
        },
        open: function() {
            this.get$TriggerElem().addClass('active');
            this.get$LoginContent().slideDown();
            this.get$TriggerElem().find('span').html('&#x25B2;');
        },
        submitLogin: function(username, password) {
            var self = this;
            this.session_provider.logIn(username, password, function(session_data) {
                if (session_data.logged_in && session_data.logged_in === true) {
                    self.setLoggedIn(session_data.full_name);
                    self.close();
                } else {
                    if (session_data.message) {
                        self.get$Element().find('.message').html(session_data.message);
                    }
                }
            });
        },
        /*====Init======*/
        init: function() {
            //Set up events:
            var self = this;
            //toggle login dropdown:
            this.get$TriggerElem().on('click', function(event) {
                if ($(this).hasClass('active')) {
                    self.close();
                } else {
                    self.open();
                }
            });
            //submit login form:
            this.get$Element().on('submit', 'form.login', function(event) {
                event.preventDefault();
                var username = $(this).find('#username').val();
                var password = $(this).find('#password').val();
                self.submitLogin.call(self, username, password);
            });
            this.session_provider.getSessionData(function(session_data){
                if (session_data.logged_in && session_data.logged_in === true) {
                    self.setLoggedIn(session_data.full_name);
                }
            });

        }
    };


    /**
     *
     *
     * ============================MessageBox=====================================================
     */
    var MessageBox = function() {
        this._template = ' \
        <div id="message-box"> \
            <ul></ul> \
        </div>';
        this.$message_box = null;
        this.$ul = null;
    };
    MessageBox.prototype = {
        get$Elem: function() {
            return this.get$MessageBox();
        },
        getElem: function() {
            return this.get$MessageBox()[0];
        },
        get$MessageBox: function() {
            if(this.$message_box === null) {
                this.$message_box = $(this._template);
            }
            return this.$message_box;
        },
        get$UL: function() {
            if(this.$ul === null) {
                this.$ul = this.get$MessageBox().find('ul');
            }
            return this.$ul;
        },

        show: function() {
            this.get$MessageBox().show();
        },
        hide: function() {
            this.get$MessageBox().hide();
        },
        setMessage: function(message_obj) {
            var text = message_obj.message;
            var $li = $('<li>' + text + '</li>');
            this.get$MessageBox().prepend($li);
        }
    };
    return UI;
})();