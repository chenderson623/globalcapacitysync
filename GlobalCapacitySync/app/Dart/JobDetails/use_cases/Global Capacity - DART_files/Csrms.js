var CSrms = {
	//root_url           : "http://globalcapacitysync.dev",
	root_url           : "http://tci.fieldportal.net",
	webCsrmsUrl        : "/GlobalCapacitySync/csrms/",
	webAppUrl          : "/GlobalCapacitySync/app/",
	webLibsUrl         : "/GlobalCapacitySync/libs/",
	webGeboUrl         : "/gebo_admin/",
	//webResourcesUrl    : "",
	//webModulesUrl      : "public/",

	getCsrmsUrl        : function(page_path) {
		return this.root_url + this.webCsrmsUrl + page_path;
	},
	getLibsUrl        : function(page_path) {
		return this.root_url + this.webLibsUrl + page_path;
	},
	getGeboUrl        : function(page_path) {
		return this.root_url + this.webGeboUrl + page_path;
	},
	//getResourcesUrl        : function(page_path) {
	//	return this.root_url + this.webResourcesUrl + page_path;
	//},
	getAppUrl        : function(page_path) {
		return this.root_url + this.webAppUrl + page_path;
	}
};