if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.OSS == 'undefined') {
    throw 'GlobalCapacity.OSS is required.';
}
GlobalCapacity.OSS.ControlsFactory = (function() {

    var ControlsFactory = function(oss, document_factory) {
        this.oss = oss;
        this.document_factory = document_factory;
        //Generated:
        this.document_obj = null;
        this.controls_factory = null;
    };
    ControlsFactory.prototype = {
        triggerSetEvent: function() {
            this.oss.emit("OSS.ControlsFactory::set", this.controls_factory);
        },
        setControlsFactory: function(controls_factory) {
            this.controls_factory = controls_factory;
            this.triggerSetEvent();
        },
        getDocumentObj: function(callback) {
            if(this.document_obj === null) {
                var self = this;
                this.document_factory.getDocumentObj(function(document_obj){
                    self.document_obj = document_obj;
                    callback(document_obj);
                });
            } else {
                callback(this.document_obj);
            }
            return this.document_obj;

        },
        createDocumentSyncControlsFactory: function(callback) { //document objects should all have this method
            var self = this;
            this.getDocumentObj(function(document_obj){
                console.log("DOCUMENT OBJ", document_obj);
                document_obj.createSyncControlsFactory(function(controls_factory){
                    console.log("DOCUMENT CONTROLS FACTORY ", controls_factory);
                    self.setControlsFactory(controls_factory);
                    console.log("INSIDE",self.oss.getOSSUI().getUI());
                    controls_factory.generateUIControls(self.oss.getOSSUI().getUI());
                    if(callback) callback(controls_factory);
                });
            });
        },
        createDocumentControlsFactory: function(callback) {
            console.log("OSS.CONTROLSFACTORY");
            //Decide what controls to run, based on session (oss), url params (oss), child window communicator (oss)
            //For now, just one controls factory available:
            this.createDocumentSyncControlsFactory(function(controls_factory){
                if(callback) callback(controls_factory);
            });

        }
    };


    return ControlsFactory;
})();