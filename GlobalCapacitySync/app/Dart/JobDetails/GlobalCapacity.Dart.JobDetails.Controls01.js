if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.Dart.JobDetails == 'undefined') {
    throw 'GlobalCapacity.Dart.JobDetails is required.';
}

GlobalCapacity.Dart.JobDetails.Controls = (function(Backbone, $, GlobalCapacity) {
    /*jshint multistr: true */
    'use strict';

    /**
     *
     *
     * ============================Nav Items================================================
     */

    var nav_table_controls_template = ' \
            <div class="table-tools-controls" style="width:600px;"> \
                <b>Schedule Table:</b> \
                <input type="radio" name="jobdetailstable-show" value="original" autocomplete="off"> Show Original \
                <input type="radio" name="jobdetailstable-show" value="sync" autocomplete="off"> Show Sync \
                <br> \
                <span style="display: inline-block;width: 111px;">&nbsp;</span> \
                <input type="radio" name="jobdetailstable-tech" value="tci" autocomplete="off"> TCI Techs Only \
                <input type="radio" name="jobdetailstable-tech" value="all" autocomplete="off"> All Techs \
            <\/div>';

    var NavTableControlsView = Backbone.View.extend({
        tagName: 'div',
        className: 'table-tools-controls',
        attributes: {
            style: 'width:600px;'
        },
        events: {
            'change input[name="jobdetailstable-show"]': 'changeShow'
        },
        render: function() {
            this.$el.empty();
            this.$el.html(nav_table_controls_template);

            this.setShow(this.model.get('jobdetailstable-show'));
            this.setTech(this.model.get('jobdetailstable-tech'));

            return this;
        },
        setShow: function(value) {
            this.$el.find('input[name="jobdetailstable-show"][value="' + value + '"]').prop('checked', true);
        },
        setTech: function(value) {
            this.$el.find('input[name="jobdetailstable-tech"][value="' + value + '"]').prop('checked', true);
        },
        changeShow: function() {
            var value = this.$el.find('input[name="jobdetailstable-show"]:checked').val();
            this.model.set('jobdetailstable-show', value);
        },
        changeTech: function() {
            var value = this.$el.find('input[name="jobdetailstable-tech"]:checked').val();
            this.model.set('jobdetailstable-tech', value);
        }
    });

    /**
     *
     *
     * ============================Table Manipulation================================================
     */
     var JobDetailsTableSwitch = function(original_table) {
        this.original_table  = original_table;
        this.sync_table      = null;
        this.$container      = $(original_table).parent();
        this.current_table   = 'original';

        this.original_table.controls = GlobalCapacity.Dart.JobDetails.TableControls(this.original_table);
     };
     JobDetailsTableSwitch.prototype = {
        changeTable: function(table_name) {
            if(table_name == this.current_table) {
                // nothing to do
                return;
            }

            if(table_name === 'original') {
                this.$container.html(this.original_table);
                this.current_table = 'original';
                return;
            }

            if(table_name === 'sync') {
                this.$container.html(this.getSyncTable());
                this.current_table = 'sync';

                window.Sortable.init();

                return;
            }
        },
        getCurrentTable: function() {
            if(this.current_table === 'original') {
                return this.original_table;
            }
            if(this.current_table === 'sync') {
                return this.sync_table;
            }
        },
        getSyncTable: function() {
            if(this.sync_table === null) {
                this.sync_table = this.createSyncTable();
            }
            return this.sync_table;
        },
        createSyncTable: function() {
            var $sync_table = $(this.original_table).clone();
            $sync_table.attr('id', $(this.original_table).attr('id') + 'Sync');

            this.makeSortable($sync_table);
            $sync_table[0].controls = GlobalCapacity.Dart.JobDetails.TableControls($sync_table[0]);

            return $sync_table[0];
        },
        makeSortable: function(table_dom) {
            var $table = $(table_dom);

            $table.attr('data-sortable', 'data-sortable');

            //need to make a thead:
            var thead_row = $table[0].rows[0];

            var $thead = $('<thead>');
            $table.prepend($thead);
            $thead.append($(thead_row).detach());

            thead_row.cells[8].setAttribute("data-sorted", "true");
            thead_row.cells[8].setAttribute("data-sorted-direction", "ascending");
//console.log("SELECT", $table.attr('id'));
            // not attached to DOM yet: window.Sortable.init({selector: '#' + $table.attr('id')});

        }
     };

    var SyncControlFactory = function(document_object) {
        this.document_object = document_object;

        this.controls_model          = null;
        this.nav_table_controls_view = null;
    };
    SyncControlFactory.prototype = {
        getModel: function() {
            if(this.controls_model === null) {
                var Model = Backbone.Model.extend({
                    defaults: {
                        'jobdetailstable-show': 'sync',
                        'jobdetailstable-tech': 'all'
                    }
                });
                this.controls_model = new Model();
            }
            return this.controls_model;
        },
        getNavTableControlsView: function() {
            if(this.nav_table_controls_view === null) {
                this.nav_table_controls_view = new NavTableControlsView({model: this.getModel()});
            }
            return this.nav_table_controls_view;
        },

        setupJobDetailTable: function() {
            var model        = this.getModel();
            var table_dom    = this.document_object.dart_dom_obj.getJobDetailsTable();
            var table_switch = new JobDetailsTableSwitch(table_dom);

            table_switch.changeTable(model.get('jobdetailstable-show'));
            this.getModel().on('change:jobdetailstable-show', function(){
                table_switch.changeTable(model.get('jobdetailstable-show'));
            }, this);
//TODO: here:
            table_switch.changeTech(model.get('jobdetailstable-tech'));
            this.getModel().on('change:jobdetailstable-tech', function(){
                table_switch.changeTech(model.get('jobdetailstable-tech'));
            }, this);

            console.log("HEADINGS", table_switch.getCurrentTable().controls.getHeadings());
        },

        generateUIControls: function(ui) {
            console.log(this.getNavTableControlsView());
            ui.setMiddleItem(this.getNavTableControlsView().render().$el);
            ui.setMiddleDivider();
            this.setupJobDetailTable();
        }
    };

    return {
        SyncControlFactory: SyncControlFactory
    };
})(Backbone, jQuery, GlobalCapacity);
