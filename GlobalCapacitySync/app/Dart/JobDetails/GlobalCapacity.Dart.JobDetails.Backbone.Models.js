//if (typeof Backbone == 'undefined') {
//    throw 'Backbone is required.';
//}
if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.Dart.JobDetails == 'undefined') {
    throw 'GlobalCapacity.Dart.JobDetails is required.';
}

if (typeof GlobalCapacity.Dart.JobDetails.Backbone == 'undefined') {
    GlobalCapacity.Dart.JobDetails.Backbone = {};
}


GlobalCapacity.Dart.JobDetails.Backbone.Models = (function() {

    /**
     *
     *
     * ============================MODELS=================================================
     */

    /**
     *
     * TaskModel:
     */
    var DartJobDetailsModel = Backbone.Model.extend({
        defaults: {
        },
    });

    return {
        DartJobDetailsModel: DartJobDetailsModel
     };


})();