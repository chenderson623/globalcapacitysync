if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.Dart.JobDetails == 'undefined') {
    throw 'GlobalCapacity.Dart.JobDetails is required.';
}

GlobalCapacity.Dart.JobDetails.Controls = (function(Backbone, $, GlobalCapacity) {
    /*jshint multistr: true */
    'use strict';

    /**
     *
     *
     * ============================Nav Items================================================
     */

    var nav_table_controls_template = _.template(' \
            <div class="table-tools-controls" style="width:250px;"> \
                <b>Schedule Table:</b> \
                <input type="checkbox" name="jobdetailstable-sync" value="sync" autocomplete="off" <% if (isSync) { %>checked="checked"<% } %> > Sync Table \
                <br> \
                <% if (isSync) { %> \
                <span style="display: inline-block;width: 111px;">&nbsp;</span> \
                <input type="checkbox" name="jobdetailstable-tech" value="tci" autocomplete="off" <% if (isTCI) { %>checked="checked"<% } %> > TCI Techs Only \
                <% } %> \
            <\/div>');

    var NavTableControlsView = Backbone.View.extend({
        tagName: 'div',
        className: 'table-tools-controls',
        attributes: {
            style: 'width:600px;'
        },
        events: {
            'change input[name="jobdetailstable-sync"]': 'changeShow',
            'change input[name="jobdetailstable-tech"]': 'changeTech'
        },
        render: function() {
            this.$el.empty();
            this.$el.html(nav_table_controls_template({
                isSync: this.model.get('jobdetailstable-show') === 'sync',
                isTCI:  this.model.get('jobdetailstable-tech') === 'tci',
            }));

            return this;
        },
        changeShow: function() {
            var value = this.$el.find('input[name="jobdetailstable-sync"]:checked').val() || 'original';
            this.model.set('jobdetailstable-show', value);
        },
        changeTech: function() {
            var value = this.$el.find('input[name="jobdetailstable-tech"]:checked').val() || 'all';
            this.model.set('jobdetailstable-tech', value);
        }
    });

    /**
     *
     *
     * ============================Table Manipulation================================================
     */
     var JobDetailsTableSwitch = function(original_table, sync_table_factory) {
        this.original_table     = original_table;
        this.sync_table_factory = sync_table_factory;
        this.$container         = $(original_table).parent();
        this.current_table      = 'original';
     };
     JobDetailsTableSwitch.prototype = {
        changeTable: function(table_name) {
            if(table_name == this.current_table) {
                // nothing to do
                return;
            }

            if(table_name === 'original') {
                this.$container.html(this.original_table);
                this.current_table = 'original';
                return;
            }

            if(table_name === 'sync') {
                this.$container.html(this.getSyncTable());
                this.current_table = 'sync';

                this.sync_table_factory.onAttachedToDOM();

                return;
            }
        },
        getCurrentTable: function() {
            if(this.current_table === 'original') {
                return this.original_table;
            }
            if(this.current_table === 'sync') {
                return this.getSyncTable();
            }
        },
        getSyncTable: function() {
            return this.sync_table_factory.getSyncTable();
        }
     };

     var SyncTableFactory = function(original_table, control_model) {
        this.original_table  = original_table;
        this.control_model   = control_model;

        this.sync_table     = null;
        this.table_controls = null;
        this.initialized    = false;
     };
     SyncTableFactory.prototype = {
        getSyncTable: function() {
            if(this.sync_table === null) {
                this.sync_table = this.createSyncTable();
            }
            return this.sync_table;
        },
        createSyncTable: function() {
            var $sync_table = $(this.original_table).clone();
            $sync_table.attr('id', $(this.original_table).attr('id') + 'Sync');

            this.makeSortable($sync_table);
            this.setupEvents();

            return $sync_table[0];
        },
        makeSortable: function(table_dom) {
            var $table = $(table_dom);

            $table.attr('data-sortable', 'data-sortable');

            //need to make a thead:
            var thead_row = $table[0].rows[0];

            var $thead = $('<thead>');
            $table.prepend($thead);
            $thead.append($(thead_row).detach());

            thead_row.cells[8].setAttribute("data-sorted", "true");
            thead_row.cells[8].setAttribute("data-sorted-direction", "ascending");

            // not attached to DOM yet: window.Sortable.init({selector: '#' + $table.attr('id')});
        },
        setupEvents: function() {
            this.control_model.on('change:jobdetailstable-tech', function(){
                this.changeTech(this.control_model.get('jobdetailstable-tech'));
            }, this);
        },
        onAttachedToDOM: function() {
            if(!this.initialized) {
                window.Sortable.init();
                this.changeTech(this.control_model.get('jobdetailstable-tech'));
                this.getControls().modifyTaskLinks();

                this.initialized = true;
            }
        },
        getControls: function() {
            if(this.table_controls === null) {
                this.table_controls = GlobalCapacity.Dart.JobDetails.TableControls(this.getSyncTable());
            }
            return this.table_controls;
        },
        changeTech: function(value) {
            if(value === 'tci') {
                return this.getControls().showTCI();
            }
            if(value === 'all') {
                return this.getControls().showAll();
            }
        }

     };

    var SyncControlFactory = function(document_object) {
        this.document_object = document_object;

        this.controls_model          = null;
        this.nav_table_controls_view = null;
    };
    SyncControlFactory.prototype = {
        getModel: function() {
            if(this.controls_model === null) {
                var Model = Backbone.Model.extend({
                    defaults: {
                        'jobdetailstable-show': 'sync',
                        'jobdetailstable-tech': 'tci'
                    }
                });
                this.controls_model = new Model();
            }
            return this.controls_model;
        },
        getNavTableControlsView: function() {
            if(this.nav_table_controls_view === null) {
                this.nav_table_controls_view = new NavTableControlsView({model: this.getModel()});
                this.getModel().on('change', this.nav_table_controls_view.render, this.nav_table_controls_view);
            }
            return this.nav_table_controls_view;
        },

        setupJobDetailTable: function() {
            var model        = this.getModel();
            var table_dom    = this.document_object.dart_dom_obj.getJobDetailsTable();
            var table_switch = new JobDetailsTableSwitch(table_dom, new SyncTableFactory(table_dom, model));

            table_switch.changeTable(model.get('jobdetailstable-show'));
            this.getModel().on('change:jobdetailstable-show', function(){
                table_switch.changeTable(model.get('jobdetailstable-show'));
            }, this);
        },

        generateUIControls: function(ui) {
            ui.setMiddleItem(this.getNavTableControlsView().render().$el);
            ui.setMiddleDivider();
            this.setupJobDetailTable();
        }
    };

    return {
        SyncControlFactory: SyncControlFactory
    };
})(Backbone, jQuery, GlobalCapacity);
