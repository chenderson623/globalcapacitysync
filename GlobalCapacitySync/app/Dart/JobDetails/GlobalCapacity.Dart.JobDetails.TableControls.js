if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.Dart.JobDetails == 'undefined') {
    throw 'GlobalCapacity.Dart.JobDetails is required.';
}
GlobalCapacity.Dart.JobDetails.TableControls = (function(DOMTableWrapper) {
    /*jshint multistr: true */
    'use strict';

    function HeadingsRow(table_wrapper) {
        this.table_wrapper = table_wrapper;
        this.headings_row_index = 0;
        this.headings_row = null;
        this.headings_array = null;
        this.getHeadingsRow = function getHeadingsRow() {
            if (this.headings_row === null) {
                this.headings_row = this.table_wrapper.getRow(this.headings_row_index);
            }
            return this.headings_row;
        };
        this.getHeadings = function getHeadings() {
            if (this.headings_array === null) {
                this.headings_array = this.getHeadingsRow().map(DOMElements.Filters.getElementCleanText);
            }
            return this.headings_array;
        };
    }

    function RowColumnMap(row_obj, column_label_array) {
        this.column_label_array = column_label_array;
        this.row_obj            = row_obj;
        this.column_map         = null;
        this._createColumnMap(column_label_array);
    }
    RowColumnMap.prototype = {
        _createColumnMap: function(column_array) {
            //TODO: fix to use indexes
            this.column_map = {};
            var col_label, col_value;
            for (var index in column_array) {
                col_label = column_array[index];
                col_value = this.row_obj.getCellValue(index);
                this.column_map[col_label] = col_value;
            }
        },
        get: function(column_label) {
            if (this.column_map[column_label] === undefined) {
                return null;
            }
            return this.column_map[column_label];
        },
        getIndex: function(column_label) {
            return this.column_label_array.indexOf(column_label);
        },
        getMap: function() {
            return this.column_map;
        }
    };

    function RowDataTransformer(column_label_array) {
        this.column_label_array = column_label_array;
        this.transform = function(row_obj) {
            var row_map = new RowColumnMap(row_obj, this.column_label_array);
            return {
                LocationName: row_map.get('Customer Name'),
                AssignedTechName: null,
                ContractTechName: row_map.get('Tech Name')
            };
        }
    }

    var TableRowsProcess_ShowTCIOnly = {
        headings: {},
        cells_processes: [],
        row_begin: 1,
        current_row: 0,
        beforeRows: function() {
            this.current_row = -1;
        },
        processRow: function(row_obj) {
            this.current_row++;
            var row_map = new RowColumnMap(row_obj, this.headings);
            var tech_name = row_map.get('Tech Name');
            var prefix = tech_name.slice(0, 2);
            if (prefix !== 'S-') {
                row_obj.getElem().style.display = "none";
            }
        },
        afterRows: function() {},
    };

    var TableRowsProcess_ShowAllTechs = {
        cells_processes: [],
        row_begin: 1,
        current_row: 0,
        beforeRows: function() {
            this.current_row = -1;
        },
        processRow: function(row_obj) {
            this.current_row++;
            row_obj.getElem().style.display = "table-row";
        },
        afterRows: function() {},
    };

    var parseTechName = function(tech_name) {
        if(tech_name.slice(0, 2) !== 'S-') {
            return null;
        }
        tech_name = tech_name.slice(2);
        //TODO: temporary fix:
        if(tech_name === 'Michael Pozzuto') tech_name = 'Mike Pozzuto';
        return tech_name;
    };

    var parseTaskId = function(task_ref) {

        var prefix = task_ref.substring(0, 2);
        var parts  = task_ref.substring(2).split('-');

        var task_type = null;
        var task_url  = null;
        switch(prefix) {
            case 'WO':
                task_type = 'Work Order';
                task_url  = GlobalCapacity.OSS.getUrls().getWorkOrderUrl(parts[0]);
                break;
            case 'TT':
                task_type = 'Trouble Ticket';
                task_url  = GlobalCapacity.OSS.getUrls().getTroubleTicketUrl(parts[0], parts[1]);
                break;
            case 'ON':
                task_type = 'Offnet Task';
                task_url  = GlobalCapacity.OSS.getUrls().getOffNetTaskUrl(parts[0]);
                break;
        }

        return {
            task_ref:     task_ref,
            task_type:    task_type,
            task_id:      parts[0],
            dispatch_seq: parts[1] || null,
            task_url:     task_url
        };
    };

    var getISODate = function(datetime_str) {
        //expects a string of the format 01/02/2008 08:00. returns 2008-01-02
        var datetime_parts = datetime_str.split(' ');
        var date_str = datetime_parts[0];

        var date_parts = date_str.split("/");
        var monthstr = date_parts[0];
        if (monthstr.length == 1) {
            monthstr = "" + '0' + monthstr;
        }

        var daystr = date_parts[1];
        if (daystr.length === 1) {
            daystr = "" + '0' + daystr;
        }

        var yearstr = date_parts[2];
        if (yearstr.length == 2) {
            yearstr = "" + '20' + yearstr;
        }

        var return_date = "" + yearstr + "-" + monthstr + "-" + daystr;
        return return_date;
    };

    var parseRow = function(row_map) {
        return {
            AssignedDate: getISODate(row_map.get('Start Date')),
            AssignedTechName: parseTechName(row_map.get('Tech Name')),
            AssignedTime: row_map.get('Contract Window')
        };
    };

    var TableRowsProcess_ModifyTaskLinks = {
        headings: {},
        cells_processes: [],
        row_begin: 1,
        current_row: 0,
        beforeRows: function() {
            this.current_row = -1;
        },
        processRow: function(row_obj) {
            this.current_row++;
            var row_map  = new RowColumnMap(row_obj, this.headings);
            var job_ref  = row_map.get('Job Ref');
            var task     = parseTaskId(job_ref);
            var row_data = parseRow(row_map);

            var task_link = task.task_url + '&SchedSeq=' + encodeURIComponent(task.dispatch_seq) + '&AssignedTechName=' + encodeURIComponent(row_data.AssignedTechName) + '&AssignedDate=' + encodeURIComponent(row_data.AssignedDate) + '&AssignedTime=' + encodeURIComponent(row_data.AssignedTime);

            var link = $('<a target="_blank" href="' + task_link + '">' + task.task_ref + '</a>');

            var column_index = row_map.getIndex('Job Ref');

            $(row_obj.getCell(column_index).getElem()).html(link);
        },
        afterRows: function() {},
    };

    return function(table_dom) {
        var table_wrapper = new DOMTableWrapper.Table(table_dom);
        var headings_row  = new HeadingsRow(table_wrapper);
        return {
            getHeadings: function() {
                return headings_row.getHeadings();
            },
            showTCI: function() {
                var table_processor  = new DOMTableWrapper.TableProcessor(table_wrapper);
                var rows_process     = _.extend(TableRowsProcess_ShowTCIOnly, {
                    headings: headings_row.getHeadings()
                });
                return table_processor.process(rows_process);
            },
            showAll: function() {
                var table_processor  = new DOMTableWrapper.TableProcessor(table_wrapper);
                return table_processor.process(TableRowsProcess_ShowAllTechs);
            },
            modifyTaskLinks: function() {
                var table_processor  = new DOMTableWrapper.TableProcessor(table_wrapper);
                var rows_process     = _.extend(TableRowsProcess_ModifyTaskLinks, {
                    headings: headings_row.getHeadings()
                });
                return table_processor.process(rows_process);
            }
        };
    };
})(DOMTableWrapper);