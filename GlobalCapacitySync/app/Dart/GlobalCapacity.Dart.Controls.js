if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.Dart == 'undefined') {
    throw 'GlobalCapacity.Dart is required.';
}

GlobalCapacity.Dart.Controls = (function() {
    /*jshint multistr: true */
    'use strict';

    /**
     *
     *
     * ============================Nav Items================================================
     */
    var NavBarDocumentType = function(dart_model) {
        this.dart_model = dart_model;

        this.template = ' \
            <div class="document-type-controls"> \
                <h4>Dart</h4> \
                <div>' + this.dart_model.get('dart_action') + '</div> \
            <\/div>';
    };
    NavBarDocumentType.prototype = {
        getHtml: function() {
            return this.template;
        }
    };


    var SyncControlFactory = function(document_object, dart_model) {
        this.document_object = document_object;
        this.dart_model      = dart_model;

        this.jobdetails_controls_factory = null;
    };
    SyncControlFactory.prototype = {
        generateUIControls: function(ui) {
            //Document-type info piece
            this.document_type_control = new NavBarDocumentType(this.dart_model);
            ui.setMiddleItem(this.document_type_control.getHtml());
            ui.setMiddleDivider();

            var self = this;

            if(this.dart_model.get('dart_action') == 'Job Details') {
                var load_jobdetails_controls = self.document_object.dart.loadDartJobDetailsControls();

                load_jobdetails_controls.then(function(controls){
                    self.jobdetails_controls_factory = new controls.SyncControlFactory(self.document_object);
                    self.jobdetails_controls_factory.generateUIControls(ui);
                });

            }
        }
    };

    return {
        SyncControlFactory: SyncControlFactory
    };
})();
