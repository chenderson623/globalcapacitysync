var GlobalCapacity = GlobalCapacity || {};
if (typeof GlobalCapacity.Dart !== 'undefined') {
    throw 'GlobalCapacity.Dart already defined.';
}

GlobalCapacity.Dart = (function() {
    /**
     *
     *
     * ============================Dart (main entry point)=======================================
     */
    Dart = {
        /**
         *
         * =============Standard Module Stuff==================================
         */
        oss: null,
        autoload: true,
        debug: false,
        getOSS: function getOSS(callback) {
            if(this.oss === null) {
                if(typeof GlobalCapacity.OSS !== 'undefined') {
                    this.oss =GlobalCapacity.OSS;
                } else {
                    //autoload it ourselves
                    //TODO: trigger warning that we are autoloading OSS
                    var self = this;
                    //OSS path must be absolute
                    this.inject_script('/public/GlobalCapacity/OSS/GlobalCapacity.OSS.js', function after_oss_load(){
                        self.oss = GlobalCapacity.OSS;
                        GlobalCapacity.OSS.getScriptLoader().setScriptLoaderMethod('LABjs');
                        callback.call(self, self.oss);
                    });
                    return;
                }
            }
            callback.call(this, this.oss);
        },
        //convenience function to allow us to load OSS programatically (probably only for testing)
        inject_script: function inject_script(src, callback) {
            if(this.autoload === false) {
                throw "Cannot autoload scripts";
            }
            var script_elem = document.createElement('script');
            script_elem.src = src;
            script_elem.onreadystatechange = script_elem.onload = function() {
                var state = script_elem.readyState;
                if (callback && !callback.done && (!state || /loaded|complete/.test(state))) {
                    callback.done = true;
                    callback();
                }
            };
            document.getElementsByTagName('head')[0].appendChild(script_elem);
        },
        loadScripts: function loadScripts(scripts, callback) {
            if(this.autoload === false) {
                throw "Cannot autoload scripts";
            }
            this.getOSS(function(oss){
                oss.loadScripts(scripts, callback);
            });
        },

        /**
         *
         *
         * ============================Loaders=============================================================
         */
        //alias for createDartDocument
        createDocumentObj: function(document_elem, callback) {
            this.createDartDocument(document_elem, callback);
        },

        createDartDocument: function(document_elem, callback) {
            var self = this;
            this.loadDartDocument(function(){
                self.createDartDOM(document_elem, function(dart_dom){
                    var dart_document = new GlobalCapacity.Dart.Document(dart_dom, self);
                    callback.call(self, dart_document);
                });
            });
        },

        loadDartDocument: function(callback) {
            if(typeof GlobalCapacity.Dart.Document !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableWrapper === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableWrapper.js'));
            }
            //scripts.push(CSrms.getAppUrl('Dart/GlobalCapacity.Dart.Views.js'));
            //scripts.push(CSrms.getAppUrl('Dart/GlobalCapacity.Dart.Parsers.js'));
            scripts.push(CSrms.getAppUrl('Dart/GlobalCapacity.Dart.Document.js'));
            this.loadScripts(scripts, callback);
        },

        createDartDOM: function(dart_document_elem, callback) {
            var self = this;
            this.loadDartDOM(function(){
                var dart_dom_obj;
                dart_dom_obj = new GlobalCapacity.Dart.DOM(dart_document_elem);
                callback(dart_dom_obj);
            });
        },

        loadDartDOM: function(callback) {
            if(typeof GlobalCapacity.Dart.DOM !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableWrapper === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableWrapper.js'));
            }
            scripts.push(CSrms.getAppUrl('Dart/GlobalCapacity.Dart.DOM.js'));
            this.loadScripts(scripts, callback);
        },

        loadControls: function(callback) {
            if(GlobalCapacity.Dart.Controls !== undefined) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.Dart.Controls);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];

                if(typeof jQuery === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('jquery-1.11.0/jquery.min.js'));
                }
                if(typeof _ === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('underscore-1.6.0/underscore.js'));
                }
                if(typeof Backbone === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('backbone-1.1.2/backbone.js'));
                    scripts.push(CSrms.getLibsUrl('backbone-forms-20141202/distribution/backbone-forms.min.js'));
                }
                if(typeof Handlebars === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('handlebars-1.3.0/handlebars.js'));
                }
                if(typeof Sortable === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('sortable-0.8.0/sortable.js'));
                    self.getOSS(function(oss){
                        oss.loadCSS(CSrms.getLibsUrl('sortable-0.8.0/css/sortable-theme-minimal.css'));
                    });
                }

                scripts.push(CSrms.getAppUrl('Dart/GlobalCapacity.Dart.Controls.js'));
                scripts.push(CSrms.getAppUrl('Dart/GlobalCapacity.Dart.Backbone.Models.js'));
                self.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.Dart.Controls);
                });
            });

            if(callback){
                promise.then(function(controls_obj){
                    callback(controls_obj);
                });
            }

            return promise;
        },

        loadDartJobDetailsControls: function(callback) {
            if(GlobalCapacity.Dart.JobDetails && GlobalCapacity.Dart.JobDetails.Controls !== undefined) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.Dart.JobDetails.Controls);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];

                scripts.push(CSrms.getAppUrl('Dart/JobDetails/GlobalCapacity.Dart.JobDetails.js'));
                scripts.push(CSrms.getAppUrl('Dart/JobDetails/GlobalCapacity.Dart.JobDetails.Controls.js'));
                scripts.push(CSrms.getAppUrl('Dart/JobDetails/GlobalCapacity.Dart.JobDetails.TableControls.js'));
                scripts.push(CSrms.getAppUrl('Dart/JobDetails/GlobalCapacity.Dart.JobDetails.Backbone.Models.js'));
                self.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.Dart.JobDetails.Controls);
                });
            });

            if(callback){
                promise.then(function(controls_obj){
                    callback(controls_obj);
                });
            }

            return promise;
        },

    };


    return Dart;
})();