
if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.Dart == 'undefined') {
    throw 'GlobalCapacity.Dart is required.';
}

GlobalCapacity.Dart.Document = (function() {
    /**
     *
     *
     * ============================Document (main entry point)=======================================
     */
    //TODO: make this prototype
    var Document = function(dart_dom_obj, dart) {
        this.dart = dart;
        this.dart_dom_obj = dart_dom_obj;
        this.dart_model = null;

        this.getDocumentType = function() {
            return "Dart";
        };

        this.isDartDocument = function() {
            return this.dart_dom_obj.isDartDocument();
        };

        this.hasJobDetails = function() {
            return this.dart_dom_obj.hasJobDetails();
        };

        this.countSchedules = function() {
            return this.getSchedules().length;
        };
        this.getSchedules = function() {
            if (this.schedules === null) {
                if (!this.dart_dom_obj) {
                    throw "No document dom is set";
                }
                this.schedules = [];
                var schedules_count = this.dart_dom_obj.countScheduleTables();
                var schedule_table_dom;
                for (var i = 0; i < schedules_count; i++) {
                    schedule_table_dom = this.dart_dom_obj.getScheduleTable(i);
                    this.schedules.push(new Schedule(schedule_table_dom));
                }
            }
            return this.schedules;
        };

        this.getSchedule = function(schedule_index) {
            if (this.schedules === null) {
                this.getSchedules();
            }
            if (this.schedules[schedule_index] === undefined) {
                throw "Schedule index " + schedule_index + " is not valid";
            }
            return this.schedules[schedule_index];
        };

        this.getScheduleMapsIterator = function(schedule_index) {
            if(typeof this.schedule_maps_iterators[schedule_index] === 'undefined') {
                this.schedule_maps_iterators[schedule_index] = new ScheduleMapsIterator(this.getSchedule(schedule_index));
            }
            return this.schedule_maps_iterators[schedule_index];
        };

        /**
         * ======================Controls Methods======================================
         *
         */
        this.getDartModel = function() {
            if(this.dart_model == null) {
                this.dart_model = new GlobalCapacity.Dart.Backbone.Models.DartModel();

                if(this.hasJobDetails()) {
                    this.dart_model.set('dart_action', 'Job Details');
                }
            }
            return this.dart_model;
        };

        this.createSyncControlsFactory = function(callback) {
            var self = this;
            var load_controls = this.dart.loadControls();

            load_controls.then(function(controls){
                self.SyncControlFactory = new controls.SyncControlFactory(self, self.getDartModel());
                callback(self.SyncControlFactory);
            });

        };

    };

    /**
     *
     *
     * ============================Schedule=========================================================
     */
    var Schedule = function(schedule_table_dom) {
        this.schedule_table_dom = schedule_table_dom;

        this.getScheduleDOMMap = function() {
            if (!this.schedule_table_dom) {
                throw "No schedule dom is set";
            }
            return this.schedule_table_dom.getScheduleDOMMap();
        };

    };

    function ScheduleMapsIterator(schedule_document) {
        this.schedule_document = schedule_document;
        this.on_schedule_row = [];

        this.schedule_dom_map = this.schedule_document.getScheduleDOMMap();
        this.schedule_task_map = this.schedule_document.getScheduleTaskMap();
        this.schedule_data_map = this.schedule_document.getScheduleDataMap();

    }

    return Document;
})();