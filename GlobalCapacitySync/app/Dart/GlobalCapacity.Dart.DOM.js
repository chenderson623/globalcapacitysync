if(typeof DOMTableWrapper == 'undefined') {
	throw 'DOMTableWrapper is required.';
}
if(typeof DOMElements == 'undefined') {
	throw 'DOMElements is required.';
}

if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.Dart == 'undefined') {
    throw 'GlobalCapacity.Dart is required.';
}

GlobalCapacity.Dart.DOM = (function() {

	/**
	 *
	 *
	 * ============================Document (main entry point)=======================================
	 */

	var DOM = function DOM(document_elem) {
		this.elem = document_elem;
        //Internal:
		this.content_div = null;
		this.parameters_elem = null;
		this.schedule_table_elems_array = null;
		this.schedule_tables = [];
    };
    DOM.prototype = {
        isDartDocument: function() {
            try {
                var appname_divs = this.elem.getElementsByClassName('appname');
                if(appname_divs.length > 0 && appname_divs[0].textContent == 'DISPATCH AGENT REPORTING TOOL') {
                    return true;
                }
                var orderDetailsDiv = this.elem.querySelector('#orderDetailTab');
                if(orderDetailsDiv) {
                    return true;
                }

            } catch(err) {
                console.log(err.toSource());
                return false;
            }
            return true;
        },

        getJobDetailsTable: function() {
            return this.elem.querySelector('#jobDetailsTab');
        },

        hasJobDetails: function() {
            return this.getJobDetailsTable() !== null;
        }

    };


	/**
	 *
	 *
	 * ============================Return Values===============================================
	 */

	return DOM;

})();