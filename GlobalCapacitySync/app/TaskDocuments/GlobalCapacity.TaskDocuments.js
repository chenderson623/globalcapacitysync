
var GlobalCapacity = GlobalCapacity || {};
GlobalCapacity.TaskDocuments = (function() {
    'use strict';
    var TaskDocuments = {
        task_types: ["Trouble Ticket", "Work Order", "Offnet"],
        /**
         *
         * =============Standard Module Stuff==================================
         */
        oss: null,
        autoload: true,
        debug: false,
        getOSS: function getOSS(callback) {
            if(this.oss === null) {
                if(typeof GlobalCapacity.OSS !== 'undefined') {
                    this.oss = GlobalCapacity.OSS;
                } else {
                    //autoload it ourselves
                    //TODO: trigger warning that we are autoloading OSS
                    var self = this;
                    //OSS path must be absolute
                    this.inject_script(CSrms.getAppUrl('OSS/GlobalCapacity.OSS.js'), function after_oss_load(){
                        self.oss = GlobalCapacity.OSS;
                        callback.call(self, self.oss);
                    });
                    return;
                }
            }
            callback.call(this, this.oss);
        },
        inject_script: function inject_script(src, callback) {
            if(this.autoload === false) {
                throw "Cannot autoload scripts";
            }
            var script_elem = document.createElement('script');
            script_elem.src = src;
            script_elem.onreadystatechange = script_elem.onload = function() {
                var state = script_elem.readyState;
                if (callback && !callback.done && (!state || /loaded|complete/.test(state))) {
                    callback.done = true;
                    callback();
                }
            };
            document.getElementsByTagName('head')[0].appendChild(script_elem);
        },
        loadScripts: function loadScripts(scripts, callback) {
            if(this.autoload === false) {
                throw "Cannot autoload scripts";
            }
            this.getOSS(function(oss){
                oss.loadScripts(scripts, callback);
            });
        },
        /**
         * rudimentary parsing - enough to tell what kind of document it is
         */
        getTaskType: function(task_document_elem) {
            var task_type = null;
            //we will tell by examining the tables:
            var tables = task_document_elem.getElementsByTagName("table");
            //for Trouble Ticket, look at the first table, first row, second cell
            if (tables.length > 0 && tables[0].rows.length > 0 && this.testRowTroubleTicket(tables[0].rows[0])) {
                task_type = "Trouble Ticket";
            }

            // for WorkOrder in The Hub
            if(task_document_elem.querySelector("div#taskDetailsDiv")) {
                tables = task_document_elem.querySelector("div#taskDetailsDiv").getElementsByTagName("table");
                if (tables.length > 1 && tables[1].rows.length > 0 && this.testRowWorkOrder(tables[1].rows[0])) {
                    task_type = "Work Order";
                }
            }

            //for Work Order, check 3rd table, 1st row, 1st cell
            if (tables.length > 2 && tables[2].rows.length > 0 && this.testRowWorkOrder(tables[2].rows[0])) {
                task_type = "Work Order";
            }

            //for Offnet Task, check 1st table, 2nd row, 2nd cell
            if (tables.length > 0 && tables[0].rows.length > 1 && this.testRowOffnetTask(tables[0].rows[1])) {
                task_type = "Offnet Task";
            }
            // Additional Offnet Task check:
            var td_sections = task_document_elem.querySelectorAll('td.section');
            for(var i=0, len=td_sections.length; i < len; i++){
                if(this.cleanText(td_sections[i].textContent) === 'ORDER STATUS DETAILS') {
                    task_type = "Offnet Task";
                }
            }

            //for Total Agility, look for div id=commercialTabs (this is actually Dart)
            var tab_divs = task_document_elem.getElementsByClassName('ui-tabs');
            for(var i=0, len=tab_divs.length;i<len;i++) {
                if(tab_divs[i].id === 'commercialTabs') {
                    task_type = "Total Agility";
                    break;
                }
            }

            console.log("TASK TYPE", task_type);
            return task_type;
        },
        /**
         *
         * =============Loaders========================================
         */
        loadWorkOrderDocumentResources: function(callback) {
            if(typeof GlobalCapacity.TaskDocuments.WorkOrder !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableTools === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableTools.js'));
            }
            if(typeof TableDataFilters === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/TableDataFilters.js'));
            }
            scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.WorkOrder.js'));
            this.loadScripts(scripts, callback);
        },
        createWorkOrderDocument: function(task_document_elem, callback) {
            var self = this;
            this.loadWorkOrderDocumentResources(function resources_loaded(){
                callback(new GlobalCapacity.TaskDocuments.WorkOrder(task_document_elem, self));
            });
        },
        loadTroubleTicketDocumentResources: function(callback) {
            if(typeof GlobalCapacity.TaskDocuments.TroubleTicket !== 'undefined') {
                callback();
                return;
            }
            var self = this;
            //need WorkOrderDocument, so load it first:
            this.loadWorkOrderDocumentResources(function(){
                var scripts = [CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.TroubleTicket.js')];
                self.loadScripts(scripts, callback);
            });
        },
        createTroubleTicketDocument: function(task_document_elem, callback) {
            var self = this;
            this.loadTroubleTicketDocumentResources(function resources_loaded(){
                callback(new GlobalCapacity.TaskDocuments.TroubleTicket(task_document_elem, self));
            });
        },
        loadOffnetTaskDocumentResources: function(callback) {
            if(typeof GlobalCapacity.TaskDocuments.OffnetTask !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableTools === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableTools.js'));
            }
            if(typeof TableDataFilters === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/TableDataFilters.js'));
            }
            scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.OffnetTask.js'));
            this.loadScripts(scripts, callback);
        },
        createOffnetTaskDocument: function(task_document_elem, callback) {
            var self = this;
            this.loadOffnetTaskDocumentResources(function resources_loaded(){
                callback(new GlobalCapacity.TaskDocuments.OffnetTask(task_document_elem, self));
            });
        },
        loadTotalAgilityDocumentResources: function(callback) {
            if(typeof GlobalCapacity.TaskDocuments.TotalAgility !== 'undefined') {
                callback();
                return;
            }
            var scripts = [];
            if(typeof DOMElements === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMElements.js'));
            }
            if(typeof DOMTableTools === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/DOMTableTools.js'));
            }
            if(typeof TableDataFilters === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/TableDataFilters.js'));
            }
            scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.TotalAgility.js'));
            this.loadScripts(scripts, callback);
        },
        createTotalAgilityDocument: function(task_document_elem, callback) {
            var self = this;
            this.loadTotalAgilityDocumentResources(function resources_loaded(){
                callback(new GlobalCapacity.TaskDocuments.TotalAgility(task_document_elem, self));
            });
        },
        //TODO: rename createTaskDocument
        loadTaskDocument: function(task_document_elem, callback) {
            var task_document = null;
            var task_type = this.getTaskType(task_document_elem);
            switch (task_type) {
                case 'Work Order':
                    task_document = this.createWorkOrderDocument(task_document_elem, callback);
                    break;
                case 'Trouble Ticket':
                    task_document = this.createTroubleTicketDocument(task_document_elem, callback);
                    break;
                case 'Offnet Task':
                    task_document = this.createOffnetTaskDocument(task_document_elem, callback);
                    break;
                case 'Total Agility':
                    task_document = this.createTotalAgilityDocument(task_document_elem, callback);
                    break;
                default:
                    throw "Cannot create task document for " + task_type;
            }
            return task_document;
        },
        loadDocumentResources: function(callback) {
            if(typeof GlobalCapacity.TaskDocuments.Document !== 'undefined') {
                callback();
                return;
            }
            var self = this;
            var scripts = [CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.Document.js')];
            self.loadScripts(scripts, callback);
        },
        createDocumentObj: function(document_elem, callback) {
            var self = this;
            this.loadDocumentResources(function(){
                self.loadTaskDocument(document_elem, function(document_obj){
                    callback(new GlobalCapacity.TaskDocuments.Document(document_obj, self));
                });
            });
        },
        loadTaskDocumentWindowController: function(callback) {
            if(typeof GlobalCapacity.TaskDocuments.TaskDocumentWindowController !== 'undefined') {
                callback(GlobalCapacity.TaskDocuments.TaskDocumentWindowController);
                return;
            }
            var scripts = [];
            if(typeof ChildWindowOpener === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/ChildWindowOpener.js'));
            }
            if(typeof ChildWindow === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/ChildWindow.js'));
            }
            if(typeof GlobalCapacity.TaskDocuments.Channel === 'undefined') {
                scripts.push(CSrms.getLibsUrl(this.oss.libs.channel));
            }
            if(typeof GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel === 'undefined') {
                scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel.js'));
            }
            scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.TaskDocumentWindowController.js'));
            this.loadScripts(scripts, function(){
                callback(GlobalCapacity.TaskDocuments.TaskDocumentWindowController);
            });
        },
        loadParseChild: function(callback) {
            if(typeof GlobalCapacity.TaskDocuments.ParseChild !== 'undefined') {
                callback(GlobalCapacity.TaskDocuments.ParseChild);
                return;
            }
            var scripts = [];
            if(typeof ParentWindow === 'undefined') {
                scripts.push(CSrms.getCsrmsUrl('js/ParentWindow.js'));
            }
            if(typeof GlobalCapacity.TaskDocuments.Channel === 'undefined') {
                scripts.push(CSrms.getLibsUrl(this.oss.libs.channel));
            }
            if(typeof GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel === 'undefined') {
                scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel.js'));
            }
            scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.ParseChild.js'));
            this.loadScripts(scripts, function(){
                callback(GlobalCapacity.TaskDocuments.ParseChild);
            });
        },
        loadCommands: function(callback) {
            if(GlobalCapacity.TaskDocuments.Commands !== undefined) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.TaskDocuments.Commands);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];
                scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.Commands.js'));
                self.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.TaskDocuments.Commands);
                });
            });

            if(callback){
                promise.then(function(commands_obj){
                    callback(commands_obj);
                });
            }

            return promise;
        },
        loadControls: function(callback) {
            if(GlobalCapacity.TaskDocuments.Controls !== undefined) {
                if(callback) callback();
                return Promise.resolve(GlobalCapacity.TaskDocuments.Controls);
            }
            var self = this;
            var promise = new Promise(function(resolve, reject){
                var scripts = [];

                if(typeof jQuery === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('jquery-1.11.0/jquery.min.js'));
                }
                if(typeof _ === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('underscore-1.6.0/underscore.js'));
                }
                if(typeof Backbone === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('backbone-1.1.2/backbone.js'));
                    scripts.push(CSrms.getLibsUrl('backbone-forms-20141202/distribution/backbone-forms.min.js'));
                }
                if(typeof Handlebars === 'undefined') {
                    scripts.push(CSrms.getLibsUrl('handlebars-1.3.0/handlebars.js'));
                }

                if(typeof FieldPortal === 'undefined') {
                    scripts.push(CSrms.getAppUrl('FieldPortal/FieldPortal.js'));
                }

                if(typeof FieldPortal === 'undefined' || FieldPortal.Backbone.Dispatch === 'undefined') {
                    scripts.push(CSrms.getAppUrl('FieldPortal/FieldPortal.Backbone.Dispatch.js'));
                }

                scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.Backbone.Forms.TaskFormTemplate.js'));
                scripts.push(CSrms.getAppUrl('TaskDocuments/GlobalCapacity.TaskDocuments.Controls.js'));
                self.loadScripts(scripts, function(){
                    resolve(GlobalCapacity.TaskDocuments.Controls);
                });
            });

            if(callback){
                promise.then(function(controls_obj){
                    callback(controls_obj);
                });
            }

            return promise;
        },

        /**
         *
         * =============Rudimentary DOM Testing==================================
         */
        cleanText: function(text) {
            //trim . strip newlines . strip extra whitespace
            return text.replace(/^\s+|\s+$/g, '').replace(/\r?\n|\r/g, ' ').replace(/\s+/g, ' ');
        },
        testRowTroubleTicket: function(table_row) {
            var cells = table_row.cells;
            if (cells.length > 1) {
                var cell_content = this.cleanText(cells[1].textContent);
                if (cell_content.substr(0, 22) === "Trouble Ticket Browser") {
                    return true;
                }
            }
            return false;
        },
        testRowWorkOrder: function(table_row) {
            var cells = table_row.cells;
            if (cells.length > 0) {
                var cell_content = this.cleanText(cells[0].textContent);
                if (cell_content === "Client Order Info" || cell_content === "Client Order Information") {
                    return true;
                }
            }
            return false;
        },
        testRowOffnetTask: function(table_row) {
            var cells = table_row.cells;
            if (cells.length > 1) {
                var cell_content = this.cleanText(cells[1].textContent);
                if (cell_content === "I S G ORDER MANAGER") {
                    return true;
                }
            }
            return false;
        },

        /**
         *
         * =============Date/Time Functions==================================
         */
        getTimeslotForHour : function(hour_value) {
            switch (hour_value) {
                case "9AM":
                    return "9AM";
                case "1PM":
                    return "1PM";
                case "08":
                case "09":
                case "10":
                case "11":
                    return "9-1";
                case "12":
                case "13":
                case "14":
                case "15":
                    return "12-4";
                case "16":
                    return "4-7";
            }
            return null;
        },
        getShortDate : function(date_str) {
            //expects a string of the format 1/02/2008. returns 1/2/08
            var date_parts = date_str.split("/");
            var monthstr = date_parts[0];
            if (monthstr.charAt(0) == '0') {
                monthstr = monthstr.charAt(1);
            }

            var daystr = date_parts[1];
            if (daystr.charAt(0) === 0) {
                daystr = daystr.charAt(1);
            }

            var yearstr = date_parts[2];
            if (yearstr.length == 4) {
                yearstr = yearstr.charAt(2) + yearstr.charAt(3);
            }

            var return_date = "" + monthstr + "/" + daystr + "/" + yearstr;
            return return_date;
        },

        getDateNoZeros : function(date_str) {
            //expects a string of the format 1/02/2008. returns 1/2/2008
            var date_parts = date_str.split("/");
            var monthstr = date_parts[0];
            if (monthstr.charAt(0) == '0') {
                monthstr = monthstr.charAt(1);
            }

            var daystr = date_parts[1];
            if (daystr.charAt(0) === 0) {
                daystr = daystr.charAt(1);
            }

            var yearstr = date_parts[2];
            if (yearstr.length == 2) {
                yearstr = '20' + yearstr;
            }

            var return_date = "" + monthstr + "/" + daystr + "/" + yearstr;

            return return_date;
        },
        getShortDate_FromDateObj : function(date_obj) {
            //expects a date obj. returns 1/2/08
            var month = date_obj.getMonth() + 1;
            var date = date_obj.getDate();
            var year = ("" + date_obj.getFullYear()).slice(-2);
            return month + '/' + date + '/' + year;
        },
        getDateNoZeros_FromDateObj : function(date_obj) {
            //expects a date obj. returns 1/2/2008
            var month = date_obj.getMonth() + 1;
            var date = date_obj.getDate();
            var year = date_obj.getFullYear();
            return month + '/' + date + '/' + year;
        },
        getStdDateString_FromDateObj : function(date_obj) {
            //expects a date obj. returns 01/02/2008
            var month = ('0' + (date_obj.getMonth() + 1)).slice(-2);
            var date = ('0' + date_obj.getDate()).slice(-2);
            var year = date_obj.getFullYear();
            return month + '/' + date + '/' + year;
        }
    };
    return TaskDocuments;
})();
