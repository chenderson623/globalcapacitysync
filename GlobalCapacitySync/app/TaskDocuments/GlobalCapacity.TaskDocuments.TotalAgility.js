
//requires: DOMTableTools, DOMElements, TableDataFilters

//var GlobalCapacity = GlobalCapacity || {};
//GlobalCapacity.TaskDocuments = GlobalCapacity.TaskDocuments || {};

if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TaskDocuments == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments is required.';
}

GlobalCapacity.TaskDocuments.TotalAgility = (function() {
    var TotalAgility = function TotalAgility(task_document_elem, task_documents) {
        this.task_document_elem = task_document_elem;
        this.task_documents = task_documents;
        //Vars:
        this.task_type = "Advanced Services";
        this.task_data = {
            TaskId: null,
            TaskType: null,
            JobSubType: null,
            LocationName: null,
            LocationAddress: null,
            LocationCity: null,
            LocationState: null,
            LocationZip: null,
            LocationPhone: null,
            LocationContact: null,
            //ContractRegionName: null,
            //ContractSpecialInst: null,
            'jobs_data:account_number': null,
            'jobs_data:provider': null,
            'jobs_data:circuit_number': null,
            'jobs_data:product': null,
            'jobs_data:ilec_circuit_numbers': null
        };
        //Internal:
        this.document_table_set = null;
    };
    TotalAgility.prototype = {
        getH3Set: function() {
            if(!this.h3_set) {
                this.h3_set = new DOMElements.ChildElementMap(this.task_document_elem, 'H3');
            }
            return this.h3_set;
        },
        findH3: function(h3_text) {
            return this.getH3Set().findByText(h3_text);
        },
        findLineDetailsH3: function() {
            return this.findH3('Line Details');
        },
        findLineDetailsBoxFrame: function() {
            var div = DOMElements.Tools.getNextSibling(this.findLineDetailsH3(), function(elem){
                return elem.nodeName.toUpperCase() === 'DIV' && elem.classList.contains('boxFrame');
            });
            if(!div) {
                throw "Line Details box frame not found";
            }
            return div;
        },
        findLSNTextNode: function() {
            var div = this.findLineDetailsBoxFrame();
            var b_set = new DOMElements.ChildElementMap(div, 'B');
            var lsn_label = b_set.findByText('LSN:');
            var lsn_text_node = DOMElements.Tools.getNextSibling(lsn_label, function(elem){
                return elem.nodeName === '#text';
            });
            if(!lsn_text_node) {
                throw "LSN Text Node not found";
            }
            return lsn_text_node;
        },
        getTableSet: function() {
            if(!this.document_table_set) {
                this.document_table_set = new TotalAgility_TableSet(this.task_document_elem);
            }
            return this.document_table_set;
        },

        getDataTable: function(table_name) {
            switch (table_name) {
                case 'CustomerInfo':
                    return this.getTableSet().getCustomerInfoTable();
                case 'AccessInfo':
                    return this.getTableSet().getAccessInfoTable();
                default:
                    throw ("Table [" + table_name + "] was not found");
            }
        },

        //=================Parse Functions================================
        parseCityStateZip: function(text_value) {
            var parsed_data = {};
            //split by -, last part will be zip code
            var parts = text_value.split('-');
            parsed_data.LocationZip = parts.pop().trim();
            //rejoin
            text_value = parts.join(' ');
            //split by comma. last part will be state
            parts = text_value.split(',');
            parsed_data.LocationState = parts.pop().trim();
            //rejoin whatever is left
            parsed_data.LocationCity = parts.join(',').trim();
            return parsed_data;
        },

        parseStreetAddress: function(text_value) {
            var parts = text_value.split(',');
            //street address is first part
            var street_address = parts.shift().trim();

            //city state zip is second part:
            var city_state_zip = parts.join(',');
            var parsed_data = this.parseCityStateZip(city_state_zip);

            parsed_data.LocationAddress = street_address;
            return parsed_data;
        },

        //=================Fill Data functions=============================
        _fill_data: function(source, dest) {
            for (var data_key in source) {
                dest[data_key] = source[data_key];
            }
            return dest;
        },
        _fill_map: function(map, source, dest) {
            var source_key;
            for (var data_key in map) {
                source_key = map[data_key];
                dest[data_key] = source[source_key];
            }
            return dest;
        },
        fillData_CustomerInfo: function(data_obj) {
            var table = this.getDataTable('CustomerInfo');
            var table_data_mapper = new DOMTableTools.TableDataMappers.EachRowHasHeaderAndData_WithSections();
            var customer_data = table_data_mapper.getData(table);

            data_obj.LocationName =  customer_data['Customer Name'];
            data_obj['jobs_data:account_number'] =  customer_data['Account#'];

            var address = customer_data['Service Address'];
            var address_data = this.parseStreetAddress(address);
            this._fill_data(address_data, data_obj);
        },
        fillData_AccessInfo: function(data_obj) {
            var table = this.getDataTable('AccessInfo');
            var table_data_mapper = new DOMTableTools.TableDataMappers.FourColumnData();
            var data = table_data_mapper.getData(table);

            var map = {
                LocationPhone: 'Site Contact Number',
                LocationContact: 'Site Contact Name',
                'jobs_data:provider': 'Provider',
                'jobs_data:circuit_number': 'Provider Circuit Number',
                'jobs_data:product': 'Access Product',
                'jobs_data:ilec_circuit_numbers': 'ILEC Circuit Number'
            };
            this._fill_map(map, data, data_obj);
        },

        //=================Main functions===================================
        getWorkLog: function() {
            throw "Needs Implementation";
        },
        getTimeslotSearch: function() {
            throw "Needs Implementation";
        },
        getTaskId: function() {
            return this.findLSNTextNode().nodeValue.trim();
        },

        getTaskType: function() {
            return this.task_type;
        },

        getOrderStatus: function() {
            throw "Needs Implementation";
        },

        getCurrentScheduleDate: function() {
            throw "Needs Implementation";
        },

        getCurrentScheduleDateObj: function() {
            throw "Needs Implementation";
        },

        getTaskData: function() {
            this.task_data.TaskType = this.getTaskType();
            this.task_data.TaskId = this.getTaskId();
            this.fillData_CustomerInfo(this.task_data);
            this.fillData_AccessInfo(this.task_data);
            return this.task_data;
        }
    };

    /**
     *
     *
     * ========================WorkOrder TableSet============================================
     *
     */
    var TotalAgility_TableSet = function(parent_container) {
        this.table_set = new DOMTableTools.TableSet(parent_container);
        this.tables = {}; //client_order_info, client_circuit_info, dslam_info, worklog, additional_notes
    };
    TotalAgility_TableSet.prototype = {
        getCachedTableByFirstCell: function(table_name, cell_content) {
            if (this.tables[table_name] === undefined) {
                this.tables[table_name] = this.findTableByFirstCell2(cell_content);
            }
            return this.tables[table_name];
        },

        //Alt:
        cleanCellFilter: function(search_cell_content, cell_elem) {
            return DOMElements.Filters.getElementCleanText(cell_elem) === search_cell_content;
        },

        findTableByFirstCell2: function(cell_content) {
            return this.table_set.findTableByCell(0, 0, this.cleanCellFilter.bind(this, cell_content));
        },
        //END Alt

        getCustomerInfoTable: function() {
            return this.getCachedTableByFirstCell('customer_info', "Customer Name");
        },
        getAccessInfoTable: function() {
            return this.getCachedTableByFirstCell('access_info', "Access Product");
        }

    };


    return TotalAgility;

})();
