if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TaskDocuments == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments is required.';
}

GlobalCapacity.TaskDocuments.Controls = (function() {
    /*jshint multistr: true */
    'use strict';


    //note: this is independent of sync
    var NavBarDropdown_TroubleTicket = function(document_object, sync_control_factory) {
        this.document_object = document_object;
        this.sync_control_factory = sync_control_factory;
        this.template = ' \
            <a data-toggle="dropdown" class="dropdown-toggle" ><span class="glyphicon glyphicon-th"></span> ' + document_object.getTaskType() + ' <b class="caret"></b></a> \
            <ul class="dropdown-menu"> \
                <li><a tabindex="-1">Refresh Page</a></li> \
                <li class="divider end-section"></li> \
                <li class="get-task-data"><a tabindex="-1">Get Task Data</a></li> \
            </ul>';
        this.view = null;
    };
    NavBarDropdown_TroubleTicket.prototype = {
        get$El: function() {
            return this.getBackboneView().$el;
        },
        getBackboneView: function() {
            if (this.view !== null) {
                return this.view;
            }
            var self = this;
            var View = Backbone.View.extend({
                tagName: 'li',
                className: 'dropdown',
                events: {
                    'click .get-task-data': 'getTaskData'
                },
                template: Handlebars.compile(self.template),
                initialize: function() {
                    this.render();
                },
                render: function() {
                    var html = this.template();
                    this.$el.html(html);
                },
                getTaskData: function() {
                    var querystring_data = getQueryVariables();

                    //TODO: move this to a command object
                    var job_model_data = self.document_object.getTaskData();

                    //Some translations
                    job_model_data.ContractRefNum = job_model_data.TaskId;
                    job_model_data.JobType        = job_model_data.TaskType;

                    var form_data = _.extend({
                        AssignedTechName: querystring_data.AssignedTechName,
                        AssignedDate:     querystring_data.AssignedDate,
                        AssignedTime:     querystring_data.AssignedTime,
                        SchedSeq:         querystring_data.SchedSeq
                    }, job_model_data);

                    var job_sync_form = new JobSyncForm(self.sync_control_factory, form_data);
                    job_sync_form.render();

                    job_sync_form.getModel().on('synced', function(){
                        self.sync_control_factory.getSidePanel().setMessage('Job Created: ' + job_sync_form.getModel().get('workorder_id'));
                        setTimeout(function(){
                            self.sync_control_factory.getSidePanel().clear();
                            self.sync_control_factory.getSidePanel().close();
                        }, 2000);
                    });

                }
            });
            this.view = new View();
            return this.view;
        }
    };

    var task_form_schema = {
        AssignedTechName: {
            title: 'Tech Name',
            type: 'Select2',
            config: {
                width: 'resolve'
            },
            options: [
                {val: '-1', label: ''},
                {val: 'Chris Barnes', label: 'Chris Barnes'},
                {val: 'Chuck Oti', label: 'Chuck Oti'},
                {val: 'Craig MiddleBrook', label: 'Craig MiddleBrook'},
                {val: 'David Martinez', label: 'David Martinez'},
                {val: 'Larry Holt', label: 'Larry Holt'},
                {val: 'Loc Tran', label: 'Loc Tran'},
                {val: 'Manuel Chacon', label: 'Manuel Chacon'},
                {val: 'Mark Fox', label: 'Mark Fox'},
                {val: 'Abel Molina', label: 'Abel Molina'},
                {val: 'Jeff Pierce', label: 'Jeff Pierce'},
                {val: 'Michael Chavez', label: 'Michael Chavez'},
                {val: 'Robert Markley', label: 'Robert Markley'},
                {val: 'Ronald Gabin', label: 'Ronald Gabin'},
                {val: 'Charles Fiehtner', label: 'Charles Fiehtner'},
                {val: 'Tomas Nieves', label: 'Tomas Nieves'},
                {val: 'David Holt', label: 'David Holt'},
                {val: 'Juan Martinez', label: 'Juan Martinez'},
                {val: 'Peter Holt', label: 'Peter Holt'},
                {val: 'Mike Pozzuto', label: 'Mike Pozzuto'},
                {val: 'Mario Jsames', label: 'Mario Jsames'},
            ],
            validators: ['required']
        },
        ContractRegionName: {
            title: 'Region',
            type: 'Select2',
            config: {
                width: 'resolve'
            },
            options: [
                { val:"Atlanta, GA", label: "Atlanta, GA"},
                { val:"Baltimore, MD", label: "Baltimore, MD"},
                { val:"Boston, MA", label: "Boston, MA"},
                { val:"Chicago, IL", label: "Chicago, IL"},
                { val:"Cleveland, OH", label: "Cleveland, OH"},
                { val:"Dallas, TX", label: "Dallas, TX"},
                { val:"Denver, CO", label: "Denver, CO"},
                { val:"Hartford, CT", label: "Hartford, CT"},
                { val:"Houston, TX", label: "Houston, TX"},
                { val:"Indianapolis, IN", label: "Indianapolis, IN"},
                { val:"Los Angeles, CA", label: "Los Angeles, CA"},
                { val:"Miami, FL", label: "Miami, FL"},
                { val:"Milwaukee, WI", label: "Milwaukee, WI"},
                { val:"Minneapolis, MN", label: "Minneapolis, MN"},
                { val:"New York, NY", label: "New York, NY"},
                { val:"Orlando, FL", label: "Orlando, FL"},
                { val:"Philadelphia, PA", label: "Philadelphia, PA"},
                { val:"Phoenix, AZ", label: "Phoenix, AZ"},
                { val:"Portland, OR", label: "Portland, OR"},
                { val:"Richmond, VA", label: "Richmond, VA"},
                { val:"Sacramento, CA", label: "Sacramento, CA"},
                { val:"San Diego, CA", label: "San Diego, CA"},
                { val:"San Francisco, CA", label: "San Francisco, CA"},
                { val:"Santa Barbara, CA", label: "Santa Barbara, CA"},
                { val:"Seattle, WA", label: "Seattle, WA"},
                { val:"Tucson, AZ", label: "Tucson, AZ"},
                { val:"Washington, DC", label: "Washington, DC"},
            ],
            validators: ['required']
        },
        AssignedDate: {
            title: 'Scheduled Date',
            type: 'BootstrapDatePicker',
            options: [{}],
            validators: ['required'],
/*            template: _.template('\
    <div class="form-group CCC field-<%= key %>">\
      <label class="control-label" for="<%= editorId %>"><%= title %></label>\
      <div class="form-input">\
        <div class="input-group date" data-date-format="dd/mm/yyyy"> \
            <span data-editor></span>\
            <span class="input-group-addon"><i class="splashy-calendar_day"></i></span> \
        </div> \
        <p class="help-block" data-error></p>\
        <p class="help-block"><%= help %></p>\
      </div>\
    </div>\
  ') */
        },
        AssignedTime: {
            title: 'Scheduled Time',
            type: 'Text',
            validators: ['required']
        },

        ContractRefNum: {
            title: 'Task ID',
            type: 'Text',
            validators: ['required']
        },
        SchedSeq: {
            title: 'Dispatch Seq',
            type: 'Text'
        },
        JobType: {
            title: 'Job Type',
            type: 'Select2',
            config: {
                width: 'resolve'
            },
            options: [
                {val: "Work Order", label: "Work Order"},
                {val: "Trouble Ticket", label: "Trouble Ticket"},
                {val: "Advanced Services", label: "Advanced Services"}
            ],
            validators: ['required']
        },
        JobSubType: {
            title: 'Job Class',
            type: 'Select2',
            config: {
                width: 'resolve'
            },
            options: [
                {val: "Consumer", label: "Consumer"},
                {val: "Business", label: "Business"},
                {val: "Broadband & Offnet", label: "Broadband & Offnet"},
                {val: "Offnet Only", label: "Offnet Only"},
                {val: "Repair Only", label: "Repair Only"},
            ],
            validators: ['required']
        },
        'jobs_data:isp': {
            title: 'ISP',
            type: 'Text'
        },


        LocationName: {
            title: 'Name',
            type: 'Text'
        },
        LocationAddress: {
            title: 'Address',
            type: 'Text'
        },
        LocationCity: {
            title: 'City',
            type: 'Text'
        },
        LocationState: {
            title: 'State',
            type: 'Text'
        },
        LocationZip: {
            title: 'Zip',
            type: 'Text'
        },
        LocationPhone: {
            title: 'Phone',
            type: 'Text'
        },


        'jobs_data:service_type': {
            title: 'Service',
            type: 'Text'
        },
        'jobs_data:cpe_type': {
            title: 'CPE Type',
            type: 'Text'
        },
        'jobs_data:circuit_number': {
            title: 'Circuit Number',
            type: 'Text'
        },
        'jobs_data:affiliate': {
            title: 'Affiliate',
            type: 'Text'
        },
        'jobs_data:scp_num': {
            title: 'SCP Num',
            type: 'Text'
        },
        'jobs_data:sow_num': {
            title: 'SOW',
            type: 'Text'
        },
        'jobs_data:work_order': {
            title: 'Work Order',
            type: 'Text'
        },
        'jobs_data:circuit_qty': {
            title: 'Circuit Qty',
            type: 'Text'
        },
        'jobs_data:ilec_circuit_numbers': {
            title: 'ILEC Circuits',
            type: 'Text'
        },

        ContractSpecialInst: {
            title: 'Notes',
            type: 'TextArea'
        }

    };

    var task_form_fieldsets = [{
        legend: 'Schedule',
        fields: ['AssignedTechName', 'ContractRegionName', 'AssignedDate', 'AssignedTime']
    }, {
        legend: 'Job Type',
        fields: ['ContractRefNum', 'SchedSeq', 'JobType', 'JobSubType', 'jobs_data:isp']
    }, {
        legend: 'End User',
        fields: ['LocationName', 'LocationAddress', 'LocationCity', 'LocationState', 'LocationZip', 'LocationPhone']
    }, {
        legend: 'Service',
        fields: ['jobs_data:service_type', 'jobs_data:cpe_type', 'jobs_data:circuit_number', 'jobs_data:affiliate', 'jobs_data:scp_num', 'jobs_data:sow_num', 'jobs_data:work_order', 'jobs_data:circuit_qty', 'jobs_data:ilec_circuit_numbers']
    }, {
        legend: 'Notes',
        fields: ['ContractSpecialInst']
    }];


    var TaskForm = Backbone.Form.extend({
        schema: task_form_schema,
        id: "task-form",
        initializeX: function() {

//TODO: cascade JobSubType
this.on('JobType:change', function(form, titleEditor, extra) {
    //console.log('CHANGED', form.fields.JobSubType.$el.find('span[data-editor]'));

    var editor = form.fields.JobSubType.editor;

    var options = [
                    {val: "", label: ""},
                    {val: "Consumer", label: "ConsumerX"},
                    {val: "Business", label: "BusinessX"},
                ];

    editor.setOptions(options);
    editor.$el.val('');
    editor.$el.trigger('change');

});


        }
    });


    var JobSyncForm = function(sync_control_factory, job_model_data) {
        this.sync_control_factory = sync_control_factory;
        this.job_model_data       = job_model_data;

        this.form = null;
        this.model = null;
    }

    JobSyncForm.prototype = {

        getForm: function() {
            if(this.form) {
                return this.form;
            }
            this.form = new TaskForm({
                model: this.getModel(),
                fieldsets: task_form_fieldsets,
                submitButton: 'Submit'
            });
            return this.form;
        },

        renderFormError: function(error_message) {
            this.getForm().$el.find('.form-errors').remove();

            var $error_el = $('<div class="form-errors col-sm-12 col-md-12">');
            var $error_message = $('<div class="alert alert-danger">');
            $error_message.append('<b>Form Errors</b><hr>');
            $error_message.append(error_message);
            $error_el.append($error_message);

            this.getForm().$el.prepend($error_el);

            $('#task-form-container').animate({
                scrollTop: $error_el.offset().top
            }, 2000);

        },

        getModel: function() {
            if(this.model) {
                return this.model;
            }
            this.model = new FieldPortal.Backbone.Dispatch.Job(this.job_model_data);

            this.model.on('sync', function(model, response, options){
                console.log("MODEL SYNC", arguments);
            });

            this.model.on('error', function(model, response, options){
                var message = (response.responseText) ? response.responseText : "Unknown error";
                this.renderFormError(message);

                //console.log("MODEL ERROR", arguments);

            }, this);

            return this.model;
        },

        scrollToError: function(form) {
            var $first_error = form.$el.find('div.has-error').first();
            $('#task-form-container').animate({
                scrollTop: $first_error.offset().top
            }, 2000);
        },

        render: function() {
            var self = this;
            var form = this.getForm();
            //Setup the submit handler:
            form.on('submit', function(e){
                e.preventDefault();
                var errors = form.commit();
                if(!errors) {
                    form.model.save();
                } else {
                    self.scrollToError(form);
                }
            });

            this.model.on('sync', function(model, response, options){
                if(response.success === false) {
                    if(response.errors && response.errors.fields) {
                        for(var field in response.errors.fields) {
                            form.fields[field].setError(response.errors.fields[field]);
                        }
                    }
                } else {
                    self.model.set('workorder_id', response.data.id);
                    self.model.trigger('synced');
                }
            });

            this.sync_control_factory.getSidePanel().setForm(form);
        }
    };


    /**
     *
     *
     * ============================UI ITEMS================================================
     */
    var NavBarDocumentType = function(document_object) {
        var document_type = document_object.getTaskType();
        var task_id = document_object.getTaskId();
        this.template = ' \
            <div class="document-type-controls"> \
                <h4 class="document-type">' + document_type + '</h4> \
                <div class="task-id">' + task_id + '</div> \
            <\/div>';
    };
    NavBarDocumentType.prototype = {
        getHtml: function() {
            return this.template;
        }
    };

    var getQueryVariables = function() {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        var query_vars = {};
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            query_vars[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
        }
        return query_vars;
    };

    var SidePanel = Backbone.View.extend({
        tagName: 'div',
        id: 'side-panel',
        width: 300,
        events: {
            'click .close-panel': 'toggle'
        },
        initialize: function() {

        },
        render: function() {
            this.opened = false;

            this.$el.width('0px');

            this.$container = $('<div class="side-panel-container" id="task-form-container">');
            this.$el.append(this.$container);

            $('body').append(this.$el);
            this.$el.css('top', $('.navbar').css('height'));
            this.body_padding = parseInt($('body').css('padding-top'));

            this.$open_close_button = $('<button type="button" class="close-panel btn btn-primary btn-xs" style="top:' + (this.body_padding + 2) + ';"><span class="glyphicon glyphicon-chevron-right"></span></button>');
            this.$el.append(this.$open_close_button);

        },
        toggle: function() {
            if(this.opened === true) {
                this.close();
            } else {
                this.open();
            }
        },
        open: function() {
            this.opened = true;
            this.$el.animate({width: this.width + 'px'});
            $('body').animate({paddingLeft: this.width + 'px'});
            console.log(this.$open_close_button);
            this.$open_close_button.animate({left: this.width + 'px'});
            this.$open_close_button.find('span').removeClass('glyphicon-chevron-right');
            this.$open_close_button.find('span').addClass('glyphicon-chevron-left');
        },
        close: function() {
            this.opened = false;
            this.$el.animate({width: '0px'});
            $('body').animate({paddingLeft: '0px'});
            this.$open_close_button.animate({left: '2px'});
            this.$open_close_button.find('span').removeClass('glyphicon-chevron-left');
            this.$open_close_button.find('span').addClass('glyphicon-chevron-right');
        },
        clear: function(message) {
            this.$container.html('');
        },
        setForm: function(form) {
            this.form = form;
            this.$container.html(form.render().$el);
            this.open();
        },
        setMessage: function(message) {
            this.$container.html('<div>' + message + '</div>');
        }

    });

    var SyncControlFactory = function(document_object) {
        this.document_object = document_object;
    };
    SyncControlFactory.prototype = {
        generateUIControls: function(ui) {
            //Document-type info piece
            this.document_type_control = new NavBarDocumentType(this.document_object);
            ui.setMiddleItem(this.document_type_control.getHtml()); //TODO: change to get$El
            ui.setMiddleDivider();

            //TODO: make method in document objects:
            //this.document_object.getNavBarControl();
            this.dropdown_control = new NavBarDropdown_TroubleTicket(this.document_object, this);
            ui.setMiddleItemLi(this.dropdown_control.get$El());
        },
        getSidePanel: function() {
            if(!this.task_form_panel) {
                this.task_form_panel = new SidePanel();
                this.task_form_panel.render();
            }
            return this.task_form_panel;
        }
    };

    return {
        SyncControlFactory: SyncControlFactory
    };
})();
