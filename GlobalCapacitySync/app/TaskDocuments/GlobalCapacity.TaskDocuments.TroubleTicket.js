'use strict';
//requires: DOMTableTools, DOMElements, TableDataFilters
//var GlobalCapacity = GlobalCapacity || {};
//GlobalCapacity.TaskDocuments = GlobalCapacity.TaskDocuments || {};
if (typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if (typeof GlobalCapacity.TaskDocuments == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments is required.';
}
GlobalCapacity.TaskDocuments.TroubleTicket = (function() {
    var TroubleTicket = function TroubleTicket(task_document_elem, task_documents) {
        this.task_document_elem = task_document_elem;
        this.task_documents = task_documents;
        //Vars:
        this.task_type = "Trouble Ticket";
        this.task_data = {
            TaskId: null,
            TaskType: null,
            JobSubType: null,
            LocationName: null,
            LocationAddress: null,
            LocationCity: null,
            LocationState: null,
            LocationZip: null,
            LocationPhone: null,
            ContractRegionName: null,
            ContractSpecialInst: null,
            "jobs_data:work_order": null,
            "jobs_data:affiliate": null,
            "jobs_data:isp": null,
            'jobs_data:service_type': null,
            'jobs_data:cpe_type': null,
            'jobs_data:circuit_number': null,
            'jobs_data:scp_num': null,
            'jobs_data:ilec_circuit_numbers': null,
            'jobs_data:circuit_qty': null
        };
        //Internal:
        this.document_table_set = null;
        this.summary_data = null;
        this.end_user_data = null;
        this.work_order_data = null;
        this.work_log = null;
        this.timeslot_search = null;
    };
    TroubleTicket.prototype = {

        getTableSet: function() {
            if(this.document_table_set === null) {
                this.document_table_set = new TroubleTicket_TableSet(this.task_document_elem);
            }
            return this.document_table_set;
        },
        _fill_data: function(source, target) {
            //will fill in target with keys in source - that are ALREADY existing in target
            for (var key in target) {
                if (target.hasOwnProperty(key) && source.hasOwnProperty(key)) {
                    target[key] = source[key];
                }
            }
        },
        //=================Summary===============================================
        getSummaryData: function() {
            if (this.summary_data === null) {
                var summary_table = this.getTableSet().getTable("Summary");
                var table_data_mapper = new DOMTableTools.TableDataMappers.EachRowHasHeaderAndData_WithSections();
                this.summary_data = table_data_mapper.getData(summary_table);
            }
            return this.summary_data;
        },
        //=================End User===============================================
        getEndUserData: function() {
            if (this.end_user_data === null) {
                var end_user_table = this.getTableSet().getTable("EndUser");
                var table_data_mapper = new DOMTableTools.TableDataMappers.EachRowHasHeaderAndData_WithSections();
                this.end_user_data = table_data_mapper.getData(end_user_table);
            }
            return this.end_user_data;
        },
        //=================Work Order===============================================
        getWorkOrderData: function() {
            if (this.work_order_data === null) {
                var work_order_container = this.getTableSet().getWorkOrderTableContainer();
                var work_order_document = new GlobalCapacity.TaskDocuments.WorkOrder(work_order_container, this.task_documents);
                this.work_order_data = work_order_document.getTaskData();
            }
            return this.work_order_data;
        },
        //=================Main functions===================================
        getWorkLog: function() {
            if (this.work_log === null) {
                var work_log_table = this.getTableSet().getTable("Worklog");
                this.work_log = new TroubleTicket_Worklog(work_log_table);
            }
            return this.work_log;
        },
        getTimeslotSearch: function() {
            if(this.timeslot_search === null) {
                this.timeslot_search = new TroubleTicket_TimeslotSearch(this.getWorkLog());
            }
            return this.timeslot_search;
        },
        getTaskId: function() {
            var data = this.getSummaryData();
            return data['TT ID:'];
        },
        getTaskType: function() {
            return this.task_type;
        },
        getOrderStatus: function() {
            var data = this.getSummaryData();
            return data['Status:'];
        },
        getTaskData: function() {
            //Start with Work Order data and fill from there:
            var work_order_data = this.getWorkOrderData();
            this._fill_data(work_order_data, this.task_data);
            this.task_data['jobs_data:work_order'] = this.task_data.TaskId;
            this.task_data.TaskId = this.getTaskId();
            this.task_data.TaskType = this.getTaskType();
            this.task_data.ContractSpecialInst = this.getSummaryData()['Problem Description:'];
            return this.task_data;
        }
    };
    /**
     *
     *
     * ========================TroubleTicket TableSet============================================
     *
     */
    var TroubleTicket_TableSet = function(parent_container_elem) {
        this.parent_container_elem = parent_container_elem;

        //this is not reliable since table could be wrapped in another element
        this.table_set_h4_map = {
            Summary: "Summary",
            EndUser: "Affected End User/Partner",
            Partner: "Partner/Transport Circuit",
            ILEC: ["ILEC/CAP", "possible name change"],
            Worklog: "Trouble Ticket Worklog/Notes",
            NEAffected: "NE Affected",
            DispatchTasks: "Dispatch Tasks",
            History: "History",
            PreviousTickets: "Previous Tickets",
            WorkOrder: "Latest Client Order"
        };
        this.h4_set = new DOMElements.ChildElementMap(this.parent_container_elem, 'H4');
        this.table_set = new DOMTableTools.TableSet(this.parent_container_elem);
        this.table_map = {};
    };
    TroubleTicket_TableSet.prototype = {
        findH4: function(h4_text) {
            return this.h4_set.findByText(h4_text);
        },
        findTableByH4Text: function(h4_text) {
            var h4_elem = this.findH4(h4_text);
            if (h4_elem === null) {
                return null;
            }
            var table_elem = DOMElements.Tools.getNextSibling(h4_elem, DOMElements.Tools.tableFilter);
            return table_elem;
        },
        //returns the first one found
        findTableByH4TextArray: function(h4_text_array) {
            var h4_text;
            var table;
            for (var i in h4_text_array) {
                h4_text = h4_text_array[i];
                table = this.findTableByH4Text(h4_text);
                if (table !== null) {
                    return table;
                }
            }
            return null;
        },
        findTableByH4Map: function(table_name) {
            var h4_text = this.table_set_h4_map[table_name];
            if (h4_text === undefined) {
                return null;
            }
            if (typeof h4_text === 'object') {
                return this.findTableByH4TextArray(h4_text);
            } else {
                return this.findTableByH4Text(h4_text);
            }
        },
        getTable: function(table_name) {
            if (this.table_map[table_name] === undefined) {
                var table = null;
                var table_dom = null;
                switch (table_name) {
                    case 'Summary':
                    case 'EndUser':
                        table = this.findTableByH4Map(table_name);
                        if (table) {
                            table_dom = new DOMTableTools.TableUtil(table);
                        }
                        break;
                    case 'Worklog':
                        var cell_compare = function(cell_elem) {
                            return DOMElements.Filters.getElementCleanText(cell_elem) === "Who@When";
                        };
                        table_dom = this.table_set.findTableByCell(0, 0, cell_compare);
                        break;
                    case 'WorkOrder':
                        var wo_container = this.getWorkOrderTableContainer();
                        var table_elems = wo_container.getElementsByTagName('TABLE');
                        table_dom = new DOMTableTools.TableUtil(table_elems[0]);
                        break;
                }
                this.table_map[table_name] = table_dom;
            }
            return this.table_map[table_name];
        },
        //===================Work Order ============================================
        getWorkOrderTableContainer: function() {
            var h4_elem = this.findH4('Latest Client Order');
            return DOMElements.Tools.getNextSibling(h4_elem, DOMElements.Tools.divFilter);
        }
    };

    /**
     *
     *
     * ========================TroubleTicket Worklog============================================
     *
     */
    var TroubleTicket_Worklog = function TroubleTicket_Worklog(worklog_table_dom) {
        this.table_dom = worklog_table_dom;
        this.table_column_map = null;
    };
    TroubleTicket_Worklog.prototype = {
        getTableColumnMap: function() {
            if (this.table_column_map === null) {
                var table_data_mapper = new DOMTableTools.TableDataMappers.FirstRowHasHeaders();
                var worklog_data = table_data_mapper.getData(this.table_dom);
                this.table_column_map = new DOMTableTools.RowArrayColumnMap(worklog_data, table_data_mapper.column_array);
            }
            return this.table_column_map;
        },
        getRowIterator: function() {
            var row_iterator = this.table_column_map.getIterator();
            return row_iterator;
        }
    };

    /**
     *
     *
     * ========================TroubleTicket Timeslot Search========================================
     *
     */
    var TroubleTicket_TimeslotSearch = function TroubleTicket_TimeslotSearch(worklog) {
        this.worklog = worklog;
    };

    TroubleTicket_TimeslotSearch.prototype = {
        findTimeSlot: function(date_obj) {
            var nozeros_date = GlobalCapacity.TaskDocuments.getDateNoZeros_FromDateObj(date_obj);

            var column_map = this.worklog.getTableColumnMap();
            var iterator = column_map.getIterator();

            var note_content;
            var timeslot = null;
            var row = iterator.next();
            while (row) {
                note_content = column_map.getRowVal(row, 'Entry');
                if (note_content.substring(0, 21) == "Created Dispatch Task") {
                    timeslot = this.findTimeInNotes_TT1(note_content, date_obj);
                }
                if (!timeslot && note_content.substring(0, 27) == "The Covad dispatch has been") {
                    timeslot = this.findTimeInNotes_TT1(note_content, date_obj);
                }
                if (!timeslot && note_content.substring(0, 11) == "ReScheduled") {
                    timeslot = this.findTimeInNotes_TT2(note_content, date_obj);
                }

                if (timeslot) break;

                row = iterator.next();
            }
            return timeslot;
        },
        findTimeInNotes_TT1 : function(note_content, date_obj) {
            var date_str = GlobalCapacity.TaskDocuments.getStdDateString_FromDateObj(date_obj);
            if (note_content.indexOf(date_str) > 0) {
                if (note_content.indexOf("End Time") < 0) {
                    //no end time
                    if (note_content.indexOf("09:00") > 0) {
                        return GlobalCapacity.TaskDocuments.getTimeslotForHour("9AM");
                    }
                    if (note_content.indexOf("13:00") > 0) {
                        return GlobalCapacity.TaskDocuments.getTimeslotForHour("1PM");
                    }
                } else {
                    if (note_content.indexOf("09:00") > 0) {
                        return GlobalCapacity.TaskDocuments.getTimeslotForHour("09");
                    }
                    if (note_content.indexOf("12:00") > 0) {
                        return GlobalCapacity.TaskDocuments.getTimeslotForHour("12");
                    }
                }
            }
            return null;
        },
        findTimeInNotes_TT2 : function(note_content, date_obj) {
            var date_str = GlobalCapacity.TaskDocuments.getStdDateString_FromDateObj(date_obj);
            //ReScheduled
            if (note_content.indexOf(date_str) > 0) {
                var note_parts = note_content.split(date_str);
                //time in second part:
                var time_parts = note_parts[1].split(":");
                var hour_str = time_parts[0].trim();
                return GlobalCapacity.TaskDocuments.getTimeslotForHour(hour_str);
            }
            return null;
        }
    };
    return TroubleTicket;
})();
