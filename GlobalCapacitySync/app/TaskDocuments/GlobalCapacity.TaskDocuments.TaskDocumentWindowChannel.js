if (typeof Channel == 'undefined') {
    throw 'jschannel is required.';
}


var GlobalCapacity = GlobalCapacity || {};
GlobalCapacity.TaskDocuments = GlobalCapacity.TaskDocuments || {};
/**
 *
 *
 */
GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel = (function() {

    /**
     *
     *
     * ============================JsChannel=====================================================
     */
    var JsChannel = function(window_elem, origin) {
        this.channel_name = 'taskdocument_parse';
        this.window_elem = window_elem;
        this.origin = '*';
        this.onReady = null;
        this.channel = null;
    };
    JsChannel.prototype = {
        getChannel: function() {
            if (this.channel === null) {
                this.channel = Channel.build({ //this is jschannel.js
                    debugOutput: false,
                    window: this.window_elem,
                    origin: this.origin,
                    scope: this.channel_name,
                    onReady: this.onReady
                });
            }
            return this.channel;
        }
    };
    /**
     *
     *
     * ============================ParentSide=====================================================
     */
    var ParentSide = function(child_window) {
        //
        //Setup channel
        this.channel = new JsChannel(child_window.getWindow());
        //
        //Setup listeners
        this.channel.getChannel().bind('Notification:TaskDocumentSide ready', this._onTaskDocumentSideReady.bind(this));
        //
        //Notify that we are ready
        this.channel.getChannel().notify({
            method: 'Notification:ParentSide ready'
        });

        this.emitter = GlobalCapacity.OSS.newEmitter();
    };
    ParentSide.prototype = {
        _onTaskDocumentSideReady: function(context, params) {
            console.log('ParentSide: TaskDocumentSide says its ready');
            console.log('ParentSide, context: ', context);
            console.log('ParentSide, params: ', params);
            this.emitter.emit('TaskDocumentSide:ready', context, params);
        },
        getTaskData: function(callback) {
            this.channel.getChannel().call({
                method: 'TaskDocumentSide:getTaskData',
                error: function(error_msg) {
                    console.log(error_msg);
                },
                success: callback
            });
        },
        closeWindow: function(callback) {
            this.channel.getChannel().call({
                method: 'TaskDocumentSide:closeWindow',
                error: function(error_msg) {
                    console.log(error_msg);
                },
                success: callback
            });
        }
    };
    /**
     *
     *
     * ============================TaskDocumentSide================================================
     */
    var TaskDocumentSide = function(parent_window, task_document) {
        var self = this;
        this.task_document = task_document;
        if (!parent_window instanceof ParentWindow) {
            throw "Needs to be instance of ParentWindow";
        }
        //
        //Setup channel
        this.channel = new JsChannel(parent_window.getWindow());
        //
        //Setup listeners
        this.channel.getChannel().bind('TaskDocumentSide:getTaskData', this.sendTaskData.bind(this));
        this.channel.getChannel().bind('TaskDocumentSide:closeWindow', this.closeWindow.bind(this));

        //
        //Notify that we are ready
        this.channel.getChannel().notify({
            method: 'Notification:TaskDocumentSide ready'
        });
    };
    TaskDocumentSide.prototype = {
        sendTaskData: function(trans) {
            var task_data = this.getTaskData();
            trans.complete(task_data);
        },
        //TODO set this up as if it was asynchronous
        getTaskData: function() {
            console.log('Task Document', this.task_document);
            console.log('Task ID', this.task_document.getTaskId());
            console.log('Order Status', this.task_document.getOrderStatus());

            //TODO: try to open task form panel. Need to access instance of TaskDocuments.Document that created the sync controls.
            //this.task_document.task_documents.loadControls().then(function(controls){
            //    controls.getSidePanel();
            //});

            return this.task_document.getTaskData();
        },
        closeWindow: function() {
            console.log("CLOSE WINDOW");
            window.close();
        }
    };
    return {
        ParentSide: ParentSide,
        TaskDocumentSide: TaskDocumentSide
    };


})();
