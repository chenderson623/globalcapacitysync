
//requires: DOMTableTools, DOMElements, TableDataFilters

//var GlobalCapacity = GlobalCapacity || {};
//GlobalCapacity.TaskDocuments = GlobalCapacity.TaskDocuments || {};

if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TaskDocuments == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments is required.';
}

GlobalCapacity.TaskDocuments.WorkOrder = (function() {
    var WorkOrder = function WorkOrder(task_document_elem, task_documents) {
        this.task_document_elem = task_document_elem;
        this.task_documents = task_documents;
        //Vars:
        this.task_type = "Work Order";
        this.task_data = {
            TaskId: null,
            TaskType: null,
            JobSubType: null,
            LocationName: null,
            LocationAddress: null,
            LocationCity: null,
            LocationState: null,
            LocationZip: null,
            LocationPhone: null,
            ContractRegionName: null,
            ContractSpecialInst: null,
            "jobs_data:affiliate": null,
            "jobs_data:isp": null,
            'jobs_data:service_type': null,
            'jobs_data:cpe_type': null,
            'jobs_data:circuit_number': null,
            'jobs_data:scp_num': null,
            'jobs_data:ilec_circuit_numbers': null,
            'jobs_data:circuit_qty': null
        };
        //Internal:
        this.document_table_set = null;
        this.client_order_info_data = null;
        this.client_circuit_info_data = null;
        this.dslam_info = null;
        this.additional_notes = null;
        this.work_log = null;
        this.timeslot_search = null;
    };
    WorkOrder.prototype = {
        getTableSet: function() {
            if(!this.document_table_set) {
                this.document_table_set = new WorkOrder_TableSet(this.task_document_elem);
            }
            return this.document_table_set;
        },

        _map_data: function(table_data, table_section_map, data_obj) {
            var section_map;
            var section_data;
            var column_label;
            for (var section in table_section_map) {
                section_map = table_section_map[section];
                section_data = table_data[section];
                if (section_data === undefined) {
                    continue;
                }
                for (var data_key in section_map) {
                    column_label = section_map[data_key];
                    data_obj[data_key] = section_data[column_label];
                }
            }
        },

        getDataTable: function(table_name) {
            switch (table_name) {
                case 'ClientOrderInfo':
                    return this.getTableSet().getClientOrderInfoTable();
                case 'ClientCircuitInfo':
                    return this.getTableSet().getClientCircuitInfoTable();
                case 'DSLAMInfo':
                    return this.getTableSet().getDSLAMInfoTable();
                case 'AdditionalNotes':
                    return this.getTableSet().getAdditionalNotesTable();
                case 'PrimaryCPE':
                    return this.getTableSet().getPrimaryCPETable();
                case 'WorkLog':
                    return this.getTableSet().getWorkLogTable();
                default:
                    throw ("Table [" + table_name + "] was not found");
            }
        },

        //=========Client Order Info============================================

        getClientOrderInfoData: function() {
            if (this.client_order_info_data === null) {
                var client_order_info_table = this.getDataTable("ClientOrderInfo");
                var table_data_mapper = new DOMTableTools.TableDataMappers.EachRowHasHeaderAndData_WithSections();
                this.client_order_info_data = table_data_mapper.getData(client_order_info_table);
            }
            return this.client_order_info_data;
        },

        fillData_ClientOrderInfo: function(data_obj) {
            var client_order_info_map = {
                'Client Order Info': {
                    TaskId: "Order Number:",
                    "jobs_data:work_order": "Order Number:",
                    "jobs_data:order_status": "Order Status:"
                },
                'Client Info': {
                    LocationName: "Company:",
                    LocationAddress: "Street:",
                    LocationCity: "City:",
                    LocationState: "State:",
                    LocationZip: "ZIP:",
                    "jobs_data:affiliate": "Affiliate Name:",
                    "jobs_data:isp": "Customer:",
                    LocationPhone: "Installation Phone:",
                }
            };

            this._map_data(this.getClientOrderInfoData(), client_order_info_map, data_obj);

            //edge cases:
            if (data_obj.LocationZip === undefined) {
                //directly accessing client_order_info_data - loaded using this.getClientOrderInfoData() above
                if (this.client_order_info_data['Client Info'] !== undefined && this.client_order_info_data['Client Info']['Zip:'] !== undefined) {
                    data_obj.LocationZip = this.client_order_info_data['Client Info']['Zip:'];
                }
            }
        },

        //=========Client Circuit Info============================================
        getPrimaryCPEData: function() {
            var table = this.getDataTable("PrimaryCPE");
            if (table) { //table may not be there
                var table_data_mapper = new DOMTableTools.TableDataMappers.EachRowHasHeaderAndData_WithSections();
                var data = table_data_mapper.getData(table);
                return data;
            }
            return {};
        },

        getClientCircuitInfoData: function() {
            if (this.client_circuit_info_data === null) {
                var client_circuit_info_table = this.getDataTable("ClientCircuitInfo");
                var table_data_mapper = new DOMTableTools.TableDataMappers.EachRowHasHeaderAndData_WithSections();
                this.client_circuit_info_data = table_data_mapper.getData(client_circuit_info_table);
            }
            return this.client_circuit_info_data;
        },

        fillData_ClientCircuitInfo: function(data_obj) {
            var client_circuit_info_map = {
                'Client Circuit Info': {
                    ContractRegionName: 'Region:',
                    'jobs_data:service_type': "Current Service:",
                    'jobs_data:cpe_type': "CPE Type:",
                    'jobs_data:circuit_number': "Client Circuit Number:"
                }
            };

            this._map_data(this.getClientCircuitInfoData(), client_circuit_info_map, data_obj);

            //edge cases:
            if (data_obj['jobs_data:cpe_type'] === undefined) {
                //may have multiple CPE
                var cpe_data = this.getPrimaryCPEData();
                if (cpe_data['Primary CPE'] !== undefined && cpe_data['Primary CPE']['CPE Type:'] !== undefined) {
                    data_obj['jobs_data:cpe_type'] = cpe_data['Primary CPE']['CPE Type:'];
                }
            }
        },

        //=================DSLAM Info====================================================
        getDSLAMInfo: function() {
            if (this.dslam_info === null) {
                var dslam_info_table = this.getDataTable('DSLAMInfo');
                this.dslam_info = new WorkOrder_DSLAMInfo(dslam_info_table);
            }
            return this.dslam_info;
        },

        //=================Additional Notes===============================================
        getAdditionalNotes: function() {
            if (this.additional_notes === null) {
                var table = this.getDataTable('AdditionalNotes');
                var cell = table.getCell(0, 1);
                this.additional_notes = DOMElements.Filters.getElementCleanText(cell);
            }
            return this.additional_notes;
        },

        //=================Service Type===============================================
        parseServiceLevel: function(service_type) {
            if (!service_type) {
                return null;
            }
            var check_service_type = service_type.toLowerCase();
            var consumer_tests = ['lpva', 'telesurfer', 'residential'];
            for (var i = 0; i < consumer_tests.length; i++) {
                if (check_service_type.indexOf(consumer_tests[i]) >= 0) {
                    return 'Consumer';
                }
            }
            return 'Business';
        },

        getServiceLevel: function() {
            var data = this.getClientCircuitInfoData();
            var service_type = data['Client Circuit Info']['Current Service:'];
            return this.parseServiceLevel(service_type);
        },

        //=================SCP===============================================

        getScpNumArray: function() {
            var additional_notes = this.getAdditionalNotes();
            var result = [];
            var scp_patt = /SCP.*?\d{2,}/ig;
            var scp_match = additional_notes.match(scp_patt);

            if (scp_match === null) {
                return result;
            }

            var num_patt = /\d{2,}/ig;
            var scp_str, num_result;
            for (var i = 0; i < scp_match.length; i++) {
                scp_str = scp_match[i];
                num_result = scp_str.match(num_patt);
                if (num_result.length !== undefined) {
                    result[i] = num_result[0];
                }
            }
            return result;
        },

        getScpNumStr: function() {
            return this.getScpNumArray().toString();
        },

        //=================Main functions===================================
        getWorkLog: function() {
            if (this.work_log === null) {
                var work_log_table = this.getDataTable("WorkLog");
                this.work_log = new WorkOrder_Worklog(work_log_table);
            }
            return this.work_log;
        },
        getTimeslotSearch: function() {
            if(this.timeslot_search === null) {
                this.timeslot_search = new WorkOrder_TimeslotSearch(this.getWorkLog());
            }
            return this.timeslot_search;
        },
        getTaskId: function() {
            var data = this.getClientOrderInfoData();
            return data['Client Order Info']['Order Number:'];
        },

        getTaskType: function() {
            return this.task_type;
        },

        getOrderStatus: function() {
            var data = this.getClientOrderInfoData();
            return data['Client Order Info']['Order Status:'];
        },

        getCurrentScheduleDate: function() {
            var data = this.getClientOrderInfoData();
            return data['Client Order Info']['Schedule Date:'];
        },

        getCurrentScheduleDateObj: function() {
            var date_str = this.getCurrentScheduleDate();
            //problem with this date string: has AM/PM, but time is in 24 hr format
            //remove the AM/PM
            date_str = date_str.replace(' AM', '');
            date_str = date_str.replace(' PM', '');
            var date_obj = new Date(date_str);
            return date_obj;
        },

        getTaskData: function() {
            this.task_data.TaskType = this.getTaskType();
            this.fillData_ClientOrderInfo(this.task_data);
            this.fillData_ClientCircuitInfo(this.task_data);

            var dslam = this.getDSLAMInfo();
            this.task_data['jobs_data:ilec_circuit_numbers'] = dslam.getIlecCircuitNumbersString();
            this.task_data['jobs_data:circuit_qty'] = dslam.countIlecCircuitNumbers();

            this.task_data.ContractSpecialInst = this.getAdditionalNotes();

            this.task_data.JobSubType = this.getServiceLevel();

            this.task_data['jobs_data:scp_num'] = this.getScpNumStr();

            return this.task_data;
        }
    };

    /**
     *
     *
     * ========================WorkOrder TableSet============================================
     *
     */
    var WorkOrder_TableSet = function(parent_container) {
        this.table_set = new DOMTableTools.TableSet(parent_container);
        this.tables = {}; //client_order_info, client_circuit_info, dslam_info, worklog, additional_notes
    };
    WorkOrder_TableSet.prototype = {
        getCachedTableByFirstCell: function(table_name, cell_content) {
            if (this.tables[table_name] === undefined) {
                this.tables[table_name] = this.findTableByFirstCell2(cell_content);
            }
            return this.tables[table_name];
        },

        findTableByFirstCell: function(cell_content) {
            var search_cell_content = cell_content;
            var cell_compare = function(cell_elem) {
                return DOMElements.Filters.getElementCleanText(cell_elem) === search_cell_content;
            };
            return this.table_set.findTableByCell(0, 0, cell_compare);
        },

        //Alt:
        cleanCellFilter: function(search_cell_content, cell_elem) {
            return DOMElements.Filters.getElementCleanText(cell_elem) === search_cell_content;
        },

        findTableByFirstCell2: function(cell_content) {
            return this.table_set.findTableByCell(0, 0, this.cleanCellFilter.bind(this, cell_content));
        },
        //END Alt

        getClientOrderInfoTable: function() {
            return this.getCachedTableByFirstCell('client_order_info', "Client Order Info");
        },

        getClientCircuitInfoTable: function() {
            return this.getCachedTableByFirstCell('client_circuit_info', "Client Circuit Info");
        },

        getDSLAMInfoTable: function() {
            return this.getCachedTableByFirstCell('dslam_info', "DSLAM Name");
        },

        getAdditionalNotesTable: function() {
            return this.getCachedTableByFirstCell('additional_notes', "Additional Notes:");
        },

        getPrimaryCPETable: function() {
            return this.getCachedTableByFirstCell('primary_cpe', "Primary CPE");
        },

        getWorkLogTable: function() {
            if (this.tables.worklog === undefined) {
                var cell_compare = function(cell_elem) {
                    return DOMElements.Filters.getElementCleanText(cell_elem).substr(0, 25) === "WORK LOG for Client Order";
                };
                this.tables.worklog = this.table_set.findTableByCell(0, 0, cell_compare);
            }
            return this.tables.worklog;
        },
    };

    /**
     *
     *
     * ========================WorkOrder DSLAM Info============================================
     *
     */
    var WorkOrder_DSLAMInfo = function WorkOrder_DSLAMInfo(dslam_info_table_dom) {
        this.table_dom = dslam_info_table_dom;

        this.ilec_circuit_numbers = null;
        this.dmarc_notes = null;
    };

    WorkOrder_DSLAMInfo.prototype = {
        gatherTableData: function() {
            this.ilec_circuit_numbers = [];
            this.dmarc_notes = [];

            //skip 1st row
            var ilec_circuit_number;
            var dmarc_note;
            for (var i = 1, len = this.table_dom.countRows(); i < len; i++) {
                ilec_circuit_number = this.table_dom.filterCell(i, 4, DOMElements.Filters.getElementCleanText); //column index = 4
                dmarc_note = this.table_dom.filterCell(i, 5, DOMElements.Filters.getElementCleanText); //column index = 5
                if (ilec_circuit_number) {
                    //check that circuit_number is unique
                    found = false;
                    for (var j = 0; j < this.ilec_circuit_numbers.length; j++) {
                        if (this.ilec_circuit_numbers[j] == ilec_circuit_number) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        this.ilec_circuit_numbers[this.ilec_circuit_numbers.length] = ilec_circuit_number;
                        this.dmarc_notes[this.dmarc_notes.length] = dmarc_note;
                    }
                }
            }
        },
        getIlecCircuitNumbers: function() {
            if (this.ilec_circuit_numbers === null) {
                this.gatherTableData();
            }
            return this.ilec_circuit_numbers;
        },
        getIlecCircuitNumbersString: function() {
            return this.getIlecCircuitNumbers().toString();
        },
        countIlecCircuitNumbers: function() {
            var circuit_numbers = this.getIlecCircuitNumbers();
            if (circuit_numbers.length === undefined) {
                return 0;
            }
            return circuit_numbers.length;
        },
        getDmarcNotes: function() {
            if (this.dmarc_notes === null) {
                this.gatherTableData();
            }
            return this.dmarc_notes;
        }
    };

    /**
     *
     *
     * ========================WorkOrder Worklog============================================
     *
     */
    var WorkOrder_Worklog = function WorkOrder_Worklog(worklog_table_dom) {
        this.table_dom = worklog_table_dom;
        this.table_column_map = null;
    };

    WorkOrder_Worklog.prototype = {
        getTableColumnMap: function() {
            if (this.table_column_map === null) {
                var table_data_mapper = new DOMTableTools.TableDataMappers.FirstRowHasHeaders();
                var worklog_data = table_data_mapper.getData(this.table_dom);
                this.table_column_map = new DOMTableTools.RowArrayColumnMap(worklog_data, table_data_mapper.column_array);
            }
            return this.table_column_map;
        },
        getRowIterator: function() {
            var row_iterator = this.table_column_map.getIterator();
            return row_iterator;
        }
    };

    /**
     *
     *
     * ========================WorkOrder Timeslot Search========================================
     *
     */
    var WorkOrder_TimeslotSearch = function WorkOrder_TimeslotSearch(worklog) {
        this.worklog = worklog;
    };

    WorkOrder_TimeslotSearch.prototype = {
        findTimeSlot: function(date_obj) {
            var nozeros_date = GlobalCapacity.TaskDocuments.getDateNoZeros_FromDateObj(date_obj);

            var column_map = this.worklog.getTableColumnMap();
            var iterator = column_map.getIterator();

            var submitter, entry;
            var row = iterator.next();
            while (row) {
                submitter = column_map.getRowVal(row, 'Submitter');
                entry = column_map.getRowVal(row, 'Event Details');

                if (submitter === "BATCHTRIGGER") {
                    if (entry.indexOf(nozeros_date) > 0) {
                        if (entry.indexOf("between 9:00") > 0) {
                            return GlobalCapacity.TaskDocuments.getTimeslotForHour("09");
                        }
                        if (entry.indexOf("between 12:00") > 0) {
                            return GlobalCapacity.TaskDocuments.getTimeslotForHour("12");
                        }
                        if (entry.indexOf("between 16:00") > 0) {
                            return GlobalCapacity.TaskDocuments.getTimeslotForHour("16");
                        }
                    }
                }

                if (submitter === "BOSERVER") {
                    if (entry.indexOf(nozeros_date) > 0) {
                        if (entry.indexOf("9:00") > 0) {
                            return GlobalCapacity.TaskDocuments.getTimeslotForHour("09");
                        }
                        if (entry.indexOf("12:00") > 0) {
                            return GlobalCapacity.TaskDocuments.getTimeslotForHour("12");
                        }
                        if (entry.indexOf("16:00") > 0) {
                            return GlobalCapacity.TaskDocuments.getTimeslotForHour("12");
                        }
                    }
                }

                if (submitter === "system") {
                    if(entry.indexOf('mpoe_repair_scheduled') > 0) {
                        if (entry.indexOf(nozeros_date) > 0) {
                            if (entry.indexOf("between 9:00") > 0) {
                                return GlobalCapacity.TaskDocuments.getTimeslotForHour("9AM");
                            }
                            if (entry.indexOf("between 12:00") > 0) {
                                return GlobalCapacity.TaskDocuments.getTimeslotForHour("1PM");
                            }
                        }
                    }
                }

                if(entry.indexOf('***** Dispatch Escalations *****') > 0) {
                    var disp_date = GlobalCapacity.TaskDocuments.getMonthDay(date_obj);
                    if (entry.indexOf(disp_date) > 0) {
                            if (entry.indexOf("between 9:00") > 0) {
                                return GlobalCapacity.TaskDocuments.getTimeslotForHour("9AM");
                            }
                            if (entry.indexOf("between 12:00") > 0) {
                                return GlobalCapacity.TaskDocuments.getTimeslotForHour("1PM");
                            }
                    }
                }

                row = iterator.next();
            }

        }
    };

    return WorkOrder;

})();
