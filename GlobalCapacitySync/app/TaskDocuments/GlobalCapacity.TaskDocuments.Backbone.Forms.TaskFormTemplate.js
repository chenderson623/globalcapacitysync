/**
 * Include this template file after backbone-forms.amd.js to override the default templates
 *
 * 'data-*' attributes control where elements are placed
 */
;(function(Form) {


  /**
   * Bootstrap 3 templates
   */
  Form.template = _.template('\
    <form class="form-inline" role="form"> \
      <div data-fieldsets></div>\
      <% if (submitButton) { %>\
        <button type="submit" class="btn btn-success"><%= submitButton %></button>\
      <% } %>\
    </form>\
  ');


  Form.Fieldset.template = _.template('\
    <fieldset data-fields>\
      <% if (legend) { %>\
        <legend><%= legend %></legend>\
      <% } %>\
    </fieldset>\
  ');


  Form.Field.template = _.template('\
    <div class="form-group field-<%= key %>">\
      <label class="control-label" for="<%= editorId %>"><%= title %></label>\
      <div class="form-input">\
        <span data-editor></span>\
        <p class="help-block" data-error></p>\
        <p class="help-block"><%= help %></p>\
      </div>\
    </div>\
  ');


  Form.NestedField.template = _.template('\
    <div class="field-<%= key %>">\
      <div title="<%= title %>" class="input-xlarge">\
        <span data-editor></span>\
        <div class="help-inline" data-error></div>\
      </div>\
      <div class="help-block"><%= help %></div>\
    </div>\
  ');

  Form.editors.Base.prototype.className = 'form-control';
  Form.Field.errorClassName = 'has-error';


  if (Form.editors.List) {

    Form.editors.List.template = _.template('\
      <div class="bbf-list">\
        <ul class="unstyled clearfix" data-items></ul>\
        <button type="button" class="btn bbf-add" data-action="add">Add</button>\
      </div>\
    ');


    Form.editors.List.Item.template = _.template('\
      <li class="clearfix">\
        <div class="pull-left" data-editor></div>\
        <button type="button" class="btn bbf-del" data-action="remove">&times;</button>\
      </li>\
    ');


    Form.editors.List.Object.template = Form.editors.List.NestedModel.template = _.template('\
      <div class="bbf-list-modal"><%= summary %></div>\
    ');

  }

  Form.editors.Select2 = Form.editors.Select.extend({

    render: function() {
      this.setOptions(this.schema.options);
      var multiple = this.schema.multiple;
      var config = this.schema.config || {};

      var elem = this;
      setTimeout(function() {
        if (multiple) {
          elem.$el.prop('multiple', true);
        }

        elem.$el.select2(config);
      }, 0);

      return this;
    },

  });
/*
  Form.editors.BootstrapDatePicker = Form.editors.Text.extend({

    tagName: 'div',

    initialize: function(options) {
      Form.editors.Base.prototype.initialize.call(this, options);

      this.$el.addClass('input-group date');
      this.$el.attr('data-date-format', "mm/dd/yyyy");

      this.$input = $('<input class="form-control" readonly="" type="text">');
      this.$el.append(this.$input);
      this.$el.append('<span class="input-group-addon"><i class="splashy-calendar_day"></i></span>');

    },

    render: function() {
      this.setValue(this.value);

      var config = this.schema.config || {};

      var elem = this;
      setTimeout(function() {
        elem.$el.datepicker(config);
      }, 0);

      return this;
    },

    getValue: function() {
      return this.$input.val();
    },

    setValue: function(value) {
      this.$input.val(value);
    },

  });
*/

function pad(number) {
  var r = String(number);
  if ( r.length === 1 ) {
    r = '0' + r;
  }
  return r;
}

Form.editors.BootstrapDatePicker = Form.editors.Text.extend({

  previousValue: '',
  datepicker: null,

  events: {
    'hide': "hasChanged"
  },

  hasChanged: function(currentValue) {
    if (currentValue !== this.previousValue){
      this.previousValue = currentValue;
      this.trigger('change', this);
    }
  },

  initialize: function(options) {
    Form.editors.Base.prototype.initialize.call(this, options);
    this.template = options.template || this.constructor.template;
  },

  render: function(){
    var $el = $($.trim(this.template({
      dateFormat: 'yyyy-mm-dd',
      value: this.value
    })));
    this.datepicker = $el.datepicker({autoclose: true});
    this.setElement($el);

    /* unneeded:
    $el.on('changeDate', function(ev){
      var datestr = ev.date.getFullYear() + '-' + pad( ev.date.getMonth() + 1 ) + '-' + pad( ev.date.getDate() );
      $el.find('input').val(datestr);
      console.log($el, $el.find('input'));
    });
    */

    return this;
  },
  getValue: function() {
    return this.$el.find('input').val();
  },
  setValue: function(value) {
    this.datepicker.setValue(value);
  },

}, {
  // STATICS
  template: _.template('<div class="input-group date" data-date-format="<%= dateFormat %>">\
  <input class="form-control" readonly="" type="text" name="AssignedDate" value="<%= value %>">\
  <span class="input-group-addon"><i class="splashy-calendar_day"></i></span></div>', null, Form.templateSettings)
});

})(Backbone.Form);
