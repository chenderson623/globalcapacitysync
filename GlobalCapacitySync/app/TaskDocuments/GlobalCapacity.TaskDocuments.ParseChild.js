'use strict';
if (typeof ParentWindow == 'undefined') {
    throw 'ParentWindow is required.';
}

if (typeof GlobalCapacity.TaskDocuments == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments is required.';
}

if (typeof GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel is required.';
}

/**
 *
 *
 */
GlobalCapacity.TaskDocuments.ParseChild = (function() {

    var TaskDocumentParserChild = function(document_elem) {
        this.parent_window = new ParentWindow();
        this.taskdocument_window_channel = null;
        this.task_documents = null;
        this.task_document = null;
    };
    TaskDocumentParserChild.prototype = {
        getTaskDocuments: function() {
            if (!this.task_documents) {
                this.task_documents = GlobalCapacity.TaskDocuments;
            }
            return this.task_documents;
        },
        getTaskDocument: function(callback) {
            var self = this;
            if (!self.task_document) {
                this.getTaskDocuments().loadTaskDocument(document, function(task_document_obj) {
                    self.task_document = task_document_obj;
                    callback(self.task_document);
                });
                return;
            }
            callback(self.task_document);
        },
        getTaskDocumentWindowChannel: function(callback) {
            console.log("ParseChild:getTaskDocumentWindowChannel");
            var self = this;
            if (!self.taskdocument_window_channel) {
                this.getTaskDocument(function(task_document) {
                    self.taskdocument_window_channel = new GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel.TaskDocumentSide(self.parent_window, task_document);
                    callback(self.taskdocument_window_channel);
                });
            }
            callback(self.taskdocument_window_channel);
        }
    };

    return TaskDocumentParserChild;
})();