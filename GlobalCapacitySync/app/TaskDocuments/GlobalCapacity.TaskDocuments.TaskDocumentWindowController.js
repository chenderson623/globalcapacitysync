'use strict';
if (typeof GlobalCapacity.TaskDocuments == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments is required.';
}

if (typeof GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel is required.';
}


var GlobalCapacity = GlobalCapacity || {};
GlobalCapacity.TaskDocuments = GlobalCapacity.TaskDocuments || {};
/**
 *
 * This runs in the parent window to control a task document in a child window
 */
GlobalCapacity.TaskDocuments.TaskDocumentWindowController = (function() {

    /**
     * Opens and controls a task document window (or iframe)
     * @param child_window_opener optional for custom window or iframe implementations
     */
    var TaskDocumentWindowController = function(window_opener) {
        this.debug = true;
        this.window_opener = window_opener;
        this.window_opener_defaults = {
            height: 800,
            width: 800,
            name: 'task_document_window'
        };
        this.emitter = GlobalCapacity.OSS.newEmitter();
        this.taskdocument_window_channel = null;
        this.taskdocument_window_channel_promise = null;
        this.subscriptions = [];
    };
    TaskDocumentWindowController.prototype = {
        getWindowOpener: function() {
            if (!this.window_opener) {
                this.window_opener = this._defaultWindowOpener();
            }
            return this.window_opener;
        },
        getTaskDocumentWindowChannel: function() {
            if(!this.taskdocument_window_channel_promise) {
                this.taskdocument_window_channel_promise = new Promise(function(resolve){
                    this.taskdocument_window_channel = new GlobalCapacity.TaskDocuments.TaskDocumentWindowChannel.ParentSide(this.getWindowOpener());
                    this.taskdocument_window_channel.emitter.onAny(function(){console.log("this.taskdocument_window_channel", this, arguments);});
                    if(this.debug) {
                        console.log('TaskDocumentWindowController.getTaskDocumentWindowChannel - taskdocument_window_channel created', this.taskdocument_window_channel);
                    }

                    this.taskdocument_window_channel.emitter.on('TaskDocumentSide:ready', function(){
                        this.emitter.emit('WindowChannel Ready', this.taskdocument_window_channel);
                        resolve(this.taskdocument_window_channel);
                    }.bind(this));
                }.bind(this));
            }
            return this.taskdocument_window_channel_promise;
        },
        openTaskDocumentWindow: function(url) {
            if(this.debug) {
                console.log('TaskDocumentWindowController.openTaskDocumentWindow',url);
            }
            this.emitter.emit('Opening ' + url);
            //TODO: add some kind of timeout/wait here - possible pop-ups are blocked
            this.getWindowOpener().open(url, this._afterOpen.bind(this));
        },
        getTaskData: function() {
            if(this.debug) {
                console.log('TaskDocumentWindowController.getTaskData - create promise');
            }
            var promise = new Promise(function(resolve){
                if(this.debug) {
                    console.log('TaskDocumentWindowController.getTaskData - call getTaskDocumentWindowChannel()');
                }
                this.getTaskDocumentWindowChannel().then(function(taskdocument_window_channel){
                    if(this.debug) {
                        console.log('TaskDocumentWindowController.getTaskData',taskdocument_window_channel);
                    }
                    this.emitter.emit('Requesting Task Data');
                    taskdocument_window_channel.getTaskData(this._afterGetTaskData.bind(this));
                }.bind(this));
            }.bind(this));
            return promise;
        },
        closeWindow: function() {
            if(this.debug) {
                console.log('TaskDocumentWindowController.closeWindow');
            }
            this.getWindowOpener().close();
            this.emitter.emit('Window Closed');
        },
        _afterGetTaskData: function(task_data) {
            if(this.debug) {
                console.log('TaskDocumentWindowController._afterGetTaskData', task_data);
            }
            this.emitter.emit('Received Task Data', task_data);
        },
        _afterOpen: function() {
            if(this.debug) {
                console.log('TaskDocumentWindowController._afterOpen');
            }
            this.emitter.emit('Window Opened');
        },
        _defaultWindowOpener: function() {
            var window_opener = new ChildWindowOpener({
                height: this.window_opener_defaults.height,
                width: this.window_opener_defaults.width,
                centerBrowser: 1
            }, this.window_opener_defaults.name);
            if(this.debug) {
                console.log('TaskDocumentWindowController._defaultWindowOpener', window_opener);
            }
            return window_opener;
        }
    };

    return TaskDocumentWindowController;
})();
