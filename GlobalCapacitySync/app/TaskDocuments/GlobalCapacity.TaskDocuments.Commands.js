'use strict';
if (typeof GlobalCapacity.TaskDocuments == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments is required.';
}


GlobalCapacity.TaskDocuments.Commands = GlobalCapacity.TaskDocuments.Commands || {};

/**
 *
 *
 */
GlobalCapacity.TaskDocuments.Commands.OpenChildWindow = (function() {

    var OpenChildWindow = function(url) {
        this.url = url;
        this.emitter = GlobalCapacity.OSS.newEmitter();
        this.window_controller = null;

        this.promise = null;
        this.resolve = null;
        this.reject = null;

        this.getPromise();

    };
    OpenChildWindow.prototype = {
        _loadTaskDocumentWindowController: function(callback) {
            var self = this;
            GlobalCapacity.TaskDocuments.loadTaskDocumentWindowController(function(TaskDocumentWindowController) {
                self.window_controller = new TaskDocumentWindowController();
                callback(self.window_controller);
            });
        },
        getPromise: function() {
            if(!this.promise) {
                var self = this;
                this.promise = new Promise(function(resolve, reject){
                    self.resolve = resolve;
                    self.reject = reject;
                });
            }
            return this.promise;
        },
        execute: function() {
            this._loadTaskDocumentWindowController(this.open_window.bind(this));
            return this.promise;
        },
        open_window: function(window_controller) {
            this.emitter.emit('open_window', this.url);
            //Want to resolve promise when window is OPEN and loaded - Note: can't do this if different domain
            var self = this;
            window_controller.emitter.on('Window Opened', function(){
                if(message.action === 'window:opened') {
                    self.resolve(window_controller);
                }
            });
            window_controller.openTaskDocumentWindow(this.url);
        }
    };

    return OpenChildWindow;
})();


/**
 *
 * Opens child window and returns a Window Channel
 */
GlobalCapacity.TaskDocuments.Commands.OpenTaskDocumentWindowController = (function() {

    var OpenTaskDocumentWindowController = function(task_model) {
        this.task_model = task_model;
        this.emitter = GlobalCapacity.OSS.newEmitter();
        //loaded below:
        this.window_controller_class = null;
        this.window_controller = null;
        this.window_channel = null;
    };
    OpenTaskDocumentWindowController.prototype = {
        execute: function() {
            GlobalCapacity.TaskDocuments.loadTaskDocumentWindowController(function(window_controller_class) {
                this.window_controller_class = window_controller_class;
                this.emitter.emit('Window Controller Loaded');
                this._open_window();
            }.bind(this));
        },
        _open_window: function() {
            this.window_controller = new this.window_controller_class();

            this.emitter.emit('Open Window', this.task_model);

            //var task_url = 'http://testtimematrix.local.dev/TT4166632.htm';
            var task_url = this.task_model.task_url;
            this.window_controller.openTaskDocumentWindow(task_url);

            this._getTaskDocumentWindowChannel();
        },
        _emitChannel: function(window_channel) {
            this.window_channel = window_channel;
            this.emitter.emit('Window Channel', window_channel);
        },
        _getTaskDocumentWindowChannel: function() {
            this.window_controller.getTaskDocumentWindowChannel().then(this._emitChannel.bind(this));
        },
        close_window: function() {
            this.window_controller.closeWindow();
            this.emitter.emit('Close Window');
        }
    };

    return OpenTaskDocumentWindowController;
})();


/**
 *
 *
 */
GlobalCapacity.TaskDocuments.Commands.PromiseCommand = (function() {
    var PromiseCommand = function() {
        this.promise = null;
        this.resolve = null;
        this.reject = null;

        this.getPromise();
    };
    PromiseCommand.prototype = {
        getPromise: function() {
            if(!this.promise) {
                var self = this;
                this.promise = new Promise(function(resolve, reject){
                    self.resolve = resolve;
                    self.reject = reject;
                });
            }
            return this.promise;
        },
        execute: function() {
            var self = this;
            do_something_async(function(result){
                self.resolve(result);
            });
            return this.promise;
        }
    };
    return PromiseCommand;
})();


/**
 *
 *
 */
GlobalCapacity.TaskDocuments.Commands.GetTaskData = (function() {
    var PromiseCommand = function(window_channel) {
        this.window_channel = window_channel;

        this.promise = null;
        this.resolve = null;
        this.reject = null;

        this.getPromise();
    };
    PromiseCommand.prototype = {
        getPromise: function() {
            if(!this.promise) {
                var self = this;
                this.promise = new Promise(function(resolve, reject){
                    self.resolve = resolve;
                    self.reject = reject;
                });
            }
            return this.promise;
        },
        _resolveTaskData: function(task_data) {
            this.resolve(task_data);
        },
        execute: function() {
            this.window_channel.getTaskData(this._resolveTaskData.bind(this));
            return this.promise;
        }
    };
    return PromiseCommand;
})();


/**
 *
 *
 */
GlobalCapacity.TaskDocuments.Commands.SaveNewTask = (function() {
    var SaveNewTask = function(task_data) {
        this.task_data = task_data;
        this.emitter = GlobalCapacity.OSS.newEmitter();
    };
    SaveNewTask.prototype = {
        execute: function() {
            this.save_data();
        },
        save_data: function() {
            console.log("SAVING", this.task_data);
            this.emitter.emit('saved');
        },
        post: function() {
            qwest.post('example.com', {
                    firstname: 'Pedro',
                    lastname: 'Sanchez',
                    age: 30
                })
                .then(function(response) {
                    // Make some useful actions
                })
                .catch(function(message) {
                    // Print the error message
                });
        }
    };

    return SaveNewTask;
})();
