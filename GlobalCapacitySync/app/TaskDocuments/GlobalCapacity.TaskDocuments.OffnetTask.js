if (typeof DOMElements == 'undefined') {
    throw 'DOMElements is required.';
}
var GlobalCapacity = GlobalCapacity || {};
GlobalCapacity.TaskDocuments = GlobalCapacity.TaskDocuments || {};
/**
 *
 * There are two ways to parse offnet task data - from javascript variable and from static html
 */
GlobalCapacity.TaskDocuments.OffnetTask = (function() {
    var OffnetTask = function OffnetTask(task_document_elem, task_documents) {
        this.task_document_elem = task_document_elem;
        this.task_documents = task_documents;
        //Vars:
        this.task_type = "Advanced Services";
        this.task_data = {
            TaskId: null,
            TaskType: null,
            JobSubType: null,
            LocationName: null,
            LocationAddress: null,
            LocationCity: null,
            LocationState: null,
            LocationZip: null,
            LocationPhone: null,
            LocationSecPhone: null,
            LocationContact: null,
            ContractRegionName: null,
            ContractSpecialInst: null,
            "jobs_data:work_order": null,
            "jobs_data:affiliate": null,
            'jobs_data:service_type': null,
            'jobs_data:circuit_number': null,
            'jobs_data:scp_num': null,
        };
        //Internal:
        this.h4_map = null;
        this.h5_map = null;
        this.dispatch_data = null;
        this.milestones_table = null;
        this.work_log = null;
        this.timeslot_search = null;
    };
    OffnetTask.prototype = {
        getH4Map: function() {
            this.h4_map = new DOMElements.ChildElementMap(this.task_document_elem, 'H4');
            return this.h4_map;
        },
        getH5Map: function() {
            this.h5_map = new DOMElements.ChildElementMap(this.task_document_elem, 'H5');
            return this.h5_map;
        },

        //=================Data From Variables=====================================
        getServiceTypeFromOinfo: function(service) {
            if(typeof service !== 'object') {
                return '';
            }
            //return the first value of the first key/value pair
            for (var k in service){
                return service[k];
            }
        },
        getSCPFromOinfo: function(scp_obj) {
            if(typeof scp_obj !== 'object') {
                return '';
            }
            //return the first key of the first key/value pair
            for (var k in scp_obj){
                return k;
            }
        },
        fillFromOinfo: function() {
            if (typeof oinfo !== 'object') {
                throw 'oinfo is not an object';
            }
            this.task_data.ContractSpecialInst = oinfo.notes;
            this.task_data['jobs_data:service_type'] = this.getServiceTypeFromOinfo(oinfo.service);
            this.task_data['jobs_data:scp_num'] = this.getSCPFromOinfo(oinfo.sowid);

            if (this.task_data['jobs_data:service_type'] == 'Advanced Services Repair') {
                this.task_data.JobSubType = "Repair Only";
            }

        },
        getAffiliateFromCinfo: function(partner_obj) {
            if(typeof partner_obj !== 'object') {
                return '';
            }
            //return the first value of the first key/value pair
            for (var k in partner_obj){
                return partner_obj[k];
            }
        },
        fillFromCinfo: function() {
            if (typeof cinfo !== 'object') {
                throw 'cinfo is not an object';
            }
            this.task_data.LocationName = cinfo.clientfirst + " " + cinfo.clientlast;
            this.task_data.LocationPhone = cinfo.sitephone;
            this.task_data.LocationSecPhone = cinfo.contactphone;
            this.task_data.LocationContact = cinfo.companyname;
            this.task_data.LocationAddress = cinfo.sitestreet + " " + cinfo.siteunit;
            this.task_data.LocationCity = cinfo.sitecity;
            this.task_data.LocationState = cinfo.sitestate;
            this.task_data.LocationZip = cinfo.sitezip;
            this.task_data.ContractRegionName = cinfo.region;
            this.task_data['jobs_data:circuit_number'] = cinfo.ccn;
            this.task_data["jobs_data:affiliate"] = this.getAffiliateFromCinfo(cinfo.partner);
            this.task_data["jobs_data:work_order"] = cinfo.wo;
            if(cinfo.wo) {
                this.task_data.JobSubType = 'Broadband & Offnet';
            }

        },

        //=================Data From HTML=====================================
        _merge_properties: function(main_obj, merge_obj) { //just a simple function that will work for our needs
            for(var k in merge_obj) {
                if(merge_obj[k] || main_obj[k] === undefined) {
                    main_obj[k] = merge_obj[k];
                }
            }
            return main_obj;
        },
        parseCityStateZip: function(text_value) {
            var parsed_data = {};
            //split by space, last part will be zip code
            var parts = text_value.split(' ');
            parsed_data.LocationZip = parts.pop().trim();
            //rejoin
            text_value = parts.join(' ');
            //split by comma. last part will be state
            parts = text_value.split(',');
            parsed_data.LocationState = parts.pop().trim();
            //rejoin whatever is left
            parsed_data.LocationCity = parts.join(',').trim();
            return parsed_data;
        },
        parseSiteInfoCell: function(site_info_cell) {
            //parse the site info cell by child nodes
            var children = site_info_cell.childNodes;
            var text_node_index = -1; //some data is in text nodes. identify some data by the text node index
            var current_i_key = null; //some data follows a <i> label. next text node will be the data
            var data = {
                LocationName: null,
                LocationAddress: null,
                LocationCity: null,
                LocationState: null,
                LocationZip: null,
                LocationPhone: null,
                LocationSecPhone: null,
                LocationContact: null,
                LocationEmail: null,
                'jobs_data:circuit_number': null,
            };
            var i_value, text_value;
            //loop all the child nodes
            for (var i = 0, len=children.length; i < len; i++) {
                if (children[i].nodeName == "BR") { //if its a BR, skip it
                    continue;
                }

                if (children[i].nodeName == "I") { //if its an <i>, save the label fot the next text element
                    i_value = children[i].childNodes[0].nodeValue;
                    switch (i_value.trim()) {
                        case 'Site:':
                            current_i_key = 'LocationPhone';
                            break;
                        case 'Alternate:':
                        case 'Primary:':
                            current_i_key = 'LocationSecPhone';
                            break;
                        case 'Email:':
                            current_i_key = 'LocationEmail';
                            break;
                        case 'CCN:':
                            current_i_key = 'jobs_data:circuit_number';
                            break;
                    }
                    continue; //go ahead an loop to the next
                }

                text_value = '';
                if (children[i].nodeName == "#text" ) { //this will be a value we want
                    text_value = children[i].nodeValue.trim();
                    text_node_index++;
                } else if(children[i].nodeName == "A") {
                    text_value = children[i].textContent.trim();
                } else { //skip to the next
                    continue;
                }

                if(text_value.trim() === '') { //this may cause problems if i_key value is blank. seems like if no value, i_key is not printed right now
                    continue;
                }

                switch (text_node_index) {
                    case 0:
                        //contact name may be concatenated with /
                        var loc_parts = text_value.split('/');
                        var contact_name = '';
                        if(loc_parts.length > 1) {
                            contact_name = loc_parts.pop();
                        }
                        data.LocationContact = contact_name.trim();
                        data.LocationName = loc_parts.join('/').trim();
                        break;
                    case 1:
                        data.LocationAddress = text_value;
                        break;
                    case 2:
                        this._merge_properties(data, this.parseCityStateZip(text_value));
                        break;
                }
                if(current_i_key) {
                    data[current_i_key] = text_value;
                    current_i_key = null;
                }

            }
            return data;
        },
        parsePartnerInfoCell: function(partner_info_cell) {
            var children = partner_info_cell.childNodes;
            var data = {
                'jobs_data:affiliate': null,
                'jobs_data:work_order': null
            };
            var text_node_index = 0;
            var text_value;
            for (var i = 0; i < children.length; i++) {
                if (children[i].nodeName !== "#text") {
                    continue;
                }

                text_value = children[i].nodeValue.trim();

                switch (text_node_index) {
                    case 0:
                        data['jobs_data:affiliate'] = text_value;
                        break;
                    case 1:
                        data['jobs_data:work_order'] = text_value;
                        break;
                }

                text_node_index++;
            }
            return data;
        },
        parseOrderInfoCell: function(order_info_cell) {
            var children = order_info_cell.childNodes;
            var data = {
                JobSubType: null,
                'jobs_data:sow_num': null,
                'jobs_data:advanced_services_type': null,
                ContractSpecialInst: null
            };
            var text_num = 0;
            var index = null;
            var notes_text = "";
            var text_value;
            for (var i = 0; i < children.length; i++) {
                if (children[i].nodeName == "BR" || children[i].nodeName == "HR") {
                    text_num++;
                    notes_text += "\n\r";
                    continue;
                }

                if (children[i].nodeName == "#text") {
                    text_value = children[i].nodeValue.trim();
                } else {
                    text_value = children[i].textContent;
                    notes_text += "\n\r";
                }

                //only interexted in the first 2 text nodes. the rest is notes
                switch (text_num) {
                    case 0:
                        data['jobs_data:advanced_services_type'] = text_value;
                        if (text_value == 'Advanced Services Repair') {
                            data.JobSubType = "Repair Only";
                        }
                        break;
                    case 1:
                        var sow_patt = /SOW\s(\d+)/i;
                        var sow_match = text_value.match(sow_patt);
                        if (sow_match && sow_match.length > 1) {
                            data['jobs_data:sow_num'] = sow_match[1];
                        }
                        break;
                }

                notes_text += text_value;
                index = null;
            }
            data.ContractSpecialInst = notes_text;
            return data;
        },
        getCustomerInfoFromHTML: function() {
            var h5_map = this.getH5Map();
            var customer_info_h5 = h5_map.findByText('CUSTOMER INFO');
            var customer_info_table = DOMElements.Tools.getNextSibling(customer_info_h5, DOMElements.Tools.tableFilter);

            //row 0:
            var partner_info_row = customer_info_table.rows[0];
            var partner_info_cell = partner_info_row.cells[1];
            var partner_info_data = this.parsePartnerInfoCell(partner_info_cell);
            var data = partner_info_data;

            var site_info_row = customer_info_table.rows[1];
            var site_info_cell = site_info_row.cells[1];
            var site_info_data = this.parseSiteInfoCell(site_info_cell);
            this._merge_properties(data, site_info_data);

            var region_info_row = customer_info_table.rows[2];
            var region_info_cell = region_info_row.cells[1];
            var region_info_data = region_info_cell.textContent.trim();
            this._merge_properties(data, {ContractRegionName: region_info_data});

            return data;
        },
        getOrderInfoFromHTML: function() {
            var h5_map = this.getH5Map();
            var order_info_h5 = h5_map.findByText('ORDER INFO');
            var order_info_table = DOMElements.Tools.getNextSibling(order_info_h5, DOMElements.Tools.tableFilter);

            var order_info_row = order_info_table.rows[0];
            var order_info_cell = order_info_row.cells[1];
            var order_info_data = this.parseOrderInfoCell(order_info_cell);

            return order_info_data;
        },
        //=================Dispatch Data========================================
        getDispatchData: function() {
            if (this.dispatch_data !== null) {
                return this.dispatch_data;
            }
            this.dispatch_data = {
                'OrderStatus': null,
                'TaskId': null,
                'DispSeq': null
            };
            var h4_map = this.getH4Map();
            var dispatch_h4 = h4_map.findByRegExp(/Order #/);
            var dispatch_text = DOMElements.Filters.getElementCleanText(dispatch_h4);
            var task_id_re = /Order\s*#(\d*)/i;
            var task_id_result = dispatch_text.match(task_id_re);
            if (task_id_result !== null) {
                this.dispatch_data.TaskId = task_id_result[1];
            }
            var disp_seq_re = /Order\s*#\d*\s*\((\d*)\)/i;
            var disp_seq_result = dispatch_text.match(disp_seq_re);
            if (disp_seq_result !== null) {
                this.dispatch_data.DispSeq = disp_seq_result[1];
            }
            var order_status_re = />\s*([\w\s]*)>/i;
            var order_status_result = dispatch_text.match(order_status_re);
            if (order_status_result !== null) {
                this.dispatch_data.OrderStatus = order_status_result[1].trim();
            }

            return this.dispatch_data;
        },

        //=================Main functions===================================
        getMilestonesTable: function() {
            if(this.milestones_table === null) {
                var milestones_h5 = this.getH5Map().findByText('MILESTONES');
                var milestones_table_elem = DOMElements.Tools.getNextSibling(milestones_h5, DOMElements.Tools.tableFilter);
                this.milestones_table = new DOMTableTools.TableUtil(milestones_table_elem);
            }
            return this.milestones_table;
        },
        getWorkLog: function() {
            if (this.work_log === null) {
                var worklog_h5 = this.getH5Map().findByText('WORKLOG');
                var worklog_table_elem = DOMElements.Tools.getNextSibling(worklog_h5, DOMElements.Tools.tableFilter);
                var work_log_table = new DOMTableTools.TableUtil(worklog_table_elem);
                this.work_log = new OffnetTask_Worklog(work_log_table);
            }
            return this.work_log;
        },
        getTimeslotSearch: function() {
            if(this.timeslot_search === null) {
                var milestones_table = this.getMilestonesTable();
                this.timeslot_search = new OffnetTask_TimeslotSearch(milestones_table);
            }
            return this.timeslot_search;
        },
        getTaskId: function() {
            var data = this.getDispatchData();
            return data.TaskId;
        },
        getTaskType: function() {
            return this.task_type;
        },
        /*
        getOrderStatus: function() {
            var data = this.getClientOrderInfoData();
            return data['Client Order Info']['Order Status:'];
        },
        getCurrentScheduleDate: function() {
            var data = this.getClientOrderInfoData();
            return data['Client Order Info']['Schedule Date:'];
        },
        getCurrentScheduleDateObj: function() {
            var date_str = this.getCurrentScheduleDate();
            //problem with this date string: has AM/PM, but time is in 24 hr format
            //remove the AM/PM
            date_str = date_str.replace(' AM', '');
            date_str = date_str.replace(' PM', '');
            var date_obj = new Date(date_str);
            return date_obj;
        },
        */
        fillFromVariables: function() {
            this.fillFromCinfo();
            if(typeof oinfo === 'object') {
                this.fillFromOinfo();
            }
        },
        fillFromHtml: function() {
            var customer_info = this.getCustomerInfoFromHTML();
            this._merge_properties(this.task_data, customer_info);
            var order_info = this.getOrderInfoFromHTML();
            this._merge_properties(this.task_data, order_info);
        },
        getTaskData: function() {
            this.task_data.TaskType = this.getTaskType();
            this.task_data.JobSubType = 'Offnet Only'; //default sub type
            this.task_data.TaskId = this.getTaskId();
            if(typeof cinfo === 'object') {
                this.fillFromVariables();
            } else {
                this.fillFromHtml();
            }

            return this.task_data;
        }
    };

    /**
     *
     *
     * ========================OffnetTask Worklog============================================
     *
     */
    var OffnetTask_Worklog = function OffnetTask_Worklog(worklog_table_dom) {
        this.table_dom = worklog_table_dom;
        this.table_column_map = null;
    };

    OffnetTask_Worklog.prototype = {
        getTableColumnMap: function() {
            if (this.table_column_map === null) {
                var table_data_mapper = new DOMTableTools.TableDataMappers.FirstRowHasHeaders();
                var worklog_data = table_data_mapper.getData(this.table_dom);
                this.table_column_map = new DOMTableTools.RowArrayColumnMap(worklog_data, table_data_mapper.column_array);
            }
            return this.table_column_map;
        },
        getRowIterator: function() {
            var row_iterator = this.table_column_map.getIterator();
            return row_iterator;
        }
    };

    /**
     *
     *
     * ========================OffnetTask Timeslot Search========================================
     *
     */
    var OffnetTask_TimeslotSearch = function OffnetTask_TimeslotSearch(milestones_table) {
        this.milestones_table = milestones_table;
    };

    OffnetTask_TimeslotSearch.prototype = {
        findTimeSlot: function(date_obj) {
            var timeslot = null;
            var short_date = GlobalCapacity.TaskDocuments.getShortDate_FromDateObj(date_obj);
            var label_text, note_text;
            for(var i=0, len=this.milestones_table.countRows(); i< len; i++) {
                label_text = this.milestones_table.getCellText(i,0);

                if (label_text.substring(0, 9) == "Resched'd" || label_text.substring(0, 7) == "Sched'd") {
                    note_text = this.milestones_table.getCellText(i,1);
                    timeslot = this.findTimeInNotes_Offnet1(note_text, short_date);
                }

                if (timeslot) break;
            }
            return timeslot;
        },
        findTimeInNotes_Offnet1: function(note_content, date) {
            if (note_content.indexOf(date) > -1) {

                if (note_content.indexOf("9AM") > -1) {
                    return GlobalCapacity.TaskDocuments.getTimeslotForHour("09");
                }
                if (note_content.indexOf("8:45AM") > -1) {
                    return GlobalCapacity.TaskDocuments.getTimeslotForHour("09");
                }
                if (note_content.indexOf("12PM") > -1) {
                    return GlobalCapacity.TaskDocuments.getTimeslotForHour("12");
                }
                if (note_content.indexOf("4PM") > -1) {
                    return GlobalCapacity.TaskDocuments.getTimeslotForHour("16");
                }
            }
            return null;
        }

    };

    return OffnetTask;
})();
