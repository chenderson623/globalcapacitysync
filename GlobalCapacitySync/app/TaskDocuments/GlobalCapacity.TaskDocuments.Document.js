'use strict';
if (GlobalCapacity.TaskDocuments === undefined) {
    throw "GlobalCapacity.TaskDocuments is required";
}
/**
 * common interface to document type objects. Holds reference to document_obj
 * @return GlobalCapacity.TaskDocuments.Document
 */
GlobalCapacity.TaskDocuments.Document = (function() {
    function Document(document_obj, globalcapacity_taskdocuments) {
        console.trace();
        var self = this;
        this.document_obj = document_obj;
        this.globalcapacity_taskdocuments = globalcapacity_taskdocuments;
        //
        //
        this.getDocumentType = function() {
            return "TaskDocument";
        };
        this.getTaskType = function() {
            return this.document_obj.getTaskType();
        };
        this.getTaskId = function() {
            return this.document_obj.getTaskId();
        };
        this.createSyncControlsFactory = function(callback) {
            var self = this;
            var load_controls = this.globalcapacity_taskdocuments.loadControls();

            this.globalcapacity_taskdocuments.loadParseChild(function(parse_child){
                var parent = window.opener || window.parent;
                if(parent !== window) {
                    var child = new GlobalCapacity.TaskDocuments.ParseChild(document);
                    child.getTaskDocumentWindowChannel(function(taskdocument_window_channel) {});
                }
                load_controls.then(function(controls){
                    self.SyncControlFactory = new controls.SyncControlFactory(self.document_obj);
                    callback(self.SyncControlFactory);
                });


            });

        };

    }
    return Document;
})();
