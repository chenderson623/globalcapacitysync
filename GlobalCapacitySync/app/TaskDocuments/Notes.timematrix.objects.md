


var GlobalCapacity = GlobalCapacity || {};
GlobalCapacity.TaskDocuments = TaskDocuments()
        this.task_types = ["Trouble Ticket", "Work Order", "Offnet"];
        this.getTaskType = function(task_document_elem) {}
        this.loadTaskDocument = function(task_document_elem, callback) {}
        this.testRowTroubleTicket = function(table_row) {}
        this.testRowWorkOrder = function(table_row) {}
        TaskDocuments.prototype = {
            getTimeslotForHour : function(hour_value) {}
            getShortDate : function(date_str) {}
            getDateNoZeros : function(date_str) {}
            getShortDate_FromDateObj : function(date_obj) {}
            getDateNoZeros_FromDateObj : function(date_obj) {}
            getStdDateString_FromDateObj : function(date_obj) {}
        }


if(typeof GlobalCapacity == 'undefined') {
    throw 'GlobalCapacity is required.';
}
if(typeof GlobalCapacity.TaskDocuments == 'undefined') {
    throw 'GlobalCapacity.TaskDocuments is required.';
}

GlobalCapacity.TaskDocuments.WorkOrder = function WorkOrder(task_document_elem, task_documents) {
        this.task_document_elem = task_document_elem;
        this.task_documents = task_documents;
        this.getTableSet = function() {
        this.getDataTable = function(table_name) {
        this.getClientOrderInfoData = function() {
        this.fillData_ClientOrderInfo = function(data_obj) {
        this.getPrimaryCPEData = function() {
        this.getClientCircuitInfoData = function() {
        this.fillData_ClientCircuitInfo = function(data_obj) {
        this.getDSLAMInfo = function() {
        this.getAdditionalNotes = function() {
        this.parseServiceLevel = function(service_type) {
        this.getServiceLevel = function() {
        this.getScpNumArray = function() {
        this.getScpNumStr = function() {
        this.getWorkLog = function() {
        this.getTimeslotSearch = function() {
        this.getTaskId = function() {
        this.getTaskType = function() {
        this.getOrderStatus = function() {
        this.getCurrentScheduleDate = function() {
        this.getCurrentScheduleDateObj = function() {
        this.getTaskData = function() {
